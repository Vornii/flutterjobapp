class LoginErrorModel {
  String? response;
  String? message;
  int? statusCode;

  LoginErrorModel({this.response, this.message, this.statusCode});

  LoginErrorModel.fromJson(Map<String, dynamic> json) {
    response = json['response'];
    message = json['message'];
    statusCode = json['statusCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['response'] = this.response;
    data['message'] = this.message;
    data['statusCode'] = this.statusCode;
    return data;
  }
}
