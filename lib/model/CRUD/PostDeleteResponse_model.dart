class PostDeleteModel {
  String? message;
  Response? response;
  int? statusCode;

  PostDeleteModel({this.message, this.response, this.statusCode});

  PostDeleteModel.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    response = json['response'] != null
        ? new Response.fromJson(json['response'])
        : null;
    statusCode = json['statusCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    if (this.response != null) {
      data['response'] = this.response!.toJson();
    }
    data['statusCode'] = this.statusCode;
    return data;
  }
}

class Response {
  String? sId;
  String? title;
  String? user;

  Response({this.sId, this.title, this.user});

  Response.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    title = json['title'];
    user = json['user'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['title'] = this.title;
    data['user'] = this.user;
    return data;
  }
}
