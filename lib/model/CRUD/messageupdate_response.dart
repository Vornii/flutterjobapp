class MessageUpdateModel {
  String? id;
  Response? response;
  int? statusCode;

  MessageUpdateModel({this.id, this.response, this.statusCode});

  MessageUpdateModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    response = json['response'] != null
        ? new Response.fromJson(json['response'])
        : null;
    statusCode = json['statusCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.response != null) {
      data['response'] = this.response!.toJson();
    }
    data['statusCode'] = this.statusCode;
    return data;
  }
}

class Response {
  String? description;
  String? user;
  String? job;

  Response({this.description, this.user, this.job});

  Response.fromJson(Map<String, dynamic> json) {
    description = json['description'];
    user = json['user'];
    job = json['job'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['description'] = this.description;
    data['user'] = this.user;
    data['job'] = this.job;
    return data;
  }
}
