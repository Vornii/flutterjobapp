class UserResponse {
  String? message;
  String? id;
  Data? data;
  int? statusCode;

  UserResponse({this.message, this.id, this.data, this.statusCode});

  UserResponse.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    id = json['id'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    statusCode = json['statusCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['id'] = this.id;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    data['statusCode'] = this.statusCode;
    return data;
  }
}

class Data {
  String? name;
  String? email;
  String? role;
  List<String>? interest;
  String? position;
  Profile? profile;
  Thumbnail? thumbnail;
  List<Experience>? experience;

  Data(
      {this.name,
        this.email,
        this.role,
        this.interest,
        this.position,
        this.profile,
        this.thumbnail,
        this.experience});

  Data.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    email = json['email'];
    role = json['role'];
    interest = json['interest'].cast<String>();
    position = json['position'];
    profile =
    json['profile'] != null ? new Profile.fromJson(json['profile']) : null;
    thumbnail = json['thumbnail'] != null
        ? new Thumbnail.fromJson(json['thumbnail'])
        : null;
    if (json['experience'] != null) {
      experience = <Experience>[];
      json['experience'].forEach((v) {
        experience!.add(new Experience.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['email'] = this.email;
    data['role'] = this.role;
    data['interest'] = this.interest;
    data['position'] = this.position;
    if (this.profile != null) {
      data['profile'] = this.profile!.toJson();
    }
    if (this.thumbnail != null) {
      data['thumbnail'] = this.thumbnail!.toJson();
    }
    if (this.experience != null) {
      data['experience'] = this.experience!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Profile {
  String? bio;
  List<String>? skill;
  List<String>? languages;
  String? currentRole;
  String? telephone;
  String? telegram;
  String? currentCompany;
  String? sId;

  Profile(
      {this.bio,
        this.skill,
        this.languages,
        this.currentRole,
        this.telephone,
        this.telegram,
        this.currentCompany,
        this.sId});

  Profile.fromJson(Map<String, dynamic> json) {
    bio = json['bio'];
    skill = json['skill'].cast<String>();
    languages = json['languages'].cast<String>();
    currentRole = json['current_role'];
    telephone = json['telephone'];
    telegram = json['telegram'];
    currentCompany = json['current_company'];
    sId = json['_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['bio'] = this.bio;
    data['skill'] = this.skill;
    data['languages'] = this.languages;
    data['current_role'] = this.currentRole;
    data['telephone'] = this.telephone;
    data['telegram'] = this.telegram;
    data['current_company'] = this.currentCompany;
    data['_id'] = this.sId;
    return data;
  }
}

class Thumbnail {
  String? sId;
  Attributes? attributes;

  Thumbnail({this.sId, this.attributes});

  Thumbnail.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    attributes = json['attributes'] != null
        ? new Attributes.fromJson(json['attributes'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    if (this.attributes != null) {
      data['attributes'] = this.attributes!.toJson();
    }
    return data;
  }
}

class Attributes {
  String? fieldname;
  String? originalname;
  String? encoding;
  String? mimetype;
  String? destination;
  String? filename;
  String? path;
  int? size;

  Attributes(
      {this.fieldname,
        this.originalname,
        this.encoding,
        this.mimetype,
        this.destination,
        this.filename,
        this.path,
        this.size});

  Attributes.fromJson(Map<String, dynamic> json) {
    fieldname = json['fieldname'];
    originalname = json['originalname'];
    encoding = json['encoding'];
    mimetype = json['mimetype'];
    destination = json['destination'];
    filename = json['filename'];
    path = json['path'];
    size = json['size'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['fieldname'] = this.fieldname;
    data['originalname'] = this.originalname;
    data['encoding'] = this.encoding;
    data['mimetype'] = this.mimetype;
    data['destination'] = this.destination;
    data['filename'] = this.filename;
    data['path'] = this.path;
    data['size'] = this.size;
    return data;
  }
}

class Experience {
  String? companyname;
  String? position;
  int? yearstart;
  int? yearend;

  Experience({this.companyname, this.position, this.yearstart, this.yearend});

  Experience.fromJson(Map<String, dynamic> json) {
    companyname = json['companyname'];
    position = json['position'];
    yearstart = json['yearstart'];
    yearend = json['yearend'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['companyname'] = this.companyname;
    data['position'] = this.position;
    data['yearstart'] = this.yearstart;
    data['yearend'] = this.yearend;
    return data;
  }
}
