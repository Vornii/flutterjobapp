class MessageResponse {
  String? message;
  String? id;
  Data? data;
  int? statusCode;

  MessageResponse({this.message, this.id, this.data, this.statusCode});

  MessageResponse.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    id = json['id'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    statusCode = json['statusCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['id'] = this.id;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    data['statusCode'] = this.statusCode;
    return data;
  }
}

class Data {
  String? description;
  String? pdfLink;
  String? user;
  String? job;

  Data({this.description, this.pdfLink, this.user, this.job});

  Data.fromJson(Map<String, dynamic> json) {
    description = json['description'];
    pdfLink = json['pdf_link'];
    user = json['user'];
    job = json['job'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['description'] = this.description;
    data['pdf_link'] = this.pdfLink;
    data['user'] = this.user;
    data['job'] = this.job;
    return data;
  }
}
