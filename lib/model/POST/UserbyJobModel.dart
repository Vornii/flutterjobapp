class JobByUserID {
  String? message;
  String? id;
  Data? data;
  int? statusCode;

  JobByUserID({this.message, this.id, this.data, this.statusCode});

  JobByUserID.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    id = json['id'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    statusCode = json['statusCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['id'] = this.id;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    data['statusCode'] = this.statusCode;
    return data;
  }
}

class Data {
  String? name;
  String? email;
  String? role;
  List<Job>? job;

  Data({this.name, this.email, this.role, this.job});

  Data.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    email = json['email'];
    role = json['role'];
    if (json['job'] != null) {
      job = <Job>[];
      json['job'].forEach((v) {
        job!.add(new Job.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['email'] = this.email;
    data['role'] = this.role;
    if (this.job != null) {
      data['job'] = this.job!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Job {
  String? sId;
  String? title;
  bool? status;
  Contact? contact;
  Address? address;
  JobDescription? jobDescription;
  List<String>? requirement;
  List<String>? category;
  String? createdAt;
  String? updatedAt;
  int? iV;

  Job(
      {this.sId,
        this.title,
        this.status,
        this.contact,
        this.address,
        this.jobDescription,
        this.requirement,
        this.category,
        this.createdAt,
        this.updatedAt,
        this.iV});

  Job.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    title = json['title'];
    status = json['status'];
    contact =
    json['contact'] != null ? new Contact.fromJson(json['contact']) : null;
    address =
    json['address'] != null ? new Address.fromJson(json['address']) : null;
    jobDescription = json['job_description'] != null
        ? new JobDescription.fromJson(json['job_description'])
        : null;
    requirement = json['requirement'].cast<String>();
    category = json['category'].cast<String>();
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['title'] = this.title;
    data['status'] = this.status;
    if (this.contact != null) {
      data['contact'] = this.contact!.toJson();
    }
    if (this.address != null) {
      data['address'] = this.address!.toJson();
    }
    if (this.jobDescription != null) {
      data['job_description'] = this.jobDescription!.toJson();
    }
    data['requirement'] = this.requirement;
    data['category'] = this.category;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}

class Contact {
  String? telephone;
  String? email;
  String? telegram;
  String? sId;

  Contact({this.telephone, this.email, this.telegram, this.sId});

  Contact.fromJson(Map<String, dynamic> json) {
    telephone = json['telephone'];
    email = json['email'];
    telegram = json['telegram'];
    sId = json['_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['telephone'] = this.telephone;
    data['email'] = this.email;
    data['telegram'] = this.telegram;
    data['_id'] = this.sId;
    return data;
  }
}

class Address {
  String? street;
  String? province;
  String? city;
  String? urilocation;
  double? latitude;
  double? longitude;
  String? sId;

  Address({this.street, this.province, this.city, this.urilocation, this.latitude,this.longitude,this.sId});

  Address.fromJson(Map<String, dynamic> json) {
    street = json['street'];
    province = json['Province'];
    city = json['City'];
    urilocation = json['Urilocation'];
    latitude = json['Latitude'];
    longitude = json['Longitude'];
    sId = json['_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['street'] = this.street;
    data['Province'] = this.province;
    data['City'] = this.city;
    data['Urilocation'] = this.urilocation;
    data['Latitude'] = this.latitude;
    data['Longitude'] = this.longitude;
    data['_id'] = this.sId;
    return data;
  }
}

class JobDescription {
  String? detail;
  String? companyName;
  int? minSalary;
  int? maxSalary;
  String? jobtype;
  int? totalPosition;
  String? experience;
  String? sId;

  JobDescription(
      {this.detail,
        this.companyName,
        this.minSalary,
        this.maxSalary,
        this.jobtype,
        this.totalPosition,
        this.experience,
        this.sId});

  JobDescription.fromJson(Map<String, dynamic> json) {
    detail = json['Detail'];
    companyName = json['CompanyName'];
    minSalary = json['MinSalary'];
    maxSalary = json['MaxSalary'];
    jobtype = json['Jobtype'];
    totalPosition = json['TotalPosition'];
    experience = json['Experience'];
    sId = json['_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Detail'] = this.detail;
    data['CompanyName'] = this.companyName;
    data['MinSalary'] = this.minSalary;
    data['MaxSalary'] = this.maxSalary;
    data['Jobtype'] = this.jobtype;
    data['TotalPosition'] = this.totalPosition;
    data['Experience'] = this.experience;
    data['_id'] = this.sId;
    return data;
  }
}
