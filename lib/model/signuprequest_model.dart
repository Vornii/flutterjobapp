class RegisterRequest {
  String? name;
  String? email;
  String? password;
  String? role;
  String? position;
  List<String>? interest;
  Profile? profile;

  RegisterRequest(
      {this.name,
        this.email,
        this.password,
        this.role,
        this.position,
        this.interest,
        this.profile});

  RegisterRequest.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    email = json['email'];
    password = json['password'];
    role = json['role'];
    position = json['position'];
    interest = json['interest'].cast<String>();
    profile =
    json['profile'] != null ? new Profile.fromJson(json['profile']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['email'] = this.email;
    data['password'] = this.password;
    data['role'] = this.role;
    data['position'] = this.position;
    data['interest'] = this.interest;
    if (this.profile != null) {
      data['profile'] = this.profile!.toJson();
    }
    return data;
  }
}

class Profile {
  String? currentCompany;

  Profile({this.currentCompany});

  Profile.fromJson(Map<String, dynamic> json) {
    currentCompany = json['current_company'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['current_company'] = this.currentCompany;
    return data;
  }
}
