class UserAdminModel {
  List<Data>? data;
  Meta? meta;

  UserAdminModel({this.data, this.meta});

  UserAdminModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <Data>[];
      json['data'].forEach((v) {
        data!.add(new Data.fromJson(v));
      });
    }
    meta = json['meta'] != null ? new Meta.fromJson(json['meta']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    if (this.meta != null) {
      data['meta'] = this.meta!.toJson();
    }
    return data;
  }
}

class Data {
  String? id;
  Attributes? attributes;

  Data({this.id, this.attributes});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    attributes = json['attributes'] != null
        ? new Attributes.fromJson(json['attributes'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    if (this.attributes != null) {
      data['attributes'] = this.attributes!.toJson();
    }
    return data;
  }
}

class Attributes {
  String? sId;
  String? name;
  String? email;
  String? role;
  String? password;
  List<String>? interest;
  String? position;
  List<Experience>? experience;
  List<Job>? job;
  Profile? profile;
  Thumbnail? thumbnail;

  Attributes(
      {this.sId,
        this.name,
        this.email,
        this.role,
        this.password,
        this.interest,
        this.position,
        this.experience,
        this.job,
        this.profile,
        this.thumbnail});

  Attributes.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    name = json['name'];
    email = json['email'];
    role = json['role'];
    password = json['password'];
    interest = json['interest'].cast<String>();
    position = json['position'];
    if (json['experience'] != null) {
      experience = <Experience>[];
      json['experience'].forEach((v) {
        experience!.add(new Experience.fromJson(v));
      });
    }
    if (json['job'] != null) {
      job = <Job>[];
      json['job'].forEach((v) {
        job!.add(new Job.fromJson(v));
      });
    }
    profile =
    json['profile'] != null ? new Profile.fromJson(json['profile']) : null;
    thumbnail = json['thumbnail'] != null
        ? new Thumbnail.fromJson(json['thumbnail'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['name'] = this.name;
    data['email'] = this.email;
    data['role'] = this.role;
    data['password'] = this.password;
    data['interest'] = this.interest;
    data['position'] = this.position;
    if (this.experience != null) {
      data['experience'] = this.experience!.map((v) => v.toJson()).toList();
    }
    if (this.job != null) {
      data['job'] = this.job!.map((v) => v.toJson()).toList();
    }
    if (this.profile != null) {
      data['profile'] = this.profile!.toJson();
    }
    if (this.thumbnail != null) {
      data['thumbnail'] = this.thumbnail!.toJson();
    }
    return data;
  }
}

class Experience {
  String? companyname;
  String? position;
  int? yearstart;
  int? yearend;

  Experience({this.companyname, this.position, this.yearstart, this.yearend});

  Experience.fromJson(Map<String, dynamic> json) {
    companyname = json['companyname'];
    position = json['position'];
    yearstart = json['yearstart'];
    yearend = json['yearend'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['companyname'] = this.companyname;
    data['position'] = this.position;
    data['yearstart'] = this.yearstart;
    data['yearend'] = this.yearend;
    return data;
  }
}

class Job {
  String? sId;
  String? title;
  bool? status;
  Contact? contact;
  Address? address;
  JobDescription? jobDescription;
  List<String>? requirement;
  List<String>? category;

  Job(
      {this.sId,
        this.title,
        this.status,
        this.contact,
        this.address,
        this.jobDescription,
        this.requirement,
        this.category});

  Job.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    title = json['title'];
    status = json['status'];
    contact =
    json['contact'] != null ? new Contact.fromJson(json['contact']) : null;
    address =
    json['address'] != null ? new Address.fromJson(json['address']) : null;
    jobDescription = json['job_description'] != null
        ? new JobDescription.fromJson(json['job_description'])
        : null;
    requirement = json['requirement'].cast<String>();
    category = json['category'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['title'] = this.title;
    data['status'] = this.status;
    if (this.contact != null) {
      data['contact'] = this.contact!.toJson();
    }
    if (this.address != null) {
      data['address'] = this.address!.toJson();
    }
    if (this.jobDescription != null) {
      data['job_description'] = this.jobDescription!.toJson();
    }
    data['requirement'] = this.requirement;
    data['category'] = this.category;
    return data;
  }
}

class Contact {
  String? telephone;
  String? email;
  String? telegram;
  String? sId;

  Contact({this.telephone, this.email, this.telegram, this.sId});

  Contact.fromJson(Map<String, dynamic> json) {
    telephone = json['telephone'];
    email = json['email'];
    telegram = json['telegram'];
    sId = json['_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['telephone'] = this.telephone;
    data['email'] = this.email;
    data['telegram'] = this.telegram;
    data['_id'] = this.sId;
    return data;
  }
}

class Address {
  String? street;
  String? province;
  String? city;
  String? urilocation;
  double? latitude;
  double? longitude;
  String? sId;

  Address(
      {this.street,
        this.province,
        this.city,
        this.urilocation,
        this.latitude,
        this.longitude,
        this.sId});

  Address.fromJson(Map<String, dynamic> json) {
    street = json['street'];
    province = json['Province'];
    city = json['City'];
    urilocation = json['Urilocation'];
    latitude = json['Latitude'];
    longitude = json['Longitude'];
    sId = json['_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['street'] = this.street;
    data['Province'] = this.province;
    data['City'] = this.city;
    data['Urilocation'] = this.urilocation;
    data['Latitude'] = this.latitude;
    data['Longitude'] = this.longitude;
    data['_id'] = this.sId;
    return data;
  }
}

class JobDescription {
  String? detail;
  String? companyName;
  int? minSalary;
  int? maxSalary;
  String? jobtype;
  int? totalPosition;
  String? experience;
  String? sId;

  JobDescription(
      {this.detail,
        this.companyName,
        this.minSalary,
        this.maxSalary,
        this.jobtype,
        this.totalPosition,
        this.experience,
        this.sId});

  JobDescription.fromJson(Map<String, dynamic> json) {
    detail = json['Detail'];
    companyName = json['CompanyName'];
    minSalary = json['MinSalary'];
    maxSalary = json['MaxSalary'];
    jobtype = json['Jobtype'];
    totalPosition = json['TotalPosition'];
    experience = json['Experience'];
    sId = json['_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Detail'] = this.detail;
    data['CompanyName'] = this.companyName;
    data['MinSalary'] = this.minSalary;
    data['MaxSalary'] = this.maxSalary;
    data['Jobtype'] = this.jobtype;
    data['TotalPosition'] = this.totalPosition;
    data['Experience'] = this.experience;
    data['_id'] = this.sId;
    return data;
  }
}

class Profile {
  String? bio;
  List<String>? skill;
  List<String>? languages;
  String? currentRole;
  String? telephone;
  String? telegram;
  String? currentCompany;
  String? sId;

  Profile(
      {this.bio,
        this.skill,
        this.languages,
        this.currentRole,
        this.telephone,
        this.telegram,
        this.currentCompany,
        this.sId});

  Profile.fromJson(Map<String, dynamic> json) {
    bio = json['bio'];
    skill = json['skill'].cast<String>();
    languages = json['languages'].cast<String>();
    currentRole = json['current_role'];
    telephone = json['telephone'];
    telegram = json['telegram'];
    currentCompany = json['current_company'];
    sId = json['_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['bio'] = this.bio;
    data['skill'] = this.skill;
    data['languages'] = this.languages;
    data['current_role'] = this.currentRole;
    data['telephone'] = this.telephone;
    data['telegram'] = this.telegram;
    data['current_company'] = this.currentCompany;
    data['_id'] = this.sId;
    return data;
  }
}

class Thumbnail {
  String? sId;
  TBAttributes? attributes;

  Thumbnail({this.sId, this.attributes});

  Thumbnail.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    attributes = json['attributes'] != null
        ? new   TBAttributes.fromJson(json['attributes'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    if (this.attributes != null) {
      data['attributes'] = this.attributes!.toJson();
    }
    return data;
  }
}

class  TBAttributes {
  String? fieldname;
  String? originalname;
  String? encoding;
  String? mimetype;
  String? destination;
  String? filename;
  String? path;
  int? size;

  TBAttributes(
      {this.fieldname,
        this.originalname,
        this.encoding,
        this.mimetype,
        this.destination,
        this.filename,
        this.path,
        this.size});

  TBAttributes.fromJson(Map<String, dynamic> json) {
    fieldname = json['fieldname'];
    originalname = json['originalname'];
    encoding = json['encoding'];
    mimetype = json['mimetype'];
    destination = json['destination'];
    filename = json['filename'];
    path = json['path'];
    size = json['size'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['fieldname'] = this.fieldname;
    data['originalname'] = this.originalname;
    data['encoding'] = this.encoding;
    data['mimetype'] = this.mimetype;
    data['destination'] = this.destination;
    data['filename'] = this.filename;
    data['path'] = this.path;
    data['size'] = this.size;
    return data;
  }
}

class Meta {
  Pagination? pagination;

  Meta({this.pagination});

  Meta.fromJson(Map<String, dynamic> json) {
    pagination = json['pagination'] != null
        ? new Pagination.fromJson(json['pagination'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.pagination != null) {
      data['pagination'] = this.pagination!.toJson();
    }
    return data;
  }
}

class Pagination {
  int? page;
  int? pagesize;
  int? pageCount;

  Pagination({this.page, this.pagesize, this.pageCount});

  Pagination.fromJson(Map<String, dynamic> json) {
    page = json['page'];
    pagesize = json['pagesize'];
    pageCount = json['pageCount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['page'] = this.page;
    data['pagesize'] = this.pagesize;
    data['pageCount'] = this.pageCount;
    return data;
  }
}
