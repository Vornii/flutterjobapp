class DeleteUserResponse {
  String? message;
  Response? response;

  DeleteUserResponse({this.message, this.response});

  DeleteUserResponse.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    response = json['response'] != null
        ? new Response.fromJson(json['response'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    if (this.response != null) {
      data['response'] = this.response!.toJson();
    }
    return data;
  }
}

class Response {
  String? sId;
  String? name;
  String? email;
  String? role;
  String? password;
  List<String>? interest;
  String? position;

  Response(
      {this.sId,
        this.name,
        this.email,
        this.role,
        this.password,
        this.interest,
        this.position});

  Response.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    name = json['name'];
    email = json['email'];
    role = json['role'];
    password = json['password'];
    interest = json['interest'].cast<String>();
    position = json['position'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['name'] = this.name;
    data['email'] = this.email;
    data['role'] = this.role;
    data['password'] = this.password;
    data['interest'] = this.interest;
    data['position'] = this.position;
    return data;
  }
}
