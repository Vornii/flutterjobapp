class RecommendResponse {
  Data? data;
  Meta? meta;

  RecommendResponse({this.data, this.meta});

  RecommendResponse.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    meta = json['meta'] != null ? new Meta.fromJson(json['meta']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    if (this.meta != null) {
      data['meta'] = this.meta!.toJson();
    }
    return data;
  }
}

class Data {
  List<Attributes>? attributes;

  Data({this.attributes});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['attributes'] != null) {
      attributes = <Attributes>[];
      json['attributes'].forEach((v) {
        attributes!.add(new Attributes.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.attributes != null) {
      data['attributes'] = this.attributes!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Attributes {
  String? sId;
  String? title;
  bool? status;
  Contact? contact;
  Address? address;
  JobDescription? jobDescription;
  List<String>? requirement;
  List<String>? category;
  User? user;
  List<String>? messages;

  Attributes(
      {this.sId,
        this.title,
        this.status,
        this.contact,
        this.address,
        this.jobDescription,
        this.requirement,
        this.category,
        this.user,
        this.messages});

  Attributes.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    title = json['title'];
    status = json['status'];
    contact =
    json['contact'] != null ? new Contact.fromJson(json['contact']) : null;
    address =
    json['address'] != null ? new Address.fromJson(json['address']) : null;
    jobDescription = json['job_description'] != null
        ? new JobDescription.fromJson(json['job_description'])
        : null;
    requirement = json['requirement'].cast<String>();
    category = json['category'].cast<String>();
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
    messages = json['messages'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['title'] = this.title;
    data['status'] = this.status;
    if (this.contact != null) {
      data['contact'] = this.contact!.toJson();
    }
    if (this.address != null) {
      data['address'] = this.address!.toJson();
    }
    if (this.jobDescription != null) {
      data['job_description'] = this.jobDescription!.toJson();
    }
    data['requirement'] = this.requirement;
    data['category'] = this.category;
    if (this.user != null) {
      data['user'] = this.user!.toJson();
    }
    data['messages'] = this.messages;
    return data;
  }
}

class Contact {
  String? telephone;
  String? email;
  String? telegram;
  String? sId;

  Contact({this.telephone, this.email, this.telegram, this.sId});

  Contact.fromJson(Map<String, dynamic> json) {
    telephone = json['telephone'];
    email = json['email'];
    telegram = json['telegram'];
    sId = json['_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['telephone'] = this.telephone;
    data['email'] = this.email;
    data['telegram'] = this.telegram;
    data['_id'] = this.sId;
    return data;
  }
}

class Address {
  String? street;
  String? province;
  String? city;
  String? urilocation;
  String? sId;

  Address({this.street, this.province, this.city, this.urilocation, this.sId});

  Address.fromJson(Map<String, dynamic> json) {
    street = json['street'];
    province = json['Province'];
    city = json['City'];
    urilocation = json['Urilocation'];
    sId = json['_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['street'] = this.street;
    data['Province'] = this.province;
    data['City'] = this.city;
    data['Urilocation'] = this.urilocation;
    data['_id'] = this.sId;
    return data;
  }
}

class JobDescription {
  String? detail;
  String? companyName;
  int? minSalary;
  int? maxSalary;
  String? jobtype;
  int? totalPosition;
  String? experience;
  String? sId;

  JobDescription(
      {this.detail,
        this.companyName,
        this.minSalary,
        this.maxSalary,
        this.jobtype,
        this.totalPosition,
        this.experience,
        this.sId});

  JobDescription.fromJson(Map<String, dynamic> json) {
    detail = json['Detail'];
    companyName = json['CompanyName'];
    minSalary = json['MinSalary'];
    maxSalary = json['MaxSalary'];
    jobtype = json['Jobtype'];
    totalPosition = json['TotalPosition'];
    experience = json['Experience'];
    sId = json['_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Detail'] = this.detail;
    data['CompanyName'] = this.companyName;
    data['MinSalary'] = this.minSalary;
    data['MaxSalary'] = this.maxSalary;
    data['Jobtype'] = this.jobtype;
    data['TotalPosition'] = this.totalPosition;
    data['Experience'] = this.experience;
    data['_id'] = this.sId;
    return data;
  }
}

class User {
  String? sId;
  String? name;
  String? email;
  String? role;
  String? password;
  List<String>? interest;
  List<String>? job;
  int? iV;
  String? position;
  Profile? profile;
  Thumbnail? thumbnail;

  User(
      {this.sId,
        this.name,
        this.email,
        this.role,
        this.password,
        this.interest,
        this.job,
        this.iV,
        this.position,
        this.profile,
        this.thumbnail});

  User.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    name = json['name'];
    email = json['email'];
    role = json['role'];
    password = json['password'];
    interest = json['interest'].cast<String>();
    job = json['job'].cast<String>();
    iV = json['__v'];
    position = json['position'];
    profile =
    json['profile'] != null ? new Profile.fromJson(json['profile']) : null;
    thumbnail = json['thumbnail'] != null
        ? new Thumbnail.fromJson(json['thumbnail'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['name'] = this.name;
    data['email'] = this.email;
    data['role'] = this.role;
    data['password'] = this.password;
    data['interest'] = this.interest;
    data['job'] = this.job;
    data['__v'] = this.iV;
    data['position'] = this.position;
    if (this.profile != null) {
      data['profile'] = this.profile!.toJson();
    }
    if (this.thumbnail != null) {
      data['thumbnail'] = this.thumbnail!.toJson();
    }
    return data;
  }
}

class Profile {
  String? bio;
  List<String>? skill;
  List<String>? languages;
  String? currentRole;
  String? telephone;
  String? telegram;
  String? currentCompany;
  String? sId;

  Profile(
      {this.bio,
        this.skill,
        this.languages,
        this.currentRole,
        this.telephone,
        this.telegram,
        this.currentCompany,
        this.sId});

  Profile.fromJson(Map<String, dynamic> json) {
    bio = json['bio'];
    skill = json['skill'].cast<String>();
    languages = json['languages'].cast<String>();
    currentRole = json['current_role'];
    telephone = json['telephone'];
    telegram = json['telegram'];
    currentCompany = json['current_company'];
    sId = json['_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['bio'] = this.bio;
    data['skill'] = this.skill;
    data['languages'] = this.languages;
    data['current_role'] = this.currentRole;
    data['telephone'] = this.telephone;
    data['telegram'] = this.telegram;
    data['current_company'] = this.currentCompany;
    data['_id'] = this.sId;
    return data;
  }
}

class Thumbnail {
  String? sId;
  ThumbnailAttributes? attributes;

  Thumbnail({this.sId, this.attributes});

  Thumbnail.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    attributes = json['attributes'] != null
        ? new ThumbnailAttributes.fromJson(json['attributes'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    if (this.attributes != null) {
      data['attributes'] = this.attributes!.toJson();
    }
    return data;
  }
}

class ThumbnailAttributes {
  String? path;

  ThumbnailAttributes({this.path});

  ThumbnailAttributes.fromJson(Map<String, dynamic> json) {
    path = json['path'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['path'] = this.path;
    return data;
  }
}

class Meta {
  Pagination? pagination;

  Meta({this.pagination});

  Meta.fromJson(Map<String, dynamic> json) {
    pagination = json['pagination'] != null
        ? new Pagination.fromJson(json['pagination'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.pagination != null) {
      data['pagination'] = this.pagination!.toJson();
    }
    return data;
  }
}

class Pagination {
  int? currentpage;
  int? pageCount;
  int? pageSize;
  int? total;

  Pagination({this.currentpage, this.pageCount, this.pageSize, this.total});

  Pagination.fromJson(Map<String, dynamic> json) {
    currentpage = json['currentpage'];
    pageCount = json['pageCount'];
    pageSize = json['pageSize'];
    total = json['total'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['currentpage'] = this.currentpage;
    data['pageCount'] = this.pageCount;
    data['pageSize'] = this.pageSize;
    data['total'] = this.total;
    return data;
  }
}
