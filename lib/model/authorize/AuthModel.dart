class UserAuthResponse {
  String? message;
  String? response;

  UserAuthResponse({this.message, this.response});

  UserAuthResponse.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    response = json['response'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['response'] = this.response;
    return data;
  }
}
