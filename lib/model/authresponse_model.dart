class LoginResponse {
  String? response;
  String? id;
  Data? data;
  String? token;
  int? statusCode;

  LoginResponse(
      {this.response, this.id, this.data, this.token, this.statusCode});

  LoginResponse.fromJson(Map<String, dynamic> json) {
    response = json['response'];
    id = json['id'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    token = json['token'];
    statusCode = json['statusCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['response'] = this.response;
    data['id'] = this.id;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    data['token'] = this.token;
    data['statusCode'] = this.statusCode;
    return data;
  }
}

class Data {
  String? email;
  String? password;

  Data({this.email, this.password});

  Data.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    password = json['password'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['password'] = this.password;
    return data;
  }
}
