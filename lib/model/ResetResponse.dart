class PasswordResetModel {
  String? message;
  String? email;
  int? statusCode;

  PasswordResetModel({this.message, this.email, this.statusCode});

  PasswordResetModel.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    email = json['email'];
    statusCode = json['statusCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['email'] = this.email;
    data['statusCode'] = this.statusCode;
    return data;
  }
}
