class ResponseUpdateUserModel {
  Response? response;
  int? statusCode;

  ResponseUpdateUserModel({this.response, this.statusCode});

  ResponseUpdateUserModel.fromJson(Map<String, dynamic> json) {
    response = json['response'] != null
        ? new Response.fromJson(json['response'])
        : null;
    statusCode = json['statusCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.response != null) {
      data['response'] = this.response!.toJson();
    }
    data['statusCode'] = this.statusCode;
    return data;
  }
}

class Response {
  String? name;
  String? role;
  List<String>? interest;
  Profile? profile;
  String? thumbnail;
  List<Experience>? experience;

  Response(
      {this.name,
        this.role,
        this.interest,
        this.profile,
        this.thumbnail,
        this.experience});

  Response.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    role = json['role'];
    interest = json['interest'].cast<String>();
    profile =
    json['profile'] != null ? new Profile.fromJson(json['profile']) : null;
    thumbnail = json['thumbnail'];
    if (json['experience'] != null) {
      experience = <Experience>[];
      json['experience'].forEach((v) {
        experience!.add(new Experience.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['role'] = this.role;
    data['interest'] = this.interest;
    if (this.profile != null) {
      data['profile'] = this.profile!.toJson();
    }
    data['thumbnail'] = this.thumbnail;
    if (this.experience != null) {
      data['experience'] = this.experience!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Profile {
  String? bio;
  List<String>? skill;
  List<String>? languages;
  String? currentRole;
  String? telephone;
  String? telegram;
  String? currentCompany;

  Profile(
      {this.bio,
        this.skill,
        this.languages,
        this.currentRole,
        this.telephone,
        this.telegram,
        this.currentCompany});

  Profile.fromJson(Map<String, dynamic> json) {
    bio = json['bio'];
    skill = json['skill'].cast<String>();
    languages = json['languages'].cast<String>();
    currentRole = json['current_role'];
    telephone = json['telephone'];
    telegram = json['telegram'];
    currentCompany = json['current_company'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['bio'] = this.bio;
    data['skill'] = this.skill;
    data['languages'] = this.languages;
    data['current_role'] = this.currentRole;
    data['telephone'] = this.telephone;
    data['telegram'] = this.telegram;
    data['current_company'] = this.currentCompany;
    return data;
  }
}

class Experience {
  String? companyname;
  String? position;
  int? yearstart;
  int? yearend;

  Experience({this.companyname, this.position, this.yearstart, this.yearend});

  Experience.fromJson(Map<String, dynamic> json) {
    companyname = json['companyname'];
    position = json['position'];
    yearstart = json['yearstart'];
    yearend = json['yearend'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['companyname'] = this.companyname;
    data['position'] = this.position;
    data['yearstart'] = this.yearstart;
    data['yearend'] = this.yearend;
    return data;
  }
}
