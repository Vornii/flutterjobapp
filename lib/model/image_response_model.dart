class ImageResponseModel {
  String? id;
  String? filename;
  Formats? formats;
  int? statusCode;

  ImageResponseModel({this.id, this.filename, this.formats, this.statusCode});

  ImageResponseModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    filename = json['filename'];
    formats =
    json['formats'] != null ? new Formats.fromJson(json['formats']) : null;
    statusCode = json['statusCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['filename'] = this.filename;
    if (this.formats != null) {
      data['formats'] = this.formats!.toJson();
    }
    data['statusCode'] = this.statusCode;
    return data;
  }
}

class Formats {
  int? size;
  String? type;
  String? path;
  String? url;

  Formats({this.size, this.type, this.path, this.url});

  Formats.fromJson(Map<String, dynamic> json) {
    size = json['size'];
    type = json['type'];
    path = json['path'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['size'] = this.size;
    data['type'] = this.type;
    data['path'] = this.path;
    data['url'] = this.url;
    return data;
  }
}
