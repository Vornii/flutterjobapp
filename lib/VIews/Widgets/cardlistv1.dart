import 'package:flutter/material.dart';
import 'package:jobapp/ViewModel/job_view_model.dart';
import 'package:jobapp/res/BaseUrl.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';

import '../../data/Response/status_response.dart';
import '../../res/Appcolor.dart';
import '../../res/Appfont.dart';
import '../Jobs_details/job_detail.dart';

class CardListRe extends StatefulWidget {
  var lists;
  var click;
 CardListRe({Key? key, this.lists , this.click}) : super(key: key);

  @override
  State<CardListRe> createState() => _CardListReState();
}

class _CardListReState extends State<CardListRe> {
  JobViewModel reviewmodel = JobViewModel();
  @override


  void initState() {
    // TODO: implement initState
    super.initState();
    print("User Recommended");
    print(widget.lists);
    reviewmodel.FetchRecommend(widget.lists);
  }
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<JobViewModel>(
      create: (context) => reviewmodel ,
      builder: (context, child) {
        return  Consumer<JobViewModel>(
          builder: (context, post, child) {
            var status  = post.jobReresponse.status;
            var length = post.jobReresponse?.data?.data?.attributes?.length;
            switch(status){
              case Status.COMPLETED:
                return


                  Container(
                    height: 260,
                    child: length == null ?
                        Center(child: Column(
                          children: [
                            Lottie.network('https://assets5.lottiefiles.com/packages/lf20_zadfo6lc.json',


                            fit: BoxFit.contain,
                              width: 230,
                              height: 230

                            ),
                            Text('Sorry, we couldn\'t find any result  ',style: TextStyle(
                              color: Colors.white,
                              fontSize: AppFont.smfont+1,
                              fontWeight: FontWeight.w400

                            ),),
                          ],
                        ),):


                    ListView.builder(
                      scrollDirection: Axis.horizontal,

                      itemCount: length ?? 0,
                      itemBuilder: (context, index) {
                        var posts = post.jobReresponse?.data?.data?.attributes?[index];
                        return Container(
                          width: 350,

                            margin: EdgeInsets.all(7.5),

                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10)

                            ),
                            child:GestureDetector(
                              onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) =>

                                  JobDetailSr(uid: posts?.sId)

                                ,)),
                                  child: Container(
                                padding: EdgeInsets.all(13.5),
                                decoration: BoxDecoration(
                                    color: AppColor.thirdcolor,

                                    borderRadius: BorderRadius.circular(25)
                                ),


                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,

                                  mainAxisSize: MainAxisSize.max,
                                  children: [
                                    Row(
                                      children: [
                                        CircleAvatar(
                                          radius: 27,
                                          backgroundImage:
                                              posts?.user?.thumbnail?.attributes?.path == null ?
                                          NetworkImage('https://i.pinimg.com/564x/48/6c/a0/486ca00640b169300b48e9ceacd8e401.jpg')
                                  :   NetworkImage('${ApiApiUrl.localurl}/${posts?.user?.thumbnail?.attributes?.path}'),
                                        ),
                                        SizedBox(width: 69,),

                                        Row(
                                          children: [
                                            Icon(Icons.location_on,
                                                color:AppColor.fourthcolor),
                                            SizedBox(width: 10,),
                                            Text('${posts?.address?.city}',
                                                style: TextStyle(
                                                    fontSize:AppFont.medfont+2,
                                                    fontWeight: FontWeight.w400,

                                                    color:Colors.black

                                                )),
                                          ],
                                        ),


                                      ],
                                    ),
                                    SizedBox(height: 25,),
                                    Row(
                                      children: [
                                        Expanded(
                                          child: Text('${posts?.title.toString().substring(0)}',
                                              style: TextStyle(
                                                fontSize:AppFont.megafont,
                                                fontWeight: FontWeight.w600,

                                                color: Colors.black,

                                                overflow: TextOverflow.ellipsis,


                                              ),
                                            overflow: TextOverflow.ellipsis,
                                            maxLines:1,
                                            softWrap:false,



                                          ),
                                        )

                                        ,
                                      ],
                                    ),
                                    SizedBox(height: 25,),
                                    Row(
                                      children: [
                                        ElevatedButton(
                                          onPressed: ()=>null,
                                          child: Text('${posts?.jobDescription?.jobtype}',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: AppFont.smfont+1
                                            ),
                                          ),
                                          style: ButtonStyle(
                                              backgroundColor:MaterialStatePropertyAll(AppColor.primarycolor),
                                              elevation: MaterialStatePropertyAll(0)
                                          ),


                                        ),
                                        SizedBox(width: 25,),
                                        ElevatedButton(
                                          onPressed: ()=>null,
                                          child: Text('${posts?.jobDescription?.experience}',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: AppFont.smfont+1
                                            ),
                                          ),
                                          style: ButtonStyle(
                                              backgroundColor:MaterialStatePropertyAll(AppColor.fourthcolor),
                                              elevation: MaterialStatePropertyAll(0)
                                          ),


                                        ),
                                        SizedBox(width: 25,),
                                        ElevatedButton(
                                          onPressed: ()=>null,

                                          child: Row(
                                            children: [

                                              Icon(Icons.people,
                                                color: Colors.white,
                                                size: 20,

                                              ),
                                              SizedBox(width: 5,),
                                              Text('${posts?.jobDescription?.totalPosition}',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: AppFont.smfont+2,
                                                    fontWeight: FontWeight.w600
                                                ),
                                              ),
                                            ],
                                          ),
                                          style: ButtonStyle(
                                              backgroundColor:MaterialStatePropertyAll(AppColor.bgcolor),
                                              elevation: MaterialStatePropertyAll(0)
                                          ),


                                        )
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            )
                        );
                      },)
                );
              case Status.LOADING:
                return Center(
                  child: CircularProgressIndicator(
                    backgroundColor: AppColor.primarycolor,
                  ),
                );
              case Status.ERROR:
                return Center(
                  child: CircularProgressIndicator(
                    backgroundColor: AppColor.primarycolor,
                  ),
                );
              default:
                return Center(
                  child: CircularProgressIndicator(
                    backgroundColor: AppColor.primarycolor,
                  ),
                );
            }

          },

        );
      },

    );
  }
}
