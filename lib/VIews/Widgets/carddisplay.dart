import 'package:flutter/material.dart';
import 'package:jobapp/data/Response/api_response_service.dart';
import 'package:jobapp/data/Response/status_response.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../ViewModel/job_view_model.dart';
import '../../model/job_model_mock.dart';
import '../../res/Appcolor.dart';
import '../../res/Appfont.dart';
import '../Jobs_details/job_detail.dart';
class CardDisplay extends StatefulWidget {
  var title;
  CardDisplay  ({Key? key, this.title}) : super(key: key);
  @override
  State<CardDisplay> createState() => _CardDisplayState();
}

class _CardDisplayState extends State<CardDisplay> with ChangeNotifier{
  var _scrollcontroller = ScrollController();
  JobViewModel jobsviewmodel = JobViewModel();
   var items = [
     Post(id: 1,title: 'Graphic Designer',Salary: 400,type: 'Remote',level: 'Junior'),
     Post(id: 22,title: 'Manager',Salary: 1300,type: 'Part Time',level: 'Junior'),
     Post(id: 3,title: 'Web Developer',Salary: 1300,type: 'Part Time',level: 'Senior'),
     Post(id: 4,title: 'Video Editor',Salary: 300,type: 'Full Time',level: 'Junior'),
     Post(id: 4,title: 'Video Editor',Salary: 300,type: 'Full Time',level: 'Junior'),
     Post(id: 4,title: 'Video Editor',Salary: 300,type: 'Full Time',level: 'Junior'),
     Post(id: 4,title: 'Video Editor',Salary: 300,type: 'Full Time',level: 'Junior'),
     Post(id: 4,title: 'Video Editor',Salary: 300,type: 'Full Time',level: 'Junior'),
     Post(id: 4,title: 'Video Editor',Salary: 300,type: 'Full Time',level: 'Junior'),
   ];
   var userid;
  var token;

  @override
  void initState() {

    print(widget.title);
    // TODO: implement initState
    // jobsviewmodel.fetchall();
    // print(jobsviewmodel.fetchall());
    // _scrollcontroller.addListener(onScrollToBottom);
    if(widget.title==''){
      checkpreferenceWithNotitle();
    }
    else{
      checkpreference(widget.title);
    }

  }
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<JobViewModel>(
      create:(context) =>jobsviewmodel , //listen to notify from jobs view model
      child: Consumer<JobViewModel>(
        //consumer = using the provider notify whenever we fetch api
        builder: (context,post,child) {


  var status =jobsviewmodel.jobApiResponse.status;

  print(status);


  var length= post.jobApiResponse.data?.data?.attributes?.length;
  print(length);

  print(length);
  switch(status){
      case Status.LOADING:
        return Center(
          child: CircularProgressIndicator(
backgroundColor: AppColor.primarycolor,
          ),
        );


      case Status.COMPLETED:
        return  ListView.builder(

          // physics: NeverScrollableScrollPhysics(),


          itemCount:length ?? 0,


          itemBuilder: (context, index) {

            var posts = jobsviewmodel.jobApiResponse.data?.data?.attributes?[index];
            // var post =  postjobviewmodel.jobApiResponse.data!.data![index].attributes;
            // dynamic path =postjobviewmodel.jobApiResponse.data!.data!.attributes![index].user?.thumbnail?.responses?.path;
            // print(postjobviewmodel.jobApiResponse.data!.data!.attributes![index].title);
            print(posts);
            return Container(
              margin: EdgeInsets.symmetric(vertical: 20),
              decoration: BoxDecoration(
                color: AppColor.thirdcolor,
                  borderRadius: BorderRadius.circular(10)
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 15),
                child:
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    ListTile(

                      style: ListTileStyle.drawer,
                      leading: CircleAvatar(
                        backgroundImage:posts?.user?.thumbnail?.attributes?.path == null ?
                        NetworkImage('https://i.pinimg.com/564x/48/6c/a0/486ca00640b169300b48e9ceacd8e401.jpg') :
                        NetworkImage('http://10.0.2.2:3000/${posts?.user?.thumbnail?.attributes?.path}')
                        ,
                      ),
                      title: Text('${posts?.title}'),
                      subtitle: Text('Salary range \$ ${posts?.jobDescription?.minSalary} -'
                          '\$ ${posts?.jobDescription?.maxSalary} / month'),
                      trailing: IconButton(onPressed: ()=>Navigator.push(context,MaterialPageRoute(builder: (context) =>
                          JobDetailSr(uid: posts?.sId) //TODO SCREEN JOB DETAIL
                        ,)),
                          icon: Icon(Icons.navigate_next_outlined)
                      ),

                      isThreeLine: true,

                    ),

                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 18),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          ElevatedButton(
                            onPressed: ()=> null,

                            child: Text('${posts?.jobDescription?.jobtype}',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: AppFont.smfont+1
                              ),
                            ),
                            style: ButtonStyle(
                                backgroundColor:MaterialStatePropertyAll(AppColor.primarycolor),
                                elevation: MaterialStatePropertyAll(0)
                            ),


                          ),
                          SizedBox(width: 25,),
                          ElevatedButton(
                            onPressed: ()=>null,
                            child: Text('${posts?.jobDescription?.experience}',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: AppFont.smfont+1
                              ),
                            ),
                            style: ButtonStyle(
                                backgroundColor:MaterialStatePropertyAll(AppColor.fourthcolor),
                                elevation: MaterialStatePropertyAll(0)
                            ),


                          ),
                          SizedBox(width: 25,),
                          ElevatedButton(
                            onPressed: ()=>null,
                            child: Text('${posts?.status == true ?  'Available' : 'Not Availble'}',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: AppFont.smfont+1
                              ),
                            ),
                            style: ButtonStyle(
                                backgroundColor:
                                posts?.status == true ?

                                MaterialStatePropertyAll(Colors.greenAccent) :
                                MaterialStatePropertyAll(Colors.grey),
                                elevation: MaterialStatePropertyAll(0)
                            ),


                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            );
          },
        );
      case Status.ERROR:
        return Center(
          child: Text('Error has been occur'),
        );
      default:
        return Center(
          child: Text('Something has error'),
        );

  }




        },

      ),
    );


  }

   void onScrollToBottom() {

     if(_scrollcontroller.position.pixels == _scrollcontroller.position.maxScrollExtent){
       print("Reached bottom");
     }
   }

  void checkpreference(String? title)async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? id = prefs.getString('userid');
    userid = id;
    print("new user id");
    print(userid);
    var tokens= prefs.getString('tokens');
    print("The token in browse screen fetch ${tokens}");
    jobsviewmodel.fetchall(tokens,title);
  }
  void checkpreferenceWithNotitle()async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? id = prefs.getString('userid');
    userid = id;
    print("new user id");
    print(userid);
    var tokens= prefs.getString('tokens');
    print("The token in browse screen fetch ${tokens}");
    jobsviewmodel.fetchall(tokens,'');
  }


}
