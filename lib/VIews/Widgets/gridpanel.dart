import 'package:flutter/material.dart';

import 'package:jobapp/VIews/AuthScreen/Login.dart';
import 'package:jobapp/VIews/AuthScreen/register.dart';
import 'package:jobapp/VIews/Widgets/Joblistv1.dart';

import '../../res/Appcolor.dart';
import '../../res/Appfont.dart';
import '../Widgets/cardlistv1.dart';
import '../Widgets/categorycardlist.dart';
class GridPanel extends StatelessWidget {
 GridPanel({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return         Container(
      height:300,
      decoration: BoxDecoration(

      ),
      child: GridView.count(
        crossAxisCount: 2,
        shrinkWrap: true,
        crossAxisSpacing: 14,
        childAspectRatio: (1/.8),
        reverse: true,
        mainAxisSpacing: 14,
        physics: NeverScrollableScrollPhysics(),
        children: [

          Container(


            decoration: BoxDecoration(
                color: AppColor.whitecolor,
                borderRadius: BorderRadius.circular(20)
            ),

            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(

                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(Icons.people_alt,color: Colors.grey,),
                      SizedBox(width: 5,),
                      Text('100 posts',
                        style: TextStyle(
                            fontSize: AppFont.medfont+5,
                            color: Colors.grey
                        ),)
                    ],
                  ),

                  Text('Remoted',
                    style: TextStyle(
                        fontSize: AppFont.medfont+4.6,
                        color: Colors.grey,
                        fontWeight: FontWeight.bold
                    ),),

                  Text('Job Announcements',
                    style: TextStyle(
                        fontSize: AppFont.bigfont,
                        color: Colors.grey,
                        fontWeight: FontWeight.w400
                    ),)
                ],
              ),
            ),

          ),//TODO row Remoted Jobs
          Container(


            decoration: BoxDecoration(
              color: AppColor.primarycolor,
                borderRadius: BorderRadius.circular(20)
            ),

            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(

                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(Icons.people_alt,color: Colors.white,),
                      SizedBox(width: 5,),
                      Text('54 posts',
                        style: TextStyle(
                            fontSize: AppFont.medfont+5,
                            color: Colors.white
                        ),)
                    ],
                  ),

                  Text('PartTime',
                    style: TextStyle(
                        fontSize: AppFont.medfont+4.6,
                        color: Colors.white,
                        fontWeight: FontWeight.bold
                    ),),

                  Text('Jobs Annoucements ',
                    style: TextStyle(
                        fontSize: AppFont.bigfont,
                        color: Colors.white,
                        fontWeight: FontWeight.w400
                    ),)
                ],
              ),
            ),

          ),//TODO row Part Time Jobs
          Container(


            decoration: BoxDecoration(
                color: AppColor.secondcolor,
                borderRadius: BorderRadius.circular(20)
            ),

            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(

                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(Icons.people_alt,color:  Colors.black,),
                      SizedBox(width: 5,),
                      Text('10 posts',
                        style: TextStyle(
                            fontSize: AppFont.medfont+5,
                            color: Colors.black,
                          fontWeight: FontWeight.bold
                        ),)
                    ],
                  ),

                  Text('FullTime',
                    style: TextStyle(
                        fontSize: AppFont.medfont+4.6,
                        color:   Colors.black,
                        fontWeight: FontWeight.bold
                    ),),

                  Text(' Jobs Annoucements',
                    style: TextStyle(
                        fontSize: AppFont.bigfont,
                        color: Colors.black,
                        fontWeight: FontWeight.w300
                    ),)
                ],
              ),
            ),

          ),//TODO row Full Time Jobs
          Container(


            decoration: BoxDecoration(
                color: AppColor.bgcolor,
                borderRadius: BorderRadius.circular(20)
            ),

            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(

                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(Icons.people_alt,color: Colors.white,),
                      SizedBox(width: 5,),
                      Text('30',
                        style: TextStyle(
                          fontSize: AppFont.medfont+5,
                          color: Colors.white,
                        ),)
                    ],
                  ),


                  Text('Oversea',
                    style: TextStyle(
                        fontSize: AppFont.medfont+4.6,
                        color: Colors.white,
                        fontWeight: FontWeight.bold
                    ),),

                  Text('Jobs Annoucements',
                    style: TextStyle(
                        fontSize: AppFont.bigfont,
                        color: Colors.white,
                        fontWeight: FontWeight.w400
                    ),)
                ],
              ),
            ),

          ),//TODO row Oversea


        ],

      ),
    );
  }
}
