import 'package:flutter/material.dart';

import 'package:jobapp/VIews/AuthScreen/Login.dart';
import 'package:jobapp/VIews/AuthScreen/register.dart';
import 'package:jobapp/VIews/Widgets/Joblistv1.dart';

import '../../res/Appcolor.dart';
import '../../res/Appfont.dart';
import '../Widgets/cardlistv1.dart';


class CategoryCardList extends StatelessWidget {
 CategoryCardList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 200,

        child:ListView(

          scrollDirection: Axis.horizontal,
          children: [
                  Container(

        padding: EdgeInsets.all(5.5),

        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10)

        ),
        child:Container(
          padding: EdgeInsets.all(13.5),
          width: 240,

          decoration: BoxDecoration(


              image:
              DecorationImage(
                  image:
                  NetworkImage('https://images.unsplash.com/photo-1665686308827-eb62e4f6604d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1470&q=80',


                  ),
                  fit: BoxFit.cover,
                  colorFilter: ColorFilter.mode(
                      Colors.black.withOpacity(0.5),
                      BlendMode.darken

                  )




              ),


              borderRadius: BorderRadius.circular(25)
          ),


          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceAround,

            mainAxisSize: MainAxisSize.max,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,

                children: [


                  Text('Business',
                      style: TextStyle(
                          fontSize:AppFont.medfont+5,
                          fontWeight: FontWeight.w700,

                          color:Colors.white

                      )),
                  SizedBox(width: 23,),
                  Icon(Icons.arrow_forward_ios,
                    size: 15,

                    color: Colors.white

                    ,)


                ],
              ),
              SizedBox(height: 25,),
              Row(
                children: [


                  Text('25',
                      style: TextStyle(
                          fontSize:AppFont.medfont+5,
                          fontWeight: FontWeight.w700,

                          color:Colors.grey

                      )),
                  SizedBox(width: 10,),
                  Text('Available Jobs',
                      style: TextStyle(
                          fontSize:AppFont.medfont,
                          fontWeight: FontWeight.w700,

                          color:Colors.grey

                      )),
                ],
              ),


            ],
          ),
        )
    ),
            Container(

                padding: EdgeInsets.all(5.5),

                decoration: BoxDecoration(


                ),
                child:Container(
                  padding: EdgeInsets.all(13.5),
                  width: 240,

                  decoration: BoxDecoration(


                      image:
                      DecorationImage(
                          image:
                          NetworkImage
                            ('https://images.unsplash.com/photo-1489875347897-49f64b51c1f8?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2070&q=80',


                          ),
                          fit: BoxFit.cover,
                          colorFilter: ColorFilter.mode(
                              Colors.black.withOpacity(0.5),
                              BlendMode.darken

                          )




                      ),


                      borderRadius: BorderRadius.circular(25)
                  ),


                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,

                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,

                        children: [


                          Text('IT / Software Dev',
                              style: TextStyle(
                                  fontSize:AppFont.medfont+5,
                                  fontWeight: FontWeight.w700,

                                  color:Colors.white

                              )),
                          SizedBox(width: 23,),
                          Icon(Icons.arrow_forward_ios,
                            size: 15,

                            color: Colors.white

                            ,)


                        ],
                      ),
                      SizedBox(height: 25,),
                      Row(
                        children: [


                          Text('25',
                              style: TextStyle(
                                  fontSize:AppFont.medfont+5,
                                  fontWeight: FontWeight.w700,

                                  color:Colors.grey

                              )),
                          SizedBox(width: 10,),
                          Text('Available Jobs',
                              style: TextStyle(
                                  fontSize:AppFont.medfont,
                                  fontWeight: FontWeight.w700,

                                  color:Colors.grey

                              )),
                        ],
                      ),


                    ],
                  ),
                )
            ),
            Container(

                padding: EdgeInsets.all(5.5),

                decoration: BoxDecoration(


                ),
                child:Container(
                  padding: EdgeInsets.all(13.5),
                  width: 240,

                  decoration: BoxDecoration(


                      image:
                      DecorationImage(
                          image:
                          NetworkImage('https://images.unsplash.com/photo-1504813184591-01572f98c85f?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1471&q=80',


                          ),
                          fit: BoxFit.cover,
                          colorFilter: ColorFilter.mode(
                              Colors.black.withOpacity(0.5),
                              BlendMode.darken

                          )




                      ),


                      borderRadius: BorderRadius.circular(25)
                  ),


                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,

                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,

                        children: [


                          Text('Health',
                              style: TextStyle(
                                  fontSize:AppFont.medfont+5,
                                  fontWeight: FontWeight.w700,

                                  color:Colors.white

                              )),
                          SizedBox(width: 23,),
                          Icon(Icons.arrow_forward_ios,
                            size: 15,

                            color: Colors.white

                            ,)


                        ],
                      ),
                      SizedBox(height: 25,),
                      Row(
                        children: [


                          Text('25',
                              style: TextStyle(
                                  fontSize:AppFont.medfont+5,
                                  fontWeight: FontWeight.w700,

                                  color:Colors.grey

                              )),
                          SizedBox(width: 10,),
                          Text('Available Jobs',
                              style: TextStyle(
                                  fontSize:AppFont.medfont,
                                  fontWeight: FontWeight.w700,

                                  color:Colors.grey

                              )),
                        ],
                      ),


                    ],
                  ),
                )
            )
          ]
          ,)
    );
  }
}
