import 'package:flutter/material.dart';
import 'package:jobapp/ViewModel/job_view_model.dart';
import 'package:jobapp/model/job_model.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../res/Appcolor.dart';
import '../../res/Appfont.dart';
import '../../res/BaseUrl.dart';
import '../Jobs_details/job_detail.dart';
class JoblistCard extends StatefulWidget {
JoblistCard({Key? key}) : super(key: key);

  @override
  State<JoblistCard> createState() => _JoblistCardState();
}

class _JoblistCardState extends State<JoblistCard> {
  @override
  JobViewModel postjobviewmodel = JobViewModel();

   var token;
  void initState() {
    // TODO: implement initState
    super.initState();

      checkpreference(); //TODO FETCH HERE




  }
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<JobViewModel>(
      create:(context) =>  postjobviewmodel,
      builder: (context, child) {
        return  Consumer<JobViewModel>(
          builder: (context, post, child) {
            var status =postjobviewmodel.jobApiResponse.status;

              print(status);


          var length= post.jobApiResponse.data?.data?.attributes?.length;
          print(post.jobApiResponse.data?.data);
          print("job ${length}");


            return length == null ?
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 50),
              child:
              Center(child: Column(
                children: [
                  Lottie.network('https://assets5.lottiefiles.com/packages/lf20_zadfo6lc.json',


                      fit: BoxFit.contain,
                      width: 230,
                      height: 230

                  ),
                  Text('Sorry, we couldn\'t find any result  ',style: TextStyle(
                      color: Colors.white,
                      fontSize: AppFont.smfont+1,
                      fontWeight: FontWeight.w400

                  ),),
                ],
              ),)
            ):

              ListView.builder(
              physics: NeverScrollableScrollPhysics(),


              itemCount:length ?? 0,


              itemBuilder: (context, index) {
                dynamic thumbnails = postjobviewmodel.jobApiResponse.data!.data!.attributes?[index].user?.thumbnail;
                var posts = postjobviewmodel.jobApiResponse.data!.data!.attributes?[index];
                // var post =  postjobviewmodel.jobApiResponse.data!.data![index].attributes;
                // dynamic path =postjobviewmodel.jobApiResponse.data!.data!.attributes![index].user?.thumbnail?.responses?.path;
                // print(postjobviewmodel.jobApiResponse.data!.data!.attributes![index].title);
                print(posts);
                return
                  Container(
                  margin: EdgeInsets.symmetric(vertical: 10),
                  decoration: BoxDecoration(
                    color: AppColor.thirdcolor,
                    borderRadius: BorderRadius.circular(10)
                  ), 
                  
                  child:length == null ?
                  Center(child: Column(
                    children: [
                      Lottie.network('https://assets9.lottiefiles.com/packages/lf20_eltfOSSxwb.json',


                          fit: BoxFit.cover,
                          width: 240,
                          height: 240

                      ),
                      Text('Sorry, we couldn\'t find any result 0 ',style: TextStyle(
                          color: Colors.grey,
                          fontSize: AppFont.smfont+1,
                          fontWeight: FontWeight.w400

                      ),),
                    ],
                  ),):

                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 15),
                    child:
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        ListTile(

                          style: ListTileStyle.drawer,
                          leading: CircleAvatar(
                            backgroundImage:posts?.user?.thumbnail?.attributes?.path == null ?
                            NetworkImage('https://i.pinimg.com/564x/48/6c/a0/486ca00640b169300b48e9ceacd8e401.jpg') :
                            NetworkImage('${ApiApiUrl.localurl}/${posts?.user?.thumbnail?.attributes?.path}')
                            ,
                          ),
                          title:         Row(
                            children: [
                              Expanded(
                                child: Text('${posts?.title.toString().substring(0)}',
                                  style: TextStyle(
                                    fontSize:AppFont.bigfont+1,
                                    fontWeight: FontWeight.w500,

                                    color: Colors.black,

                                    overflow: TextOverflow.ellipsis,


                                  ),
                                  overflow: TextOverflow.ellipsis,
                                  maxLines:1,
                                  softWrap:false,



                                ),
                              )

                              ,
                            ],
                          ),
                          subtitle: Text('Salary range \$ ${posts?.jobDescription?.minSalary} -'
                              '\$ ${posts?.jobDescription?.maxSalary} / month'),
                          trailing: IconButton(onPressed: ()=>Navigator.push(context,MaterialPageRoute(builder: (context) =>

                              JobDetailSr(uid: posts?.sId) //TODO SCREEN JOB DETAIL
                            ,)),
                              icon: Icon(Icons.navigate_next_outlined)
                          ),

                          isThreeLine: true,

                        ),
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 18),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              ElevatedButton(
                                onPressed: ()=>null,

                                child: Text('${posts?.jobDescription?.jobtype}',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: AppFont.smfont+1
                                  ),
                                ),
                                style: ButtonStyle(
                                    backgroundColor:MaterialStatePropertyAll(AppColor.primarycolor),
                                    elevation: MaterialStatePropertyAll(0)
                                ),


                              ),
                              SizedBox(width: 25,),
                              ElevatedButton(
                                onPressed: ()=>null,
                                child: Text('${posts?.jobDescription?.experience}',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: AppFont.smfont+1
                                  ),
                                ),
                                style: ButtonStyle(
                                    backgroundColor:MaterialStatePropertyAll(AppColor.bgcolor),
                                    elevation: MaterialStatePropertyAll(0)
                                ),


                              ),
                              SizedBox(width: 25,),
                              ElevatedButton(
                                onPressed: ()=>null,
                                child: Text('${posts?.status == true ?  'Available' : 'Not Availble'}',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: AppFont.smfont+1
                                  ),
                                ),
                                style: ButtonStyle(

                                    backgroundColor:
                                    posts?.status == true ?

                                    MaterialStatePropertyAll(


                                        Colors.greenAccent


                                    ) :
                                    MaterialStatePropertyAll(


                                        AppColor.fourthcolor


                                    ),
                                    elevation: MaterialStatePropertyAll(0)
                                ),


                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                );
              },
            );
          },

        );
      },

    );
  }

  void checkpreference()async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var tokens= prefs.getString('tokens');
    print("The token fetch ${tokens}");
    await postjobviewmodel.fetchall(tokens,'');
  }
}
