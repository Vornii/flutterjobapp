import 'package:bulleted_list/bulleted_list.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:jobapp/ViewModel/job_view_model.dart';
import 'package:jobapp/model/message_model.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:timeago/timeago.dart' as timeago;


import '../../../../res/Appcolor.dart';
import '../../../../res/Appfont.dart';
import '../../../data/Response/status_response.dart';
import '../Jobs_details/postjob_screen.dart';
import 'LongPosts.dart';
import 'editpostview.dart';
import 'jobspost.dart';


class  LongPostScreen extends StatefulWidget {
  var open;
  LongPostScreen({Key? key, this.open}) : super(key: key);

  @override
  State< LongPostScreen> createState() => _LongPostScreenState();
}

class _LongPostScreenState extends State< LongPostScreen> {
  JobViewModel postviewmodel = JobViewModel();
  var heightmodel = 900.5;

  var jobid ;
  var tokenid ;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setupfetch();
  }
  Widget build(BuildContext context) {
    var open = widget.open;
    return Scaffold(
      appBar: AppBar(

        elevation: 0,
        backgroundColor: AppColor.primarycolor,
        automaticallyImplyLeading:open == null ? true : false , //set to false = no go back bar
        iconTheme: IconThemeData(
            color: Colors.white
        ),
        title:  Text('Job Announcement',style: TextStyle(
            color: Colors.white
        ),),
        centerTitle: true,

      ),
      backgroundColor: AppColor.thirdcolor,
      body: ChangeNotifierProvider<JobViewModel>(
        create:(context) =>  postviewmodel ,
        builder: (context, child) {
          return  Consumer<JobViewModel>(
            builder: (context, jobs, child) {
              var status = postviewmodel.PostByUIdResponse.status;
              print('Length ${jobs.PostByUIdResponse?.data?.data?.job?.length}' );
              var postlength = jobs.PostByUIdResponse?.data?.data?.job?.length;
              print("Status API Posts by User id  MEssage ${status}");

              if(postviewmodel.PostDeleteResponse.status != Status.LOADING){
                WidgetsBinding.instance.addPostFrameCallback((timeStamp) {

                  Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(
                      builder: (context) => LongPostScreen()), (Route route) => route.isFirst);


                  // Navigator.push(context, MaterialPageRoute(builder: (context) {
                  //   return UserPostScreen();
                  // },));



                  // Navigator.pushAndRemoveUntil(
                  //     context,
                  //     MaterialPageRoute(
                  //         builder: (BuildContext context) =>
                  //             UserPostScreen(isUpdate:true)),
                  //         (Route<dynamic> route) =>true );
                  // Navigator.pushReplacement(
                  //     context,
                  //     MaterialPageRoute(
                  //         builder: (BuildContext context) => super.widget));

                });
              }

              switch(status){

                case Status.COMPLETED:
                  return ChangeNotifierProvider<JobViewModel>(
                    create: (context) =>  postviewmodel ,
                    builder: (context, child) {
                      return Consumer<JobViewModel>(
                        builder: (context, jobs, child) {
                          var status = jobs.PostByUIdResponse.status;
                          var items = jobs.PostByUIdResponse.data?.data;
                          var joblist = items?.job;
                          var length = joblist?.length;
                          switch (status){
                            case Status.COMPLETED:
                              return Container(
                                margin: EdgeInsets.symmetric(vertical: 5),
                                padding: EdgeInsets.symmetric(horizontal: 3,vertical: 5),
                                child: ListView.builder(

                                  itemCount: length ?? 0,
                                  // physics: NeverScrollableScrollPhysics(),
                                  // physics: NeverScrollableScrollPhysics(),
                                  itemBuilder: (context, index) {
                                    return GestureDetector(

                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(vertical: 3),
                                        child: Card(


                                            elevation: 0,
                                            shape:RoundedRectangleBorder(
                                                borderRadius: BorderRadius.circular(12)
                                            ) ,
                                            child:   GestureDetector(
                                              onTap: () {

                                              },
                                              child: Padding(
                                                padding: const EdgeInsets.all(0),
                                                child: ListTile(
                                                  leading:IconButton.outlined(onPressed: () {
                                                    // setState(() {
                                                    //
                                                    //   // jobid = joblist?[index].sId;
                                                    //   // print(jobid);
                                                    //   // print(tokenid);
                                                    //
                                                    // });


                                                    showDialog(context: context, builder: (context) {
                                                      return AlertDialog(
                                                        elevation: 0,
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius: BorderRadius.circular(10)
                                                        ),




                                                        backgroundColor: Colors.white,

                                                        scrollable: true,

                                                        content: Column(


                                                          children: [
                                                            Row(
                                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                              children: [
                                                                Text('Choose',style: TextStyle(
                                                                    color: AppColor.primarycolor,
                                                                    fontSize: AppFont.bigfont+5,
                                                                    fontWeight: FontWeight.w500

                                                                ),),

                                                                IconButton(onPressed: ()=>Navigator.pop(context),

                                                                    icon: Icon(Icons.close,color: AppColor.primarycolor,)
                                                                )

                                                              ],
                                                            ),

                                                            Divider(color: AppColor.primarycolor),


                                                            Container(

                                                              child: Row(
                                                                children: [


                                                                  Expanded(

                                                                    child: TextButton(onPressed: () async{
                                                                      Navigator.push(context,MaterialPageRoute(builder: (context) =>

                                                                          CreateScreen(isUpdate:

                                                                          true,jobspost:joblist?[index],slong:true)
                                                                        ,));


                                                                      print("jobid ${jobid}");
                                                                    },
                                                                        style: ButtonStyle(


                                                                        ),

                                                                        child: Row(
                                                                          children: [
                                                                            Icon(Icons.edit,size: 20,color: AppColor.primarycolor,),
                                                                            SizedBox(width: 10,),
                                                                            Text('Edit Your Post',style: TextStyle(
                                                                                color: AppColor.primarycolor,
                                                                                fontSize: AppFont.bigfont

                                                                            ),),
                                                                          ],
                                                                        )),
                                                                  ),


                                                                ],
                                                              ),
                                                            ),





                                                            Container(

                                                              child: Row(
                                                                children: [


                                                                  Expanded(

                                                                    child: TextButton(onPressed: ()=>null,
                                                                        style: ButtonStyle(


                                                                        ),

                                                                        child: Row(

                                                                          children: [

                                                                            Icon(Icons.edit_off_sharp,size: 20,color: AppColor.bgcolor,),

                                                                            SizedBox(width: 10,),

                                                                            Text('Make It Unavailable',
                                                                              style: TextStyle(

                                                                                  color: AppColor.bgcolor,

                                                                                  fontSize: AppFont.bigfont

                                                                              ),

                                                                            ),

                                                                          ],
                                                                        )),
                                                                  ),


                                                                ],
                                                              ),
                                                            ),



                                                            Container(

                                                              child: Row(
                                                                children: [


                                                                  Expanded(

                                                                    child: TextButton(onPressed: ()  async{
                                                                      print("Press delete");
                                                                      // setState(() {
                                                                      //
                                                                      //
                                                                      // });
                                                                      var jid = joblist?[index].sId;
                                                                      print(joblist?[index].sId);

                                                                      await postviewmodel.deleteByJobId(jid, tokenid);
                                                                      print(jobid);

                                                                      print("jobid ${jobid}");

                                                                    },
                                                                        style: ButtonStyle(

                                                                        ),

                                                                        child: Row(
                                                                          children: [
                                                                            Icon(Icons.delete,size: 20,          color: AppColor.fourthcolor,),
                                                                            SizedBox(width: 10,),
                                                                            Text('Delete Your Post',style: TextStyle(
                                                                                color: AppColor.fourthcolor,
                                                                                fontSize: AppFont.bigfont

                                                                            ),),
                                                                          ],
                                                                        )),
                                                                  ),


                                                                ],
                                                              ),
                                                            ),






                                                          ],
                                                        )  ,

                                                        // title: Text('Edit or Modify',style: TextStyle(
                                                        //   fontSize: AppFont.bigfont
                                                        // ),),


                                                      );
                                                    },);
                                                  }, icon: Icon(Icons.more_vert)),
                                                  trailing: Text('2 days ago',style: TextStyle(
                                                      color: Colors.grey
                                                  ),),



                                                  title: Row(

                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,

                                                    children: [

                                                      Text('${ joblist?[index].title.toString()?? 'No Title'}...',
                                                        overflow: TextOverflow.ellipsis,

                                                        style: TextStyle(
                                                          fontWeight: FontWeight.w500,
                                                          overflow:  TextOverflow.fade,

                                                          color: AppColor.primarycolor,

                                                        ),),



                                                    ],
                                                  ),
                                                  contentPadding:  EdgeInsets.symmetric(horizontal: 10,vertical: 5),
                                                  isThreeLine: true,

                                                  subtitle: Text('${ joblist?[index].jobDescription?.detail.toString() ?? 'No Description'}'
                                                    ,
                                                    maxLines: 2,
                                                    overflow: TextOverflow.ellipsis,


                                                    style: TextStyle(
                                                        fontSize: AppFont.smfont+1,
                                                        wordSpacing: 1,
                                                        height: 1.3


                                                    ),),




                                                ),
                                              ),
                                            )
                                        ),
                                      ),
                                    );
                                  },

                                ),
                              );

                            case Status.LOADING:
                              return Center(
                                child: CircularProgressIndicator(),
                              );

                            default:
                              return Text('Default');
                          }

                        },

                      );
                    },

                  );

                case Status.LOADING:
                  return Center(
                    child: CircularProgressIndicator(

                    ),
                  );
                default:
                  return Center(
                    child: CircularProgressIndicator(

                    ),
                  );

              }

            },

          );
        },

      ),
    );
  }

  void setupfetch() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();

 tokenid = prefs.get('tokens');

    String? id = prefs.getString('userid');
    print("user id ${id}");
    await postviewmodel.getJobPostsByuid(id);

  }
}

