import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:bulleted_list/bulleted_list.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:jobapp/ViewModel/post_view_model.dart';
import 'package:provider/provider.dart';
import 'package:dio/dio.dart';
import 'package:timeago/timeago.dart' as timeago;
import '../../../../res/Appcolor.dart';
import '../../../../res/Appfont.dart';
import 'package:open_file/open_file.dart';
//OpenFile.open("/sdcard/example.txt", type: "text/plain", uti: "public.plain-text");
import '../../../data/Response/status_response.dart';
import '../../res/BaseUrl.dart';
class Viewmoremessage extends StatefulWidget {
  var mid;
  Viewmoremessage({Key? key , this.mid}) : super(key: key);

  @override
  State<Viewmoremessage> createState() => _ViewmoremessageState();
}

class _ViewmoremessageState extends State<Viewmoremessage> {
  PostViewModel msgidviewmodel = PostViewModel();
  var         isDownloading = false;
  var isopenfile = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("Get msg id: ");
    print(widget.mid);
    msgidviewmodel.fetchmsgbyid(widget.mid);
  }
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor:  AppColor.primarycolor,
        elevation: 0,
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.max,
          // children: [
          //   Text('Inbox'),
          //   Text('10 messages',style: TextStyle(
          //       fontSize: AppFont.smfont+1
          //   ),),
          // ],
        ),

        centerTitle: true,

      ),
      backgroundColor: AppColor.secondcolor,

      body: SafeArea(
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 5,horizontal: 0),
          child: ChangeNotifierProvider<PostViewModel>(
            create: (context) => msgidviewmodel,
            builder: (context, child) {
              return   Consumer<PostViewModel>(
                builder: (context, msg, child) {
                  var status = msg.GetIdMsgResponse.status;
                  var message = msg.GetIdMsgResponse.data?.data;
                  var pdflink =  message?.pdfLink?.split('\document');
                  print(pdflink?[1]);
                  print(status);
                  print( message?.pdfLink);
                  switch(status){

                    case Status.COMPLETED:

                      return Container(

                          width: double.maxFinite,
                          height: double.maxFinite,
                          padding: EdgeInsets.all(13),

                          child: SingleChildScrollView(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [


                                ListTile(
                                  leading: CircleAvatar(
                                    backgroundImage: message?.user?.thumbnail?.attributes?.path == null ?

                                    NetworkImage(

                                        'https://images.unsplash.com/photo-1552058544-f2b08422138a?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=699&q=80') :
                                    NetworkImage(

                                        '${ApiApiUrl.localurl}/${message?.user?.thumbnail?.attributes?.path}'),
                                  ),
                                  contentPadding: EdgeInsets.all(5),

                                  title: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text('${message?.user?.name}',style: TextStyle(
                                          fontWeight: FontWeight.w500
                                      ),),
                                      Text('${timeago.format(DateTime.parse('${message?.publishDate}'))}',style: TextStyle(
                                          fontWeight: FontWeight.w400,
                                          fontSize: AppFont.medfont
                                      ),),
                                      Icon(Icons.phone)
                                    ],
                                  ),
                                  subtitle: Text('to me'),
                                ),

                                Divider(
                                  color: AppColor.primarycolor,
                                ),
                                SizedBox(height: 10,),
                                Text('${message?.job?.title}',
                                  textAlign: TextAlign.justify,
                                  style: TextStyle(
                                      fontSize: AppFont.megafont,
                                      color: Colors.black,

                                      fontWeight: FontWeight.w500
                                  ),
                                ),
                                SizedBox(height: 10,),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,

                                  children: [
                                    Text('Reply to your post',

                                      style: TextStyle(
                                          fontSize: AppFont.bigfont,
                                          color: Colors.black.withOpacity(0.84),
                                          fontWeight: FontWeight.w400,
                                          height: 2
                                      ),
                                    ),
                                    IconButton(onPressed: () {

                                    }, icon: Icon(Icons.arrow_forward,size: 20,))
                                  ],
                                ),      SizedBox(height: 10,),
                                Text('${message?.description}',

                                  style: TextStyle(
                                      fontSize: AppFont.bigfont,
                                      color: Colors.black.withOpacity(0.54),
                                      fontWeight: FontWeight.w400,
                                      height: 2
                                  ),
                                ),
                                SizedBox(height: 20,),



                                Text('Upload File',

                                  style: TextStyle(
                                      fontSize: AppFont.megafont-4,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500
                                  ),
                                ),             SizedBox(height: 20,),
                                Divider(
                                  color: AppColor.primarycolor,
                                ),
                                SizedBox(height: 20,),


                                Card(
                                  color:Colors.white,
                                  child: ListTile(
                                    leading: Icon(Icons.file_copy,color: AppColor.bgcolor,),
                                    title: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text("${pdflink![1].substring(1)}",
                                          style: TextStyle(
                                            fontSize: AppFont.medfont,
                                            fontWeight: FontWeight.bold,

                                          ),

                                        ),


                                        // Text("${pdflink![1].substring(1).split('.')?[1]}",
                                        //   style: TextStyle(
                                        //       fontSize: AppFont.medfont,
                                        //
                                        //   ),
                                        //
                                        // ),
                                      ],
                                    ),
                                    trailing:       isDownloading == false ?
                                    IconButton(
                                        onPressed: () async{
                                          setState(() {
                                            isDownloading = true;

                                          });



                                          await openfile(
                                              url:'${ApiApiUrl.localurl}/${message?.pdfLink}',
                                              fileName:'${message?.pdfLink}'
                                          );
                                          setState(() {
                                            isDownloading = false;
                                            isopenfile = true;
                                          });
                                        },
                                        tooltip: 'click here to download',
                                        icon: isopenfile ==true ? Icon(Icons.file_download_done_sharp,color: AppColor.primarycolor,) :
                                        Icon(Icons.download,color: AppColor.primarycolor,)
                                    )
                                        :CircularProgressIndicator(
                                      backgroundColor: AppColor.bgcolor,
                                      color:  AppColor.primarycolor,

                                    ),

                                  ),
                                )
                              ],
                            ),
                          )
                      );

                    case Status.LOADING:
                      return Center(
                        child: CircularProgressIndicator(

                        ),
                      );
                    default:
                      return Center(
                        child: CircularProgressIndicator(

                        ),
                      );
                  }

                },

              );
            },

          ),

        ),
      ),
    );
  }

  Future openfile({required String url, required String fileName}) async {

    final file = await downloadFile(url,fileName); //TODO FUNCTION TO DOWNLOAD FILES
    print(file);
    if(file==null){
      //TODOO if null we end
      return null;
      print("null");
    }
    else{
      print("open file noww");
      await OpenFile.open(file.path); //TODO aFTER DOWWNLOAD WE OPEN THE FILES
      // final result =await FilePicker.platform.pickFiles();

    }


  }

  Future downloadFile(String url, String fileName) async {
    print("download file");
    final appstorage = await getApplicationDocumentsDirectory();
    print(appstorage);
    print(url);

    final file = File('${appstorage.path}/${fileName}');
    try{

      final response =await Dio().get(url,

        options: Options(
            responseType: ResponseType.bytes,
            followRedirects: false,
            sendTimeout: null
        ),

      );

      final raf =file.openSync(mode:FileMode.write); //TodO open up file
      raf.writeFromSync(response.data); //TODO data download from url
      await raf.close();
      return file;
    }catch(e){
      print(e);
      return e;
    }



  }
}

