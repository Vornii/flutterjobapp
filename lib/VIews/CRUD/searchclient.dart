import 'package:bulleted_list/bulleted_list.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart';
import 'package:jobapp/VIews/CRUD/editmessage.dart';


import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:timeago/timeago.dart' as timeago;


import '../../../../res/Appcolor.dart';
import '../../../../res/Appfont.dart';
import '../../../data/Response/status_response.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../ViewModel/job_view_model.dart';
import '../../ViewModel/post_view_model.dart';
import '../../model/Admin/AdminJobPostModel.dart';
import '../../model/CRUD/UpdateAvRequest.dart';
import '../../model/message_model.dart';
import '../Admin/UserAdminView.dart';
import '../Admin/adminedituser.dart';
import '../Admin/adminpostview.dart';
import '../CRUD/editpostview.dart';
import '../Jobs_details/Msg/msg_detail.dart';
import '../Jobs_details/job_detail.dart';
import '../Jobs_details/pfscreen.dart';
import '../Jobs_details/postjob_screen.dart';
import '../WelcomeScreen/defaultscreen.dart';



class SearchInUser extends StatefulWidget {


  var job;
  var users;
  var ismsg;

  SearchInUser({
    super.key,


    this.job,
    this.users,
    this.ismsg
  });

  @override
  State<SearchInUser> createState() => _SearchInUserState();
}

class _SearchInUserState extends State<SearchInUser> {
  JobViewModel postviewmodel = JobViewModel();
  PostViewModel msgviewmodel = PostViewModel();
  var jobid;
  var tokenid;
  var   isMsg;
  var isloading = false;
  var txtsearch = TextEditingController();
  var  joblist = [];
  var msglist = [];
  var userrefer = [];
  var userslist = [];
  var referencejob =[];
  var referencemsg=[];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // isMsg = widget.message;
    // print("msg: ${isMsg}");
    setupfetch();
    // print(widget.job?.length);
    if(widget?.job !=null){
      joblist.addAll(widget?.job);
      referencejob.addAll(widget?.job);
    }
    if(widget?.users !=null){
      userrefer.addAll(widget?.users);
      userslist.addAll(widget?.users);
    }
    if(widget?.ismsg == true) {
      msglist.addAll(widget.job);
      referencemsg.addAll(widget.job);
    }
    print("widget is");
    print(     widget.ismsg );
    print(widget?.users);

  }
  Widget build(BuildContext context) {
    return Scaffold(

      backgroundColor: AppColor.thirdcolor,
      body:
      widget.ismsg == true ?
      ChangeNotifierProvider<PostViewModel>(
        create:(context) =>   msgviewmodel,


        builder: (context, child) {


          return

            Consumer<PostViewModel>(
              builder: (context, jobs, child) {
              var state   =  Status.COMPLETED;
                // var jobitem = jobs.jobApiResponseAll.data?.data?.attributes;
                // print(jobitem?.length);
                print(state);



                if(msgviewmodel.UpdateMessageResponse.status != Status.LOADING){
                  WidgetsBinding.instance.addPostFrameCallback((timeStamp) {

                    ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(content: Text('Your Message has updated'))
                    );

                    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(
                        builder: (context) =>   UserPostScreen(message:true )), (Route route)

                    => route.isFirst);
                  });
                }
              if(postviewmodel.DeleteResponseMsg.status != Status.LOADING){
                WidgetsBinding.instance.addPostFrameCallback((timeStamp) {

                  ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(content: Text('Your message has been deleted!!!'))
                  );
                  Navigator.push(context, MaterialPageRoute(builder: (context) => ScreenDefault(),));
                });
              }
                switch(state){

                  case Status.COMPLETED:
                    return
                      CustomScrollView(
                        slivers: [

                          SliverAppBar(


                            elevation: 0,


                            automaticallyImplyLeading:true,
                            iconTheme:IconThemeData(
                                color: AppColor.thirdcolor
                            ),

                            pinned: true,
                            titleSpacing: 0,
                            title:         Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,

                              children: [

                                Expanded(

                                  child: Container(
                                    height: 40,
                                    child: TextField(


                                      controller: txtsearch,

                                      onSubmitted: (value) {
                                        setState(() {

                                          txtsearch.text = value;
                                          print(txtsearch.text);

                                          msglist =   referencemsg.where((element) => element.description.toLowerCase().
                                          startsWith(txtsearch.text.toLowerCase())).toList();




                                          print(joblist);


                                        });

                                      },


                                      decoration: InputDecoration(

                                        // enabledBorder: UnderlineInputBorder(
                                        //     borderSide: BorderSide(
                                        //         color: AppColor.whitecolor
                                        //     )
                                        //
                                        //
                                        // ),

                                          hintText: 'Search Your Message Title',
                                          hintStyle: TextStyle(
                                            color: AppColor.primarycolor,

                                          ),
                                          fillColor: AppColor.thirdcolor,
                                          filled: true ,
                                          focusedBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(

                                              )


                                          )
                                      ),


                                    ),
                                  ),
                                  flex: 4,
                                ),
                                Expanded(
                                  child: Text(
                                    'Search',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: AppFont.bigfont,
                                        color: AppColor.thirdcolor

                                    ),

                                  ),
                                )



                              ],
                            ),



                            // title: Text('Inbox'),

                            backgroundColor: AppColor.primarycolor,


                          ),
                          SliverList(
                              delegate: SliverChildBuilderDelegate(
                                childCount: msglist.length,
                                    (context, index) {
                                  return
                                    RefreshIndicator(
                                      onRefresh:() async{

                                      },
                                      backgroundColor: AppColor.primarycolor,
                                      child: Container(
                                        padding: EdgeInsets.symmetric(vertical: 0),
                                        child:
                                        Column(
                                          children: [
                                            GestureDetector(
                                              child:

                                              Card(

                                                margin: EdgeInsets.symmetric(vertical: 0,horizontal: 0),
                                                color: AppColor.thirdcolor,
                                                elevation: 0,


                                                child:

                                                Column(

                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [

                                                    ListTile(

                                                      tileColor: AppColor.thirdcolor,


                                                      leading:IconButton.outlined(onPressed: () {
                                                        showDialog(context: context, builder: (context) {
                                                          return AlertDialog(
                                                            elevation: 0,
                                                            shape: RoundedRectangleBorder(
                                                                borderRadius: BorderRadius.circular(10)
                                                            ),




                                                            backgroundColor: Colors.white,

                                                            scrollable: true,

                                                            content: Column(


                                                              children: [

                                                                Row(
                                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                  children: [
                                                                    Text('Modify',style: TextStyle(
                                                                        color: AppColor.primarycolor,
                                                                        fontSize: AppFont.bigfont+5,
                                                                        fontWeight: FontWeight.w500

                                                                    ),),

                                                                    IconButton(onPressed: ()=>Navigator.pop(context),

                                                                        icon: Icon(Icons.close,color: AppColor.primarycolor,)
                                                                    )

                                                                  ],
                                                                ),

                                                                Divider(color: AppColor.primarycolor),


                                                                Container(

                                                                  child: Row(
                                                                    children: [


                                                                      Expanded(

                                                                        child: TextButton(onPressed: () async{
                                                                          Navigator.push(context,MaterialPageRoute(builder: (context) =>
                                                                              EditMessageScreen(message:msglist[index],)
                                                                          ));




                                                                        },
                                                                            style: ButtonStyle(


                                                                            ),

                                                                            child: Row(
                                                                              children: [
                                                                                Icon(Icons.edit,size: 20,color: AppColor.primarycolor,),
                                                                                SizedBox(width: 10,),
                                                                                Text('Edit',style: TextStyle(
                                                                                    color: AppColor.primarycolor,
                                                                                    fontSize: AppFont.bigfont

                                                                                ),),
                                                                              ],
                                                                            )),
                                                                      ),


                                                                    ],
                                                                  ),
                                                                ),








                                                                Container(

                                                                  child: Row(
                                                                    children: [


                                                                      Expanded(

                                                                        child: TextButton(onPressed: ()  async{
                                                                          print("Press delete");
                                                                          // setState(() {
                                                                          //
                                                                          //
                                                                          // });

                                                                          Navigator.pop(context);
                                                                          showDialog(context: context, builder: (context) {
                                                                            return AlertDialog(
                                                                              elevation: 0,





                                                                              backgroundColor: AppColor.thirdcolor,

                                                                              // scrollable: true,




                                                                              title: Text('Warning',style: TextStyle(
                                                                                  fontSize: AppFont.bigfont+5,
                                                                                  color: AppColor.fourthcolor,
                                                                                  fontWeight: FontWeight.w500
                                                                              ),),
                                                                              content: Text('Are you sure to delete this message?',style: TextStyle(
                                                                                  fontSize: AppFont.bigfont,
                                                                                  color: AppColor.primarycolor,
                                                                                  fontWeight: FontWeight.w400
                                                                              ),),
                                                                              actions: [
                                                                                Padding(
                                                                                  padding: const EdgeInsets.all(8.0),
                                                                                  child: GestureDetector(


                                                                                    child: Text('CANCEL',style: TextStyle(
                                                                                      fontSize: AppFont.bigfont,
                                                                                        color: AppColor.primarycolor

                                                                                    ),



                                                                                    ),
                                                                                    onTap: () {
                                                                                      Navigator.pop(context);
                                                                                    },
                                                                                  ),
                                                                                ),
                                                                                Padding(
                                                                                  padding: const EdgeInsets.all(8.0),
                                                                                  child: GestureDetector(

                                                                                    child: Text('DELETE',style: TextStyle(
                                                                                        fontSize: AppFont.bigfont,
                                                                                        color: AppColor.fourthcolor,
                                                                                        fontWeight: FontWeight.w400
                                                                                    ),



                                                                                    ),
                                                                                    onTap: () async{
                                                                                      print(msglist?[index].sId);
                                                                                      await postviewmodel.deletemsgbyid(msglist?[index].sId);
                                                                                      setState(() {

                                                                                      });

                                                                                    },
                                                                                  ),
                                                                                )
                                                                              ],


                                                                            );
                                                                          },
                                                                          );



                                                                        },
                                                                            style: ButtonStyle(

                                                                            ),

                                                                            child: Row(
                                                                              children: [
                                                                                Icon(Icons.delete,size: 20,               color: Colors.redAccent,),
                                                                                SizedBox(width: 10,),
                                                                                Text('Delete Your Message',style: TextStyle(
                                                                                    color: Colors.redAccent,
                                                                                    fontWeight: FontWeight.w500,
                                                                                    fontSize: AppFont.bigfont

                                                                                ),),
                                                                              ],
                                                                            )),
                                                                      ),


                                                                    ],
                                                                  ),
                                                                ),






                                                              ],
                                                            )  ,

                                                            // title: Text('Edit or Modify',style: TextStyle(
                                                            //   fontSize: AppFont.bigfont
                                                            // ),),


                                                          );
                                                        },);
                                                      }, icon: Icon(Icons.more_vert,color: Colors.blueGrey,)),
                                                      contentPadding: EdgeInsets.all(15),



                                                      title: Row(
                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                        children: [

                                                          Expanded(

                                                            child: Text('${msglist?[index].description}',style: TextStyle(
                                                              fontWeight: FontWeight.w600,
                                                              color: AppColor.primarycolor,

                                                            ),
                                                                maxLines: 1,

                                                                overflow: TextOverflow.ellipsis
                                                            ),
                                                          ),



                                                        ],
                                                      ),

                                                      subtitle: Column(
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: [
                                                          SizedBox(height: 8,),

                                                          Text('message to post: ${msglist?[index].job?.title}',style: TextStyle(
                                                            fontWeight: FontWeight.w400,
                                                            color: AppColor.primarycolor,
                                                            fontSize: AppFont.bigfont-2

                                                          ),

                                                            maxLines: 2,
                                                            overflow: TextOverflow.ellipsis,
                                                          )

                                                        ],
                                                      ),
                                                      // trailing: GestureDetector(
                                                      //     onTap: () {
                                                      //       // Navigator.push(context, MaterialPageRoute(builder: (context) {
                                                      //       //   return JobDetailSr(uid:jobitem![index].sId);
                                                      //       // },));
                                                      //
                                                      //     },
                                                      //
                                                      //     child: Text('View',style: TextStyle(
                                                      //
                                                      //         color: AppColor.primarycolor.withOpacity(0.75),
                                                      //         fontWeight:  FontWeight.w400,
                                                      //         fontSize: AppFont.smfont+1
                                                      //
                                                      //     ),)),



                                                    ),
                                                    Divider(),

                                                    SizedBox(height: 20,),
                                                  ],
                                                ),
                                              ),
                                            ),

                                          ],
                                        ),
                                      ),
                                    );






                                },



                              )


                          ),





                        ],

                      ) ;



                  case Status.LOADING:
                    return Center(
                      child: CircularProgressIndicator(
                        backgroundColor:Colors.indigo,
                        color: AppColor.primarycolor,



                      ),
                    );

                  case Status.ERROR:
                    return Text('Error has been occur');
                  default:
                    return Text('null');

                }
              },

            );
        },

      ) :
      ChangeNotifierProvider<JobViewModel>(
        create:(context) =>   postviewmodel,


        builder: (context, child) {


          return

            Consumer<JobViewModel>(
              builder: (context, jobs, child) {
                var state = jobs.jobApiResponseAll.status;
                var jobitem = jobs.jobApiResponseAll.data?.data?.attributes;
                // print(jobitem?.length);
                print(state);
                if(jobs.PostDeleteResponse.status != Status.LOADING){
                  WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                    ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(content: Text('The users has been deleted!!!'))
                    );
                    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(
                        builder: (context) =>  postadminview()), (Route route) => route.isFirst);




                  });
                }

                if(postviewmodel.DeleteuserRes.status != Status.LOADING){
                  WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(content:
                    Text('User has been Deleted')));

                    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(
                      builder: (context) =>     useradminview(isadmin : true),


                    ), (Route route) => route.isFirst);
                    // Navigator.push(context, MaterialPageRoute(builder: (context) => PfScreen(
                    //   open: true,
                    //   userid: widget.userid,
                    //   isupdate: true,
                    //
                    //
                    //
                    // ) ,));


                  });
                }
                switch(state){

                  case Status.COMPLETED:
                    return
                      CustomScrollView(
                        slivers: [

                          SliverAppBar(


                            elevation: 0,


                            automaticallyImplyLeading:true,
                            iconTheme:IconThemeData(
                                color: AppColor.thirdcolor
                            ),

                            pinned: true,
                            titleSpacing: 0,
                            title:         Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,

                              children: [

                                Expanded(

                                  child: Container(
                                    height: 40,
                                    child: TextField(


                                      controller: txtsearch,

                                      onSubmitted: (value) {
                                        setState(() {

                                          txtsearch.text = value;
                                          print(txtsearch.text);
                                          joblist =   referencejob.where((element) => element.title.toLowerCase().
                                          startsWith(txtsearch.text.toLowerCase())).toList();




                                          print(joblist);


                                        });

                                      },


                                      decoration: InputDecoration(

                                        // enabledBorder: UnderlineInputBorder(
                                        //     borderSide: BorderSide(
                                        //         color: AppColor.whitecolor
                                        //     )
                                        //
                                        //
                                        // ),

                                          hintText: 'Search Job title',
                                          hintStyle: TextStyle(
                                            color: AppColor.primarycolor,

                                          ),
                                          fillColor: AppColor.thirdcolor,
                                          filled: true ,
                                          focusedBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(

                                              )


                                          )
                                      ),


                                    ),
                                  ),
                                  flex: 4,
                                ),
                                Expanded(
                                  child: Text(
                                    'Search',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: AppFont.bigfont,
                                        color: AppColor.thirdcolor

                                    ),

                                  ),
                                )



                              ],
                            ),



                            // title: Text('Inbox'),

                            backgroundColor: AppColor.primarycolor,


                          ),
                          SliverList(
                              delegate: SliverChildBuilderDelegate(
                                childCount: joblist.length,
                                    (context, index) {
                                  return
                                    RefreshIndicator(
                                      onRefresh:() async{

                                      },
                                      backgroundColor: AppColor.primarycolor,
                                      child: Container(
                                        padding: EdgeInsets.symmetric(vertical: 0),
                                        child:
                                        Column(
                                          children: [
                                            GestureDetector(
                                              child:

                                              Card(

                                                margin: EdgeInsets.symmetric(vertical: 0,horizontal: 0),
                                                color: AppColor.thirdcolor,
                                                elevation: 0,


                                                child:

                                                Column(

                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [

                                                    ListTile(

                                                      tileColor: AppColor.thirdcolor,


                                                      leading:IconButton.outlined(onPressed: () {
                                                        showDialog(context: context, builder: (context) {
                                                          return AlertDialog(
                                                            elevation: 0,
                                                            shape: RoundedRectangleBorder(
                                                                borderRadius: BorderRadius.circular(10)
                                                            ),




                                                            backgroundColor: Colors.white,

                                                            scrollable: true,

                                                            content: Column(


                                                              children: [

                                                                Row(
                                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                  children: [
                                                                    Text('Modify',style: TextStyle(
                                                                        color: AppColor.primarycolor,
                                                                        fontSize: AppFont.bigfont+5,
                                                                        fontWeight: FontWeight.w500

                                                                    ),),

                                                                    IconButton(onPressed: ()=>Navigator.pop(context),

                                                                        icon: Icon(Icons.close,color: AppColor.primarycolor,)
                                                                    )

                                                                  ],
                                                                ),

                                                                Divider(color: AppColor.primarycolor),


                                                                Container(

                                                                  child: Row(
                                                                    children: [


                                                                      Expanded(

                                                                        child: TextButton(onPressed: () async{
                                                                          Navigator.push(context,MaterialPageRoute(builder: (context) =>

                                                                              CreateScreen(isUpdate: true,jobspost:joblist?[index],admin:true)
                                                                            ,));



                                                                        },
                                                                            style: ButtonStyle(


                                                                            ),

                                                                            child: Row(
                                                                              children: [
                                                                                Icon(Icons.edit,size: 20,color: AppColor.primarycolor,),
                                                                                SizedBox(width: 10,),
                                                                                Text('Edit',style: TextStyle(
                                                                                    color: AppColor.primarycolor,
                                                                                    fontSize: AppFont.bigfont

                                                                                ),),
                                                                              ],
                                                                            )),
                                                                      ),


                                                                    ],
                                                                  ),
                                                                ),








                                                                Container(

                                                                  child: Row(
                                                                    children: [


                                                                      Expanded(

                                                                        child: TextButton(onPressed: ()  async{
                                                                          print("Press delete");
                                                                          // setState(() {
                                                                          //
                                                                          //
                                                                          // });
                                                                          var jid = joblist?[index].sId;
                                                                          // print(joblist?[index].sId);
                                                                          print(jobid);
                                                                          print(tokenid);

                                                                          setState(() {
                                                                            isloading = true;
                                                                          });
                                                                          Navigator.pop(context);
                                                                          showDialog(context: context, builder: (context) {
                                                                            return AlertDialog(
                                                                              elevation: 0,





                                                                              backgroundColor: Colors.indigo,

                                                                              // scrollable: true,




                                                                              title: Text('Warning',style: TextStyle(
                                                                                  fontSize: AppFont.bigfont+5,
                                                                                  color: AppColor.fourthcolor,
                                                                                  fontWeight: FontWeight.w500
                                                                              ),),
                                                                              content: Text('Are you sure to delete this post?',style: TextStyle(
                                                                                  fontSize: AppFont.bigfont,
                                                                                  color: AppColor.thirdcolor
                                                                              ),),
                                                                              actions: [
                                                                                Padding(
                                                                                  padding: const EdgeInsets.all(8.0),
                                                                                  child: GestureDetector(


                                                                                    child: Text('CANCEL',style: TextStyle(
                                                                                      fontSize: AppFont.bigfont,
                                                                                      color: AppColor.thirdcolor,

                                                                                    ),



                                                                                    ),
                                                                                    onTap: () {
                                                                                      Navigator.pop(context);
                                                                                    },
                                                                                  ),
                                                                                ),
                                                                                Padding(
                                                                                  padding: const EdgeInsets.all(8.0),
                                                                                  child: GestureDetector(

                                                                                    child: Text('DELETE',style: TextStyle(
                                                                                        fontSize: AppFont.bigfont,
                                                                                        color: AppColor.fourthcolor
                                                                                    ),



                                                                                    ),
                                                                                    onTap: () async{
                                                                                      await postviewmodel.deleteByJobId(jid, tokenid);

                                                                                    },
                                                                                  ),
                                                                                )
                                                                              ],


                                                                            );
                                                                          },
                                                                          );






                                                                        },
                                                                            style: ButtonStyle(

                                                                            ),

                                                                            child: Row(
                                                                              children: [
                                                                                Icon(Icons.delete,size: 20,          color: AppColor.fourthcolor,),
                                                                                SizedBox(width: 10,),
                                                                                Text('Delete',style: TextStyle(
                                                                                    color: AppColor.fourthcolor,
                                                                                    fontSize: AppFont.bigfont

                                                                                ),),
                                                                              ],
                                                                            )),
                                                                      ),


                                                                    ],
                                                                  ),
                                                                ),






                                                              ],
                                                            )  ,

                                                            // title: Text('Edit or Modify',style: TextStyle(
                                                            //   fontSize: AppFont.bigfont
                                                            // ),),


                                                          );
                                                        },);
                                                      }, icon: Icon(Icons.more_vert,color: Colors.blueGrey,)),
                                                      contentPadding: EdgeInsets.all(15),



                                                      title: Row(
                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                        children: [

                                                          Expanded(

                                                            child: Text('${joblist?[index].title}',style: TextStyle(
                                                              fontWeight: FontWeight.w600,
                                                              color: AppColor.primarycolor,

                                                            ),
                                                                maxLines: 1,

                                                                overflow: TextOverflow.ellipsis
                                                            ),
                                                          ),



                                                        ],
                                                      ),

                                                      subtitle: Column(
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: [
                                                          SizedBox(height: 5,),

                                                          Text('${joblist?[index].jobDescription?.detail}',style: TextStyle(
                                                            fontWeight: FontWeight.w400,
                                                            color: AppColor.primarycolor,

                                                          ),

                                                            maxLines: 2,
                                                            overflow: TextOverflow.ellipsis,
                                                          )

                                                        ],
                                                      ),
                                                      trailing: GestureDetector(
                                                          onTap: () {
                                                            Navigator.push(context, MaterialPageRoute(builder: (context) {
                                                              return JobDetailSr(uid:jobitem![index].sId);
                                                            },));

                                                          },

                                                          child: Text('View',style: TextStyle(

                                                              color: AppColor.primarycolor.withOpacity(0.75),
                                                              fontWeight:  FontWeight.w400,
                                                              fontSize: AppFont.smfont+1

                                                          ),)),



                                                    ),
                                                    Divider(),

                                                    SizedBox(height: 20,),
                                                  ],
                                                ),
                                              ),
                                            ),

                                          ],
                                        ),
                                      ),
                                    );






                                },



                              )


                          ),





                        ],

                      ) ;



                  case Status.LOADING:
                    return Center(
                      child: CircularProgressIndicator(
                        backgroundColor:Colors.indigo,
                        color: AppColor.primarycolor,



                      ),
                    );

                  case Status.ERROR:
                    return Text('Error has been occur');
                  default:
                    return Text('null');

                }
              },

            );
        },

      )
    );


  }

  void setupfetch()  async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String? id = prefs.getString('userid');
    tokenid = prefs.get('tokens');

    print("user id ${id}");


    await postviewmodel.getJobPostsByAll(id);


  }

  void _updateuserpost(postid,status) async {


    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? uid = prefs.getString('userid');
    print(uid);
    var token = prefs.get('tokens');
    if(status == true){
      status = false;
    }
    else{
      status = true;
    }
    print(status);


    print(token);
    print(postid  );

    await postviewmodel.updateUserClient(uid,postid,status, token);
  }
}
