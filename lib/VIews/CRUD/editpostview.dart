import 'package:bulleted_list/bulleted_list.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:jobapp/VIews/CRUD/searchclient.dart';
import 'package:jobapp/ViewModel/job_view_model.dart';
import 'package:jobapp/model/message_model.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:timeago/timeago.dart' as timeago;


import '../../../../res/Appcolor.dart';
import '../../../../res/Appfont.dart';
import '../../../data/Response/status_response.dart';
import '../Jobs_details/Msg/msg_detail.dart';
import 'LongPosts.dart';
import 'jobspost.dart';


class  UserPostScreen extends StatefulWidget {
  var open;
  var isUpdate;
  var message;
  UserPostScreen({Key? key, this.open , this.isUpdate,this.message}) : super(key: key);

  @override
  State< UserPostScreen> createState() => _UserPostScreenState();
}

class _UserPostScreenState extends State< UserPostScreen> {
  JobViewModel postviewmodel = JobViewModel();
var heightmodel = 900.5;
var msg ;
  @override
  void initState(){
    // TODO: implement initState
    super.initState();

    msg = widget.message;

    setupfetch();
    print(msg);
  }
  Widget build(BuildContext context) {
    var open = widget.open;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.indigo,
        elevation: 0,
        automaticallyImplyLeading:open == null ? true : false , //set to false = no go back bar
        iconTheme: IconThemeData(
            color: Colors.white
        ),
        title:  Text('Activity',style: TextStyle(
          color:  Colors.white,
          fontWeight: FontWeight.w400
        ),),
        centerTitle: true,

      ),
      backgroundColor: AppColor.primarycolor,
      body: SafeArea(

        child: ChangeNotifierProvider<JobViewModel>(
          create:(context) =>  postviewmodel ,
          builder: (context, child) {
            return  Consumer<JobViewModel>(
              builder: (context, jobs, child) {
                var status = postviewmodel.PostByUIdResponse.status;
                print('Length ${jobs.PostByUIdResponse?.data?.data?.job?.length}' );
                var postlength = jobs.PostByUIdResponse?.data?.data?.job?.length;
                print(postlength);

                var msglength = postviewmodel.MessageResponse?.data?.data?.length;
                print("Status API Posts by User id  MEssage ${status}");


                switch(status){
                  case Status.COMPLETED:
                    var jobitem =  jobs.PostByUIdResponse?.data?.data?.job;
                    var  message =  jobs.MessageResponse.data?.data;
                    print(message?.length);
                    return

                      postlength == 0 ? 
                    Center(child: Text('You have no announcement',style: TextStyle(color: Colors.white),))
                
                :
                      Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          //




                          // Padding(
                          //   padding: const EdgeInsets.all(15.0),
                          //   child: Row(
                          //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          //
                          //     children: [
                          //       Text('${ msg == true ? 'Your Message ' : 'Your Posts'}',
                          //
                          //         style: TextStyle(
                          //             color:Colors.white,
                          //             fontWeight: FontWeight.w400,
                          //
                          //             fontSize: AppFont.megafont-5
                          //         ),
                          //       ),
                          //
                          //       GestureDetector(
                          //           onTap: () {
                          //             if(msg== true){
                          //               Navigator.push(context, MaterialPageRoute(builder: (context) {
                          //                 return SearchInUser(job:  message  ,ismsg: true,);
                          //               },));
                          //             }
                          //             else{
                          //               Navigator.push(context, MaterialPageRoute(builder: (context) {
                          //                 return SearchInUser(job: jobitem ,ismsg : false);
                          //               },));
                          //             }
                          //
                          //           },
                          //
                          //           child: Icon(Icons.search_sharp,color: Colors.white,))
                          //
                          //     ],
                          //   ),
                          // ),






                          Expanded(

                              child:

                              posteditview(message:msg  )
                          ),


                          SizedBox(height: 10,),

                        ],
                      ),
                    );

                  case Status.LOADING:
                    return Center(
                      child: CircularProgressIndicator(
                        color: AppColor.bgcolor,
                        backgroundColor: AppColor.primarycolor,

                      ),
                    );
                  default:
                    return Center(
                      child: CircularProgressIndicator(

                      ),
                    );

                }

              },

            );
          },

        ),
      ),
    );
  }

  void setupfetch() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String? id = prefs.getString('userid');
    print("user id ${id}");
    if(msg == true){
      await postviewmodel.getMsgById(id);

    }
   await postviewmodel.getJobPostsByuid(id);

  }
}

