import 'package:bulleted_list/bulleted_list.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart';
import 'package:jobapp/VIews/CRUD/editmessage.dart';
import 'package:jobapp/ViewModel/job_view_model.dart';
import 'package:jobapp/model/message_model.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:timeago/timeago.dart' as timeago;


import '../../../../res/Appcolor.dart';
import '../../../../res/Appfont.dart';
import '../../../data/Response/status_response.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../model/CRUD/UpdateAvRequest.dart';
import '../../model/message_model.dart';
import '../Jobs_details/Msg/msg_detail.dart';
import '../Jobs_details/job_detail.dart';
import '../Jobs_details/postjob_screen.dart';
import '../WelcomeScreen/defaultscreen.dart';
import 'LongPosts.dart';
import 'editpostview.dart';

class posteditview extends StatefulWidget {
  var message;
posteditview({
    super.key,
  this.message
  });

  @override
  State<posteditview> createState() => _posteditviewState();
}

class _posteditviewState extends State<posteditview> {
  JobViewModel postviewmodel = JobViewModel();
  var jobid;
  var tokenid;
  var   isMsg;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isMsg = widget.message;
    print("msg: ${isMsg}");
    setupfetch();
  }
  Widget build(BuildContext context) {
    return  isMsg == true?
    ChangeNotifierProvider<JobViewModel>(
      create: (context) =>  postviewmodel ,
      builder: (context, child) {
        return Consumer<JobViewModel>(
          builder: (context, Message, child) {
            var status = Message.MessageResponse.status;
            var length = Message.MessageResponse.data?.data?.length;

            print("post length ${length}");



        if(postviewmodel.DeleteResponseMsg.status != Status.LOADING){
          WidgetsBinding.instance.addPostFrameCallback((timeStamp) {

            ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(content: Text('Your message has been deleted!!!'))
            );
            Navigator.push(context, MaterialPageRoute(builder: (context) => ScreenDefault(),));
          });
        }
            switch(status){
              case Status.COMPLETED:

                return  length == null ?
                Container(
                  margin: EdgeInsets.symmetric(vertical: 100),
                  child: Center(child: Column(
                    children: [

                      Image.network('https://cdn3d.iconscout.com/3d/premium/thumb/mail-download-6985959-5691446.png',


                          fit: BoxFit.cover,
                          width: 200,
                          height: 200

                      ),
                      Text('0 post announcement' ,style: TextStyle(
                          color: Colors.white,
                          fontSize: AppFont.bigfont,
                          fontWeight: FontWeight.w500

                      ),),

                      SizedBox(height: 5,),

                      Text('You have no mail in your job announcement' ,style: TextStyle(
                          color: Colors.white,
                          fontSize: AppFont.smfont+1,
                          fontWeight: FontWeight.w400

                      ),),
                    ],
                  ),),
                ) :

                    ListView.builder(
                  itemCount: length ?? 0,

                  itemBuilder: (context, index) {
                    var msg = Message.MessageResponse.data?.data?[index];
                    return



                    GestureDetector(

                      child: Card(
                        color: Colors.indigo,
                          elevation: 1,
                          margin: EdgeInsets.symmetric(vertical: 0.5),
                         
                          shape:RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(0)
                          ) ,
                          child:   GestureDetector(
                            onTap: () {

                            },
                            child: Padding(
                              padding: const EdgeInsets.all(0),
                              child: ListTile(
                                leading:IconButton.outlined(onPressed: () {
                                  showDialog(context: context, builder: (context) {
                                    return AlertDialog(
                                      elevation: 0,





                                      backgroundColor: Colors.indigo,

                                      scrollable: true,

                                      content: Column(


                                        children: [
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text('Choose',style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: AppFont.bigfont+5,
                                                  fontWeight: FontWeight.w500

                                              ),),

                                              IconButton(onPressed: ()=>Navigator.pop(context),

                                                  icon: Icon(Icons.close,  color: Colors.white,)
                                              )

                                            ],
                                          ),
                                          Divider(),



                                          Container(

                                            child: Row(
                                              children: [


                                                Expanded(

                                                  child: TextButton(onPressed: () async{
                                                    Navigator.push(context,MaterialPageRoute(builder: (context) =>
                                                          EditMessageScreen(message:msg,)
                                                      ));


                                                    print("jobid");
                                                  },
                                                      style: ButtonStyle(


                                                      ),

                                                      child: Row(
                                                        children: [
                                                          Icon(Icons.edit,size: 20,       color: AppColor.thirdcolor,),
                                                          SizedBox(width: 10,),
                                                          Text('Edit Your Message',style: TextStyle(
                                                              color: AppColor.thirdcolor,
                                                              fontSize: AppFont.bigfont

                                                          ),),
                                                        ],
                                                      )),
                                                ),


                                              ],
                                            ),
                                          ),









                                          Container(

                                            child: Row(
                                              children: [


                                                Expanded(

                                                  child: TextButton(onPressed: ()  async{
                                                    print("Press delete");
                                                    // setState(() {
                                                    //
                                                    //
                                                    // });
                                                    print(msg?.sId);
                                                    Navigator.pop(context);
                                                    showDialog(context: context, builder: (context) {
                                                      return AlertDialog(
                                                        elevation: 0,





                                                        backgroundColor: Colors.indigo,

                                                        // scrollable: true,




                                                        title: Text('Warning',style: TextStyle(
                                                            fontSize: AppFont.bigfont+5,
                                                            color: AppColor.fourthcolor,
                                                            fontWeight: FontWeight.w500
                                                        ),),
                                                        content: Text('Are you sure to delete this message?',style: TextStyle(
                                                            fontSize: AppFont.bigfont,
                                                            color: AppColor.thirdcolor
                                                        ),),
                                                        actions: [
                                                          GestureDetector(


                                                            child: Text('CANCEL',style: TextStyle(
                                                              fontSize: AppFont.bigfont,
                                                              color: AppColor.thirdcolor,

                                                            ),



                                                            ),
                                                            onTap: () {
                                                              Navigator.pop(context);
                                                            },
                                                          ),
                                                          GestureDetector(

                                                            child: Text('DELETE',style: TextStyle(
                                                                fontSize: AppFont.bigfont,
                                                                color: AppColor.fourthcolor
                                                            ),



                                                            ),
                                                            onTap: () async{
                                                              await postviewmodel.deletemsgbyid(msg?.sId);

                                                            },
                                                          )
                                                        ],


                                                      );
                                                    },
                                                    );



                                                  },
                                                      style: ButtonStyle(

                                                      ),

                                                      child: Row(
                                                        children: [
                                                          Icon(Icons.delete,size: 20,               color: Colors.redAccent,),
                                                          SizedBox(width: 10,),
                                                          Text('Delete Your Message',style: TextStyle(
                                                              color: Colors.redAccent,
                                                              fontWeight: FontWeight.w500,
                                                              fontSize: AppFont.bigfont

                                                          ),),
                                                        ],
                                                      )),
                                                ),


                                              ],
                                            ),
                                          ),






                                        ],
                                      )  ,

                                      // title: Text('Edit or Modify',style: TextStyle(
                                      //   fontSize: AppFont.bigfont
                                      // ),),


                                    );
                                  },

                                  );
                                }, icon: Icon(Icons.more_vert,color: Colors.white,)),

                              trailing:       Text('${timeago.format(DateTime.parse('${msg?.publishDate}'))}',style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: AppFont.medfont-2,
                                  color:Colors.white
                            ),),



                                title: Row(

                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,

                                  children: [

                                    Text('${ msg?.description ?? 'No Title'}',
                                      softWrap: false,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,

                                      style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        overflow:  TextOverflow.fade,

                                        color:Colors.white,


                                      ),),



                                  ],
                                ),
                                enabled: true,

                                contentPadding:  EdgeInsets.symmetric(horizontal: 10,vertical: 5),
                                isThreeLine: true,

                                subtitle: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [

                                        Text('Message To Post: '
                                          ,
                                          maxLines: 2,
                                          overflow: TextOverflow.ellipsis,


                                          style: TextStyle(
                                              fontSize: AppFont.smfont-1,
                                              color:Colors.grey,
                                              wordSpacing: 1,


                                              height: 1.3


                                          ),),
                                        SizedBox(height: 5,),
                                        Text('${msg?.job?.title.toString()}'
                                          ,
                                          softWrap: false,
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,



                                          style: TextStyle(
                                              fontSize: AppFont.smfont-1,
                                              wordSpacing: 1,
                                              height: 1.3,
                                              fontWeight: FontWeight.w400,
                                            color:Colors.grey,


                                          ),),
                                      ],
                                    ),
                                    Divider(


                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        Navigator.push(context, MaterialPageRoute(builder: (context) =>MsgDetailScr(
                                          mid: msg?.sId,
                                        ),));
                                      },
                                      child: Text('View Message'
                                        ,
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis,


                                        style: TextStyle(
                                            fontSize: AppFont.smfont+1,
                                            wordSpacing: 1,
                                            fontWeight: FontWeight.w500,
                                            height: 1.3,
                                          color: AppColor.whitecolor


                                        ),),
                                    ),
                                  ],
                                ),





                              ),
                            ),
                          )
                      ),
                    );
                  },

                ) ;
              case Status.LOADING:
                return Center(
                  child: CircularProgressIndicator(

                  ),
                );
              default:
                return Center(
                  child: CircularProgressIndicator(

                  ),
                );
            }


          },

        );
      },

    ):


    ChangeNotifierProvider<JobViewModel>(
      create: (context) =>  postviewmodel ,
      builder: (context, child) {
        return Consumer<JobViewModel>(
          builder: (context, jobs, child) {
            var status = jobs.PostByUIdResponse.status;
            var items = jobs.PostByUIdResponse.data?.data;


            var joblist = items?.job;

            var length = joblist?.length ?? 0;
            print("post length ${length}");

            if(jobs.PostDeleteResponse.status != Status.LOADING){
              WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(content: Text('Your job announcement has been deleted!!!'))
                );
                Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(
                    builder: (context) =>  UserPostScreen()), (Route route) => route.isFirst);




              });
            }
            if(jobs. UserUpdateResponse.status != Status.LOADING){
              WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(content: Text('Your job announcement has been updated!!!'))
                );
                Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(
                    builder: (context) =>   UserPostScreen(message:false)), (Route route)

                => route.isFirst);




              });
            }
            return
              // length == 0 ?
              // Container(
              //   margin: EdgeInsets.symmetric(vertical: 100),
              //   child: Center(child: Column(
              //     children: [
              //
              //       Image.network('https://cdn3d.iconscout.com/3d/premium/thumb/mail-download-6985959-5691446.png',
              //
              //
              //           fit: BoxFit.cover,
              //           width: 200,
              //           height: 200
              //
              //       ),
              //       Text('0 post Announcement' ,style: TextStyle(
              //           color: Colors.white,
              //           fontSize: AppFont.bigfont,
              //           fontWeight: FontWeight.bold
              //
              //       ),),
              //       SizedBox(height: 5,),
              //
              //       Text('You have no new post yet, lets started posting' ,style: TextStyle(
              //           color: Colors.white,
              //           fontSize: AppFont.smfont+1,
              //           fontWeight: FontWeight.w400
              //
              //       ),),
              //     ],
              //   ),),
              // ) :
            ListView.builder(
              itemCount: length ,

              // physics: NeverScrollableScrollPhysics(),
              // physics: NeverScrollableScrollPhysics(),
              itemBuilder: (context, index) {
                return GestureDetector(

                  child:  length == 0 ?
                      Center(child: Text('You have no job announcement yet'),)
                  :

                    Card(

                      color: Colors.indigo,



                    margin: EdgeInsets.symmetric(vertical: 0.5),
                      //TODO spacing here


                      shadowColor: Colors.white,
                      borderOnForeground: true,
                      surfaceTintColor: Colors.white,

                      elevation: 0,
                      shape:RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(1),
                        side: BorderSide(
                          color: Colors.white,

                          style: BorderStyle.none

                        )
                      ) ,
                      child:   GestureDetector(
                        onTap: () {

                        },
                        child: Padding(
                          padding: const EdgeInsets.all(0),
                          child: ListTile(
                            leading:IconButton.outlined(onPressed: () {
                              showDialog(context: context, builder: (context) {
                                return AlertDialog(
                                  elevation: 0,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10)
                                  ),




                                  backgroundColor: Colors.white,

                                  scrollable: true,

                                  content: Column(


                                    children: [

                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text('Choose',style: TextStyle(
                                              color: AppColor.primarycolor,
                                              fontSize: AppFont.bigfont+5,
                                              fontWeight: FontWeight.w500

                                          ),),

                                          IconButton(onPressed: ()=>Navigator.pop(context),

                                              icon: Icon(Icons.close,color: AppColor.primarycolor,)
                                          )

                                        ],
                                      ),

                                      Divider(color: AppColor.primarycolor),


                                      Container(

                                        child: Row(
                                          children: [


                                            Expanded(

                                              child: TextButton(onPressed: () async{
                                                Navigator.push(context,MaterialPageRoute(builder: (context) =>

                                                CreateScreen(isUpdate: true,jobspost:joblist?[index])
                                                  ,));


                                                print("jobid ${jobid}");
                                              },
                                                  style: ButtonStyle(


                                                  ),

                                                  child: Row(
                                                    children: [
                                                      Icon(Icons.edit,size: 20,color: AppColor.primarycolor,),
                                                      SizedBox(width: 10,),
                                                      Text('Edit Your Post',style: TextStyle(
                                                          color: AppColor.primarycolor,
                                                          fontSize: AppFont.bigfont

                                                      ),),
                                                    ],
                                                  )),
                                            ),


                                          ],
                                        ),
                                      ),





                                      Container(

                                        child: Row(
                                          children: [


                                            Expanded(

                                              child: TextButton(onPressed: ()=>null,
                                                  style: ButtonStyle(


                                                  ),

                                                  child: Row(

                                                    children: [

                                                      Icon(Icons.edit_off_sharp,size: 20,

                                                        color: joblist![index].status == true ?

                                                        AppColor.fourthcolor      :
                                                          AppColor.bgcolor


                                                      ),

                                                      SizedBox(width: 10,),

                                                      GestureDetector(
                                                        onTap:() {
                                                          _updateuserpost(joblist?[index].sId,joblist?[index].status);
                                                        },

                                                        child: joblist![index].status == true ?

                                                        Text('Make it Unavailable',
                                                          style: TextStyle(

                                                            color: AppColor.fourthcolor,
                                                            fontWeight: FontWeight.w500,

                                                            fontSize: AppFont.bigfont

                                                        ),

                                                        ) :
                                                        Text('Make it Available',
                                                          style: TextStyle(

                                                              color: AppColor.bgcolor,

                                                              fontSize: AppFont.bigfont

                                                          ),

                                                        ),
                                                      ),

                                                    ],
                                                  )),
                                            ),


                                          ],
                                        ),
                                      ),



                                      Container(

                                        child: Row(
                                          children: [


                                            Expanded(

                                              child: TextButton(onPressed: ()  async{
                                                print("Press delete");
                                                // setState(() {
                                                //
                                                //
                                                // });
                                                var jid = joblist?[index].sId;
                                                print(joblist?[index].sId);
                                                Navigator.pop(context);
                                                showDialog(context: context, builder: (context) {
                                                  return AlertDialog(
                                                    elevation: 0,





                                                    backgroundColor: Colors.indigo,

                                                    // scrollable: true,




                                                    title: Text('Warning',style: TextStyle(
                                                        fontSize: AppFont.bigfont+5,
                                                        color: AppColor.fourthcolor,
                                                        fontWeight: FontWeight.w500
                                                    ),),
                                                    content: Text('Are you sure to delete this post?',style: TextStyle(
                                                        fontSize: AppFont.bigfont,
                                                        color: AppColor.thirdcolor
                                                    ),),
                                                    actions: [
                                                      Padding(
                                                        padding: const EdgeInsets.all(8.0),
                                                        child: GestureDetector(


                                                          child: Text('CANCEL',style: TextStyle(
                                                            fontSize: AppFont.bigfont,
                                                            color: AppColor.thirdcolor,

                                                          ),



                                                          ),
                                                          onTap: () {
                                                            Navigator.pop(context);
                                                          },
                                                        ),
                                                      ),
                                                      Padding(
                                                        padding: const EdgeInsets.all(8.0),
                                                        child: GestureDetector(

                                                          child: Text('DELETE',style: TextStyle(
                                                              fontSize: AppFont.bigfont,
                                                              color: AppColor.fourthcolor
                                                          ),



                                                          ),
                                                          onTap: () async{
                                                            await postviewmodel.deleteByJobId(jid, tokenid);
                                                            print('job id ${jid} ${tokenid}');

                                                          },
                                                        ),
                                                      )
                                                    ],


                                                  );
                                                },
                                                );

                                                print(jobid);

                                                print("jobid ${jobid}");

                                              },
                                                  style: ButtonStyle(

                                                  ),

                                                  child: Row(
                                                    children: [
                                                      Icon(Icons.delete,size: 20,          color: AppColor.fourthcolor,),
                                                      SizedBox(width: 10,),
                                                      Text('Delete Your Post',style: TextStyle(
                                                          color: AppColor.fourthcolor,
                                                          fontSize: AppFont.bigfont

                                                      ),),
                                                    ],
                                                  )),
                                            ),


                                          ],
                                        ),
                                      ),






                                    ],
                                  )  ,

                                  // title: Text('Edit or Modify',style: TextStyle(
                                  //   fontSize: AppFont.bigfont
                                  // ),),


                                );
                              },);
                            }, icon: Icon(Icons.more_vert,color: Colors.white,)),
                            trailing:       Text('${timeago.format(DateTime.parse('${joblist?[index].updatedAt}'))}',style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: AppFont.medfont-2,
                              color: Colors.white
                            ),),



                            title: Row(

                              mainAxisAlignment: MainAxisAlignment.spaceBetween,

                              children: [

                                Expanded(
                                  child: Text('${ joblist?[index].title.toString()?? 'No Title'}...',
                                    softWrap: false,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,

                                    style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      overflow:  TextOverflow.fade,

                                      color:Colors.white,
                                    
                                  ),),
                                ),



                              ],
                            ),
                            contentPadding:  EdgeInsets.symmetric(horizontal: 10,vertical: 5),
                            isThreeLine: true,

                            subtitle: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,

                              children: [
                                SizedBox(height: 10,),

                                Text('${ joblist?[index].jobDescription?.detail.toString() ?? 'No Description'}'
                                  ,
                                  softWrap: false,
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,


                                  style: TextStyle(
                                    fontSize: AppFont.smfont+1,
                                    wordSpacing: 1,
                                    height: 1.3,
                                    color: Colors.grey


                                ),),
                                Divider(),
                                GestureDetector(
                                  onTap: () {
                               Navigator.push(context,MaterialPageRoute(builder: (context) =>
                                        JobDetailSr(uid:joblist?[index].sId)));

                                    //TODO SCREEN JOB DETAIL
                                  },
                                  child: Row(
                                    children: [
                                      Text('View Post'
                                        ,
                                        softWrap: false,
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,


                                        style: TextStyle(
                                            fontSize: AppFont.smfont+1,
                                            wordSpacing: 1,
                                            height: 1.3,
                                            color: AppColor.whitecolor


                                        ),),
                                      SizedBox(width: 10,),
                                      Icon(
                                          CupertinoIcons.arrow_right_square_fill
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),




                          ),
                        ),
                      )
                  )
                  ,
                );
              },

            );
          },

        );
      },

    );
  }

  void setupfetch()  async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String? id = prefs.getString('userid');
    tokenid = prefs.get('tokens');

    print("user id ${id}");
    if(    isMsg == true){
      await postviewmodel.getMsgById(id);
    }
    else{
      await postviewmodel.getJobPostsByuid(id);
    }

  }

void _updateuserpost(postid,status) async {


    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? uid = prefs.getString('userid');
    print(uid);
    var token = prefs.get('tokens');
    if(status == true){
      status = false;
    }
    else{
      status = true;
    }
    print(status);


    print(token);
    print(postid  );

    await postviewmodel.updateUserClient(uid,postid,status, token);
  }
}
