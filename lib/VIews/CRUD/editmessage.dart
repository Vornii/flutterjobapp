import 'dart:io';

import 'package:bulleted_list/bulleted_list.dart';
import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:jobapp/model/Put/update_user_model.dart';
import 'package:jobapp/VIews/Jobs_details/pfscreen.dart';
import 'package:jobapp/VIews/WelcomeScreen/browsescreen.dart';
import 'package:jobapp/ViewModel/post_view_model.dart';
import 'package:jobapp/model/Experience_model.dart';
import 'package:jobapp/model/job_model_mock.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';

import '../../ViewModel/image_view_model.dart';
import '../../ViewModel/job_view_model.dart';
import '../../data/Response/status_response.dart';
import '../../res/Appcolor.dart';
import '../../res/Appfont.dart';
import '../WelcomeScreen/Filterscreen.dart';
import '../WelcomeScreen/defaultscreen.dart';
import 'editpostview.dart';

class EditMessageScreen extends StatefulWidget {
  var open;

  var userid;
  var users;
  var message;

  EditMessageScreen(
      {Key? key, this.open, this.users, this.userid, this.message})
      : super(key: key);

  @override
  State<EditMessageScreen> createState() => _EditMessageScreenState();
}

class _EditMessageScreenState extends State<EditMessageScreen> {
  var isDownloading = false;
  var isopenfile = false;

  var msg;
  var uid;
  var isloading = false;

  PostViewModel editviewmodel = PostViewModel();

  var filepath;
  var jobsid;

  var txtadditional = TextEditingController();
  var pdflink;
  var messageid;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(widget.userid);

    msg = widget.message;
    txtadditional.text = msg.description;
    uid = msg.user?.sId;
    filepath = msg.pdfLink;
    pdflink = filepath.split('\document');
    print(msg.pdfLink);
    jobsid = msg.job?.sId;
    messageid = msg.sId;
    print(msg.pdfLink);
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColor.bgcolor,
        elevation: 0,
        // automaticallyImplyLeading:open == null ? true : false , //set to false = no go back bar
        iconTheme: IconThemeData(color: Colors.white
            // color: open == null ? AppColor.primarycolor : Colors.white
            ),
        title: Text(
          'Edit Message',
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
      ),
      backgroundColor: AppColor.thirdcolor,
      body: SafeArea(
        child: ChangeNotifierProvider<PostViewModel>(
          create: (context) => editviewmodel,
          builder: (context, child) {
            return SingleChildScrollView(
              child: Consumer<PostViewModel>(
                builder: (context, message, _) {
                  var status = editviewmodel.UpdateMessageResponse.status;
                  if (editviewmodel.UpdateMessageResponse.status !=
                      Status.LOADING) {
                    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                      ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(content: Text('Your Message has updated')));

                      Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(
                              builder: (context) =>
                                  UserPostScreen(message: true)),
                          (Route route) => route.isFirst);
                    });
                  } else if (editviewmodel.UpdateMessageResponse.status ==
                      Status.COMPLETED) {
                    setState(() {
                      isloading = false;
                    });
                  }
                  print(status);

                  return Container(
                    margin: EdgeInsets.symmetric(vertical: 20),
                    padding: EdgeInsets.symmetric(
                      horizontal: 23,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          height: 100,
                          child: TextField(
                            style: TextStyle(
                              fontSize: AppFont.smfont + 1,
                              color: AppColor.primarycolor,
                              height: 2,
                            ),
                            controller: txtadditional,
                            onSubmitted: (value) {
                              setState(() {
                                txtadditional.text = value;
                              });
                            },
                            enabled: true,
                            maxLength: 55,
                            decoration: InputDecoration(
                              hintText: 'Additional Message',

                              labelText: 'Additional Message',

                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: AppColor.primarycolor, width: 2)),

                              // helperText:'965689896' ,
                              labelStyle: TextStyle(
                                  color: AppColor.primarycolor,
                                  fontWeight: FontWeight.bold,
                                  fontSize: AppFont.bigfont),

                              // suffix: Icon(
                              //   Icons.delete_forever,
                              //   size: 22,
                              //   color: AppColor.primarycolor,
                              // )
                            ),
                          ),
                        ),
                        Card(
                          color: Colors.white,
                          elevation: 0,
                          child: ListTile(
                            leading: IconButton.outlined(
                                onPressed: () {
                                  showDialog(
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        elevation: 0,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(10)),

                                        backgroundColor: Colors.white,

                                        scrollable: true,

                                        content: Column(
                                          children: [
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Text(
                                                  'Modify',
                                                  style: TextStyle(
                                                      color:
                                                          AppColor.primarycolor,
                                                      fontSize:
                                                          AppFont.bigfont + 5,
                                                      fontWeight:
                                                          FontWeight.w600),
                                                ),
                                                IconButton(
                                                    onPressed: () =>
                                                        Navigator.pop(context),
                                                    icon: Icon(
                                                      Icons.close,
                                                      color:
                                                          AppColor.primarycolor,
                                                    ))
                                              ],
                                            ),
                                            Divider(
                                                color: AppColor.primarycolor),
                                            Container(
                                              child: Row(
                                                children: [
                                                  Expanded(
                                                    child: TextButton(
                                                        onPressed: () async {
                                                          print("working");

                                                          FilePickerResult?
                                                              result =
                                                              await FilePicker
                                                                  .platform
                                                                  .pickFiles();

                                                          if (result == null) {
                                                            //if they dontsubmitfile

                                                            return null;
                                                          } else {
                                                            final file = result
                                                                .files.first;

                                                            print(file.path);
                                                            print(
                                                                file.extension);

                                                            print(file.name);
                                                            setState(() {
                                                              filepath =
                                                                  file.path;
                                                              pdflink = file
                                                                  .path
                                                                  .toString()
                                                                  .split(
                                                                      '/file_picker');
                                                            });
                                                            Navigator.pop(
                                                                context);
                                                          }
                                                          setState(() {});
                                                        },
                                                        style: ButtonStyle(),
                                                        child: Row(
                                                          children: [
                                                            Icon(Icons.download,
                                                                size: 20,
                                                                color: AppColor
                                                                    .primarycolor),
                                                            SizedBox(
                                                              width: 10,
                                                            ),
                                                            Text(
                                                              'Upload new files',
                                                              style: TextStyle(
                                                                  color: AppColor
                                                                      .primarycolor,
                                                                  fontSize: AppFont
                                                                      .bigfont),
                                                            ),
                                                          ],
                                                        )),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),

                                        // title: Text('Edit or Modify',style: TextStyle(
                                        //   fontSize: AppFont.bigfont
                                        // ),),
                                      );
                                    },
                                  );
                                },
                                icon: Icon(Icons.more_vert)),
                            title: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "${pdflink?[1].substring(1)}",
                                  style: TextStyle(
                                    fontSize: AppFont.medfont,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),

                                // Text("${pdflink![1].substring(1).split('.')?[1]}",
                                //   style: TextStyle(
                                //       fontSize: AppFont.medfont,
                                //
                                //   ),
                                //
                                // ),
                              ],
                            ),
                            // trailing:              IconButton(
                            //     onPressed: () async{
                            //       setState(() {
                            //         isDownloading = true;
                            //
                            //       });
                            //
                            //
                            //
                            //       await openfile(
                            //           url:'http://10.0.2.2:3000/',
                            //           fileName:'newfile.pdf'
                            //       );
                            //       setState(() {
                            //         isDownloading = false;
                            //         isopenfile = true;
                            //       });
                            //     },
                            //     tooltip: 'click here to download',
                            //     icon: isopenfile ==true ? Icon(Icons.file_download_done_sharp,color: AppColor.primarycolor,) :
                            //     Icon(Icons.download,color: AppColor.primarycolor,)
                            // )
                          ),
                        ),
                        SizedBox(
                          height: 25,
                        ),
                        ElevatedButton(
                          onPressed: () async {
                            print("btn press!!!");
                            print(filepath);
                            print(txtadditional.text);

                            print(filepath);
                            if (txtadditional.text == '' || filepath == null) {
                              return showDialog(
                                context: context,
                                builder: (context) {
                                  return AlertDialog(
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(14)),
                                    backgroundColor: AppColor.primarycolor,
                                    elevation: 0,
                                    title: Column(
                                      children: [
                                        Icon(
                                          Icons.warning,
                                          size: 60,
                                          color: AppColor.fourthcolor,
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          'Missing',
                                          style: TextStyle(
                                              color: AppColor.fourthcolor,
                                              fontSize: 18,
                                              fontWeight: FontWeight.w500),
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          'One or two more missing informations in your dialog , please fill everything',
                                          style: TextStyle(
                                            color: AppColor.fourthcolor,
                                            fontSize: 12,
                                            fontWeight: FontWeight.w400,
                                          ),
                                          textAlign: TextAlign.center,
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Row(
                                          children: [
                                            Expanded(
                                              child: ElevatedButton(
                                                onPressed: () =>
                                                    Navigator.pop(context),
                                                child: Text('Try again'),
                                                style: ButtonStyle(
                                                    backgroundColor:
                                                        MaterialStatePropertyAll(
                                                            AppColor
                                                                .fourthcolor)),
                                              ),
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                  );
                                },
                              );
                            }
                            if (filepath == msg.pdfLink) {
                              print("true");
                              print(filepath);
                              print(msg.pdfLink);
                              setState(() {
                                isloading = true;
                              });

                              //
                              await editviewmodel.updateMessageNopdf(
                                  messageid, txtadditional.text, uid, jobsid);
                            } else {
                              await editviewmodel.updateMessageAPI(messageid,
                                  txtadditional.text, uid, jobsid, filepath);
                            }

                            //TODO POST MSG HERE COME
                            // mid,desc,userid,jobid,filepath
                          },
                          child: isloading == false
                              ? Text(
                                  'Save Changes',
                                  style: TextStyle(color: Colors.white),
                                )
                              : Container(
                                  width: 76,
                                  child: Center(
                                    child: Container(
                                      width: 20,
                                      height: 20,
                                      child: CircularProgressIndicator(
                                        color: AppColor.bgcolor,
                                        backgroundColor: AppColor.primarycolor,
                                      ),
                                    ),
                                  ),
                                ),
                          style: ButtonStyle(
                              backgroundColor:
                                  MaterialStatePropertyAll(AppColor.bgcolor)),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                      ],
                    ),
                  );
                },
              ),
            );
          },
        ),
      ),
    );
  }

  Future openfile({required String url, required String fileName}) async {
    final file =
        await downloadFile(url, fileName); //TODO FUNCTION TO DOWNLOAD FILES
    print(file);
    if (file == null) {
      //TODOO if null we end
      return null;
      print("null");
    } else {
      print("open file noww");
      await OpenFile.open(file.path); //TODO aFTER DOWWNLOAD WE OPEN THE FILES
      // final result =await FilePicker.platform.pickFiles();
    }
  }

  Future downloadFile(String url, String fileName) async {
    print("download file");
    final appstorage = await getApplicationDocumentsDirectory();
    print(appstorage);
    print(url);

    final file = File('${appstorage.path}/${fileName}');
    try {
      final response = await Dio().get(
        url,
        options: Options(
            responseType: ResponseType.bytes,
            followRedirects: false,
            sendTimeout: null),
      );

      final raf = file.openSync(mode: FileMode.write); //TodO open up file
      raf.writeFromSync(response.data); //TODO data download from url
      await raf.close();
      return file;
    } catch (e) {
      print(e);
      return e;
    }
  }

  showdialoglanguages(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) {
        return customdialog();
      },
    );
  }
}

void getuserdata() {}

class customdialog extends StatefulWidget {
  var lists;
  var check;
  var selectedlanguages;

  customdialog({Key? key, this.lists, this.check, this.selectedlanguages})
      : super(key: key);

  @override
  State<customdialog> createState() => _customdialogState();
}

class _customdialogState extends State<customdialog> {
  var listofcountries;

  var ischeck;
  Set<String?> selectedlanguages = {};

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    listofcountries = widget.lists;
    ischeck = widget.check;
    selectedlanguages = widget.selectedlanguages;
  }

  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('Langauges'),
      content: Column(
        children: List.generate(listofcountries.length, (index) {
          return CheckboxListTile(
            value: ischeck[index],
            controlAffinity: ListTileControlAffinity.leading,
            onChanged: (bool? value) {
              //value = true;
              setState(() {
                print(listofcountries[index].toString());

                ischeck[index] = value;

                if (ischeck[index] == false) {
                  selectedlanguages.remove(listofcountries[index].toString());
                } else {
                  selectedlanguages.add(listofcountries[index].toString());
                }

                print(listofcountries[index]);
              });
            },
            title: Text(
              '${listofcountries[index]}',
              style: TextStyle(fontSize: AppFont.medfont),
            ),
            activeColor: AppColor.primarycolor,
            checkColor: Colors.white,
          );
        }),
      ),
      actions: [
        ElevatedButton(
          onPressed: () {
            print(selectedlanguages);

            Navigator.pop(context, selectedlanguages);
          },
          child: Text('Save'),
          style: ButtonStyle(
              backgroundColor: MaterialStatePropertyAll(AppColor.primarycolor)),
        ),
        ElevatedButton(
          onPressed: () {
            // selectedlanguages.clear();
            Navigator.pop(context);
            print(selectedlanguages);
          },
          child: Text('Cancel'),
          style: ButtonStyle(
              backgroundColor: MaterialStatePropertyAll(AppColor.fourthcolor)),
        ),
      ],
    );
  }
}
