import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jobapp/VIews/AuthScreen/Login.dart';
import 'package:jobapp/VIews/AuthScreen/register.dart';
import 'package:jobapp/VIews/Choose/Load1.dart';
import 'package:jobapp/VIews/Choose/Load2.dart';
import 'package:jobapp/VIews/Jobs_details/filterform.dart';
import 'package:jobapp/VIews/Jobs_details/job_detail.dart';
import 'package:jobapp/VIews/Jobs_details/pfscreen.dart';
import 'package:jobapp/VIews/WelcomeScreen/browsescreen.dart';
import 'package:flutter_config/flutter_config.dart';
import 'package:jobapp/VIews/WelcomeScreen/defaultscreen.dart';
import 'package:jobapp/VIews/SplashScreen/splashscreen.dart';
import 'package:jobapp/VIews/testscreen.dart';
import 'package:jobapp/ViewModel/job_view_model.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'SplashScreen/loadingone.dart';
import 'Jobs_details/Msg/messages.dart';
import 'Jobs_details/postjob_screen.dart';
import 'Jobs_details/Msg/msg_detail.dart';
import 'Jobs_details/Msg/msgedit.dart';
import 'WelcomeScreen/home.dart';
import 'WelcomeScreen/screendiscover.dart';
import 'WelcomeScreen/screenfindjob.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await FlutterConfig.loadEnvVariables();
  runApp( MyApp());
}

class MyApp extends StatefulWidget {
 MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // This widget is the root of your application.
  var isauth;
  var first;
  @override
  void initState() {
    // TODO: implement initState

      checkauth();

      firstloader();




    print(isauth);
    print("hello orld");


  }
  Widget build(BuildContext context) {
    return Platform.isAndroid ? MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(

        primarySwatch: Colors.blue,
        fontFamily: 'Roboto',

      ),
      // home: ScreenLoadingOne(),
    home:
        //TODO first falsemeaning first time open app
    first == false ?
    ScreenLoadingOne() :

    isauth == false ?

    SplashScreen (isauth:true)  :     SplashScreen (isauth:false)







      // initialRoute: isauth ==true ? '/findjob' : '/home',
      // routes: {
      //   // When navigating to the "/" route, build the FirstScreen widget.
      //   '/': (context) => const Myloading_one(),
      //   '/load': (context) => Myloading_two(),
      //   '/login' : (context) =>Mylogin(),
      //   '/home':(context) => MyHomePage(),
      //   '/register':(context) => Myregister(),
      //   // When navigating to the "/second" route, build the SecondScreen widget.
      //   '/second': (context) => const MyHomePage(),
      //   '/findjob' :(context)=> ScreenFindJob(),
      //   '/disoverjob' :(context)=> DiscoverScreen(),
      //   '/browse' :(context)=>BrowseScreen(),
      //   '/jd' :(context)=>JobDetailSr(),
      //   '/bio' :(context)=>PfScreen(),
      //   '/test':(context)=>Tester(),
      //   '/create':(context)=>CreateScreen(),
      //   '/filter':(context)=>FilterScreen(),
      //   '/inbox':(context)=>InboxScr(),
      //   '/checkinbox':(context)=>MsgDetailScr(),
      //   '/editmsg':(context)=> Msgmodify()
      //
      //
      // },
    ) : CupertinoApp() ;
  }
  firstloader()  async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
  first =prefs.getBool('open') ?? false;
  if(first == false){
    prefs.setBool('open', true);
  }




  }
checkauth()  async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    isauth =prefs.getString('tokens')?? false;
    setState(() {
      isauth =prefs.getString('tokens')?? false;
    });

    print(isauth);

  }
}

