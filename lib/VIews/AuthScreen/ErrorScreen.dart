import 'package:flutter/material.dart';
import 'package:jobapp/res/Appcolor.dart';
import 'package:lottie/lottie.dart';

import '../main.dart';

class NointernetScreen extends StatefulWidget {
  const NointernetScreen({Key? key}) : super(key: key);

  @override
  State<NointernetScreen> createState() => _NointernetScreenState();
}

class _NointernetScreenState extends State<NointernetScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: null,
      backgroundColor: AppColor.primarycolor,
      body: Center(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Lottie.asset(
                fit: BoxFit.cover,
                height: 300,
                width: 300,
                'assets/images/Icon/28248-404-error.json',


              ),
              SizedBox(height: 25,),
              GestureDetector(
                onTap: () {
                 Navigator.push(context, MaterialPageRoute(builder: (context) {
                   return MyApp();
                 },));
                },
                child: Text('Tap here to reconnect',
                  style: TextStyle(
                      color: Colors.grey
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
