import 'package:flutter/material.dart';

import 'package:jobapp/VIews/AuthScreen/Login.dart';
import 'package:jobapp/VIews/Choose/Load1.dart';
import 'package:provider/provider.dart';

import '../../ViewModel/job_view_model.dart';
import '../../data/Response/status_response.dart';
import '../../model/signuprequest_model.dart';
import '../../res/Appcolor.dart';
import '../../res/Appfont.dart';
import '../../res/Validator/register_validator.dart';

class Myregister extends StatefulWidget {
  const Myregister({Key? key}) : super(key: key);

  @override
  State<Myregister> createState() => _MyregisterState();
}

class _MyregisterState extends State<Myregister>
    with Signupvalidator, ChangeNotifier {
  @override
  bool value = false;
  var keyname = GlobalKey<FormState>();
  var keypass = GlobalKey<FormState>();
  var keyemail = GlobalKey<FormState>();
  var name = TextEditingController();
  var isNameValid = true;
  var isEmailValid = true;
  var isPassValid = true;
  var passeye;
  var password = TextEditingController();
  var email = TextEditingController();
  var isloading;

  JobViewModel registerviewmodel = JobViewModel();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    password.text = '';
    email.text = '';
    name.text = '';
    isloading = false;
  }

  void onRegister() async {
    if (isNameValid == false || isEmailValid == false || isPassValid == false) {
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            elevation: 0,
            title: Column(
              children: [
                Icon(
                  Icons.warning,
                  size: 60,
                  color: AppColor.primarycolor,
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Error',
                  style: TextStyle(color: AppColor.primarycolor, fontSize: 18),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Please Check your form again',
                  style: TextStyle(
                      color: AppColor.primarycolor,
                      fontSize: 14,
                      fontWeight: FontWeight.w400),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Expanded(
                      child: ElevatedButton(
                        onPressed: () => Navigator.pop(context),
                        child: Text('Try again'),
                        style: ButtonStyle(
                            backgroundColor: MaterialStatePropertyAll(
                                AppColor.primarycolor)),
                      ),
                    ),
                  ],
                )
              ],
            ),
          );
        },
      );
      return null;
    }
    print(name.text);
    if (name.text == '' || email.text == '' || password.text == '') {
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(14)),
            elevation: 0,
            title: Column(
              children: [
                Icon(
                  Icons.warning,
                  size: 60,
                  color: AppColor.primarycolor,
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'Please Check your form again',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: AppColor.primarycolor,
                      fontSize: AppFont.bigfont + 4,
                      fontWeight: FontWeight.w500),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  'One or two field is missing in your form',
                  style: TextStyle(
                      color: AppColor.primarycolor,
                      fontSize: AppFont.smfont + 1,
                      fontWeight: FontWeight.w400),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Expanded(
                      child: ElevatedButton(
                        onPressed: () => Navigator.pop(context),
                        child: Text('Try again'),
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStatePropertyAll(AppColor.primarycolor),
                            shape: MaterialStatePropertyAll(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12)))),
                      ),
                    ),
                  ],
                )
              ],
            ),
          );
        },
      );
      return null;
    }

    setState(() {
      isloading = true;
    });

    await registerviewmodel.checkEmail(email.text);
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColor.thirdcolor,
        elevation: 0,
        iconTheme: IconThemeData(color: AppColor.primarycolor),
      ),
      backgroundColor: AppColor.thirdcolor,
      body: ChangeNotifierProvider<JobViewModel>(
        create: (context) => registerviewmodel,
        builder: (context, _) {
          return SafeArea(
            child: Consumer<JobViewModel>(
              builder: (context, register, _) {
                if (register.checkemailResponse?.status == Status.COMPLETED) {
                  WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                    register.checkemailResponse?.status = Status.LOADING;

                    Navigator.push(context, MaterialPageRoute(
                      builder: (context) {
                        return Myloading_one(
                          email: email.text,
                          password: password.text,
                          name: name.text,
                        );
                      },
                    ));
                    setState(() {
                      isloading = false;
                    });

                    // Navigator.push(context, MaterialPageRoute(builder: (context) =>ScreenDefault()));
                    // print(res.loginApiResponse.data.message);
                  });
                }
                if (register.checkemailResponse?.status == Status.ERROR) {
                  print("Duplicate email");
                  ;
                  WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                    register.checkemailResponse?.status = Status.LOADING;
                    showDialog(
                      context: context,
                      builder: (context) {
                        return AlertDialog(
                          elevation: 0,
                          title: Column(
                            children: [
                              Icon(
                                Icons.warning,
                                size: 60,
                                color: AppColor.primarycolor,
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                'Email Existed',
                                style: TextStyle(
                                    color: AppColor.primarycolor, fontSize: 18),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                'Another user with the assoicated email already used',
                                style: TextStyle(
                                  color: AppColor.primarycolor,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400,
                                ),
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: [
                                  Expanded(
                                    child: ElevatedButton(
                                      onPressed: () => Navigator.pop(context),
                                      child: Text('Try again'),
                                      style: ButtonStyle(
                                          backgroundColor:
                                              MaterialStatePropertyAll(
                                                  AppColor.primarycolor)),
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        );
                      },
                    );
                    setState(() {
                      isloading = false;
                    });
                  });
                }
                return LayoutBuilder(
                  builder: (context, constraints) {
                    return Container(
                      margin: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 48, horizontal: 35),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Center(
                                child: Image.asset(
                                  'assets/images/Icon/Frame 2 (1).png',

                                  //
                                  width: constraints.maxWidth * 0.5,
                                  height: 146,
                                  alignment: Alignment.center,
                                ),
                              ),
                              SizedBox(
                                height: 24,
                              ),
                              Center(
                                child: RichText(
                                  textAlign: TextAlign.center,
                                  text: TextSpan(
                                      text: 'Become a ',
                                      style: TextStyle(
                                        fontSize: AppFont.megafont,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.white,
                                      ),
                                      children: [
                                        TextSpan(
                                          text: 'Member',
                                          style: TextStyle(
                                            fontSize: AppFont.megafont,
                                            fontWeight: FontWeight.w500,
                                            color: AppColor.primarycolor,
                                          ),
                                        )
                                      ]),
                                  maxLines: 10,
                                ),
                              ),
                              SizedBox(
                                height: 30,
                              ),
                              Text(
                                'New around here, get ready to become a part of member right now !!! ',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: AppColor.primarycolor.withOpacity(0.8),
                                  fontSize: AppFont.medfont,
                                  fontWeight: FontWeight.w400,
                                ),
                                maxLines: 10,
                              ),
                              SizedBox(
                                height: 40,
                              ),
                              Column(
                                children: [
                                  Form(
                                    key: keyname,
                                    child: TextFormField(
                                      validator: (name) {
                                        if (isNameEmpty(name.toString())) {
                                          setState(() {
                                            isNameValid = true;
                                          });
                                          if (NameValid(name.toString())) {
                                            return null;
                                          } else {
                                            setState(() {
                                              isNameValid = false;
                                            });
                                            return 'Name must be in Alphabet only';
                                          }
                                          return null;
                                        } else {
                                          setState(() {
                                            isNameValid = false;
                                          });
                                          return 'Please enter your name';
                                        }
                                      },
                                      style: TextStyle(
                                          color: AppColor.primarycolor),
                                      cursorColor: Colors.white,
                                      keyboardType: TextInputType.name,
                                      controller: name,
                                      decoration: InputDecoration(
                                          fillColor: AppColor.whitecolor,
                                          contentPadding: EdgeInsets.symmetric(
                                              vertical: 12, horizontal: 10),
                                          hintText: 'Full name',
                                          hintStyle: TextStyle(
                                              color: AppColor.primarycolor
                                                  .withOpacity(0.5),
                                              fontSize: AppFont.medfont + 4),

                                          // suffix: GestureDetector(
                                          //   onTap: () => setState(() {
                                          //     name.clear();
                                          //   }),
                                          //   child: Icon(
                                          //     Icons.highlight_remove,
                                          //     color: AppColor.primarycolor.withOpacity(0.5,
                                          //
                                          //     ),
                                          //     size: 23,
                                          //   ),
                                          // ),

                                          floatingLabelBehavior:
                                              FloatingLabelBehavior.auto,
                                          floatingLabelStyle: TextStyle(
                                            color: AppColor.primarycolor,
                                            fontSize: 20,
                                            shadows: List.filled(
                                                10,
                                                Shadow(
                                                  color: Colors.black,
                                                )),
                                            fontWeight: FontWeight.w500,
                                          ),
                                          //
                                          // prefix: Icon(Icons.drive_file_rename_outline,
                                          //               color: AppColor.primarycolor,
                                          // ),
                                          filled: true,
                                          labelStyle: TextStyle(
                                              color: AppColor.primarycolor,
                                              fontWeight: FontWeight.w500),
                                          border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                          ),
                                          focusColor: Colors.white,
                                          enabled: true,
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            borderSide: BorderSide(
                                                width: 1.2,
                                                color: AppColor.primarycolor),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            borderSide: BorderSide(
                                                color: AppColor.whitecolor),
                                          )),
                                      enabled: true,
                                      onChanged: (value) {
                                        if (keypass.currentState!.validate()) {
                                          keypass.currentState!.save();
                                          //if valid wwe save the key email to helptext
                                        }
                                      },
                                      onFieldSubmitted: (value) {
                                        setState(() {
                                          name.text = value;
                                        });
                                        if (keyname.currentState!.validate()) {
                                          keyname.currentState!.save();
                                          //if valid wwe save the key email to helptext
                                        }
                                        setState(() {
                                          name.text = value;
                                          name.selection =
                                              TextSelection.fromPosition(
                                                  TextPosition(
                                                      offset:
                                                          name.text.length));
                                        });
                                      },
                                      maxLength: 25,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Container(
                                    child: Form(
                                      key: keyemail,
                                      child: TextFormField(
                                        validator: (email) {
                                          if (isEmailEmpty(email.toString())) {
                                            setState(() {
                                              isEmailValid = true;
                                            });
                                            if (!isValidEmail(
                                                email.toString())) {
                                              setState(() {
                                                isEmailValid = false;
                                              });
                                              return 'Invalid Email Address';
                                            } else {
                                              setState(() {
                                                isEmailValid = true;
                                              });
                                              return null;
                                            }
                                            return null;
                                          } else {
                                            setState(() {
                                              isEmailValid = false;
                                            });
                                            return 'Please enter an email';
                                          }
                                        },
                                        style: TextStyle(
                                          color: AppColor.primarycolor,
                                        ),
                                        cursorColor: Colors.white,
                                        keyboardType: TextInputType.name,
                                        controller: email,
                                        onChanged: (value) {
                                          if (keyemail.currentState!
                                              .validate()) {
                                            keyemail.currentState!.save();
                                            //if valid wwe save the key email to helptext

                                            setState(() {
                                              email.text = value;
                                              email.selection =
                                                  TextSelection.fromPosition(
                                                      TextPosition(
                                                          offset: email
                                                              .text.length));
                                            });
                                          }
                                        },
                                        onFieldSubmitted: (value) {
                                          setState(() {
                                            email.text = value;
                                          });
                                          if (keyemail.currentState!
                                              .validate()) {
                                            keyemail.currentState!.save();
                                            //if valid wwe save the key email to helptext
                                          }
                                        },
                                        decoration: InputDecoration(
                                            contentPadding:
                                                EdgeInsets.symmetric(
                                                    vertical: 13,
                                                    horizontal: 12),
                                            fillColor: AppColor.whitecolor,
                                            hintText: 'Email Address',
                                            hintStyle: TextStyle(
                                              color: AppColor.primarycolor
                                                  .withOpacity(0.5),
                                            ),

                                            //     suffix: GestureDetector(
                                            //   onTap: ()=>setState(() {
                                            //     email.clear();
                                            //   }),
                                            //   child: Icon(
                                            //
                                            //
                                            //       Icons.highlight_remove,
                                            //       size: 18,
                                            //       color: AppColor.primarycolor.withOpacity(0.5)
                                            //   ),
                                            // ),

                                            floatingLabelBehavior:
                                                FloatingLabelBehavior.auto,
                                            floatingLabelStyle: TextStyle(
                                              color: AppColor.primarycolor,
                                              fontSize: 20,
                                              shadows: List.filled(
                                                  10,
                                                  Shadow(
                                                    color: Colors.black,
                                                  )),
                                              fontWeight: FontWeight.w400,
                                            ),

                                            // prefixIcon: Icon(Icons.email_outlined,
                                            //   color: AppColor.primarycolor.withOpacity(0.85),
                                            // ),
                                            filled: true,
                                            labelStyle: TextStyle(
                                                color: AppColor.primarycolor,
                                                fontWeight: FontWeight.w500),
                                            border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                            ),
                                            focusColor: Colors.white,
                                            enabled: true,
                                            focusedBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide(
                                                  width: 1.2,
                                                  color: AppColor.primarycolor),
                                            ),
                                            enabledBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              borderSide: BorderSide(
                                                  color: Colors.white),
                                            )),
                                        enabled: true,
                                        maxLength: 40,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Form(
                                    key: keypass,
                                    child: TextFormField(
                                      validator: (password) {
                                        if (isPasswordEmpty(
                                            password.toString())) {
                                          setState(() {
                                            isPassValid = true;
                                          });
                                          if (isPasswordValidate(
                                              password.toString())) {
                                            setState(() {
                                              isPassValid = true;
                                            });
                                            return null;
                                          } else {
                                            setState(() {
                                              isPassValid = false;
                                            });

                                            return 'Password must be at least 5 characters';
                                          }
                                          return null;
                                        } else {
                                          return 'Please enter your password';
                                        }
                                      },
                                      style: TextStyle(
                                          color: AppColor.primarycolor),
                                      obscureText:
                                          passeye == true ? false : true,
                                      cursorColor: Colors.white,
                                      controller: password,
                                      onChanged: (value) {
                                        if (keypass.currentState!.validate()) {
                                          keypass.currentState!.save();
                                        }
                                        setState(() {
                                          password.text = value;
                                          password.selection =
                                              TextSelection.fromPosition(
                                                  TextPosition(
                                                      offset: password
                                                          .text.length));
                                        });
                                      },
                                      onFieldSubmitted: (value) {
                                        setState(() {
                                          password.text = value;
                                        });
                                        if (keypass.currentState!.validate()) {
                                          keypass.currentState!.save();
                                        }
                                      },
                                      decoration: InputDecoration(
                                          fillColor: AppColor.whitecolor,
                                          contentPadding: EdgeInsets.symmetric(
                                              vertical: 13, horizontal: 12),
                                          hintText: 'Password',
                                          hintStyle: TextStyle(
                                            color: AppColor.primarycolor
                                                .withOpacity(0.5),
                                          ),
                                          suffixIcon: IconButton(
                                            onPressed: () {
                                              setState(() {
                                                if (passeye == true) {
                                                  passeye = false;
                                                } else {
                                                  passeye = true;
                                                }
                                              });
                                            },
                                            icon: passeye == true
                                                ? Icon(
                                                    Icons
                                                        .remove_red_eye_outlined,
                                                    color: AppColor.primarycolor
                                                        .withOpacity(0.9),
                                                  )
                                                : Icon(
                                                    Icons.remove_red_eye,
                                                    color: AppColor.primarycolor
                                                        .withOpacity(0.9),
                                                  ),
                                          ),
                                          // labelText: 'Password',
                                          floatingLabelBehavior:
                                              FloatingLabelBehavior.auto,
                                          floatingLabelStyle: TextStyle(
                                            color: AppColor.primarycolor,
                                            fontSize: 20,
                                            shadows: List.filled(
                                                10,
                                                Shadow(
                                                  color: Colors.black,
                                                )),
                                            fontWeight: FontWeight.w500,
                                          ),

                                          // prefixIcon: Icon(Icons.lock,
                                          //   color: AppColor.primarycolor,
                                          // ),
                                          filled: true,
                                          labelStyle: TextStyle(
                                              color: AppColor.primarycolor,
                                              fontWeight: FontWeight.w500),
                                          border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                          ),
                                          focusColor: Colors.white,
                                          enabled: true,
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            borderSide: BorderSide(
                                                width: 1.2,
                                                color: AppColor.primarycolor),
                                          ),
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            borderSide:
                                                BorderSide(color: Colors.white),
                                          )),
                                      enabled: true,
                                      maxLength: 15,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  ElevatedButton(
                                      onPressed: !isloading ? onRegister : null,
                                      style: ElevatedButton.styleFrom(
                                          backgroundColor:
                                              AppColor.primarycolor,
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                          )),
                                      child: Padding(
                                        padding: const EdgeInsets.all(12.0),
                                        child: isloading == true
                                            ? Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceEvenly,
                                                children: [
                                                  Center(
                                                    child: Container(
                                                      width: 20,
                                                      height: 20,
                                                      child:
                                                          CircularProgressIndicator(
                                                        color: AppColor
                                                            .primarycolor,
                                                        backgroundColor:
                                                            Colors.indigo,
                                                      ),
                                                    ),
                                                  )
                                                ],
                                              )
                                            : Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceEvenly,
                                                children: [
                                                  Text(
                                                    'Register',
                                                    style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: AppFont.bigfont,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                      )),
                                  SizedBox(
                                    height: 30,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      RichText(
                                        text: TextSpan(
                                            text: 'Already a member? ',
                                            style: TextStyle(
                                              fontSize: AppFont.medfont,
                                              fontWeight: FontWeight.w400,
                                              color: AppColor.primarycolor,
                                            ),
                                            children: []),
                                        maxLines: 10,
                                      ),
                                      GestureDetector(
                                        onTap: () => Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) => Mylogin(),
                                            )),
                                        child: Text(
                                          'Click here',
                                          style: TextStyle(
                                            fontSize: AppFont.bigfont,
                                            fontWeight: FontWeight.w500,
                                            color: AppColor.bgcolor,
                                          ),
                                        ),
                                      )
                                    ],
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                );
              },
            ),
          );
        },
      ),
    );
  }
}
