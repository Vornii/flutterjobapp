import 'dart:math';

import 'package:flutter/material.dart';
import 'package:jobapp/VIews/AuthScreen/EmailSent.dart';

import 'package:jobapp/VIews/AuthScreen/register.dart';
import 'package:jobapp/ViewModel/job_view_model.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../ViewModel/post_view_model.dart';
import '../../data/Response/status_response.dart';
import '../../model/authrequest_model.dart';
import '../../model/authresponse_model.dart';
import '../../res/Appcolor.dart';
import '../../res/Appfont.dart';
import '../../res/Validator/loginvalidate.dart';
import '../WelcomeScreen/defaultscreen.dart';

class MyResetScreen extends StatefulWidget {

  MyResetScreen({Key? key}) : super(key: key);

  @override
  State< MyResetScreen> createState() => _MyResetScreenState();
}

class _MyResetScreenState extends State< MyResetScreen> with CheckValidateLogin {
  @override
  bool value = false;
  var statusAuth;
  var userid;
  var isloading  =false;


  var ispasshide = true;
  var txtemail = TextEditingController();
  var txtpass = TextEditingController();
  String? tokenid;
  var response;
  var keypass = GlobalKey<FormState>();
  var keyemail = GlobalKey<FormState>();
  var validate = true;
  var usersid;

  PostViewModel resetviewmodel = PostViewModel();
  void initState() {
    // TODO: implement initState
    super.initState();
    // txtemail.text = 'Vornkh87@gmail.com';
    checkuid();

  }
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: AppColor.thirdcolor,
          elevation: 0,
          iconTheme: IconThemeData(
              color: Colors.black
          ),



          automaticallyImplyLeading:true //set to false = no go back bar

      ),
      backgroundColor: AppColor.thirdcolor,
      body: ChangeNotifierProvider< PostViewModel>(
        create: (context) => resetviewmodel ,
        builder: (context, child) {

          return Consumer< PostViewModel>(

            builder: (context, res, child) {
              // var responselogin =res.loginApiResponse.data;
              print(resetviewmodel.ResetPassResponse.status);
              var status = resetviewmodel.ResetPassResponse.status;
              print(status);

              if(resetviewmodel.ResetPassResponse.status != Status.LOADING){

                WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                  print("working");





                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return MySuccessScreen(email:txtemail.text,uid:usersid  );
                  },));




                });
                // ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Success') ));

                // print( tokenid);
                // print( statusAuth);



                //TODO if users post and notify back from server complete to front end

              }
              // else if(res.ResetPassResponse.status!= Status.LOADING){
              //   WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
              //     resetviewmodel.dispose();
              //     showDialog(context: context, builder: (context) {
              //       return AlertDialog(
              //
              //         elevation: 0,
              //
              //         title: Column(
              //           children: [
              //             Icon(Icons.warning,size: 60,color: AppColor.primarycolor,),
              //             SizedBox(height: 10,),
              //             Text('Error',style:
              //             TextStyle(
              //                 color:AppColor.primarycolor,
              //                 fontSize: 18
              //             ),),
              //             SizedBox(height: 10,),
              //             Text('Invalid Password or Email, Please recheck',style:
              //             TextStyle(
              //                 color: AppColor.primarycolor,
              //                 fontSize: 14,
              //                 fontWeight: FontWeight.w400
              //             ),),
              //
              //             SizedBox(height: 10,),
              //
              //             Row(
              //               children: [
              //                 Expanded(
              //                   child: ElevatedButton(onPressed: () => Navigator.pop(context),
              //                     child: Text('Try again'),
              //                     style: ButtonStyle(
              //                         backgroundColor: MaterialStatePropertyAll(AppColor.primarycolor)
              //                     ),
              //                   ),
              //                 ),
              //               ],
              //             )
              //           ],
              //         ),
              //
              //       );
              //     },);
              //
              //
              //   });
              //
              // }
              // if(res.ResetPassResponse.status== Status.ERROR){
              //   WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
              //
              //     resetviewmodel.dispose();
              //       isloading = false;
              //
              //
              //   });
              //
              // }

              return  Container(
                margin: EdgeInsets.symmetric(vertical: 0,horizontal: 0),

                child: SingleChildScrollView(


                  child: Container(

                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 68,horizontal: 30),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        mainAxisSize: MainAxisSize.max,

                        children: [

                          SizedBox(
                            height: 24,
                          ),

                          SizedBox(height: 20,),


                          SizedBox(height: 60,),

                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,

                            children: [
                            Text('Reset Password',style: TextStyle(
                              color: AppColor.primarycolor,
                              fontSize: AppFont.megafont,
                              fontWeight: FontWeight.bold
                          ),),
                              SizedBox(height: 10,),
                              Text('Please enter your email address'
                                  'associate to the account you have registered to then we will send you an'
                                  'message to reset password',style: TextStyle(
                                  color: AppColor.primarycolor,
                                  fontSize: AppFont.medfont,
                                  fontWeight: FontWeight.w400,
                                height: 1.6
                              ),),

                              SizedBox(height: 30,),

                              Form(
                                key: keyemail,
                                child: TextFormField(
                                  controller: txtemail,
                                  onChanged: (email) {



                                    if(keyemail.currentState!.validate()){
                                      keyemail.currentState!.save();


                                    }
                                    else{
                                      setState(() {
                                        validate = false;
                                      });
                                    }


                                  },
                                  validator: (email) {

                                    if(isValidEmail(email.toString())){
                                      return null;

                                    }
                                    else if(!isEmailLength(email.toString())){
                                      return 'Missing Email address';
                                    }

                                    else{
                                      return 'Invalid Email address';
                                    }





                                  },
                                  onFieldSubmitted: (value) {

                                    setState(() {
                                      txtemail.text = value;
                                      if(isValidEmail(    txtemail.text.toString())){

                                        validate = true;
                                        return null;
                                      }
                                      else if(!isEmailLength(    txtemail.text.toString())){
                                        validate = false;
                                      }

                                      else{
                                        validate = false;
                                      }

                                    });

                                  },
                                  style: TextStyle(

                                      color: AppColor.primarycolor
                                  ),

                                  cursorColor: Colors.white,
                                  keyboardType: TextInputType.name,

                                  decoration: InputDecoration(
                                    hintText: 'Email address',

                                      fillColor:Colors.white,
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(color: Colors.white),
                                    borderRadius: BorderRadius.circular(10) ,
                                      ),


                                      // hintText: 'youremail@gmail.com',
                                      hintStyle: TextStyle(
                                        color: AppColor.primarycolor.withOpacity(0.5),

                                      ),
                                      suffixIcon: IconButton(
                                        onPressed: () {
                                          setState(() {
                                            txtemail.clear();
                                          });
                                        },
                                        icon: Icon(   Icons.highlight_remove_outlined,
                                          color: AppColor.primarycolor.withOpacity(0.5),
                                        ),
                                      ),

                                      // label: Text('Email',style: TextStyle(
                                      //
                                      // ),),



                                      floatingLabelBehavior: FloatingLabelBehavior.auto,
                                      floatingLabelStyle: TextStyle(
                                        color:AppColor.primarycolor,


                                        fontSize:20,
                                        shadows:List.filled(10, Shadow(
                                          color: Colors.black,

                                        )),

                                        fontWeight: FontWeight.w500,


                                      ),

                                      prefixIcon: Icon(Icons.email_outlined,
                                        color: AppColor.primarycolor
                                      ),
                                      filled: true,
                                      labelStyle: TextStyle(
                                          color: AppColor.primarycolor,
                                          fontWeight: FontWeight.w500
                                      ),
                                      border: OutlineInputBorder(



                                        borderRadius: BorderRadius.circular(15),




                                      ),
                                      focusColor: Colors.white,

                                      enabled: true ,
                                      counterStyle: TextStyle(
                                          color: Colors.white
                                      ),

                                      focusedBorder: OutlineInputBorder(

                                        borderRadius: BorderRadius.circular(10),


                                        borderSide: BorderSide(
                                            width: 1.42,
                                            color: Colors.green

                                        ),

                                      )

                                  ),


                                  enabled: true,


                                ),
                              ),
                              SizedBox(height: 40,),







                              ElevatedButton(
                                  onPressed:() async {
                                    if(txtemail.text =='' || validate == false){
                                      print("Invalid Email or password");

                                      return   showDialog(context: context, builder: (context) {
                                        return AlertDialog(

                                          elevation: 0,

                                          title: Column(
                                            children: [
                                              Icon(Icons.warning,size: 60,color: AppColor.primarycolor,),
                                              SizedBox(height: 10,),
                                              Text('Error',style:
                                              TextStyle(
                                                  color:AppColor.primarycolor,
                                                  fontSize: 18
                                              ),),
                                              SizedBox(height: 10,),
                                              Text('Please Check your form again',style:
                                              TextStyle(
                                                  color: AppColor.primarycolor,
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w400
                                              ),),

                                              SizedBox(height: 10,),

                                              Row(
                                                children: [
                                                  Expanded(
                                                    child: ElevatedButton(onPressed: () => Navigator.pop(context),
                                                      child: Text('Try again'),
                                                      style: ButtonStyle(
                                                          backgroundColor: MaterialStatePropertyAll(AppColor.primarycolor)
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              )
                                            ],
                                          ),

                                        );
                                      },);

                                    }
                                    else{
                                    print(txtemail.text);
                                    print(usersid);
                                    setState(() {
                                      isloading = true;
                                    });
                                    await resetviewmodel.postResetPassword(usersid, txtemail.text);



                                    // await loginviewmodel.postLogin(txtemail.text,txtpass.text);
                                    // Navigator.push(context, MaterialPageRoute(builder: (context) {
                                    //   return MySuccessScreen();
                                    // },));


                                    }








                                  },



                                  style: ElevatedButton.styleFrom(
                                      backgroundColor:AppColor.bgcolor,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10) ,
                                      )
                                  ),

                                  child: Padding(
                                    padding: const EdgeInsets.all(0.0),
                                    child: Row(
                                      mainAxisAlignment:MainAxisAlignment.spaceEvenly,
                                      children: [
                                        if(isloading == true)

                                          Container(
                                            height: 25,
                                            width: 25,

                                            child: Center(
                                              child: CircularProgressIndicator(
                                                color: AppColor.primarycolor,



                                              ),
                                            ),
                                          ),

                                         if(isloading == false) Text('Reset Password',


                                          style: TextStyle(
                                            color: Colors.white,

                                            fontSize: AppFont.bigfont,
                                          ),
                                        ),

                                      ],
                                    ),
                                  )

                              ),
                              SizedBox(height: 30,),


                            ],
                          )


                        ],
                      ),
                    ),
                  ),
                ),
              ) ;
            },

          );
        },

      ),
    );
  }
  void checkuid() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // usersid = prefs.getString('userid');

  }
  void checkauth(tokenid,userid)  async {
    print("check prefer");
    SharedPreferences prefs = await SharedPreferences.getInstance();

    print(tokenid);
    prefs.setString('userid',userid);
    prefs.setString('tokens', tokenid);


    print(userid  );

  }
}
