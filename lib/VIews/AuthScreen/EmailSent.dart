import 'dart:math';

import 'package:flutter/material.dart';

import 'package:jobapp/VIews/AuthScreen/register.dart';
import 'package:jobapp/ViewModel/job_view_model.dart';
import 'package:jobapp/ViewModel/post_view_model.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../data/Response/status_response.dart';
import '../../model/authrequest_model.dart';
import '../../model/authresponse_model.dart';
import '../../res/Appcolor.dart';
import '../../res/Appfont.dart';
import '../../res/Validator/loginvalidate.dart';
import '../WelcomeScreen/defaultscreen.dart';
import 'Login.dart';

class MySuccessScreen extends StatefulWidget {
  var email;
  var uid;

  MySuccessScreen({Key? key, this.email,this.uid  }) : super(key: key);

  @override
  State< MySuccessScreen> createState() => _MySuccessScreenState();
}

class _MySuccessScreenState extends State< MySuccessScreen> with CheckValidateLogin {
  @override
  bool value = false;
  var statusAuth;
  var userid;


  var ispasshide = true;
  var txtemail = TextEditingController();
  var txtpass = TextEditingController();
  String? tokenid;
  var response;
  var keypass = GlobalKey<FormState>();
  var keyemail = GlobalKey<FormState>();
PostViewModel resetviewmodel = PostViewModel();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    txtemail.text = 'Vornkh87@gmail.com';
  }
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColor.thirdcolor,
        elevation: 0,
        iconTheme: IconThemeData(
            color: Colors.black
        ),



        automaticallyImplyLeading:true //set to false = no go back bar

      ),

      backgroundColor: AppColor.thirdcolor,
      body: ChangeNotifierProvider< PostViewModel>(
        create: (context) => resetviewmodel,
        builder: (context, child) {

          return Consumer< PostViewModel>(

            builder: (context, res, child) {
              // var responselogin =res.loginApiResponse.data;
              print(resetviewmodel.ResetPassResponse.status);
              var status = resetviewmodel.ResetPassResponse.status;
              print(status);

              if(resetviewmodel.ResetPassResponse.status== Status.COMPLETED){
                print("success sending again");





                //TODO if users post and notify back from server complete to front end

              }


              return  Container(
                margin: EdgeInsets.symmetric(vertical: 0,horizontal: 0),

                child: SingleChildScrollView(


                  child: Container(

                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 58,horizontal: 40),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        mainAxisSize: MainAxisSize.max,

                        children: [

                          SizedBox(
                            height: 24,
                          ),

                          SizedBox(height: 20,),


                          SizedBox(height: 60,),

                          Column(
                            crossAxisAlignment: CrossAxisAlignment.center,

                            children: [
                              Text('Your Email Has been Sent',style: TextStyle(
                                  color: AppColor.primarycolor,
                                  fontSize: AppFont.megafont,
                                  fontWeight: FontWeight.bold
                              ),),
                              SizedBox(height: 10,),
                              Text('Your email has been sent associated to the email you have registered. Please Check Your email now!!!',style: TextStyle(
                                  color: AppColor.primarycolor,
                                  fontSize: AppFont.medfont,
                                  fontWeight: FontWeight.w300,

                                  height: 1.6,

                              ),
                              textAlign: TextAlign.center,
                              ),
                              Center(
                                child: Lottie.network('https://assets1.lottiefiles.com/private_files/lf30_o0calpsv.json',

                                fit: BoxFit.cover,
                                  width: 250,
                                  height: 250
                                ),
                              ),

                              SizedBox(height: 20,),









                              ElevatedButton(
                                  onPressed:() async {
                                    print(txtemail.text);
                                    await resetviewmodel.postResetPassword(widget.uid,widget.email);



                                  },



                                  style: ElevatedButton.styleFrom(
                                      backgroundColor:AppColor.bgcolor,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10) ,
                                      )
                                  ),

                                  child: Padding(
                                    padding: const EdgeInsets.all(12.0),
                                    child: Row(
                                      mainAxisAlignment:MainAxisAlignment.spaceEvenly,
                                      children: [
                                        Text('Resent Your Email',


                                          style: TextStyle(
                                            color: Colors.white,

                                            fontSize: AppFont.bigfont,
                                          ),
                                        ),

                                      ],
                                    ),
                                  )

                              ),
                              SizedBox(height: 10,),
                              ElevatedButton(
                                  onPressed:() async {
                                    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(
                                        builder: (context) => Mylogin()), (Route route) =>false);




                                  },



                                  style: ElevatedButton.styleFrom(
                                      backgroundColor:AppColor.primarycolor,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10) ,
                                      )
                                  ),

                                  child: Padding(
                                    padding: const EdgeInsets.all(12.0),
                                    child: Row(
                                      mainAxisAlignment:MainAxisAlignment.spaceEvenly,
                                      children: [
                                        Text('Login',


                                          style: TextStyle(
                                            color: Colors.white,

                                            fontSize: AppFont.bigfont,
                                          ),
                                        ),

                                      ],
                                    ),
                                  )

                              ),


                            ],
                          )


                        ],
                      ),
                    ),
                  ),
                ),
              ) ;
            },

          );
        },

      ),
    );
  }

  void checkauth(tokenid,userid)  async {
    print("check prefer");
    SharedPreferences prefs = await SharedPreferences.getInstance();

    print(tokenid);
    prefs.setString('userid',userid);
    prefs.setString('tokens', tokenid);


    print(userid  );

  }
}
