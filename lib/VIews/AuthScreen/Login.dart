import 'dart:math';

import 'package:flutter/material.dart';
import 'package:jobapp/VIews/AuthScreen/Resetpassword.dart';

import 'package:jobapp/VIews/AuthScreen/register.dart';
import 'package:jobapp/ViewModel/job_view_model.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../data/Response/status_response.dart';
import '../../model/authrequest_model.dart';
import '../../model/authresponse_model.dart';
import '../../res/Appcolor.dart';
import '../../res/Appfont.dart';
import '../../res/Validator/loginvalidate.dart';
import '../WelcomeScreen/defaultscreen.dart';

class Mylogin extends StatefulWidget {
  var isReset;

  Mylogin({Key? key})
      : super(
          key: key,
        );

  @override
  State<Mylogin> createState() => _MyloginState();
}

class _MyloginState extends State<Mylogin> with CheckValidateLogin {
  @override
  bool value = false;
  var statusAuth;
  var userid;
  var isloading = false;

  var ispasshide = true;
  var txtemail = TextEditingController();
  var txtpass = TextEditingController();
  String? tokenid;
  var response;
  var keypass = GlobalKey<FormState>();
  var keyemail = GlobalKey<FormState>();
  JobViewModel loginviewmodel = JobViewModel();
  var ischeck;
  var labelpass = 'Password';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    checkemail();
  }
  void onlogin () async {
    if(txtemail.text =='' || txtpass.text ==''){
      print("Invalid Email or password");

      return   showDialog(context: context, builder: (context) {
        return AlertDialog(

          elevation: 0,

          title: Column(
            children: [
              Icon(Icons.warning,size: 60,color: AppColor.primarycolor,),
              SizedBox(height: 10,),
              Text('Error',style:
              TextStyle(
                  color:AppColor.primarycolor,
                  fontSize: 18
              ),),
              SizedBox(height: 10,),
              Text('Please Check your form again',style:
              TextStyle(
                  color: AppColor.primarycolor,
                  fontSize: 14,
                  fontWeight: FontWeight.w400
              ),),

              SizedBox(height: 10,),

              Row(
                children: [
                  Expanded(
                    child: ElevatedButton(onPressed: () => Navigator.pop(context),
                      child: Text('Try again'),
                      style: ButtonStyle(
                          backgroundColor: MaterialStatePropertyAll(AppColor.primarycolor)
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),

        );
      },);

    }
    else{
      setState(() {
        isloading = true;
      });
      print(txtemail.text);
      print(txtpass.text);
      await loginviewmodel.postLogin(txtemail.text,txtpass.text);

    }



  }
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColor.primarycolor,
        elevation: 0,
        iconTheme: IconThemeData(color: AppColor.whitecolor),
      ),
      backgroundColor: AppColor.primarycolor,
      body: ChangeNotifierProvider<JobViewModel>(
        create: (context) => loginviewmodel,
        builder: (context, child) {
          return Consumer<JobViewModel>(
            builder: (context, res, child) {
              // var responselogin =res.loginApiResponse.data;
              print(loginviewmodel.loginApiResponse.status);
              var status = loginviewmodel.loginApiResponse.status;
              print(status);
              if (loginviewmodel.loginApiResponse.status == Status.COMPLETED) {
                WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                  saveduser(
                      loginviewmodel.loginApiResponse.data?.data?.email,
                      ischeck,
                      loginviewmodel.loginApiResponse.data?.data?.password);

                  tokenid = res.loginApiResponse.data?.token;
                  statusAuth = res.loginApiResponse.data?.response;
                  userid = res.loginApiResponse.data?.id;
                  checkauth(tokenid, userid);
                  print(tokenid);
                  if (tokenid != null) {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ScreenDefault(),
                        ));
                    return null;
                  }
                  setState(() {
                    isloading = false;
                  });
                  if (tokenid == null) {
                    showDialog(
                      context: context,
                      builder: (context) {
                        return AlertDialog(
                          elevation: 0,
                          title: Column(
                            children: [
                              Icon(
                                Icons.warning,
                                size: 60,
                                color: AppColor.primarycolor,
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                'Error',
                                style: TextStyle(
                                    color: AppColor.primarycolor, fontSize: 18),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                'Invalid Password or Email, Please recheck',
                                style: TextStyle(
                                    color: AppColor.primarycolor,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: [
                                  Expanded(
                                    child: ElevatedButton(
                                      onPressed: () => Navigator.pop(context),
                                      child: Text('Try again'),
                                      style: ButtonStyle(
                                          backgroundColor:
                                              MaterialStatePropertyAll(
                                                  AppColor.primarycolor)),
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        );
                      },
                    );
                  }
                });
                // ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Success') ));
                loginviewmodel.loginApiResponse.status = Status.LOADING;
                // print( tokenid);
                // print( statusAuth);

                //TODO if users post and notify back from server complete to front end
              } else if (loginviewmodel.loginApiResponse.status ==
                  Status.ERROR) {
                //TODO If login invalid then we redirect to loading

                // print(loginviewmodel.loginApiResponse.data?. );
                // var messageerror = res.loginApiResponse?.data?.;

                WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                  print("User not sign up yet");
                  setState(() {
                    isloading = false;
                  });

                  // print(loginviewmodel.loginApiResponse.);
                  showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(14)),
                        backgroundColor: AppColor.primarycolor,
                        elevation: 0,
                        title: Column(
                          children: [
                            Icon(
                              Icons.warning,
                              size: 60,
                              color: AppColor.fourthcolor,
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              'Please Sign Up',
                              style: TextStyle(
                                  color: AppColor.fourthcolor,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 18),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              'You have not yet sign up, please make register',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: AppColor.fourthcolor,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w400),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                Expanded(
                                  child: ElevatedButton(
                                    onPressed: () => Navigator.push(context,
                                        MaterialPageRoute(
                                      builder: (context) {
                                        return Myregister();
                                      },
                                    )),
                                    child: Text('Register '),
                                    style: ButtonStyle(
                                        backgroundColor:
                                            MaterialStatePropertyAll(
                                                AppColor.fourthcolor),
                                        shape: MaterialStatePropertyAll(
                                            RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        10)))),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      );
                    },
                  );
                });

                loginviewmodel.loginApiResponse.status = Status.LOADING;
              }

              return LayoutBuilder(
                builder: (BuildContext ctx, constraint) {
                  return Container(
                    margin: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 48, horizontal: 40),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Center(
                              child: Image.asset(
                                'assets/images/Icon/Frame 2 (1).png',

                                //
                                width: constraint.maxWidth * 0.5,
                                height: 146,
                                alignment: Alignment.center,
                              ),
                            ),
                            SizedBox(
                              height: 24,
                            ),
                            Center(
                              child: RichText(
                                textAlign: TextAlign.center,
                                text: TextSpan(
                                    text: 'Login ',
                                    style: TextStyle(
                                      fontSize: AppFont.megafont,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.white,
                                    ),
                                    children: []),
                                maxLines: 10,
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Center(
                              child: Text(
                                'Welcome back, get ready to find jobs?!!! ',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.white.withOpacity(0.5),
                                  fontSize: AppFont.medfont,
                                  fontWeight: FontWeight.w500,
                                ),
                                maxLines: 10,
                              ),
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            Column(
                              children: [
                                SizedBox(
                                  height: 10,
                                ),
                                Form(
                                  key: keyemail,
                                  child: TextFormField(
                                    controller: txtemail,
                                    onChanged: (email) {
                                      if (keyemail.currentState!.validate()) {
                                        keyemail.currentState!.save();
                                      }

                                      setState(() {
                                        txtemail.text = email;
                                        txtemail.selection =
                                            TextSelection.fromPosition(
                                                TextPosition(
                                                    offset:
                                                        txtemail.text.length));
                                      });
                                    },
                                    validator: (email) {
                                      if (isValidEmail(email.toString())) {
                                        return null;
                                      } else if (!isEmailLength(
                                          email.toString())) {
                                        return 'Missing Email address';
                                      } else {
                                        return 'Invalid Email address';
                                      }
                                    },
                                    onFieldSubmitted: (value) {
                                      setState(() {
                                        txtemail.text = value;
                                      });
                                    },
                                    style:
                                        TextStyle(color: AppColor.primarycolor),
                                    cursorColor: Colors.white,
                                    keyboardType: TextInputType.name,
                                    decoration: InputDecoration(
                                        fillColor: AppColor.secondcolor,
                                        hintText: 'Email',
                                        hintStyle: TextStyle(
                                          color: AppColor.primarycolor
                                              .withOpacity(0.5),
                                        ),
                                        suffixIcon: IconButton(
                                          onPressed: () {
                                            setState(() {
                                              txtemail.clear();
                                            });
                                          },
                                          icon: Icon(
                                            Icons.highlight_remove,
                                            color: AppColor.primarycolor
                                                .withOpacity(0.9),
                                          ),
                                        ),

                                        // label: Text('Email',style: TextStyle(
                                        //
                                        // ),),

                                        floatingLabelBehavior:
                                            FloatingLabelBehavior.auto,
                                        floatingLabelStyle: TextStyle(
                                          color: Colors.white,
                                          backgroundColor:
                                              AppColor.primarycolor,
                                          fontSize: 20,
                                          shadows: List.filled(
                                              10,
                                              Shadow(
                                                color: Colors.black,
                                              )),
                                          fontWeight: FontWeight.w500,
                                        ),
                                        prefixIcon: Icon(
                                          Icons.email_outlined,
                                          color: AppColor.primarycolor,
                                        ),
                                        filled: true,
                                        labelStyle: TextStyle(
                                            color: AppColor.primarycolor,
                                            fontWeight: FontWeight.w500),
                                        border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                        ),
                                        focusColor: Colors.white,
                                        enabled: true,
                                        counterStyle:
                                            TextStyle(color: Colors.white),
                                        focusedBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          borderSide: BorderSide(
                                              width: 1.42, color: Colors.green),
                                        )),
                                    enabled: true,
                                    maxLength: 50,
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Form(
                                  key: keypass,
                                  child: TextFormField(
                                    validator: (password) {
                                      if (isPasswordLength(
                                          password.toString())) {
                                        return null;
                                      } else {
                                        return 'Please enter a password';
                                      }
                                    },
                                    controller: txtpass,
                                    onChanged: (value) {
                                      //
                                      //   txtpass.text = value;
                                      //
                                      if (keypass.currentState!.validate()) {
                                        keypass.currentState!.save();
                                      }

                                      // setState(() {
                                      //   txtpass.text = value;
                                      //   txtpass.selection = TextSelection.fromPosition(TextPosition(offset:  txtpass.text.length));
                                      //
                                      // });
                                    },
                                    onFieldSubmitted: (value) {
                                      setState(() {
                                        txtpass.text = value;
                                      });
                                      if (keypass.currentState!.validate()) {
                                        keypass.currentState!.save();
                                      }
                                    },
                                    style:
                                        TextStyle(color: AppColor.primarycolor),
                                    obscureText: ispasshide,
                                    cursorColor: Colors.white,
                                    decoration: InputDecoration(
                                        counterStyle:
                                            TextStyle(color: Colors.white),
                                        fillColor: AppColor.secondcolor,
                                        hintText: 'Password',
                                        hintStyle: TextStyle(
                                          color: AppColor.primarycolor
                                              .withOpacity(0.5),
                                        ),
                                        suffixIcon: ispasshide == true
                                            ? IconButton(
                                                onPressed: () {
                                                  setState(() {
                                                    ispasshide = false;
                                                  });
                                                },
                                                icon: Icon(
                                                  Icons.remove_red_eye,
                                                  color: AppColor.primarycolor
                                                      .withOpacity(0.9),
                                                ),
                                              )
                                            : IconButton(
                                                onPressed: () {
                                                  setState(() {
                                                    ispasshide = true;
                                                  });
                                                },
                                                icon: Icon(
                                                  Icons.remove_red_eye_outlined,
                                                  color: AppColor.primarycolor
                                                      .withOpacity(0.9),
                                                ),
                                              ),
                                        floatingLabelBehavior:
                                            FloatingLabelBehavior.auto,
                                        floatingLabelStyle: TextStyle(
                                          color: Colors.white,
                                          backgroundColor:
                                              AppColor.primarycolor,
                                          fontSize: 20,
                                          shadows: List.filled(
                                              10,
                                              Shadow(
                                                color: Colors.black,
                                              )),
                                          fontWeight: FontWeight.w500,
                                        ),
                                        prefixIcon: Icon(
                                          Icons.lock,
                                          color: AppColor.primarycolor,
                                        ),
                                        hintMaxLines: 3,
                                        filled: true,
                                        labelStyle: TextStyle(
                                            color: AppColor.primarycolor,
                                            fontWeight: FontWeight.w500),
                                        border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                        ),
                                        focusColor: Colors.white,
                                        enabled: true,
                                        focusedBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          borderSide: BorderSide(
                                            width: 1.32,
                                            color: Colors.green,
                                          ),
                                        )),
                                    enabled: true,
                                    maxLength: 20,
                                  ),
                                ),
                                Container(
                                  width: double.maxFinite,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Expanded(
                                        child: GestureDetector(
                                          onTap: () {
                                            Navigator.push(context,
                                                MaterialPageRoute(
                                              builder: (context) {
                                                return MyResetScreen();
                                              },
                                            ));
                                          },
                                          child: Text(
                                            'Forget password?',
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: AppFont.bigfont - 2,
                                              fontWeight: FontWeight.w400,
                                            ),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Row(
                                          // TODO CHECKBOX REMEBER
                                          children: [
                                            Checkbox(
                                              value: this.value,
                                              focusColor: AppColor.bgcolor,
                                              side: BorderSide(
                                                  color: AppColor.fourthcolor,
                                                  width: 2),
                                              splashRadius: 10,
                                              activeColor: AppColor.fourthcolor,
                                              onChanged: (value) {
                                                setState(() {
                                                  this.value = value!;
                                                  ischeck = value;
                                                  print(value);
                                                });
                                              },
                                            ),
                                            Text(
                                              'Remember me',
                                              style: TextStyle(
                                                color: AppColor.whitecolor,
                                                fontSize: AppFont.bigfont - 2,
                                                fontWeight: FontWeight.w500,
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                ElevatedButton(
                                    onPressed: isloading ? null : onlogin,
                                    style: ElevatedButton.styleFrom(
                                        backgroundColor: AppColor.fourthcolor,
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                        )),
                                    child: isloading == true
                                        ? Padding(
                                            padding: const EdgeInsets.all(0.0),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: [
                                                Container(
                                                  width: 25,
                                                  height: 25,
                                                  child:
                                                      CircularProgressIndicator(
                                                    backgroundColor:
                                                        Colors.indigo,
                                                    color:
                                                        AppColor.primarycolor,
                                                  ),
                                                )
                                              ],
                                            ),
                                          )
                                        : Padding(
                                            padding: const EdgeInsets.all(12.0),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: [
                                                Text(
                                                  'Login',
                                                  style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: AppFont.bigfont,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          )),
                                SizedBox(
                                  height: 30,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    RichText(
                                      text: TextSpan(
                                          text: 'New member? become a member  ',
                                          style: TextStyle(
                                              fontSize: AppFont.medfont,
                                              fontWeight: FontWeight.w400,
                                              color: Colors.white
                                                  .withOpacity(0.4)),
                                          children: []),
                                      maxLines: 10,
                                    ),
                                    GestureDetector(
                                      onTap: () => Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) => Myregister(),
                                          )),
                                      child: Text(
                                        'here',
                                        style: TextStyle(
                                            fontSize: AppFont.bigfont,
                                            fontWeight: FontWeight.w500,
                                            color: AppColor.fourthcolor),
                                      ),
                                    )
                                  ],
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                },
              );
            },
          );
        },
      ),
    );
  }

  void checkauth(tokenid, userid) async {
    print("check prefer");
    SharedPreferences prefs = await SharedPreferences.getInstance();

    print(tokenid);
    prefs.setString('userid', userid);
    prefs.setString('tokens', tokenid);

    print(userid);
  }

  void saveduser(emailuser, ischeck, passuser) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('email', emailuser);
    if (ischeck == true) {
      prefs.setString('password', passuser);
    }
  }

  void checkemail() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var email = prefs.getString('email').toString();
    var pass = prefs.getString('password').toString();
    if (email == 'null') {
      email = '';
    }
    if (pass == 'null') {
      pass = '';
    }
    txtemail.text = email.toString();
    txtpass.text = pass.toString();
  }
}
