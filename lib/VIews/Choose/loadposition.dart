import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';

import 'package:jobapp/VIews/AuthScreen/Login.dart';

import 'package:lottie/lottie.dart';

import '../../res/Appcolor.dart';
import '../../res/Appfont.dart';
import 'Load2.dart';

class LoadPosition extends StatefulWidget {
  var choice;
  var email;
  var password;
  var name;

 LoadPosition({Key? key ,this.choice,this.email,this.password,this.name}) : super(key: key);

  @override
  State<LoadPosition> createState() => _LoadPositionState();
}

class _LoadPositionState extends State<LoadPosition> {
  var txtposition = TextEditingController();
  var txtcompany = TextEditingController();
  var keyposition = GlobalKey<FormState>();
  final TextEditingController _typeAheadController = TextEditingController();

  List<String> companies = [
    'Aeon Mall Cambodia',
    'AirAsia Cambodia',
    'ANZ Royal Bank',
    'Beeline Cambodia',
    'Bitkub Capital',
    'Cambrew',
    'Canadia Bank',
    'Cambodia Airways',
    'Cambodia Deposit Insurance Corporation',
    'Cambodia General Insurance',
    'Cambodia International Bank',
    'Cambodia Mekong Bank',
    'Cambodia Post',
    'Cambodia Securities Exchange',
    'Cambodia Telecom',
    'Cambodia Water Supply Authority',
    'Chaney International',
    'Chip Mong Group',
    'Credit Suisse (Cambodia) Ltd',
    'Dtac Cambodia',
    'Fubon Bank (Cambodia) Ltd',
    'GMAC Cambodia',
    'Harvest Development',
    'Hun Sen Foundation',
    'ING (Cambodia) Ltd',
    'Khmer Times',
    'Khmer Tire Corporation',
    'Krung Thai Bank (Cambodia) Ltd',
    'LOréal Cambodia',
    'Mahosot Hospital',
    'Mekong Capital',
    'Mitsubishi Corporation (Cambodia) Ltd',
    'NagaCorp',
    'Num Banh Chok',
    'Phnom Penh Water Supply Authority',
    'Rising Sun Beer',
    'Seagate Technology (Cambodia) Ltd',
    'Sokha Hotels and Resorts',
    'Standard Chartered Bank (Cambodia) Ltd',
    'Sushi Tei',
    'Technogym Cambodia',
    'Union Development Group',
    'Wing (Cambodia) Ltd',
    'WorldBridge International',
  ];
List<String> jobsTitle = [
    "Software Developer",
    "Data Scientist",
    "Artificial Intelligence (AI) Specialist",
    "Blockchain Developer",
    "UX/UI Designer",
    "Cyber Security Engineer",
    "Cloud Architect",
    "Data Analyst",
    "Digital Marketing Specialist",
    "Robotics Engineer",
    "Renewable Energy Engineer",
    "Sustainability Consultant",
    "Medical Technologist",
    "Registered Nurse",
    "Physician Assistant",
    "Home Health Aide",
    "Chef",
    "Financial Manager",
    "Project Manager",
    "Teacher",
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(
        backgroundColor: AppColor.thirdcolor,
        elevation: 0,
        iconTheme: IconThemeData(
            color: AppColor.primarycolor
        ),
      ),
      backgroundColor: AppColor.thirdcolor,
      body: Container(
        margin: EdgeInsets.symmetric(vertical: 0,horizontal: 0),

        child: SingleChildScrollView(


          child: Container(

            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 48,horizontal: 50),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.max,

                children: [
                  Lottie.asset('assets/images/35132-people-with-mobile-phones.json',
                    fit: BoxFit.cover,
                    alignment: Alignment.center,

                    //
                    // //
                    // width: 112,
                    // height: 330,

                  ),

                  // Lottie.network('https://assets10.lottiefiles.com/packages/lf20_z4uccqir.json',
                  //   fit: BoxFit.cover,
                  //   alignment: Alignment.center,
                  //
                  //   //
                  //   // //
                  //   // width: 112,
                  //   // height: 330,
                  //
                  // ),

                  SizedBox(
                    height: 24,
                  ),
                  Center(
                    child: RichText(

                      textAlign: TextAlign.center,
                      text: TextSpan(
                          text: 'Describe ',
                          style: TextStyle(
                            fontSize: AppFont.megafont,
                            fontWeight: FontWeight.w500,
                            color: Colors.white,



                          ),

                          children: [
                            TextSpan(
                              text: 'Your Self',
                              style: TextStyle(
                                fontSize: AppFont.megafont,
                                fontWeight: FontWeight.w500,
                                color: AppColor.bgcolor,



                              ),
                            )
                          ]

                      ),


                      maxLines: 10,


                    ),
                  ),
                  SizedBox(height: 20,),

                  Center(
                    child:
                        widget.choice == 'Job Seeker'?

                    Text('What is your current or previous position ?',
                      textAlign: TextAlign.center,

                      style: TextStyle(
                        color:AppColor.bgcolor,
                        fontSize: AppFont.medfont,
                        fontWeight: FontWeight.w400,



                      ),
                      maxLines: 10,
                    ) :
                        Text('What is your agency or company names ?',
                          textAlign: TextAlign.center,

                          style: TextStyle(
                            color:AppColor.bgcolor,
                            fontSize: AppFont.medfont,
                            fontWeight: FontWeight.w400,



                          ),
                          maxLines: 10,
                        )

                    ,
                  ),
                  Container(child: SizedBox(height: 20,)),

                  Container(
                    height: 68,
                    child:
                    widget.choice == 'Hiring Person'?
                    Form(
                      key: keyposition,

                      child: TypeAheadFormField(

                        textFieldConfiguration: TextFieldConfiguration(
                            controller:   txtcompany,
                            onSubmitted: (value) {
                              setState(() {
                                txtcompany.text = value;
                              });
                            },

                            decoration: InputDecoration(
                               hintText: 'Companies Name',
                              suffix: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    txtcompany.clear();
                                  });
                                },

                                  child: Icon(Icons.highlight_remove_rounded,size: 15)),

                              filled: true,
                              enabledBorder: OutlineInputBorder(

                                borderRadius: BorderRadius.circular(10),


                                borderSide: BorderSide(
                                    color: Colors.white



                                ),

                              ),
                              fillColor: AppColor.whitecolor,
                             focusedBorder: OutlineInputBorder(


                            borderRadius: BorderRadius.circular(10),




                          borderSide: BorderSide(
                              color: AppColor.primarycolor



                          ),

                        ),
                            ),



                          style: TextStyle(
                            color: AppColor.primarycolor,
                            fontSize: AppFont.smfont+2
                          )
                        ),
                        suggestionsCallback: (pattern) {

                           return companies.where((element) => element.toLowerCase().
                            startsWith(txtcompany.text.toLowerCase())

                            );


                        },
                        itemBuilder: (context, suggestion) {
                          return Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10)
                            ),
                            child: ListTile(
                              title: Text(suggestion.toString(),style: TextStyle(

                                color: AppColor.primarycolor,
                                fontSize: AppFont.smfont+2
                              ),),
                            ),
                          );
                        },



                        transitionBuilder: (context, suggestionsBox, controller) {
                          return suggestionsBox;

                        },

                        onSuggestionSelected: (suggestion) {
                          setState(() {
                           txtcompany.text = suggestion.toString();
                            print(  txtcompany.text );

                          });

                        },
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Please select a company name';
                          }
                        },
                        onSaved: (value) {

                          txtcompany.text  = value.toString();
                            print(  txtcompany.text );

                        },

                      ),
                    )


                        :


                    Form(
                      key: keyposition,

                      child: TypeAheadFormField(

                        textFieldConfiguration: TextFieldConfiguration(
                            controller: txtposition,
                            onSubmitted: (value) {
                              setState(() {
                                txtposition.text = value;
                              });
                            },

                            decoration: InputDecoration(
                              hintText: 'Your Job Position',
                              suffix: GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      txtposition.clear();
                                    });
                                  },

                                  child: Icon(Icons.highlight_remove_rounded,size: 15)),

                              filled: true,
                              enabledBorder: OutlineInputBorder(

                                borderRadius: BorderRadius.circular(10),


                                borderSide: BorderSide(
                                    color: Colors.white



                                ),

                              ),
                              fillColor: AppColor.whitecolor,
                              focusedBorder: OutlineInputBorder(


                                borderRadius: BorderRadius.circular(10),




                                borderSide: BorderSide(
                                    color: AppColor.primarycolor



                                ),

                              ),
                            ),



                            style: TextStyle(
                                color: AppColor.primarycolor,
                                fontSize: AppFont.smfont+2
                            )
                        ),
                        suggestionsCallback: (pattern) {

                          return jobsTitle.where((element) => element.toLowerCase().
                          startsWith(txtposition.text.toLowerCase())

                          );


                        },
                        itemBuilder: (context, suggestion) {
                          return Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10)
                            ),
                            child: ListTile(
                              title: Text(suggestion.toString(),style: TextStyle(

                                  color: AppColor.primarycolor,
                                  fontSize: AppFont.smfont+2
                              ),),
                            ),
                          );
                        },



                        transitionBuilder: (context, suggestionsBox, controller) {
                          return suggestionsBox;

                        },

                        onSuggestionSelected: (suggestion) {
                          setState(() {
                            txtposition.text = suggestion.toString();
                            print(txtposition.text);

                          });

                        },
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Please select a company name';
                          }
                        },
                        onSaved: (value) {

                          txtposition.text = value.toString();
                          print(txtposition.text);

                        },

                      ),
                    )
                  ),

                  SizedBox(height: 20,),
                  Container(
                    child:
                    widget.choice == 'Job Seeker' ?
                    Center(
                      child:
                      txtposition.text =='' ?
                          null :

                      IconButton(
                          onPressed: () {
                            if(txtposition.text ==''){
                              return null;
                            }

                            Navigator.push(context, MaterialPageRoute(builder: (context) {
                              print("press");
                              print(txtposition.text);

                              return Myloading_two(

                                  name: widget.name,
                                  password: widget.password,
                                  email:widget.email,

                                  choice: widget.choice,
                                  position:txtposition.text




                              );
                            },));
                          },
                          icon: Icon(
                            Icons.navigate_next_sharp,
                            size: 30,
                          ))
                    ) :

                    Center(
                        child:
                        txtcompany.text =='' ?
                        null :

                        IconButton(
                            onPressed: () {
                              if(    txtcompany.text ==''){
                                return null;
                              }

                              Navigator.push(context, MaterialPageRoute(builder: (context) {
                                print("press");
                                print(    txtcompany.text);

                                return Myloading_two(

                                    name: widget.name,
                                    password: widget.password,
                                    email:widget.email,


                                    choice: widget.choice,
                                    position:null,
                                    companyname:txtcompany.text





                                );
                              },));
                            },
                            icon: Icon(
                              Icons.navigate_next_sharp,
                              size: 30,
                            ))
                    ) ,
                  ),

                  SizedBox(height: 20,),

                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [

                      Container(
                        width: 10,
                        height: 10,
                        decoration: BoxDecoration(
                          color: Colors.black.withOpacity(0.14),
                          shape: BoxShape.circle,

                        ),
                      ),
                      SizedBox(width: 20,),
                      Container(
                        width: 10,
                        height: 10,
                        decoration: BoxDecoration(
                          color: AppColor.bgcolor,
                          shape: BoxShape.circle,

                        ),
                      ),
                      SizedBox(width: 20,),
                      Container(
                        width: 10,
                        height: 10,
                        decoration: BoxDecoration(
                          color: Colors.black.withOpacity(0.14),
                          shape: BoxShape.circle,

                        ),
                      ), //todo dot
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
