import 'package:flutter/material.dart';

import 'package:jobapp/VIews/AuthScreen/Login.dart';

import 'package:lottie/lottie.dart';

import '../../res/Appcolor.dart';
import '../../res/Appfont.dart';
import 'Load2.dart';
import 'loadposition.dart';

class Myloading_one extends StatefulWidget {
  var name;
  var email;
  var password;

 Myloading_one({Key? key,this.name,this.email,this.password}) : super(key: key);

  @override
  State<Myloading_one> createState() =>_Myloading_one();
}

class _Myloading_one extends State<Myloading_one> {
  bool value = false;
  var jobseeker ;
  var hiringperson;
  var email;
  var pass;
  var name;
  var isselected  ;
  var isselecteddown  ;
  var choice ;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // print(widget.email);
    // print(widget.password);
    // print(widget.name);
  }

  Widget build(BuildContext context) {
    pass= widget.password;
    name = widget.name;
    email = widget.email;

    return Scaffold(

      appBar: AppBar(
        backgroundColor: AppColor.thirdcolor,
        elevation: 0,
        iconTheme: IconThemeData(
            color: AppColor.primarycolor
        ),
      ),
      backgroundColor: AppColor.thirdcolor,
      body: Container(
        margin: EdgeInsets.symmetric(vertical: 0,horizontal: 0),

        child: SingleChildScrollView(


          child: Container(

            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 48,horizontal: 50),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.max,

                children: [


                // Lottie.network('https://assets10.lottiefiles.com/packages/lf20_z4uccqir.json',
                //       fit: BoxFit.cover,
                //       alignment: Alignment.center,
                //
                //       //
                //       // //
                //       // width: 112,
                //       // height: 330,
                //
                //     ),
                  Lottie.asset('assets/images/35132-people-with-mobile-phones.json',
                    fit: BoxFit.cover,
                    alignment: Alignment.center,

                    //
                    // //
                    // width: 112,
                    // height: 330,

                  ),
                    SizedBox(
                      height: 24,
                    ),
                    Center(
                      child: RichText(

                        textAlign: TextAlign.center,
                        text: TextSpan(
                          text: 'Getting ',
                            style: TextStyle(
                              fontSize: AppFont.megafont,
                              fontWeight: FontWeight.w500,
                              color: Colors.white,



                            ),

                          children: [
                            TextSpan(
                              text: 'Started',
                          style: TextStyle(
                          fontSize: AppFont.megafont,
                          fontWeight: FontWeight.w500,
                          color: AppColor.bgcolor,



                        ),
                            )
                          ]

                        ),


                        maxLines: 10,


                      ),
                    ),
                  SizedBox(height: 30,),
                  Text('Tell me. What are you purpose of using this job finder app for?',
                    textAlign: TextAlign.center,

                    style: TextStyle(
                      color:AppColor.bgcolor,
                      fontSize: AppFont.medfont,
                      fontWeight: FontWeight.w400,



                    ),
                    maxLines: 10,
                  ),
                  SizedBox(height: 40,),


                  Center(
                    child: ElevatedButton(
                      onPressed: () {
                        setState(() {
                          isselecteddown = false;
                          isselected = true;
                          choice = 'Hiring Person';
                          print(choice);
                        });
                      },
                        // onPressed: ()=>Navigator.push(context, MaterialPageRoute(builder: (context) =>)),
                        style: ElevatedButton.styleFrom(
                            backgroundColor:isselected == true ? AppColor.bgcolor : AppColor.whitecolor,

                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10) ,
                            )
                        ),

                      child:         Padding(
                        padding: const EdgeInsets.all(13.0),
                        child: Text('I’m a Hire agent from  company   ',




                          style: TextStyle(
                            color: isselected == true ? Colors.white : Colors.black,

                            fontSize: AppFont.bigfont,
                          ),
                        ),
                      ),

                    ),
                  ),
                  SizedBox(height: 20,),

                  Center(
                    child: ElevatedButton(
                        onPressed: () {
                          setState(() {
                            isselecteddown = true;
                            isselected = false;
                            choice = 'Job Seeker';
                            print(choice);
                          });
                        },
                        // onPressed: ()=>Navigator.push(context, MaterialPageRoute(builder: (context) =>)),
                        style: ElevatedButton.styleFrom(
                            backgroundColor: isselecteddown == true ? AppColor.bgcolor : AppColor.whitecolor,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10) ,
                            )
                        ),

                        child:         Padding(
                          padding: const EdgeInsets.all(13.0),
                          child: Text('I’m Looking for a job from other  ',



                            style: TextStyle(
                              color: isselecteddown == true ? Colors.white : Colors.black,

                              fontSize: AppFont.bigfont,
                            ),
                          ),
                        ),

                    ),
                  ),
                  SizedBox(height: 20,),
                  Center(
                    child: isselected == true || isselecteddown == true ?
                    IconButton(
                        onPressed: ()=>{
                          Navigator.push(context, MaterialPageRoute(builder: (context) {
                            return LoadPosition (
                              choice: choice,
                              name: name,
                              password: pass,
                              email: email


                            );
                          },))
                                        // Navigator.push(context, MaterialPageRoute(builder: (context) {
                                        //   return Myloading_two(
                                        //     choice: choice,
                                        //     name: name,
                                        //     password: pass,
                                        //     email: email
                                        //
                                        //
                                        //   );
                                        // },))
                        },
                        icon: Icon(
                            Icons.navigate_next,
                          size: 30,
                          color: AppColor.primarycolor,

                        )) : Text(''),
                    

                  ),
                  SizedBox(height: 20,),
                  Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [

                        Container(
                          width: 10,
                          height: 10,
                          decoration: BoxDecoration(
                            color: AppColor.bgcolor,
                            shape: BoxShape.circle,

                          ),
                        ),
                        SizedBox(width: 20,),

                        Container(
                          width: 10,
                          height: 10,
                          decoration: BoxDecoration(
                            color: Colors.black.withOpacity(0.14),
                            shape: BoxShape.circle,

                          ),
                        ), //todo dot
                      ],
                    )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
