import 'package:flutter/material.dart';

import 'package:jobapp/VIews/AuthScreen/Login.dart';
import 'package:jobapp/ViewModel/job_view_model.dart';
import 'package:jobapp/model/signuprequest_model.dart';

import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../data/Response/status_response.dart';
import '../../res/Appcolor.dart';
import '../../res/Appfont.dart';
import '../WelcomeScreen/defaultscreen.dart';
import '../WelcomeScreen/home.dart';
import '../WelcomeScreen/screendiscover.dart';
import '../WelcomeScreen/screenfindjob.dart';

class Myloading_two extends StatefulWidget {
  var choice;
  var email;
  var password;
  var name;
  var position;
  var    companyname;
 Myloading_two({Key? key,this.choice,this.email,this.password,this.name, this.position,this.   companyname}) : super(key: key);

  @override
  State<Myloading_two> createState() =>_Myloading_two();
}

class _Myloading_two extends State<Myloading_two> {
    JobViewModel registerviewmodel = JobViewModel();
    var isloading = false;

  @override



  void initState() {
    // TODO: implement initState
    super.initState();
    print("Category section");
    print(widget.choice);
    print(widget.email);
    print(widget.password);
    print(widget.name);
    print(widget.position);
    print("Company name: ${widget.companyname}");
  }
    var cateitem =[
      "Software developer",
      "Data scientist",
      "Cyber security engineer",
      "Cloud developer",
      "DevOps engineer",
      "Digital marketing specialist",
      "Project manager",
      "Product manager",
      "UX designer",
      "UI designer",
      "Content writer",
      "SEO specialist",
      "Social media manager",
      "Customer service representative",
      "Sales representative",
      "Accountant",
      "Networing"
          "Financial analyst",
      "Human resources manager",
      "Operations manager",
      "Logistics manager",
      "Supply chain manager",
      "Healthcare professional",
      "Teacher",
      "Engineer",
      "Construction worker",
      "Truck driver",
      "Retail salesperson",
    ];
  var iscategoryselect = [false,false,false,false,false
    ,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false
    ,false,false,false,false,false,false,false,false];
  Set<String> selectcategory = {};
  var choices;
  var selecteditem;
  var uid;
  bool value = false;

  Widget build(BuildContext context) {
     choices = widget.choice;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColor.thirdcolor,
        elevation: 0,
        iconTheme: IconThemeData(
          color: AppColor.primarycolor
        ),

      ),
      backgroundColor: AppColor.thirdcolor,
      body: SafeArea(

        child: Container(
          margin: EdgeInsets.symmetric(vertical: 0,horizontal: 0),

          child: ChangeNotifierProvider<JobViewModel>(
            create: (context) => registerviewmodel,
            builder: (context, child) {
              return Consumer<JobViewModel>(
                builder: (context,signup, child) {
                  var res = signup.signupApiResponse.data;
                  var status = registerviewmodel.signupApiResponse.status;
                  print(status);
                  // print
                  if (registerviewmodel.signupApiResponse.status == Status.COMPLETED){

                    print("response");

                    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                      uid = signup.signupApiResponse?.data?.data?.id;
                      var tokens = signup.signupApiResponse.data?.token;
                      var email = signup.signupApiResponse.data?.data?.email;

                      print("user id ${uid}");
                      //TODO SAVE USER ID LOGIN
                      setUpToken(uid,tokens,email);

                      if(uid !=null){
                        Navigator.push(context, MaterialPageRoute(builder: (context) =>
                            ScreenDefault(),));
                        return null;
                      }



                      // Navigator.push(context, MaterialPageRoute(builder: (context) =>ScreenDefault()));
                      // print(res.loginApiResponse.data.message);



                    });
                  }
                  else if(registerviewmodel.signupApiResponse.status == Status.LOADING ){
                    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                      //
                      // setState(() {
                      //   isloading =false;
                      // });


                      // Navigator.push(context, MaterialPageRoute(builder: (context) =>ScreenDefault()));
                      // print(res.loginApiResponse.data.message);



                    });
                  }
                  return SingleChildScrollView(


                    child: Container(

                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 48,horizontal: 50),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          mainAxisSize: MainAxisSize.max,

                          children: [
                            // Text('User choice: ${choices}'),

                            Center(
                              child: RichText(

                                textAlign: TextAlign.center,
                                text: TextSpan(
                                    text: 'Let\'s ',
                                    style: TextStyle(
                                      fontSize: AppFont.megafont,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.white,



                                    ),

                                    children: [
                                      TextSpan(
                                        text: 'Started',
                                        style: TextStyle(
                                          fontSize: AppFont.megafont,
                                          fontWeight: FontWeight.w600,
                                          color: AppColor.bgcolor,



                                        ),
                                      )
                                    ]

                                ),


                                maxLines: 10,


                              ),
                            ),
                            SizedBox(height: 30,),
                            Text('Tell us, Which type of career are you most interesting in? ',
                              textAlign: TextAlign.center,

                              style: TextStyle(
                                color:AppColor.bgcolor,
                                fontSize: AppFont.medfont,
                                fontWeight: FontWeight.w500,



                              ),
                              maxLines: 10,
                            ),
                            SizedBox(height: 40,),





                            Container(

                              width: double.maxFinite,
                              child: Wrap(
                                //TODO Categories chip

                                alignment: WrapAlignment.spaceBetween,
                                runSpacing: 2,
                                spacing: 4,
                                clipBehavior: Clip.antiAliasWithSaveLayer,

                                direction: Axis.horizontal,
                                children: List.generate( cateitem.length, (index) =>
                                    GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          selectcategory.add(cateitem[index]);
                                          print(cateitem[index]);
                                        });

                                      },
                                      child: InputChip(

                                        label: Text('${cateitem[index]}',style:
                                        TextStyle(
                                          color: iscategoryselect[index] ==true ? Colors.white : Colors.black
                                          ,   fontSize: AppFont.smfont+1,

                                        ),),
                                        elevation: 0,

                                        backgroundColor:AppColor.whitecolor ,
                                        selected:iscategoryselect[index] == true ? true : false ,


                                        onDeleted: () {

                                          setState(() {
                                            iscategoryselect[index] = false;
                                            selectcategory.remove(cateitem[index]);
                                          });

                                        },



                                        onSelected: (value) {
                                          setState(() {
                                            iscategoryselect[index] =true;
                                            // print(cateitem[index]);

                                            selectcategory.add(cateitem[index]);
                                            // selectcategory.add(value.toString());
                                            // print(   selectcategory);


                                          });
                                          // print(selectcategory);
                                        },
                                        selectedColor: AppColor.bgcolor,
                                        isEnabled: true,
                                        deleteIconColor: Colors.white,
                                        iconTheme: IconThemeData(
                                            color: Colors.white
                                        ),
                                        checkmarkColor: Colors.white,
                                        // selectedShadowColor: Colors.white,


                                      ),
                                    ),),

                              ),
                            ), //TODO CATEGORY CHOOSE






                            SizedBox(height: 25,),
                            //TODO Comfirm button here
                            Container(
                              child: selectcategory.length > 0 ?
                              ElevatedButton(
                                  onPressed: ()   async {
                                    setState(() {
                                      isloading = true;
                                    });
                                    var interestlist  = selectcategory.toList();
                                    print("submiting");
                                    // print(interestlist );
                                    print(widget.position);
                                    var object = RegisterRequest(
                                        name: widget.name,
                                        password: widget.password,
                                        email: widget.email,

                                        role: widget.choice,
                                        profile: Profile(
                                          currentCompany: widget.companyname ?? 'No detail'
                                        ),
                                        position: widget.position ?? 'No detail',

                                        interest: interestlist,
                                    );
                                    print(object);

                                   await registerviewmodel.postSignup(object);
                                   setState(() {

                                   });




                                  },
                                  style:selectcategory.length > 0 ? ElevatedButton.styleFrom(


                                      backgroundColor:AppColor.bgcolor,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10) ,
                                      )
                                  ) : null,

                                  child: selectcategory.length > 0 ? Padding(
                                    padding: const EdgeInsets.all(12.0),
                                    child: isloading == true ?

                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Container(
                                          width: 20,
                                          height: 20,
                                          child: Center(
                                            child: CircularProgressIndicator(
                                              backgroundColor:Colors.indigo,
                                              color: AppColor.primarycolor,



                                            ),
                                          ),
                                        ),
                                      ],
                                    ) :
                                    Row(
                                      mainAxisAlignment:MainAxisAlignment.spaceEvenly,
                                      children: [
                                        Text('Comfirm',


                                          style: TextStyle(
                                            color: Colors.white,

                                            fontSize: AppFont.bigfont,
                                          ),
                                        ),

                                      ],
                                    )
                             ,
                                  ) : null

                              ) : null,
                            ),
                            SizedBox(height: 50,),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  width: 10,
                                  height: 10,
                                  decoration: BoxDecoration(
                                    color: Colors.black.withOpacity(0.14),
                                    shape: BoxShape.circle,

                                  ),
                                ),
                                SizedBox(width: 20,),
                                Container(
                                  width: 10,
                                  height: 10,
                                  decoration: BoxDecoration(
                                    color: Colors.black.withOpacity(0.14),
                                    shape: BoxShape.circle,

                                  ),
                                ),
                                SizedBox(width: 20,),
                                Container(
                                  width: 10,
                                  height: 10,
                                  decoration: BoxDecoration(
                                    color: AppColor.bgcolor,
                                    shape: BoxShape.circle,

                                  ),
                                ),


                              ],
                            ),

                          ],
                        ),
                      ),
                    ),
                  );
                },

              );
            },

          ),
        ),
      ),
    );
  }
}

void setUpToken(uid,tokens,email) async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  print("user here");
  print(uid);
  print(tokens);
  prefs.remove('userid');
  prefs.remove('tokens');
  prefs.remove('password');
  prefs.setString('userid',uid ?? 'noToken' );
  prefs.setString('tokens', tokens);
  prefs.setString('email',email);


}
