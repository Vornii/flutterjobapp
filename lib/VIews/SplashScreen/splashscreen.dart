
import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jobapp/VIews/WelcomeScreen/defaultscreen.dart';
import 'package:jobapp/VIews/WelcomeScreen/home.dart';
import 'package:jobapp/res/Appfont.dart';
import 'package:lottie/lottie.dart';

import '../Admin/adminview.dart';
class SplashScreen extends StatefulWidget {
  var isauth;
 SplashScreen({Key? key,this.isauth}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Timer(Duration(seconds: 5), () {
    if(widget.isauth == false){
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context){
            return     ScreenDefault();

          }));
  }
    else{
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context){

            return MyHomePage();
          }));

    }
    });
  }
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:  Colors.indigo,
      body: LayoutBuilder(
        builder: (context, constraints) {
          return    Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,


            children: [
              Center(
                child: Column(

                  children: [
                    Image.asset('assets/images/Icon/Frame 2 (1).png',

                      //
                      width: constraints.maxWidth*0.5,
                      height: 146,
                      alignment: Alignment.center,
                    ),
                    SizedBox(height: 15,),
                    Text('Job Finder',style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                      fontSize: AppFont.megafont+5
                    ),),
                    SizedBox(height: 10,),
                    Text('Find less and earn more, job application',style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w400,
                        fontSize: AppFont.bigfont
                    ),)
                  ],
                ),
              ),

              Center(
                child: Lottie.asset('assets/images/Icon/135381-rocket-loading.json',
                fit: BoxFit.cover,
                  width: 200,
                  height: 200

                ),
              ),

            ],
          );
        },
      )

    );
  }
}
