import 'dart:math';

import 'package:flutter/material.dart';
import 'package:jobapp/VIews/AuthScreen/Resetpassword.dart';

import 'package:jobapp/VIews/AuthScreen/register.dart';
import 'package:jobapp/VIews/WelcomeScreen/home.dart';
import 'package:jobapp/ViewModel/job_view_model.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../data/Response/status_response.dart';
import '../../model/authrequest_model.dart';
import '../../model/authresponse_model.dart';
import '../../res/Appcolor.dart';
import '../../res/Appfont.dart';
import '../../res/Validator/loginvalidate.dart';
import '../AuthScreen/Login.dart';

class ScreenLoadingTwo extends StatelessWidget {
  const ScreenLoadingTwo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: null,
      backgroundColor: AppColor.primarycolor,
      body: Container(
        margin: EdgeInsets.symmetric(vertical: 0,horizontal: 0),

        child: SingleChildScrollView(


          child: Container(

            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 48,horizontal: 50),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                mainAxisSize: MainAxisSize.max,

                children: [
                  SizedBox(height: 130,),
                  Lottie.asset('assets/images/80356-online-learning.json',
                    fit: BoxFit.cover,
                    alignment: Alignment.center,

                    //
                    // //
                    // width: 112,
                    // height: 330,

                  ),

                  SizedBox(
                    height: 24,
                  ),
                  Center(
                    child: RichText(

                      textAlign: TextAlign.center,
                      text: TextSpan(
                          text: 'Advance Jobs Seeking Experience',
                          style: TextStyle(
                            fontSize: AppFont.megafont,
                            fontWeight: FontWeight.w500,
                            color: Colors.white,



                          ),

                          children: [

                          ]

                      ),


                      maxLines: 10,


                    ),
                  ),
                  SizedBox(height: 30,),
                  Text('No More worries in communication or finding jobs online, You are in the right place.',
                    textAlign: TextAlign.center,

                    style: TextStyle(
                      color:AppColor.whitecolor.withOpacity(0.65),
                      fontSize: AppFont.medfont,
                      fontWeight: FontWeight.w400,



                    ),
                    maxLines: 10,
                  ),
                  SizedBox(height: 40,),










                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: 10,
                        height: 10,
                        decoration: BoxDecoration(
                          color: Colors.white.withOpacity(0.14),
                          shape: BoxShape.circle,

                        ),
                      ),
                      SizedBox(width: 20,),//todo dot
                      Container(
                        width: 10,
                        height: 10,
                        decoration: BoxDecoration(
                          color: AppColor.bgcolor,
                          shape: BoxShape.circle,

                        ),
                      ),



                    ],
                  ),
                  SizedBox(height: 20,),
                  ElevatedButton(
                      onPressed:() async {
                        Navigator.push(context, MaterialPageRoute(builder: (context) {
                          return  MyHomePage();
                        },));



                      },



                      style: ElevatedButton.styleFrom(
                          backgroundColor:Colors.indigo,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10) ,
                          )
                      ),

                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Row(
                          mainAxisAlignment:MainAxisAlignment.spaceEvenly,
                          children: [
                            Text('Continue',


                              style: TextStyle(
                                color: Colors.white,

                                fontSize: AppFont.bigfont,
                              ),
                            ),

                          ],
                        ),
                      )

                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}


