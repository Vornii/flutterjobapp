// import 'package:url_launcher/url_launcher.dart';
//
// // if we need to use lat long we use the first function
// class MapUtils {
//   MapUtils._();
//
//   static Future<void> openMap(double latitude, double longitude) async {
//     String googleUrl =
//         'https://www.google.com/maps/dir/?api=1&destination=$latitude,$longitude';
//     if (await canLaunch(googleUrl)) {
//       await launch(googleUrl);
//     } else {
//       throw 'Could not open the map.';
//     }
//   }
//
// // if a normal query is to be resolved use the second function
//   void launchMap(String address) async {
//     String query = Uri.encodeComponent(address);
//     String googleUrl = "https://www.google.com/maps/search/?api=1&query=$query";
//
//     if (await canLaunch(googleUrl)) {
//       await launch(googleUrl);
//     }
//   }