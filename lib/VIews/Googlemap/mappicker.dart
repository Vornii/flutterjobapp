import 'dart:async';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:jobapp/res/Appcolor.dart';
// import 'package:google_maps_flutter_web/google_maps_flutter_web.dart' as webGM;

class MapPicker extends StatefulWidget {
  static const DEFAULT_ZOOM = 16.4746;


  double initZoom;
  LatLng initCoordinates;

var latitude;
var longitude;
  LatLng? value;

  MapPicker(
      {Key? key,
     this.latitude,
        this.longitude,
        this.initZoom = DEFAULT_ZOOM,
        this.initCoordinates = KINSHASA_LOCATION})
      : super(key: key);
  static const KINSHASA_LOCATION = LatLng(-4.325, 15.322222);
  @override
  State<MapPicker> createState() => _MapPickerState();
}

class _MapPickerState extends State<MapPicker> {
  final Completer<GoogleMapController> _controller = Completer();
  var newlocation;
  Position? currentLocation;

  // LatLng currentlocation =  LatLng();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(currentLocation);
    // getUserLocation();
  }
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
         elevation: 0,
        backgroundColor: Colors.indigo,
        title: Text('Choose location'),

      ),
      body: Center(
        child: SizedBox(
          // width: 400,
          height: double.maxFinite,

          child: LayoutBuilder(
            builder: (context, constraints) {
              var maxWidth = constraints.biggest.width;
              var maxHeight = constraints.biggest.height;

              return SingleChildScrollView(
                child: Column(
                  children: [
                    Stack(
                      children: <Widget>[
                        SizedBox(
                          height: 600,
                          width: maxWidth,
                          child: GoogleMap(
                            initialCameraPosition: CameraPosition(
                              target:LatLng(widget!.latitude,widget!.longitude),
                              zoom: widget.initZoom,
                            ),
                            onMapCreated: (GoogleMapController controller) {
                              _controller.complete(controller);
                            },
                            onCameraMove: (CameraPosition newPosition) {
                              // print(newPosition.target.toJson());
                              //TODO Return new location value
                              widget.value = newPosition.target;

                              setState(() {
                                newlocation = widget.value;
                              });
                              print(newlocation);
                            },
                            mapType: MapType.normal,
                            myLocationButtonEnabled: true,
                            myLocationEnabled: false,
                            zoomGesturesEnabled: true,
                            padding: const EdgeInsets.all(0),
                            buildingsEnabled: true,
                            cameraTargetBounds: CameraTargetBounds.unbounded,
                            compassEnabled: true,
                            indoorViewEnabled: false,
                            mapToolbarEnabled: true,
                            minMaxZoomPreference: MinMaxZoomPreference.unbounded,
                            rotateGesturesEnabled: true,
                            scrollGesturesEnabled: true,
                            tiltGesturesEnabled: true,
                            trafficEnabled: false,
                          ),
                        ),
                        Positioned(
                          bottom: maxHeight / 2,
                          right: (maxWidth - 30) / 2,
                          child: const Icon(
                            Icons.location_pin,
                            size: 33,
                            color:Colors.indigo
                          ),
                        ),
                        Positioned(
                          bottom: 30,
                          left: 30,
                          child: Container(
                            color: Colors.white,
                            child: IconButton(
                              onPressed: () async {
                                var position = await _determinePosition();
                                final GoogleMapController controller =
                                await _controller.future;
                                controller.animateCamera(
                                    CameraUpdate.newCameraPosition(CameraPosition(
                                        target: LatLng(
                                            position.latitude, position.longitude),
                                        zoom: widget.initZoom)));
                              },
                              icon: const Icon(Icons.my_location),
                            ),
                          ),
                        ),



                      ],
                    ),
                    SingleChildScrollView(
                      
                      child: Container(
                        margin: EdgeInsets.symmetric(vertical: 20),
                        padding: EdgeInsets.all(10),

                        width: double.maxFinite,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 22,vertical: 0),
                          child: ElevatedButton(onPressed:()  async {


                            setState(() {
                              newlocation = widget.value;
                              print(newlocation);
                              Navigator.pop(context, newlocation);
                            });
                            //





                            // postapi(object);


                            //TODO POST HERE


                            // print(controller.toList());
                          },
                              style: ButtonStyle(
                                  backgroundColor: MaterialStatePropertyAll(AppColor.bgcolor),
                                  shape: MaterialStatePropertyAll(RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(6)
                                  ))
                              ),child:Text('Comfirm Location')
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }
  //
  // getUserLocation() async {
  //   currentLocation = await locateUser();
  //   setState(() {
  //     _center = LatLng(currentLocation.latitude, currentLocation.longitude);
  //   });
  //   print('center $_center');
  // }
  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    return await Geolocator.getCurrentPosition();
  }
}