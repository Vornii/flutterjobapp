import 'package:bulleted_list/bulleted_list.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart';
import 'package:jobapp/VIews/CRUD/editmessage.dart';
import 'package:jobapp/ViewModel/job_view_model.dart';
import 'package:jobapp/model/message_model.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:timeago/timeago.dart' as timeago;


import '../../../../res/Appcolor.dart';
import '../../../../res/Appfont.dart';
import '../../../data/Response/status_response.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../model/CRUD/UpdateAvRequest.dart';
import '../../model/message_model.dart';
import '../CRUD/editpostview.dart';
import '../Jobs_details/Msg/msg_detail.dart';
import '../Jobs_details/job_detail.dart';
import '../Jobs_details/pfscreen.dart';
import '../Jobs_details/postjob_screen.dart';
import '../WelcomeScreen/defaultscreen.dart';
import 'SearchJobsAdmin.dart';


class postadminview extends StatefulWidget {
  var message;
  var  isadmin;
  postadminview({
    super.key,
    this.message,
    this.isadmin
  });

  @override
  State<postadminview> createState() => _postadminviewState();
}

class _postadminviewState extends State<postadminview> {
  JobViewModel postviewmodel = JobViewModel();
  var jobid;
  var tokenid;
  var   isMsg;
  var isloading = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isMsg = widget.message;
    print("msg: ${isMsg}");
    setupfetch();
  }
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   backgroundColor: AppColor.primarycolor,
      //   elevation: 0,
      //   automaticallyImplyLeading:open == null ? true : false , //set to false = no go back bar
      //   iconTheme: IconThemeData(
      //       color: Colors.white
      //   ),
      //   title: Row(
      //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //     mainAxisSize: MainAxisSize.max,
      //     children: [
      //
      //       Column(
      //         crossAxisAlignment: CrossAxisAlignment.start,
      //         children: [
      //           Text('Message'),
      //           SizedBox(height: 3,),
      //           Row(
      //             children: [
      //               Text('${joblen} Posts - ',style: TextStyle(
      //                 fontSize: AppFont.smfont+1,
      //                 color: Colors.grey
      //               ),),
      //               Text('${    messagelen} Messages',style: TextStyle(
      //                   fontSize: AppFont.smfont+1,
      //                   color: Colors.grey
      //               ),),
      //             ],
      //           )
      //
      //         ],
      //       ),
      //       GestureDetector(
      //         onTap: () {
      //           setState(() {
      //             print("hello world");
      //             if(customicon == Icons.search){
      //
      //               iconbar = true;
      //             }
      //           });
      //
      //
      //         },
      //           child:  iconbar == true ?
      //         TextField(
      //           decoration: InputDecoration(
      //             fillColor: AppColor.whitecolor,
      //             focusedBorder: UnderlineInputBorder(
      //                borderSide: BorderSide(
      //                  color: AppColor.primarycolor
      //                )
      //             ),
      //
      //
      //
      //             filled: true,
      //             enabled: true,
      //             focusColor:Colors.white ,
      //
      //             prefixIcon: Icon(Icons.search_sharp,color: AppColor.primarycolor,),
      //             hintText: 'Search Messages Title',
      //
      //
      //           ),
      //           // controller: txtsearch,
      //           onSubmitted:(value) {
      //
      //             setState(() {
      //
      //
      //             });
      //
      //           },
      //
      //           enabled: true ,
      //
      //
      //
      //         ):
      //               Icon(Icons.search)
      //
      //
      //
      //       )
      //
      //       // Text('10 messages',style: TextStyle(
      //       //   fontSize: AppFont.smfont+1
      //       // ),),
      //     ],
      //   ),
      //   centerTitle: true,
      //
      // ),
      backgroundColor: AppColor.thirdcolor,
      body: ChangeNotifierProvider<JobViewModel>(
        create:(context) =>   postviewmodel,


        builder: (context, child) {


     return

       Consumer<JobViewModel>(
              builder: (context, jobs, child) {
                var state = jobs.jobApiResponseAll.status;
                var jobitem = jobs.jobApiResponseAll.data?.data?.attributes;
                print(jobitem?.length);
                print(state);
                if(jobs.PostDeleteResponse.status != Status.LOADING){
                  WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                    ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(content: Text('Your job announcement has been deleted!!!'))
                    );
                    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(
                        builder: (context) =>  postadminview(isadmin: true,)), (Route route) => route.isFirst);




                  });
                }
               switch(state){

                 case Status.COMPLETED:
                   return CustomScrollView(
                     slivers: [

                       SliverAppBar(

                         expandedHeight: 120,
                         elevation: 0,


                         automaticallyImplyLeading:false,

                         pinned: true,
                         titleSpacing: 0,


                         flexibleSpace:  FlexibleSpaceBar(
                           collapseMode: CollapseMode.parallax,
                           titlePadding: EdgeInsets.symmetric(vertical: 15,horizontal: 40),



                           title:
                               widget.isadmin == true ?
                               Column(
                                 mainAxisAlignment: MainAxisAlignment.end,
                                 crossAxisAlignment: CrossAxisAlignment.start,
                                 children: [

                                   Row(
                                     children: [
                                       Image.asset(
                                         'assets/images/Icon/business-analysis.png',
                                         fit: BoxFit.contain,
                                         height: 30,
                                         width: 30,
                                       ),
                                       SizedBox(width: 5,),
                                       Expanded(
                                         child: Text('Job Announcement',style: TextStyle(
                                             fontSize: AppFont.megafont-6,

                                             fontWeight: FontWeight.w500
                                         ),),
                                       ),
                                       InkWell(
                                         onTap: () {
                                           Navigator.push(context, MaterialPageRoute(builder: (context) {
                                             return adminjobsearch(job:jobs.jobApiResponseAll.data!.data!.attributes);
                                           },));
                                         },
                                           child: Icon(Icons.search,color: AppColor.whitecolor,size: 15,)) ,

                                     ],
                                   ),
                                   SizedBox(height: 6,),
                                   Row(
                                     mainAxisAlignment: MainAxisAlignment.spaceAround,
                                     crossAxisAlignment: CrossAxisAlignment.center,

                                     children: [
                                       Expanded(
                                         child: Text('Total Announcement: ${jobitem?.length ?? 0}',style: TextStyle(
                                             fontSize: AppFont.smfont-2,
                                             fontWeight: FontWeight.w400,
                                           color: Colors.grey
                                         ),),
                                       ),






                                     ],
                                   ),

                                 ],
                               ) :


                           Column(
                             mainAxisAlignment: MainAxisAlignment.end,
                             crossAxisAlignment: CrossAxisAlignment.start,
                             children: [

                               Row(
                                 children: [
                                   Image.network(
                                     'https://static.vecteezy.com/system/resources/previews/008/509/301/original/3d-mail-email-message-icon-illustration-png.png',
                                     fit: BoxFit.contain,
                                     height: 30,
                                     width: 30,
                                   ),
                                   Expanded(
                                     child: Text('Job Announcement',style: TextStyle(
                                         fontSize: AppFont.megafont-4,

                                         fontWeight: FontWeight.w500
                                     ),),
                                   ),

                                 ],
                               ),
                               SizedBox(height: 2,),
                               Row(
                                 mainAxisAlignment: MainAxisAlignment.spaceBetween,

                                 children: [
                                   Expanded(
                                     child: Text('Total Announcement: ${jobitem?.length ?? 0}',style: TextStyle(
                                         fontSize: AppFont.smfont-2,
                                         fontWeight: FontWeight.w400
                                     ),),
                                   )


                                 ],
                               ),
                             ],
                           ),


                         ),

                         // title: Text('Inbox'),

                         backgroundColor: AppColor.primarycolor,


                       ),
                       SliverList(
                           delegate: SliverChildBuilderDelegate(
                             childCount: jobitem?.length ?? 0,
                                 (context, index) {
                               return
                                 RefreshIndicator(
                                   onRefresh:() async{

                                   },
                                   backgroundColor: AppColor.primarycolor,
                                   child: Container(
                                     padding: EdgeInsets.symmetric(vertical: 0),
                                     child:

                                     isloading == true ?
                                     Column(
                                       children: [
                                     Center(
                                     child: CircularProgressIndicator(
                                     backgroundColor: AppColor.primarycolor,
                                     ))


                                       ]
                                     ):
                                     Column(
                                       children: [
                                         GestureDetector(
                                           child:

                                           Card(

                                             margin: EdgeInsets.symmetric(vertical: 0,horizontal: 0),
                                             color: AppColor.thirdcolor,
                                             elevation: 0,


                                             child:

                                             Column(

                                               crossAxisAlignment: CrossAxisAlignment.start,
                                               children: [

                                                 ListTile(

                                                   tileColor: AppColor.thirdcolor,


                                                   leading:IconButton.outlined(onPressed: () {
                                                     showDialog(context: context, builder: (context) {
                                                       return AlertDialog(
                                                         elevation: 0,
                                                         shape: RoundedRectangleBorder(
                                                             borderRadius: BorderRadius.circular(10)
                                                         ),




                                                         backgroundColor: Colors.white,

                                                         scrollable: true,

                                                         content: Column(


                                                           children: [

                                                             Row(
                                                               mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                               children: [
                                                                 Text('Modify',style: TextStyle(
                                                                     color: AppColor.primarycolor,
                                                                     fontSize: AppFont.bigfont+5,
                                                                     fontWeight: FontWeight.w500

                                                                 ),),

                                                                 IconButton(onPressed: ()=>Navigator.pop(context),

                                                                     icon: Icon(Icons.close,color: AppColor.primarycolor,)
                                                                 )

                                                               ],
                                                             ),

                                                             Divider(color: AppColor.primarycolor),


                                                             Container(

                                                               child: Row(
                                                                 children: [


                                                                   Expanded(

                                                                     child: TextButton(onPressed: () async{
                                                                       Navigator.push(context,MaterialPageRoute(builder: (context) =>

                                                                           CreateScreen(isUpdate: true,jobspost:jobitem?[index],admin:true)
                                                                         ,));



                                                                     },
                                                                         style: ButtonStyle(


                                                                         ),

                                                                         child: Row(
                                                                           children: [
                                                                             Icon(Icons.edit,size: 20,color: AppColor.primarycolor,),
                                                                             SizedBox(width: 10,),
                                                                             Text('Edit post',style: TextStyle(
                                                                                 color: AppColor.primarycolor,
                                                                                 fontSize: AppFont.bigfont

                                                                             ),),
                                                                           ],
                                                                         )),
                                                                   ),


                                                                 ],
                                                               ),
                                                             ),








                                                             Container(

                                                               child: Row(
                                                                 children: [


                                                                   Expanded(

                                                                     child: TextButton(onPressed: ()  async{
                                                                       print("Press delete");
                                                                       // setState(() {
                                                                       //
                                                                       //
                                                                       // });
                                                                       var jid = jobitem?[index].sId;
                                                                       // print(joblist?[index].sId);
                                                                       print(jobid);
                                                                       print(tokenid);
                                                                       // setState(() {
                                                                       //      isloading = true;
                                                                       // });
                                                                       Navigator.pop(context);
                                                                       showDialog(context: context, builder: (context) {
                                                                         return AlertDialog(
                                                                           elevation: 0,





                                                                           backgroundColor:AppColor.thirdcolor,

                                                                           // scrollable: true,




                                                                           title: Text('Warning',style: TextStyle(
                                                                               fontSize: AppFont.bigfont+5,
                                                                               color: AppColor.fourthcolor,
                                                                               fontWeight: FontWeight.w500
                                                                           ),),
                                                                           content: Text('Are you sure to delete this post?',style: TextStyle(
                                                                               fontSize: AppFont.bigfont,
                                                                               color: AppColor.primarycolor
                                                                           ),),
                                                                           actions: [
                                                                             Padding(
                                                                               padding: const EdgeInsets.all(8.0),
                                                                               child: GestureDetector(


                                                                                 child: Text('CANCEL',style: TextStyle(
                                                                                   fontSize: AppFont.bigfont,
                                                                                     color: AppColor.primarycolor

                                                                                 ),



                                                                                 ),
                                                                                 onTap: () {
                                                                                   Navigator.pop(context);
                                                                                 },
                                                                               ),
                                                                             ),
                                                                             Padding(
                                                                               padding: const EdgeInsets.all(8.0),
                                                                               child: GestureDetector(

                                                                                 child: Text('DELETE',style: TextStyle(
                                                                                     fontSize: AppFont.bigfont,
                                                                                     color: AppColor.fourthcolor
                                                                                 ),



                                                                                 ),
                                                                                 onTap: () async{
                                                                                   await postviewmodel.deleteByJobId(jid, tokenid);

                                                                                 },
                                                                               ),
                                                                             )
                                                                           ],


                                                                         );
                                                                       },
                                                                       );






                                                                     },
                                                                         style: ButtonStyle(

                                                                         ),

                                                                         child: Row(
                                                                           children: [
                                                                             Icon(Icons.delete,size: 20,          color: AppColor.fourthcolor,),
                                                                             SizedBox(width: 10,),
                                                                             Text('Delete post',style: TextStyle(
                                                                                 color: AppColor.fourthcolor,
                                                                                 fontSize: AppFont.bigfont

                                                                             ),),
                                                                           ],
                                                                         )),
                                                                   ),


                                                                 ],
                                                               ),
                                                             ),






                                                           ],
                                                         )  ,

                                                         // title: Text('Edit or Modify',style: TextStyle(
                                                         //   fontSize: AppFont.bigfont
                                                         // ),),


                                                       );
                                                     },);
                                                   }, icon: Icon(Icons.more_vert,color: Colors.blueGrey,)),
                                                   contentPadding: EdgeInsets.all(15),



                                                   title: Row(
                                                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                     children: [

                                                       Expanded(
                                                         child: Text('${jobitem?[index].title}',style: TextStyle(
                                                           fontWeight: FontWeight.w600,
                                                           color: AppColor.primarycolor,

                                                             overflow: TextOverflow.ellipsis


                                                         ),
                                                           maxLines: 1,
                                                           
                                                           overflow: TextOverflow.ellipsis

                                                         ),
                                                       ),



                                                     ],
                                                   ),

                                                   subtitle: Column(
                                                     crossAxisAlignment: CrossAxisAlignment.start,
                                                     children: [
                                                       SizedBox(height: 5,),

                                                       Text('${jobitem?[index].jobDescription?.detail}',style: TextStyle(
                                                           fontWeight: FontWeight.w400,
                                                           color: AppColor.primarycolor
                                                       ),
                                                         maxLines: 2,
                                                         overflow: TextOverflow.ellipsis,

                                                       ),

                                                     ],
                                                   ),
                                                   trailing: GestureDetector(
                                                       onTap: () {
                                                         Navigator.push(context, MaterialPageRoute(builder: (context) {
                                                           return JobDetailSr(uid:jobitem![index].sId);
                                                         },));

                                                       },

                                                       child: Text('View',style: TextStyle(

                                                         color: AppColor.primarycolor.withOpacity(0.75),
                                                         fontWeight:  FontWeight.w400,
                                                         fontSize: AppFont.smfont+1

                                                       ),)),



                                                 ),
                                                 Divider(),

                                                 SizedBox(height: 20,),
                                               ],
                                             ),
                                           ),
                                         ),

                                       ],
                                     ),
                                   ),
                                 );






                             },



                           )


                       ),

                       SliverToBoxAdapter(
                         child: jobitem?.length == null || jobitem?.length ==0 ?
                         Container(
                           margin: EdgeInsets.symmetric(vertical: 100),
                           child: Center(child: Column(
                             children: [

                               Image.network('https://cdn3d.iconscout.com/3d/premium/thumb/mail-download-6985959-5691446.png',


                                   fit: BoxFit.cover,
                                   width: 200,
                                   height: 200

                               ),
                               Text('No new Jobs Announcement ' ,style: TextStyle(
                                   color: AppColor.primarycolor,
                                   fontSize: AppFont.bigfont,
                                   fontWeight: FontWeight.w600

                               ),),
                               SizedBox(height: 5,),

                               Text('There is seem to be no new post by users yet' ,style: TextStyle(
                                   color: AppColor.primarycolor,
                                   fontSize: AppFont.smfont+1,
                                   fontWeight: FontWeight.w400

                               ),),
                             ],
                           ),),
                         )
                             : null,
                       )



                     ],

                   );
                 case Status.LOADING:
                   return Center(
                     child: CircularProgressIndicator(
                       backgroundColor:Colors.indigo,
                       color: AppColor.primarycolor,



                     ),
                   );

                 case Status.ERROR:
                   return Text('Error has been occur');
                 default:
                   return Text('null');

               }
              },

            );
        },

      ),
    );


  }

  void setupfetch()  async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String? id = prefs.getString('userid');
    tokenid = prefs.get('tokens');

    print("user id ${id}");


      await postviewmodel.getJobPostsByAll(id);


  }

  void _updateuserpost(postid,status) async {


    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? uid = prefs.getString('userid');
    print(uid);
    var token = prefs.get('tokens');
    if(status == true){
      status = false;
    }
    else{
      status = true;
    }
    print(status);


    print(token);
    print(postid  );

    await postviewmodel.updateUserClient(uid,postid,status, token);
  }
}
