

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:jobapp/VIews/AuthScreen/Login.dart';
import 'package:jobapp/VIews/AuthScreen/register.dart';
import 'package:jobapp/VIews/Jobs_details/filterform.dart';
import 'package:jobapp/VIews/WelcomeScreen/HiringScreen.dart';
import 'package:jobapp/VIews/WelcomeScreen/JobSeekerScreen.dart';
import 'package:jobapp/VIews/WelcomeScreen/screendiscover.dart';
import 'package:jobapp/VIews/WelcomeScreen/screenfindjob.dart';
import 'package:jobapp/VIews/Widgets/Joblistv1.dart';
import 'package:jobapp/ViewModel/job_view_model.dart';
import 'package:jobapp/res/BaseUrl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../data/Response/status_response.dart';
import '../../res/Appcolor.dart';
import '../../res/Appfont.dart';
import '../Admin/adminview.dart';
import '../CRUD/jobspost.dart';
import '../Jobs_details/pfscreen.dart';
import '../Jobs_details/Msg/messages.dart';
import '../Jobs_details/postjob_screen.dart';
import '../WelcomeScreen/defaultscreen.dart';
import '../WelcomeScreen/home.dart';
import 'UserAdminView.dart';
import 'adminpostview.dart';
class  AdminScreen extends StatefulWidget {
  var token;
  var uid;
  var tb;
   AdminScreen({Key? key,this.token,this.uid,this.tb  }) : super(key: key);

  @override
  State<AdminScreen> createState() => _AdminScreenState();
}

class _AdminScreenState extends State<AdminScreen> {
  var gridtitle = ['Home','User','Jobs','Create Jobs','Logoff'];
  var gridicon = [
    Icon(
   Icons.home_filled,color: Colors.blueAccent

    ,
    size: 56,
  ),
    Icon(
    Icons.account_circle_rounded,color: AppColor.primarycolor

    ,
    size: 56,
  ),
    Icon(
      CupertinoIcons.chart_pie_fill,color:Colors.indigo

      ,
      size: 56,
    ),

    Icon(
      Icons.poll_sharp,color:Colors.orangeAccent

      ,
      size: 56,
    ),
    Icon(
     Icons.login_outlined,color:Colors.redAccent

      ,
      size: 56,
    )

  ];
  // var gridicon = [
  //   Image.asset('assets/images/Icon/business-analysis.png',
  //     fit: BoxFit.contain,
  //     width: 80,
  //     height: 80,
  //
  //   ),
  //   Image.asset('assets/images/Icon/pngwing.com.png',
  //     fit: BoxFit.contain,
  //     width: 80,
  //     height: 80,
  //
  //   ),
  //   Image.asset('assets/images/Icon/faq-file.png',
  //     fit: BoxFit.contain,
  //     width: 80,
  //     height: 80,
  //
  //   ),
  //   Image.asset('assets/images/Icon/chatting.png',
  //     fit: BoxFit.cover,
  //     width: 190,
  //     height: 90,
  //
  //
  //   ),
  //   Image.asset('assets/images/Icon/business-analysis.png',
  //     fit: BoxFit.cover,
  //     width: 90,
  //     height: 90,
  //
  //   ),
  //   Image.asset('assets/images/Icon/business-analysis.png',
  //     fit: BoxFit.cover,
  //     width: 90,
  //     height: 90,
  //
  //   )
  //
  // ];
  var uids;
  var tokenss;
  var griddesc = [

    'Return to client screen',
    'Check all users account ',
    'Check jobs post ',
    'Create new jobs  ',
    'Switch from this account',
  ];
  var screenlist = [
    ScreenHiring() ,
    useradminview(isadmin : true),
    postadminview(isadmin : true),
    CreateScreen(admin: true,isUpdate: false,),
    MyHomePage() ,

  ];


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.uid = uids;
    widget.token = tokenss;
    print("Thumbnail link  ${widget.tb  } ");



  }
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar(
        backgroundColor: AppColor.primarycolor,
        title:Padding(
          padding: const EdgeInsets.all(18.0),
          child: Text('Admin Dashboard'),
        ),
        elevation: 0,



        //
        // centerTitle: true,

        actions: [


          GestureDetector(
            onTap: () => Navigator.push(context,MaterialPageRoute(builder: (context) => PfScreen(),)),
            child: CircleAvatar(
                maxRadius: 20,
                backgroundColor: Colors.white24,
                backgroundImage:
                    widget.tb == null ?


                NetworkImage('https://i.pinimg.com/564x/48/6c/a0/486ca00640b169300b48e9ceacd8e401.jpg') :

                    NetworkImage('${ApiApiUrl.localurl}/${widget.tb}')

            ),
          ),
          SizedBox(width: 15,)
        ],


        automaticallyImplyLeading:false,
      ) ,
      backgroundColor: AppColor.thirdcolor,
      body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [



              
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 45,horizontal: 18),
                  child: GridView.builder(gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                    childAspectRatio: 1,
                    crossAxisSpacing: 10,

                    mainAxisSpacing: 10

                  ),

                      itemCount: gridtitle.length,

                      itemBuilder: (context, index) {

                        return         GestureDetector(
                          onTap: () {
                            if(index == 5){
                              logoutcheck();

                            }
                            Navigator.push(context, MaterialPageRoute(builder: (context) {
                              return screenlist[index];
                            },));

                          },

                          child: Card(
                            elevation:1,
                            color:



                          Colors.white,
                            // color: AppColor.whitecolor.withOpacity(0.55),
                            // shadowColor: Colors.purpleAccent,

                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15)
                            ),

                            child: Padding(
                              padding: const EdgeInsets.all(18.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,

                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  gridicon[index],

                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text('${gridtitle[index]}',
                                        style: TextStyle(
                                            fontSize: AppFont.bigfont+2,
                                            color:AppColor.primarycolor,
                                            fontWeight: FontWeight.bold
                                        ),

                                      ),
                                      SizedBox(height: 5,),
                                      Text('${griddesc[index]}',
                                        style: TextStyle(
                                            fontSize: AppFont.bigfont-2,
                                            color:AppColor.primarycolor,
                                            fontWeight: FontWeight.w400
                                        ),

                                      ),

                                    ],
                                  )
                                ],
                              ),
                            ) ,
                          ),
                        );
                      },),
                ),
              ),
            ],
          )),
    );
  }

  void logoutcheck() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print("remove");
    prefs.remove('tokens');
    prefs.remove('userid');
  }

  // void gettokenNuser() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   setState(() {
  //     token = prefs.get('tokens');
  //     uid = prefs.getString('userid');
  //     print("admin token ${token}");
  //     print("userid ${uid }");
  //   });
  // }
}
