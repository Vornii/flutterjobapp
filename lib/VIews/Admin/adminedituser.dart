import 'dart:io';

import 'package:bulleted_list/bulleted_list.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:jobapp/model/Put/update_user_model.dart';
import 'package:jobapp/VIews/Jobs_details/pfscreen.dart';
import 'package:jobapp/VIews/WelcomeScreen/browsescreen.dart';
import 'package:jobapp/ViewModel/post_view_model.dart';
import 'package:jobapp/model/Experience_model.dart';
import 'package:jobapp/model/job_model_mock.dart';
import 'package:jobapp/res/BaseUrl.dart';
import 'package:provider/provider.dart';


import '../../ViewModel/image_view_model.dart';
import '../../ViewModel/job_view_model.dart';
import '../../data/Response/status_response.dart';
import '../../model/signuprequest_model.dart';
import '../../res/Appcolor.dart';
import '../../res/Appfont.dart';
import '../WelcomeScreen/Filterscreen.dart';
import 'UserAdminView.dart';

class CreateUser extends StatefulWidget {
  var open ;
  var userid;
  var users;
  var admin;
  var post;
  CreateUser({Key? key,this.open , this.users,this.userid,this.admin,this.post}) : super(key: key);

  @override
  State<CreateUser> createState() => _CreateUserState();
}

class _CreateUserState extends State<CreateUser> {


  Set<String> selectedlanguages= {};
  var ischeck = [false,false,false,false,false,false,false,false];
  dynamic imagefile;
  var noexperience = false;



  List<TextEditingController> controller = [];//TODO DYNAMIC CONTROLLER TEXT
  List<TextEditingController> controller_com = [];//TODO DYNAMIC CONTROLLER TEXT
  List<TextEditingController> controller_roles= [];//TODO DYNAMIC CONTROLLER TEXT
  List<TextEditingController> controller_ystart = [];//TODO DYNAMIC CONTROLLER TEXT
  List<TextEditingController> controller_yend = [];//TODO DYNAMIC CONTROLLER TEXT
  var   isSave = false;
  void _showmultiselect() async {
    final   Set<String>? result = await showDialog(context: context, builder: (context) {
      return customdialog(lists: listofcountries, check: ischeck ,selectedlanguages: selectedlanguages,);
    },);
    if(result!=null){
      setState(() {
        selectedlanguages = result; //TODO When navigator pop this return resut user selected
      });

    }
    else{

    }
  }


//TODO DYNAMIC CONTROLLER TEXT

  List<String> skills = [];
  var isSelected = false;
  var typejob;
  var txttelephone = TextEditingController();
  var txttelegram = TextEditingController();
  var txtemail = TextEditingController();
  var txtbio = TextEditingController();
  var rolesselect =[
    'Admin',
    'Hiring Person',
    'Job Seeker'
  ];
  var cateitem =[
    'Artichecture',
    'Hotel/Hospitality',
    'Finance',
    'Assistance/Secretary',
    'Technician',
    'Engineer',
    'Civil Engineer',
    'Lawyer',
    'Marketing/Sale',
    'IT',
    'Doctor/Health',
    'HR',
    'Project Manager',
    'Accountant',
    'Software Developer',
    'Social Media Specialist',
    'Sales Representative',
    'Graphic Designer ',
    'Teacher',
    'Manager'
  ];
  var iscategoryselecttwo = [false,false,false,false,false
    ,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false];
  var tid;
  var   isUpdatePf;
  JobViewModel homeviewmodel =   JobViewModel();
  int _selectedIndex = 0;
  var iscategoryselect = [false,false,false,false,false
    ,false,false,false,false,false,false];
  var isSelectedPt = false;
  var isSelectedFt = false;
  var isSelectedOt = false;
  var txtusername = TextEditingController();
  var txtposition = TextEditingController();
  var txtcurrole = TextEditingController();
  var txtcurcom = TextEditingController();
  var txtpass = TextEditingController();

  List<String?> listofcountries = ['Khmer', 'English', 'Chinese', 'Japanese', 'Korean',

    'Philippines', 'Thailand', 'French'
  ];

  Set<String> selectcategory = {};
  var allitem =[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
  var selecteditem;
  var company;
  var yearstart;
  var yearend;
  var roles;
  var explength = [1];
  var ishaveExpen = true;
  List<ExperienceModel> experiencelist = [

  ];
  var getimage = false ;
  var imagenetwork;
  var jobskills =[100];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if(widget.open == null  ){
      print(widget.userid);
      homeviewmodel.fetchUserbyId(widget.userid);
      print(widget.users.name);

      txtusername.text =  widget.users.name  ?? '';
      txtbio.text =  widget.users.profile?.bio ?? '';
      txttelephone.text =  widget.users.profile?.telephone ?? '';
      txttelegram.text =  widget.users.profile?.telegram ?? '';
      txtposition.text =  widget.users.position ?? '';
      selecteditem = widget.users.role ;

      txtcurrole.text = widget.users?.role ?? '';
      txtcurcom.text = widget.users.profile?.currentCompany ?? '';
      tid = widget.users.thumbnail?.sId ?? null;
      imagenetwork = widget.users.thumbnail?.attributes?.path;

      if(widget.users?.profile?.languages!=null){
        selectedlanguages.addAll(widget.users?.profile?.languages);
        for(int i =0;i<listofcountries.length;i++){
          print(listofcountries[i]);
          if(selectedlanguages.contains(listofcountries[i]) ){
            print("founded");
            ischeck[i] = true;

          }
        }
      }



      for (int i = 0; i < 1; i++) {

        controller.add(TextEditingController());

      }



      var length = widget.users.profile?.skill?.length;
      print("Profile skill length: ${length}");
      if(length != null){
        for(int index =0 ;index<length ;index++){
          print(index);

          jobskills.add(index);


        }
        jobskills.removeLast();
        for (int i = 0; i < length; i++) {

          controller.add(TextEditingController());
        }

        for (int i = 0; i < length; i++) {
          controller[i].text = widget.users.profile!.skill![i ];
          skills.add(controller[i].text);

        }
      }
      else{
        jobskills =[100];
        controller[0].text =='';
        skills.insert(0,controller[0].text);

      }



      if(widget.users.experience.length== null){
        print(widget.users.experience.length);
        noexperience = true;
        print("true no pfs");

        for (int i = 0; i < jobskills.length; i++) {

          controller.add(TextEditingController());

        }



        for(int index =0 ;index< jobskills.length ;index++){

          print(index);
          jobskills.add(index);

        }
        // for (int i = 0; i < explength.length; i++) {
        //
        //   controller_com.add(TextEditingController());
        //   controller_ystart.add(TextEditingController());
        //   controller_yend.add(TextEditingController());
        //   controller_roles.add(TextEditingController());
        //
        // }
        isSave = false;
      }
      else{
        isSave = true;
        noexperience = false;
        ishaveExpen =true;
        print("true");
        explength.removeLast();
        for (int index = 0; index < widget.users.experience.length; index++) {

          explength.add(index);

        }

        for (int i = 0; i < explength.length; i++) {

          controller_com.add(TextEditingController());
          controller_ystart.add(TextEditingController());
          controller_yend.add(TextEditingController());
          controller_roles.add(TextEditingController());

        }
        for (int i = 0; i < explength.length; i++) {

          controller_com[i].text = widget.users.experience[i].companyname;
          controller_roles[i].text = widget.users.experience[i].position;
          controller_ystart[i].text = widget.users.experience[i].yearend.toString();
          controller_yend[i].text = widget.users.experience[i].yearstart.toString();
          var yearstart = controller_ystart[i].text;
          var yearend = controller_yend[i].text;
          experiencelist.add(ExperienceModel(
            CompanyName:         controller_com[i].text,
            roles:          controller_roles[i].text,

            Yearst: yearstart  ,
            Yeared:yearend,
          ));



        }

        print(explength.length);
        print(experiencelist.length);
        print(experiencelist);
      }


      print("lenght skills;");
      // print(jobskills.length);
      // print(explength.length);
      if(explength.length == 0){
        explength.add(1);
        isSave = false;
        noexperience = true;
      }
      if(widget.users.profile?.skill?.length == 0){
        jobskills.add(100);
      }
      // print(widget.users?.experience);

    }
    else{

    }







  }
  Widget build(BuildContext context) {

    var open = widget.open;
    for (int i = 0; i < explength.length; i++) {

      controller_com.add(TextEditingController());
      controller_ystart.add(TextEditingController());
      controller_yend.add(TextEditingController());
      controller_roles.add(TextEditingController());

    }

    for (int i = 0; i < jobskills.length; i++) {
      controller.add(TextEditingController());
    }
    return Scaffold(
      appBar:AppBar(
        backgroundColor: Colors.indigo  ,
        elevation: 0,
        automaticallyImplyLeading:open == null ? true : false , //set to false = no go back bar
        iconTheme: IconThemeData(
            color: open == null ? Colors.white : Colors.white
        ),
        title: Text('Customize User',style: TextStyle(
            color: Colors.white
        ),),
        centerTitle: true,


      ),
      backgroundColor: AppColor.thirdcolor,
      body: SafeArea(
        child: widget.post == true ?
          ChangeNotifierProvider<  JobViewModel>(
          create: (context) => homeviewmodel ,
      builder: (context, child) {
        return SingleChildScrollView(
          child: Consumer<  JobViewModel>(
            builder:(context, images, _) {

              print(images.GetImageRes.status );
              if(images.GetImageRes.status   != Status.LOADING  ){

                WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(content:
                  Text('Image has been uploads')));
                  print("image id");
                  print(homeviewmodel.GetImageRes?.data?.id);

                  tid = homeviewmodel.GetImageRes?.data?.id;
                  getimage = true;
                  // if(getimage == true){
                  //   setState(() {
                  //     getimage = false;
                  //   });
                  // }
                  images.GetImageRes.status = Status.LOADING;


                });
              }
              if(homeviewmodel.UserUpdateResponse.status   != Status.LOADING  ){

                WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(content:
                  Text('Profile has been Updated')));

                  Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(
                      builder: (context) =>  useradminview()), (Route route) => route.isFirst);
                  // Navigator.push(context, MaterialPageRoute(builder: (context) => PfScreen(
                  //   open: true,
                  //   userid: widget.userid,
                  //   isupdate: true,
                  //
                  //
                  //
                  // ) ,));


                });
              }
              if(homeviewmodel.signupApiResponse.status == Status.COMPLETED){
                WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(content:
                  Text('User has been created')));

                  Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(
                      builder: (context) =>  useradminview()), (Route route) => route.isFirst);
                  // Navigator.push(context, MaterialPageRoute(builder: (context) => PfScreen(
                  //   open: true,
                  //   userid: widget.userid,
                  //   isupdate: true,
                  //
                  //
                  //
                  // ) ,));


                });
              }
              var thumbnail = images?.GetImageRes?.data;
              var status = homeviewmodel.userApiResponse.status;
              print(status);
              var user = homeviewmodel.userApiResponse?.data?.data;
              print("user state ${status}");

              // if(homeviewmodel.GetImageRes.status == Status.COMPLETED){
              //   WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
              //
              //
              //
              //
              //     ScaffoldMessenger.of(context).showSnackBar(
              //
              //       SnackBar(content: Text('Upload Image Successfully'))
              //
              //     );
              //   });
              //
              // }

              return Container(
                margin: EdgeInsets.symmetric(vertical: 38),
                padding: EdgeInsets.symmetric(horizontal: 23,),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: () async {

                        await _getCameraImage();


                      },
                      child: Center(
                        child:
                        tid == null ?

                        imagefile != null ?

                        ClipRRect(
                            borderRadius: BorderRadius.circular(50.0), //add border radius


                            child: Image.file(imagefile,fit:BoxFit.cover,width: 100,height: 100,)):
                        CircleAvatar(
                            radius: 50,
                            backgroundColor: AppColor.primarycolor,


                            child: IconButton(
                              onPressed:() {



                                setState(() {
                                  _getCameraImage();
                                });

                              },
                              icon: Icon(Icons.add) ,
                            )


                        )


                            :    ClipRRect(
                            borderRadius: BorderRadius.circular(50.0), //add border radius


                            child: Image.network('${ApiApiUrl.localurl}/${ imagenetwork }'


                              ,width: 100,
                              height: 100,
                              fit: BoxFit.cover ,)) ,

                      ),
                    ),
                    SizedBox(height: 20,),
                    GestureDetector(
                      onTap: (){

                        setState(() {
                          _getCameraImage();
                        });
                      },
                      child: GestureDetector(
                        onTap: () {
                          _getCameraImage();
                          setState(() {

                          });
                        },
                        child: Center(child: Text('Browse or Upload Profile',style: TextStyle(
                            color: AppColor.primarycolor.withOpacity(0.756),
                            fontWeight: FontWeight.w400
                        ),)),
                      ),
                    ),


                    SizedBox(height: 30,),
                    SizedBox(height: 30,),
                    TextField(
                      style: TextStyle(
                        fontSize: AppFont.smfont+1,

                        color: AppColor.primarycolor,
                        height: 2,

                      ),
                      readOnly: false,
                      controller: txtemail,
                      onSubmitted: (value) {
                        setState(() {
                          txtemail.text = value;
                        });
                      },
                      decoration: InputDecoration(
                        hintText: 'Email Address',

                        labelText: 'Email',

                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColor.primarycolor,

                                width: 2
                            )
                        ),





                        // helperText:'965689896' ,
                        labelStyle: TextStyle(
                            color: AppColor.primarycolor,
                            fontWeight: FontWeight.bold,
                            fontSize: AppFont.bigfont
                        ),
                        // suffix: Icon(
                        //   Icons.delete_forever,
                        //   size: 22,
                        //   color: AppColor.primarycolor,
                        // )



                      ),


                    ),
                    SizedBox(height: 30,),
                    TextField(
                      style: TextStyle(
                        fontSize: AppFont.smfont+1,

                        color: AppColor.primarycolor,
                        height: 2,

                      ),
                      controller: txtusername,
                      onChanged: (value) {
                        setState(() {


                          txtusername.text = value;
                          txtusername.selection = TextSelection.fromPosition(TextPosition(offset:

                          txtusername.text.length));
                        });
                      },
                      onSubmitted: (value) {
                        setState(() {
                          txtusername.text = value;
                        });
                      },
                      decoration: InputDecoration(
                        hintText: 'Sovath Roth',
                        suffix: GestureDetector(
                          onTap: () {
                            setState(() {
                              txtusername.clear();

                            });
                          },
                          child: Icon(Icons.highlight_remove_outlined,size: 18,),
                        ),

                        labelText: 'Username',
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColor.primarycolor,
                                width: 2
                            )
                        ),






                        // helperText:'965689896' ,
                        labelStyle: TextStyle(
                            color: AppColor.primarycolor,
                            fontWeight: FontWeight.bold,
                            fontSize: AppFont.bigfont
                        ),
                        // suffix: Icon(
                        //   Icons.delete_forever,
                        //   size: 22,
                        //   color: AppColor.primarycolor,
                        // )



                      ),


                    ),

                    SizedBox(height: 20,),


                    TextField(
                      style: TextStyle(
                        fontSize: AppFont.smfont+1,

                        color: AppColor.primarycolor,
                        height: 2,

                      ),
                      controller: txtposition,
                      onSubmitted: (value) {
                        setState(() {
                          txtposition.text = value;
                          print(txtposition.text);
                        });
                      },
                      onChanged: (value) {
                        setState(() {

                          print(txtposition.text);
                          txtposition.text = value;
                          txtposition.selection = TextSelection.fromPosition(TextPosition(offset:

                          txtposition.text.length));
                        });
                      },
                      decoration: InputDecoration(
                        hintText: 'IT Manager',
                        suffix: GestureDetector(
                          onTap: () {
                            setState(() {
                              txtposition.clear();

                            });
                          },
                          child: Icon(Icons.highlight_remove_outlined,size: 18,),
                        ),
                        labelText: 'Current Position',
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColor.primarycolor,
                                width: 2
                            )
                        ),





                        // helperText:'965689896' ,
                        labelStyle: TextStyle(
                            color: AppColor.primarycolor,
                            fontWeight: FontWeight.bold,
                            fontSize: AppFont.bigfont
                        ),
                        // suffix: Icon(
                        //   Icons.delete_forever,
                        //   size: 22,
                        //   color: AppColor.primarycolor,
                        // )



                      ),


                    ),
                    SizedBox(height: 30,),

                    TextField(
                      style: TextStyle(
                        fontSize: AppFont.smfont+1,

                        color: AppColor.primarycolor,
                        height: 2,

                      ),
                      controller: txtpass  ,


                      onSubmitted: (value) {
                        setState(() {
                          txtpass.text = value;

                        });
                      },
                      onChanged: (value) {
                        setState(() {
                          txtpass.text = value;
                          txtpass.selection = TextSelection.fromPosition(TextPosition(offset:

                          txtpass.text.length));

                        });
                      },
                      maxLines:1,
                      keyboardType: TextInputType.number,

                      decoration: InputDecoration(
                        hintText: 'password',
                        labelText: 'Password',
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColor.primarycolor,
                                width: 2
                            )
                        ),
                        suffix: GestureDetector(
                          onTap: () {
                            setState(() {
                              txtbio.clear();

                            });
                          },
                          child: Icon(Icons.highlight_remove_outlined,size: 18,),
                        ),






                        // helperText:'965689896' ,
                        labelStyle: TextStyle(
                            color: AppColor.primarycolor,
                            fontWeight: FontWeight.bold,
                            fontSize: AppFont.bigfont
                        ),




                      ),


                    ),



                    SizedBox(height: 30,),
                    Text('Select Your Role',style: TextStyle(
                      color: AppColor.primarycolor,
                      fontWeight: FontWeight.bold
                    ),),
                    SizedBox(height: 30,),
                    Row(
                      children: [
                        Expanded(
                          child: Container(
                            height: 45,
                            padding: EdgeInsets.all(5),

                            decoration: BoxDecoration(
                                color: AppColor.whitecolor,
                                borderRadius: BorderRadius.circular(10)
                            ),
                            child: DropdownButton(
                              borderRadius: BorderRadius.circular(12),
                              underline: SizedBox(),
                              menuMaxHeight: 400,
                              alignment: Alignment.center,
                              iconEnabledColor: AppColor.primarycolor, //TODO ARROW ICON
                              iconDisabledColor: AppColor.bgcolor,

                              autofocus: true,
                              isExpanded: true,
                              value: selecteditem==null ? rolesselect.elementAt(0) : selecteditem,
                              dropdownColor: AppColor.whitecolor,
                              focusColor: AppColor.whitecolor,



                              items:List.generate( rolesselect.length , (index){
                                return DropdownMenuItem(


                                  value:rolesselect[index],

                                  alignment: Alignment.center,

                                  enabled: true,
                                  child: Text('${rolesselect[index]}',
                                    style: TextStyle(
                                        color: AppColor.primarycolor,
                                        fontSize: AppFont.smfont+3
                                    ),
                                  ),
                                  onTap: () {

                                  },
                                );
                              }),
                              onChanged:(value) {

                                setState(() {
                                  selecteditem = value;
                                  print(selecteditem);
                                });
                              },
                              elevation: 0,

                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 30,),
                    // TextField(
                    //   style: TextStyle(
                    //     fontSize: AppFont.smfont+1,
                    //
                    //     color: AppColor.primarycolor,
                    //     height: 2,
                    //
                    //   ),
                    //   readOnly: false,
                    //   controller: txtcurrole,
                    //   onSubmitted: (value) {
                    //     setState(() {
                    //       txtcurrole.text = value;
                    //     });
                    //   },
                    //   decoration: InputDecoration(
                    //     hintText: 'Admin',
                    //
                    //     labelText: 'Role',
                    //
                    //     focusedBorder: UnderlineInputBorder(
                    //         borderSide: BorderSide(
                    //             color: AppColor.primarycolor,
                    //
                    //             width: 2
                    //         )
                    //     ),
                    //
                    //
                    //
                    //
                    //
                    //     // helperText:'965689896' ,
                    //     labelStyle: TextStyle(
                    //         color: AppColor.primarycolor,
                    //         fontWeight: FontWeight.bold,
                    //         fontSize: AppFont.bigfont
                    //     ),
                    //     // suffix: Icon(
                    //     //   Icons.delete_forever,
                    //     //   size: 22,
                    //     //   color: AppColor.primarycolor,
                    //     // )
                    //
                    //
                    //
                    //   ),
                    //
                    //
                    // ),





                    SizedBox(height: 30,),

                    Container(

                      width: double.maxFinite,
                      child: Wrap(
                        //TODO Categories chip

                        alignment: WrapAlignment.spaceBetween,
                        runSpacing: 2,
                        spacing: 4,
                        clipBehavior: Clip.antiAliasWithSaveLayer,

                        direction: Axis.horizontal,
                        children: List.generate( cateitem.length, (index) =>
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  selectcategory.add(cateitem[index]);
                                  print(cateitem[index]);
                                });

                              },
                              child: InputChip(

                                label: Text('${cateitem[index]}',style:
                                TextStyle(
                                  color: iscategoryselecttwo[index] ==true ? Colors.white : Colors.black
                                  ,   fontSize: AppFont.smfont+1,

                                ),),
                                elevation: 0,

                                backgroundColor:AppColor.whitecolor ,
                                selected:iscategoryselecttwo[index] == true ? true : false ,


                                onDeleted: () {

                                  setState(() {
                                    iscategoryselecttwo[index] = false;
                                    selectcategory.remove(cateitem[index]);
                                  });

                                },



                                onSelected: (value) {
                                  setState(() {
                                    iscategoryselecttwo[index] =true;
                                    // print(cateitem[index]);

                                    selectcategory.add(cateitem[index]);
                                    // selectcategory.add(value.toString());
                                    // print(   selectcategory);


                                  });
                                  // print(selectcategory);
                                },
                                selectedColor: AppColor.bgcolor,
                                isEnabled: true,
                                deleteIconColor: Colors.white,
                                iconTheme: IconThemeData(
                                    color: Colors.white
                                ),
                                checkmarkColor: Colors.white,
                                // selectedShadowColor: Colors.white,


                              ),
                            ),),

                      ),
                    ), //TODO CATEGORY CHOOSE
                    SizedBox(height: 30,),
                    Container(
                      child: selectcategory.length > 0 ?
                      ElevatedButton(
                          onPressed: ()   async {
                            var interestlist  = selectcategory.toList();
                            print("submiting");
                            // print(interestlist );

                            var object = RegisterRequest(
                              name: txtusername.text,
                              password: txtpass.text,
                              email: txtemail.text,

                              role: selecteditem.toString(),
                              position: txtposition.text ?? 'No detail',

                              interest: interestlist,
                            );
                            print(object);
                            await homeviewmodel.postSignup(object);

                            setState(() {

                            });


                          },
                          style:selectcategory.length > 0 ? ElevatedButton.styleFrom(


                              backgroundColor:AppColor.bgcolor,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10) ,
                              )
                          ) : null,

                          child: selectcategory.length > 0 ? Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Row(
                              mainAxisAlignment:MainAxisAlignment.spaceEvenly,
                              children: [
                                Text('Comfirm',


                                  style: TextStyle(
                                    color: Colors.white,

                                    fontSize: AppFont.bigfont,
                                  ),
                                ),

                              ],
                            ),
                          ) : null

                      ) : null,
                    ),


                    // GestureDetector(
                    //   onTap: () {
                    //     print("Skill requirement ${skills}");
                    //   },
                    //   child: Text('Check requirement'),
                    // ),






                  ],
                ),
              );

            },

          ),
        );
      },

    ) :

        ChangeNotifierProvider<  JobViewModel>(
          create: (context) => homeviewmodel ,
          builder: (context, child) {
            return SingleChildScrollView(
              child: Consumer<  JobViewModel>(
                builder:(context, images, _) {

                  print(images.GetImageRes.status );
                  if(images.GetImageRes.status   != Status.LOADING  ){

                    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content:
                      Text('Image has been uploads')));
                      print("image id");
                      print(homeviewmodel.GetImageRes?.data?.id);

                      tid = homeviewmodel.GetImageRes?.data?.id;
                      getimage = true;
                      // if(getimage == true){
                      //   setState(() {
                      //     getimage = false;
                      //   });
                      // }



                    });
                  }
                  if(homeviewmodel.UserUpdateResponse.status   != Status.LOADING  ){

                    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content:
                      Text('Profile has been Updated')));

                      Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(
                          builder: (context) =>  useradminview()), (Route route) => route.isFirst);
                      // Navigator.push(context, MaterialPageRoute(builder: (context) => PfScreen(
                      //   open: true,
                      //   userid: widget.userid,
                      //   isupdate: true,
                      //
                      //
                      //
                      // ) ,));


                    });
                  }
                  var thumbnail = images?.GetImageRes?.data;
                  var status = homeviewmodel.userApiResponse.status;
                  print(status);
                  var user = homeviewmodel.userApiResponse?.data?.data;
                  print("user state ${status}");

                  // if(homeviewmodel.GetImageRes.status == Status.COMPLETED){
                  //   WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                  //
                  //
                  //
                  //
                  //     ScaffoldMessenger.of(context).showSnackBar(
                  //
                  //       SnackBar(content: Text('Upload Image Successfully'))
                  //
                  //     );
                  //   });
                  //
                  // }

                  return Container(
                    margin: EdgeInsets.symmetric(vertical: 38),
                    padding: EdgeInsets.symmetric(horizontal: 23,),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        GestureDetector(
                          onTap: () async {

                            await _getCameraImage();


                          },
                          child: Center(
                            child:
                            tid == null ?

                            imagefile != null ?

                            ClipRRect(
                                borderRadius: BorderRadius.circular(50.0), //add border radius


                                child: Image.file(imagefile,fit:BoxFit.cover,width: 100,height: 100,)):
                            CircleAvatar(
                                radius: 50,
                                backgroundColor: AppColor.primarycolor,


                                child: IconButton(
                                  onPressed:() {



                                    setState(() {
                                      _getCameraImage();
                                    });

                                  },
                                  icon: Icon(Icons.add) ,
                                )


                            )


                                :    ClipRRect(
                                borderRadius: BorderRadius.circular(50.0), //add border radius


                                child: Image.network('${ApiApiUrl.localurl}/${ imagenetwork }'


                                  ,width: 100,
                                  height: 100,
                                  fit: BoxFit.cover ,)) ,

                          ),
                        ),
                        SizedBox(height: 20,),
                        GestureDetector(
                          onTap: (){

                            setState(() {
                              _getCameraImage();
                            });
                          },
                          child: GestureDetector(
                            onTap: () {
                              _getCameraImage();
                              setState(() {

                              });
                            },
                            child: Center(child: Text('Browse or Upload Profile',style: TextStyle(
                                color: AppColor.primarycolor.withOpacity(0.756),
                                fontWeight: FontWeight.w400
                            ),)),
                          ),
                        ),
                        SizedBox(height: 30,),
                        TextField(
                          style: TextStyle(
                            fontSize: AppFont.smfont+1,

                            color: AppColor.primarycolor,
                            height: 2,

                          ),
                          controller: txttelephone,
                          onChanged: (value) {
                            if(value.contains('0',0)){
                              print("true contain 0");

                              setState(() {
                                value.substring(1);
                                txttelephone.text = value;

                                txttelephone.selection = TextSelection.fromPosition(TextPosition(offset:

                                txttelephone.text.length));
                              });



                            }
                          },
                          onSubmitted: (value) {
                            setState(() {
                              txttelephone.text = value;
                              if(txttelephone.text.contains('0',0)){
                                print("true contain 0");
                                txttelephone.text = value.substring(1);;

                                print(   txttelephone.text) ;





                              }
                            });
                          },

                          decoration: InputDecoration(
                            hintText: ' 85598955',
                            prefix: Text('+855 '),

                            labelText: 'Telephone',
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppColor.primarycolor,
                                    width: 2
                                )
                            ),
                            suffix: GestureDetector(
                              onTap: () {
                                setState(() {
                                  txttelephone.clear();

                                });
                              },
                              child: Icon(Icons.highlight_remove_outlined,size: 18,),
                            ),





                            // helperText:'965689896' ,
                            labelStyle: TextStyle(
                                color: AppColor.primarycolor,
                                fontWeight: FontWeight.bold,
                                fontSize: AppFont.bigfont
                            ),
                            // suffix: Icon(
                            //   Icons.delete_forever,
                            //   size: 22,
                            //   color: AppColor.primarycolor,
                            // )



                          ),


                        ),


                        SizedBox(height: 30,),
                        TextField(
                          style: TextStyle(
                            fontSize: AppFont.smfont+1,

                            color: AppColor.primarycolor,

                            height: 2,

                          ),
                          controller: txttelegram,
                          onSubmitted: (value) {
                            setState(() {
                              txttelegram.text = value;
                            });
                          },
                          onChanged: (value) {
                            setState(() {
                              txttelegram.text = value;
                              txttelegram.selection = TextSelection.fromPosition(TextPosition(offset:

                              txttelegram.text.length));
                            });
                          },
                          decoration: InputDecoration(
                            hintText: 'http://teleme.com',

                            labelText: 'Telegram Link',
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppColor.primarycolor,

                                    width: 2
                                )
                            ),





                            // helperText:'965689896' ,
                            labelStyle: TextStyle(
                                color: AppColor.primarycolor,
                                fontSize: AppFont.bigfont,
                                fontWeight: FontWeight.bold
                            ),
                            suffix: GestureDetector(
                              onTap: () {
                                setState(() {
                                  txttelegram.clear();

                                });
                              },
                              child: Icon(Icons.highlight_remove_outlined,size: 18,),
                            ),
                            // suffix: Icon(
                            //   Icons.delete_forever,
                            //   size: 22,
                            //   color: AppColor.primarycolor,
                            // )



                          ),


                        ),
                        SizedBox(height: 30,),
                        TextField(
                          style: TextStyle(
                            fontSize: AppFont.smfont+1,

                            color: AppColor.primarycolor,
                            height: 2,

                          ),
                          controller: txtposition,
                          onSubmitted: (value) {
                            setState(() {
                              txtposition.text = value;
                              print(txtposition.text);
                            });
                          },
                          onChanged: (value) {
                            setState(() {

                              print(txtposition.text);
                              txtposition.text = value;
                              txtposition.selection = TextSelection.fromPosition(TextPosition(offset:

                              txtposition.text.length));
                            });
                          },
                          decoration: InputDecoration(
                            hintText: 'IT Manager',
                            suffix: GestureDetector(
                              onTap: () {
                                setState(() {
                                  txtposition.clear();

                                });
                              },
                              child: Icon(Icons.highlight_remove_outlined,size: 18,),
                            ),
                            labelText: 'Current Position',
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppColor.primarycolor,
                                    width: 2
                                )
                            ),





                            // helperText:'965689896' ,
                            labelStyle: TextStyle(
                                color: AppColor.primarycolor,
                                fontWeight: FontWeight.bold,
                                fontSize: AppFont.bigfont
                            ),
                            // suffix: Icon(
                            //   Icons.delete_forever,
                            //   size: 22,
                            //   color: AppColor.primarycolor,
                            // )



                          ),


                        ),
                        SizedBox(height: 20,),


                        TextField(
                          style: TextStyle(
                            fontSize: AppFont.smfont+1,

                            color: AppColor.primarycolor,
                            height: 2,

                          ),
                          controller: txtusername,
                          onChanged: (value) {
                            setState(() {


                              txtusername.text = value;
                              txtusername.selection = TextSelection.fromPosition(TextPosition(offset:

                              txtusername.text.length));
                            });
                          },
                          onSubmitted: (value) {
                            setState(() {
                              txtusername.text = value;
                            });
                          },
                          decoration: InputDecoration(
                            hintText: 'Sovath Roth',
                            suffix: GestureDetector(
                              onTap: () {
                                setState(() {
                                  txtusername.clear();

                                });
                              },
                              child: Icon(Icons.highlight_remove_outlined,size: 18,),
                            ),

                            labelText: 'Username',
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppColor.primarycolor,
                                    width: 2
                                )
                            ),






                            // helperText:'965689896' ,
                            labelStyle: TextStyle(
                                color: AppColor.primarycolor,
                                fontWeight: FontWeight.bold,
                                fontSize: AppFont.bigfont
                            ),
                            // suffix: Icon(
                            //   Icons.delete_forever,
                            //   size: 22,
                            //   color: AppColor.primarycolor,
                            // )



                          ),


                        ),
                        SizedBox(height: 30,),

                        TextField(
                          style: TextStyle(
                            fontSize: AppFont.smfont+1,

                            color: AppColor.primarycolor,
                            height: 2,

                          ),
                          controller: txtbio  ,


                          onSubmitted: (value) {
                            setState(() {
                              txtbio.text = value;

                            });
                          },
                          onChanged: (value) {
                            setState(() {
                              txtbio.text = value;
                              txtbio.selection = TextSelection.fromPosition(TextPosition(offset:

                              txtbio.text.length));

                            });
                          },
                          maxLines: null,

                          decoration: InputDecoration(
                            hintText: ' Describe your company history, experience , worksplace or condition...',
                            labelText: 'Bio',
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppColor.primarycolor,
                                    width: 2
                                )
                            ),
                            suffix: GestureDetector(
                              onTap: () {
                                setState(() {
                                  txtbio.clear();

                                });
                              },
                              child: Icon(Icons.highlight_remove_outlined,size: 18,),
                            ),






                            // helperText:'965689896' ,
                            labelStyle: TextStyle(
                                color: AppColor.primarycolor,
                                fontWeight: FontWeight.bold,
                                fontSize: AppFont.bigfont
                            ),




                          ),


                        ),



                        SizedBox(height: 30,),

                        Text('Languages',
                          style: TextStyle(
                              fontSize: AppFont.bigfont,
                              fontWeight: FontWeight.bold,
                              color: AppColor.primarycolor

                          ),),
                        SizedBox(height: 30,),
                        Row(
                          children: [
                            Expanded(
                              child: ElevatedButton(onPressed: () async {
                                _showmultiselect();
                                print(selectedlanguages);


                              },
                                  style: ButtonStyle(
                                      backgroundColor: MaterialStatePropertyAll(AppColor.primarycolor),
                                      shape: MaterialStatePropertyAll(RoundedRectangleBorder(borderRadius:
                                      BorderRadius.circular(12)))
                                  ),

                                  child: Text('Choose')),
                            ),
                          ],
                        ),


                        SizedBox(height: 30,),
                        TextField(
                          style: TextStyle(
                            fontSize: AppFont.smfont+1,

                            color: AppColor.primarycolor,
                            height: 2,

                          ),
                          controller: txtcurcom,
                          onSubmitted: (value) {
                            setState(() {
                              txtcurcom.text = value;
                            });
                          },
                          onChanged: (value) {
                            setState(() {
                              txtcurcom.text = value;
                              txtcurcom.selection = TextSelection.fromPosition(TextPosition(offset:

                              txtcurcom.text.length));

                            });
                          },
                          decoration: InputDecoration(
                            hintText: ' Digital Terinal Solution',

                            labelText: 'Company Name',
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    color: AppColor.primarycolor,
                                    width: 2
                                )
                            ),
                            suffix: GestureDetector(
                              onTap: () {
                                setState(() {
                                  txtcurcom.clear();

                                });
                              },
                              child: Icon(Icons.highlight_remove_outlined,size: 18,),
                            ),





                            // helperText:'965689896' ,
                            labelStyle: TextStyle(
                                color: AppColor.primarycolor,
                                fontWeight: FontWeight.bold,
                                fontSize: AppFont.bigfont
                            ),
                            // suffix: Icon(
                            //   Icons.delete_forever,
                            //   size: 22,
                            //   color: AppColor.primarycolor,
                            // )



                          ),


                        ),



                        // SizedBox(height: 30,),
                        // TextField(
                        //   style: TextStyle(
                        //     fontSize: AppFont.smfont+1,
                        //
                        //     color: AppColor.primarycolor,
                        //     height: 2,
                        //
                        //   ),
                        //   readOnly: true,
                        //   controller: txtcurrole,
                        //   onSubmitted: (value) {
                        //     setState(() {
                        //       txtcurrole.text = value;
                        //     });
                        //   },
                        //   decoration: InputDecoration(
                        //     // hintText: 'Ex: SoftWare Developer',
                        //
                        //     labelText: 'Your Roles',
                        //
                        //     focusedBorder: UnderlineInputBorder(
                        //         borderSide: BorderSide(
                        //             color: AppColor.primarycolor,
                        //
                        //             width: 2
                        //         )
                        //     ),
                        //
                        //
                        //
                        //
                        //
                        //     // helperText:'965689896' ,
                        //     labelStyle: TextStyle(
                        //         color: AppColor.primarycolor,
                        //         fontWeight: FontWeight.bold,
                        //         fontSize: AppFont.bigfont
                        //     ),
                        //     // suffix: Icon(
                        //     //   Icons.delete_forever,
                        //     //   size: 22,
                        //     //   color: AppColor.primarycolor,
                        //     // )
                        //
                        //
                        //
                        //   ),
                        //
                        //
                        // ),
                        SizedBox(height: 30,),
                        Text('Select Your Role',style: TextStyle(
                            color: AppColor.primarycolor,
                            fontWeight: FontWeight.bold
                        ),),
                        SizedBox(height: 30,),
                        Row(
                          children: [
                            Expanded(
                              child: Container(
                                height: 45,
                                padding: EdgeInsets.all(5),

                                decoration: BoxDecoration(
                                    color: AppColor.whitecolor,
                                    borderRadius: BorderRadius.circular(10)
                                ),
                                child: DropdownButton(
                                  borderRadius: BorderRadius.circular(12),
                                  underline: SizedBox(),
                                  menuMaxHeight: 400,
                                  alignment: Alignment.center,
                                  iconEnabledColor: AppColor.primarycolor, //TODO ARROW ICON
                                  iconDisabledColor: AppColor.bgcolor,

                                  autofocus: true,
                                  isExpanded: true,
                                  value: selecteditem==null ? rolesselect.elementAt(0) : selecteditem,
                                  dropdownColor: AppColor.whitecolor,
                                  focusColor: AppColor.whitecolor,



                                  items:List.generate( rolesselect.length , (index){
                                    return DropdownMenuItem(


                                      value:rolesselect[index],

                                      alignment: Alignment.center,

                                      enabled: true,
                                      child: Text('${rolesselect[index]}',
                                        style: TextStyle(
                                            color: AppColor.primarycolor,
                                            fontSize: AppFont.smfont+3
                                        ),
                                      ),
                                      onTap: () {

                                      },
                                    );
                                  }),
                                  onChanged:(value) {

                                    setState(() {
                                      selecteditem = value;
                                      print(selecteditem);
                                    });
                                  },
                                  elevation: 0,

                                ),
                              ),
                            ),
                          ],
                        ),

                        SizedBox(height: 30,),
                        Column(
                          children: List.generate(
                            jobskills.length

                            , (index) =>

                              TextField(
                                style: TextStyle(
                                  fontSize: AppFont.smfont+1,

                                  color: AppColor.primarycolor,
                                  height: 2,

                                ),

                                controller: controller[index], //TODO DYNAMIC TEXT CONTROLLER
                                onSubmitted: (value) {

                                  print(index);
                                  setState(() {
                                    print("submit press");
                                    print(index);
                                    print(skills);

                                    //
                                    // if(controller[index].text == value){
                                    //
                                    // }
                                    controller[index].text == value;
                                    skills.insert(index,controller[index].text);
                                    var removeindex = index + 1;
                                    skills.removeAt(removeindex );

                                    // controller[index].text = value;
                                    // skills.removeAt(index);
                                    // skills.insert(index,controller[index].text);

                                    // print(widget.users.profile!.skill[index]);



                                    // if(widget.users.profile!.skill[index] != null){
                                    //
                                    //   skills.removeAt(index);
                                    //   controller[index].clear();
                                    //   controller[index].text = value.toString();
                                    //   skills.insert(index, value.toString());
                                    //
                                    //
                                    // }
                                    // else{
                                    //   controller[index].text = value.toString();
                                    //   print(controller[index].text);
                                    //   skills.add("asd");
                                    //
                                    //
                                    //
                                    //
                                    //   // print(controller[index].text);
                                    // }



                                  });


                                },

                                onEditingComplete: () {
                                  setState(() {
                                    print("submit press");
                                    print(index);
                                    print(skills);

                                    //
                                    // if(controller[index].text == value){
                                    //
                                    // }
                                    controller[index].text =='';
                                    skills.insert(index,controller[index].text);
                                    var removeindex = index + 1;
                                    skills.removeAt(removeindex );

                                    // controller[index].text = value;
                                    // skills.removeAt(index);
                                    // skills.insert(index,controller[index].text);

                                    // print(widget.users.profile!.skill[index]);



                                    // if(widget.users.profile!.skill[index] != null){
                                    //
                                    //   skills.removeAt(index);
                                    //   controller[index].clear();
                                    //   controller[index].text = value.toString();
                                    //   skills.insert(index, value.toString());
                                    //
                                    //
                                    // }
                                    // else{
                                    //   controller[index].text = value.toString();
                                    //   print(controller[index].text);
                                    //   skills.add("asd");
                                    //
                                    //
                                    //
                                    //
                                    //   // print(controller[index].text);
                                    // }



                                  });
                                },
                                onTap: () {
                                  setState(() {
                                    print("submit press");
                                    print(index);
                                    print(skills);

                                    //
                                    // if(controller[index].text == value){
                                    //
                                    // }
                                    controller[index].text =='';
                                    skills.insert(index,controller[index].text);
                                    var removeindex = index + 1;
                                    skills.removeAt(removeindex );

                                    // controller[index].text = value;
                                    // skills.removeAt(index);
                                    // skills.insert(index,controller[index].text);

                                    // print(widget.users.profile!.skill[index]);



                                    // if(widget.users.profile!.skill[index] != null){
                                    //
                                    //   skills.removeAt(index);
                                    //   controller[index].clear();
                                    //   controller[index].text = value.toString();
                                    //   skills.insert(index, value.toString());
                                    //
                                    //
                                    // }
                                    // else{
                                    //   controller[index].text = value.toString();
                                    //   print(controller[index].text);
                                    //   skills.add("asd");
                                    //
                                    //
                                    //
                                    //
                                    //   // print(controller[index].text);
                                    // }



                                  });
                                },
                                onChanged: (value) {
                                  setState(() {
                                    print("submit press");
                                    print(index);
                                    print(skills);

                                    //
                                    // if(controller[index].text == value){
                                    //
                                    // }
                                    controller[index].text == value;
                                    skills.insert(index,controller[index].text);
                                    var removeindex = index + 1;
                                    skills.removeAt(removeindex );

                                    // controller[index].text = value;
                                    // skills.removeAt(index);
                                    // skills.insert(index,controller[index].text);

                                    // print(widget.users.profile!.skill[index]);



                                    // if(widget.users.profile!.skill[index] != null){
                                    //
                                    //   skills.removeAt(index);
                                    //   controller[index].clear();
                                    //   controller[index].text = value.toString();
                                    //   skills.insert(index, value.toString());
                                    //
                                    //
                                    // }
                                    // else{
                                    //   controller[index].text = value.toString();
                                    //   print(controller[index].text);
                                    //   skills.add("asd");
                                    //
                                    //
                                    //
                                    //
                                    //   // print(controller[index].text);
                                    // }



                                  });
                                },

                                decoration: InputDecoration(
                                  hintText: 'Write Your Skills',

                                  labelText: 'Skills ${index+1}',
                                  focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: AppColor.primarycolor,
                                          width: 2
                                      )
                                  ),





                                  // helperText:'965689896' ,
                                  labelStyle: TextStyle(
                                      color: AppColor.primarycolor,
                                      fontWeight: FontWeight.bold,
                                      fontSize: AppFont.bigfont
                                  ),

                                  suffix:                    Container(
                                    //TODO DELETE SKILLS
                                    child:jobskills.length <=1 ?Text('')   :CircleAvatar(
                                      radius: 20,
                                      backgroundColor: AppColor.thirdcolor   ,

                                      child: IconButton(onPressed:() {


                                        setState(() {
                                          //
                                          // controller.removeLast();
                                          //
                                          // skills.removeLast() ;
                                          print("press delete on ${index}");
                                          print("press ${jobskills.length}");
                                          jobskills.removeAt(index);

                                          // jobskills.map((e) => jobskills.add(e));

                                          controller.removeAt(index);
                                          skills.removeAt(index);
                                          print(jobskills);
                                          print(skills);
                                          // skills.removeAt(index);
                                          // jobskills.removeLast();
                                          // skills.removeLast();
                                          // controller.removeAt(jobskills.length-1);

                                          print(jobskills);

                                        });
                                      },
                                        style: ButtonStyle(
                                          backgroundColor: MaterialStatePropertyAll(AppColor.fourthcolor),

                                        ),

                                        icon:Icon(Icons.remove_circle_sharp,color:AppColor.bgcolor,size: 28,),
                                      ),
                                    ),
                                  ),



                                ),



                              ),),
                        ),
                        SizedBox(height: 30,),


                        Center(
                          child: Container(
                            child:   jobskills.length ==null ? null   :Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  child:    jobskills.length >=7 ?null   :CircleAvatar(
                                    radius: 15,
                                    backgroundColor: AppColor.primarycolor,
                                    child: IconButton(

                                      onPressed:() {
                                        setState(() {




                                          jobskills.add(jobskills.length +1);
                                          var index = jobskills.length - 1;
                                          print(index );
                                          controller[index].text= "";
                                          print(controller[ index].text);

                                          // controller.insert(jobskills.length, TextEditingController());

                                          controller[index].text =='';
                                          skills.insert(index,controller[index].text);

                                          //
                                          // for (int i = 0; i < jobskills.length; i++) {
                                          //
                                          //   skills.add(controller[i].text);
                                          //
                                          // }

                                        });
                                        print(jobskills.length);
                                      },
                                      style: ButtonStyle(
                                          backgroundColor: MaterialStatePropertyAll(AppColor.fourthcolor)
                                      ),
                                      icon:Icon(Icons.add,color: Colors.white,size: 14,),
                                    ),
                                  ),
                                ),

                                SizedBox(
                                  width: jobskills.length ==  1 ? null : 25,
                                ),



                              ],
                            ),
                          ),
                        ),

                        SizedBox(height: 30,),



                        SizedBox(height: 20,),
                        Column(
                          mainAxisSize: MainAxisSize.max,
                          children: List.generate( explength.length, (index) =>

                              Column(

                                children: [
                                  Container(
                                    width: double.maxFinite,
                                    child: Row(

                                      children: [
                                        Text('Experience #${index+1}',
                                          style: TextStyle(
                                              fontSize: AppFont.bigfont,
                                              fontWeight: FontWeight.bold,
                                              color: AppColor.primarycolor

                                          ),),

                                        Container(
                                          child:     explength.length ==1

                                              ?Text('')   :
                                          IconButton(onPressed:() {

                                            setState(() {
                                              // ishaveExpen = false;
                                              print("press delete on ${index}");



                                              // jobskills.map((e) => jobskills.add(e));
                                              print("press delete on ${index}");


                                              explength.removeAt(index);
                                              print(   "Length : ${explength}");

                                              controller_yend.removeAt(index);
                                              controller_ystart.removeAt(index);
                                              controller_roles.removeAt(index);
                                              controller_com.removeAt(index);
                                              experiencelist.removeAt(index);

                                              print("Button Remove");
                                              // print(experiencelist.length);

                                            });
                                          },
                                            style: ButtonStyle(
                                                backgroundColor: MaterialStatePropertyAll(AppColor.bgcolor)
                                            ),

                                            icon:Icon(Icons.remove_circle,
                                              color: AppColor.bgcolor,size: 22,),
                                          ),
                                        ),


                                      ],
                                    ),
                                  ),
                                  TextField(
                                    style: TextStyle(
                                      fontSize: AppFont.smfont+1,

                                      color: AppColor.primarycolor,
                                      height: 2,

                                    ),
                                    controller: controller_com[index], //TODO DYNAMIC TEXT CONTROLLER
                                    onSubmitted: (value) {
                                      controller_com[index].text = value.toString();

                                    },

                                    decoration: InputDecoration(
                                      hintText: 'Digital Osaka',
                                      labelText: 'Company',
                                      focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: AppColor.primarycolor,
                                              width: 2
                                          )
                                      ),





                                      // helperText:'965689896' ,
                                      labelStyle: TextStyle(
                                          color: AppColor.primarycolor,
                                          fontWeight: FontWeight.bold,
                                          fontSize: AppFont.bigfont
                                      ),
                                      suffix: GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            controller_com[index].clear();

                                          });
                                        },
                                        child: Icon(Icons.highlight_remove_outlined,size: 18,),
                                      ),



                                    ),



                                  ),
                                  TextField(
                                    style: TextStyle(
                                      fontSize: AppFont.smfont+1,

                                      color: AppColor.primarycolor,
                                      height: 2,

                                    ),
                                    controller: controller_roles[index], //TODO DYNAMIC TEXT CONTROLLER
                                    onSubmitted: (value) {
                                      controller_roles[index].text = value.toString();

                                    },

                                    decoration: InputDecoration(
                                      hintText: 'Mobile App Developer',
                                      labelText: 'Position',
                                      focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: AppColor.primarycolor,
                                              width: 2
                                          )
                                      ),






                                      // helperText:'965689896' ,
                                      labelStyle: TextStyle(
                                          color: AppColor.primarycolor,
                                          fontWeight: FontWeight.bold,
                                          fontSize: AppFont.bigfont
                                      ),
                                      suffix: GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            controller_roles[index].clear();

                                          });
                                        },
                                        child: Icon(Icons.highlight_remove_outlined,size: 18,),
                                      ),



                                    ),



                                  ),

                                  Row(
                                    children: [

                                      Expanded(
                                        child: TextField(
                                          style: TextStyle(
                                            fontSize: AppFont.smfont+1,

                                            color: AppColor.primarycolor,
                                            height: 2,

                                          ),
                                          controller: controller_ystart[index], //TODO DYNAMIC TEXT CONTROLLER
                                          onSubmitted: (value) {
                                            controller_ystart[index].text = value.toString();

                                          },

                                          decoration: InputDecoration(
                                            hintText: '2020',
                                            labelText: 'Year Start ',
                                            focusedBorder: UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: AppColor.primarycolor,
                                                    width: 2
                                                )
                                            ),





                                            // helperText:'965689896' ,
                                            labelStyle: TextStyle(
                                                color: AppColor.primarycolor,
                                                fontWeight: FontWeight.bold,
                                                fontSize: AppFont.bigfont
                                            ),
                                            suffix: GestureDetector(
                                              onTap: () {
                                                setState(() {
                                                  controller_ystart[index].clear();

                                                });
                                              },
                                              child: Icon(Icons.highlight_remove_outlined,size: 18,),
                                            ),



                                          ),



                                        ),
                                      ),
                                      SizedBox(width: 30,),

                                      Expanded(
                                        child: TextField(
                                          style: TextStyle(
                                            fontSize: AppFont.smfont+1,

                                            color: AppColor.primarycolor,
                                            height: 2,

                                          ),
                                          controller: controller_yend[index], //TODO DYNAMIC TEXT CONTROLLER
                                          onSubmitted: (value) {
                                            controller_yend[index].text = value.toString();

                                          },

                                          decoration: InputDecoration(
                                            hintText: '2023',
                                            labelText: 'Year End ',
                                            focusedBorder: UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: AppColor.primarycolor,
                                                    width: 2
                                                )
                                            ),





                                            // helperText:'965689896' ,
                                            labelStyle: TextStyle(
                                                color: AppColor.primarycolor,
                                                fontWeight: FontWeight.bold,
                                                fontSize: AppFont.bigfont
                                            ),
                                            suffix: GestureDetector(
                                              onTap: () {
                                                setState(() {
                                                  controller_ystart[index].clear();

                                                });
                                              },
                                              child: Icon(Icons.highlight_remove_outlined,size: 18,),
                                            ),



                                          ),



                                        ),
                                      ),


                                    ],
                                  ),
                                  SizedBox(height: 20,),
                                  // Center(
                                  //   child: Container(
                                  //       child:
                                  //
                                  //       explength.length ==7 ?Text('')
                                  //           :Row(
                                  //         mainAxisAlignment: MainAxisAlignment.center,
                                  //         children: [
                                  //           Container(
                                  //             child:
                                  //             isSave ==true?
                                  //
                                  //             explength.length ==7 ?null   :
                                  //             CircleAvatar(
                                  //               radius: 15,
                                  //               backgroundColor: AppColor.primarycolor,
                                  //               child: IconButton(onPressed:() {
                                  //
                                  //                 setState(() {
                                  //                   if(controller_com[index].text ==''){
                                  //
                                  //                   }
                                  //                   ishaveExpen = true;
                                  //                   explength.add(explength.length+1);
                                  //                   print(explength.length);
                                  //                   isSave  = false;
                                  //                   print(experiencelist.length);
                                  //                 });
                                  //               },
                                  //                 style: ButtonStyle(
                                  //                     backgroundColor: MaterialStatePropertyAll(AppColor.fourthcolor)
                                  //                 ),
                                  //
                                  //                 icon:Icon(Icons.add,color: Colors.white,size: 14,),
                                  //               ),
                                  //             ) : null,
                                  //           ),
                                  //
                                  //
                                  //           SizedBox(
                                  //
                                  //             width:   isSave == true ?25 : 0,
                                  //
                                  //
                                  //           ),
                                  //
                                  //
                                  //           Container(
                                  //             child:
                                  //             isSave ==false ?
                                  //             GestureDetector(
                                  //
                                  //               child:
                                  //               controller_com[index] !=''?
                                  //
                                  //               ElevatedButton(
                                  //
                                  //
                                  //                 onPressed: () {
                                  //                   setState(() {
                                  //                     isSave = true;
                                  //                     var index =       experiencelist.length;
                                  //
                                  //                     experiencelist.add(
                                  //                         ExperienceModel(
                                  //                             CompanyName: controller_com[index].text,
                                  //                             roles: controller_roles[index].text,
                                  //                             Yearst: controller_ystart[index].text,
                                  //                             Yeared: controller_yend[index].text
                                  //
                                  //                         ) );
                                  //
                                  //
                                  //
                                  //
                                  //
                                  //                     print(experiencelist);
                                  //                     // print(experiencelist[1].roles);
                                  //                   });
                                  //                 },
                                  //                 child: Text('Save',style: TextStyle(
                                  //                   fontSize: AppFont.smfont+1
                                  //                 ),),
                                  //                 style: ButtonStyle(
                                  //                   backgroundColor: MaterialStatePropertyAll(AppColor.bgcolor),
                                  //                   shape: MaterialStatePropertyAll(RoundedRectangleBorder(borderRadius:
                                  //
                                  //                   BorderRadius.circular(10)))
                                  //
                                  //
                                  //                 ),
                                  //
                                  //               ) : null,
                                  //             ) : null,
                                  //           ),
                                  //           SizedBox(width: explength.length == 1 ? null : 25,),
                                  //
                                  //
                                  //         ],
                                  //       )
                                  //
                                  //   ),
                                  // ),


                                ],
                              ),


                          ),
                        ),
                        SizedBox(height: 30,),

                        //TODO ADD REQUIREMENT
                        SizedBox(width: 15,),

                        Container(
                            child:

                            explength.length ==7 ?Text('')
                                :Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  child:
                                  isSave ==true?

                                  explength.length ==7 ?null   :
                                  CircleAvatar(
                                    radius: 15,
                                    backgroundColor: AppColor.primarycolor,
                                    child: IconButton(onPressed:() {

                                      setState(() {
                                        // if(controller_com[index].text ==''){
                                        //
                                        // }
                                        ishaveExpen = true;
                                        explength.add(explength.length+1);
                                        print(explength.length);
                                        isSave  = false;
                                        print(experiencelist.length);
                                      });
                                    },
                                      style: ButtonStyle(
                                          backgroundColor: MaterialStatePropertyAll(AppColor.fourthcolor)
                                      ),

                                      icon:Icon(Icons.add,color: Colors.white,size: 14,),
                                    ),
                                  ) : null,
                                ),


                                SizedBox(

                                  width:   isSave == true ?25 : 0,


                                ),


                                Container(
                                  child:
                                  isSave ==false ?
                                  GestureDetector(

                                    child:


                                    ElevatedButton(


                                      onPressed: () {
                                        setState(() {

                                          var index =       experiencelist.length;
                                          if(controller_com[index].text == ''

                                              ||  controller_roles[index].text =='' ||
                                              controller_ystart[index].text == ''
                                              ||  controller_yend[index].text == ''
                                          ){
                                            print("Cant add experience");
                                            showDialog(context: context, builder: (context) {
                                              return AlertDialog(

                                                elevation: 0,

                                                title: Column(
                                                  children: [
                                                    Icon(Icons.warning,size: 60,color: AppColor.primarycolor,),
                                                    SizedBox(height: 10,),
                                                    Text('Error',style:
                                                    TextStyle(
                                                        color:AppColor.primarycolor,
                                                        fontSize: 18
                                                    ),),
                                                    SizedBox(height: 10,),
                                                    Text('Please Check your form again',style:
                                                    TextStyle(
                                                        color: AppColor.primarycolor,
                                                        fontSize: 14,
                                                        fontWeight: FontWeight.w400
                                                    ),),

                                                    SizedBox(height: 10,),

                                                    Row(
                                                      children: [
                                                        Expanded(
                                                          child: ElevatedButton(onPressed: () => Navigator.pop(context),
                                                            child: Text('Try again'),
                                                            style: ButtonStyle(
                                                                backgroundColor: MaterialStatePropertyAll(AppColor.primarycolor)
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    )
                                                  ],
                                                ),

                                              );
                                            },);

                                            isSave ==false;

                                            return null;

                                          }
                                          else{
                                            isSave = true;
                                            experiencelist.add(
                                                ExperienceModel(
                                                    CompanyName: controller_com[index].text,
                                                    roles: controller_roles[index].text,
                                                    Yearst: controller_ystart[index].text,
                                                    Yeared: controller_yend[index].text

                                                ) );
                                          }







                                          print(experiencelist);
                                          // print(experiencelist[1].roles);
                                        });
                                      },
                                      child: Text('Save',style: TextStyle(
                                          fontSize: AppFont.smfont+1
                                      ),),
                                      style: ButtonStyle(
                                          backgroundColor: MaterialStatePropertyAll(AppColor.primarycolor),
                                          shape: MaterialStatePropertyAll(RoundedRectangleBorder(borderRadius:

                                          BorderRadius.circular(10)))


                                      ),

                                    ) ,
                                  ) : null,
                                ),
                                SizedBox(width: explength.length == 1 ? null : 25,),


                              ],
                            )

                        ),
                        SizedBox(height: 35,),
                        // GestureDetector(
                        //   onTap: () {
                        //     print("Skill requirement ${skills}");
                        //   },
                        //   child: Text('Check requirement'),
                        // ),

                        Container(
                          width: double.maxFinite,
                          child:


                          ElevatedButton(onPressed: ()  async {
                            print("selectedlanguages ${selectedlanguages}");
                            print(txtposition.text);
                            print(txttelephone.text);
                            print(txtcurcom.text);
                            print(txtbio.text);

                            print(skills);
                            var lang = selectedlanguages.toList();
                            print( lang );
                            print(tid);
                            print(widget.userid);
                            print(widget.users.role);
                            if(txtposition.text == '' || txtposition.text == null ||
                                txttelephone.text == '' ||   txttelephone.text == null ||
                            txtusername.text =='' ||
                                selectedlanguages.length <=0 ||
                                txtposition.text == '' ||
                                txttelegram.text ==''
                            ){
                              print(tid);
                              return        showDialog(context: context, builder: (context) {
                                return AlertDialog(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(14)
                                  ),
                                  backgroundColor:AppColor.primarycolor ,

                                  elevation: 0,

                                  title: Column(
                                    children: [
                                      Icon(Icons.warning,size: 60,color: AppColor.fourthcolor,),
                                      SizedBox(height: 10,),
                                      Text('Missing',style:
                                      TextStyle(
                                          color:AppColor.fourthcolor,
                                          fontSize: 18,
                                          fontWeight: FontWeight.w500
                                      ),),
                                      SizedBox(height: 10,),
                                      Text('One or two more missing informations in your dialog , please fill everything',style:
                                      TextStyle(
                                        color:AppColor.fourthcolor,
                                        fontSize: 12,

                                        fontWeight: FontWeight.w400,

                                      ),
                                        textAlign: TextAlign.center,

                                      ),

                                      SizedBox(height: 10,),

                                      Row(
                                        children: [
                                          Expanded(
                                            child: ElevatedButton(onPressed: () => Navigator.pop(context),
                                              child: Text('Try again'),
                                              style: ButtonStyle(
                                                  backgroundColor: MaterialStatePropertyAll(AppColor.fourthcolor  )
                                              ),
                                            ),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),

                                );
                              },);
                            }

                            var object = UpdateuserModel(
                                interest: widget.users.interest,


                                experience:List.generate( experiencelist.length, (index) =>
                                    Experienceuser(
                                        companyname: '${experiencelist[index].CompanyName}',
                                        position: '${experiencelist[index].roles}',
                                        yearstart:int.parse(experiencelist![index].Yearst!),

                                        yearend:int.parse(experiencelist![index].Yearst!)

                                    )),
                                name:txtusername.text,
                                role:selecteditem.toString(),
                                position: txtposition.text,

                                profile: UserProfile(
                                    telephone: txttelephone.text,
                                    telegram: txttelegram.text,
                                    currentCompany:            txtcurcom.text,
                                    bio: txtbio.text,
                                    currentRole: selecteditem.toString(),
                                    languages: lang,
                                    skill: skills
                                ),
                                thumbnail:tid
                            );

                            print(tid)  ;
                            print(widget.userid);
                            isUpdatePf = true;
                            if(widget.post == true){
                              await homeviewmodel.postUserAPI(widget.userid, object, 'tokens');
                              return null;
                            }
                            await homeviewmodel.updateUserAPI(widget.userid, object, 'tokens');

                            //TODDO UPDATE HERE
                          }, //TODO SAVE CHANGES


                            child: Text('Update Profile'),

                            style: ButtonStyle(
                                backgroundColor: MaterialStatePropertyAll(AppColor.bgcolor)
                            ),
                          ),
                        ),





                      ],
                    ),
                  );

                },

              ),
            );
          },

        ),
      ),

    );

  }
  _getCameraImage() async {


    PickedFile? filepick = await ImagePicker().
    getImage(source: ImageSource.gallery,
      maxHeight: 250,
      maxWidth: 250,

    );
    if(filepick != null){
      print("File has send");
      print(filepick?.path);

      imagefile = File(filepick.path);
      print(      imagefile);


      await homeviewmodel.postImage(filepick.path);


      return null;
    }
    print("File has not been picked");
  }
  showdialoglanguages(BuildContext context){
    showDialog(context: context, builder: (context) {

      return customdialog();

    },);
  }


}

void getuserdata() {

}





class customdialog extends StatefulWidget {
  var lists;
  var check;
  var selectedlanguages;
  customdialog({Key? key , this.lists , this.check , this.selectedlanguages} ) : super(key: key);

  @override
  State<customdialog> createState() => _customdialogState();
}

class _customdialogState extends State<customdialog> {

  var listofcountries;

  var ischeck;
  Set<String?> selectedlanguages= {};
  @override

  void initState() {
    // TODO: implement initState
    super.initState();
    listofcountries = widget.lists;
    ischeck = widget.check;
    selectedlanguages = widget.selectedlanguages;
  }
  Widget build(BuildContext context) {

    return AlertDialog(
      backgroundColor: AppColor.thirdcolor,
      scrollable: true,
      shape:RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(14)
      ) ,

      title: Row(
        children: [
          Icon(Icons.language,color: AppColor.primarycolor  ,),
          SizedBox(width: 10,),
          Text('Langauges',style: TextStyle(
            color: AppColor.primarycolor  ,
          ),),
        ],
      ),
      content:    Column(
        children: List.generate(listofcountries.length, (index) {

          return   CheckboxListTile(

            value: ischeck[index],
            controlAffinity: ListTileControlAffinity.leading,
            onChanged: (bool? value) {
              //value = true;
              setState(() {
                print(listofcountries[index].toString());

                ischeck[index] = value;

                if(      ischeck[index] == false){
                  selectedlanguages.remove(listofcountries[index].toString());
                }
                else{
                  selectedlanguages.add(listofcountries[index].toString());
                }


                print(listofcountries[index]);
              });


            },
            title: Text('${listofcountries[index]}',style: TextStyle(
                fontSize: AppFont.medfont
            ),),
            activeColor: AppColor.primarycolor,
            checkColor:Colors.white,




          );
        }),
      ),
      actions: [
        ElevatedButton(

          onPressed: () {

            print(selectedlanguages);
            if(selectedlanguages.length <=0){
              print("Not selected languages yet");
              return null;

            }

            Navigator.pop(context,selectedlanguages);




          }, child: Text('Save'),
          style: ButtonStyle(
              backgroundColor: MaterialStatePropertyAll(AppColor.bgcolor)
          ),

        ),
        ElevatedButton(onPressed: () {
          setState(() {
            if(selectedlanguages.length == 0 ){
              selectedlanguages.clear();
              widget.check = false;
            }


            print(selectedlanguages);
            Navigator.pop(context);


          });



        }, child: Text('Close'),

          style: ButtonStyle(
              backgroundColor: MaterialStatePropertyAll(AppColor.fourthcolor)
          ),
        ),

      ],
    );
  }
}




