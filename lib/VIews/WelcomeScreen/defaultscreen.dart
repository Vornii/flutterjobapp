import 'dart:async';

import 'package:flutter/material.dart';

import 'package:jobapp/VIews/AuthScreen/Login.dart';
import 'package:jobapp/VIews/AuthScreen/register.dart';
import 'package:jobapp/VIews/Jobs_details/filterform.dart';
import 'package:jobapp/VIews/WelcomeScreen/HiringScreen.dart';
import 'package:jobapp/VIews/WelcomeScreen/JobSeekerScreen.dart';
import 'package:jobapp/VIews/WelcomeScreen/screendiscover.dart';
import 'package:jobapp/VIews/WelcomeScreen/screenfindjob.dart';
import 'package:jobapp/VIews/Widgets/Joblistv1.dart';
import 'package:jobapp/ViewModel/job_view_model.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../data/Response/status_response.dart';
import '../../res/Appcolor.dart';
import '../../res/Appfont.dart';
import '../Admin/adminview.dart';
import '../AuthScreen/ErrorScreen.dart';
import '../Jobs_details/pfscreen.dart';
import '../Jobs_details/Msg/messages.dart';
import '../Widgets/cardlistv1.dart';
import 'browsescreen.dart';

class ScreenDefault extends StatefulWidget {


  @override
  State<ScreenDefault> createState() => _ScreenDefaultState();
}

class _ScreenDefaultState extends State<ScreenDefault> {
  var navlabel =['Search','Inbox','Home','Discover','Profile'];
 var uid ;
 var tokenss;
  var appbarlabel =['Filter Jobs','Inbox','Discover','Profile'];
var name ='';

  var isAuth;
  var ScreenTab = [
    FilterScreen(open: true ),

    InboxScr(open: true),

    ScreenFindJob(),

    DiscoverScreen(open: true),

    PfScreen(open: true)

  ];





  var iconlabel =<Widget> [
    Icon(Icons.search),
    Icon(Icons.messenger_sharp),
    Icon(Icons.home_filled),
    Icon(Icons.language_sharp),
    Icon(Icons.account_circle)
  ];
  var selectedindex = 2;
JobViewModel postjobviewmodel = JobViewModel();
  JobViewModel userviewmodel = JobViewModel();
  JobViewModel msgviewmodel = JobViewModel();
  var        messagelength = 0;
  var stop = false;
  @override

  void initState() {
    // TODO: implement initState
checkauth();


  }
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   backgroundColor: AppColor.primarycolor,
      //   elevation: 0,
      //   iconTheme: IconThemeData(
      //       color: AppColor.primarycolor
      //   ),
      //
      // ),
      backgroundColor: AppColor.primarycolor,

      body: ChangeNotifierProvider<JobViewModel>(
        create: (context) => userviewmodel,
        builder: (context, child) {
          return Consumer<JobViewModel>(
            builder: (context, user, child) {
              var status = user.userApiResponse.status;
              if(status == Status.COMPLETED){
                WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                  var total =0;

                  var index= msgviewmodel.messageResponse?.data?.data?.job?.length;


                  print(total);
                  if(stop == false){
                    setState(() {
                      for(int i =0;i<index!;i++){
                        total += msgviewmodel.messageResponse!.data!.data!.job![i].messages!.length;

                      }
                      messagelength = total;
                      stop = true;

                    });
                  }





                });
              }
              print("Fetching from api :${status}");

              switch(status ){
                case Status.LOADING:
                  // Future.delayed(Duration(seconds: 4), (){
                  // return Navigator.pushReplacement(context,MaterialPageRoute(builder: (context) {
                  //   return NointernetScreen();
                  // },));
                  // });
                  return Center(
                    child: GestureDetector(

                      child: CircularProgressIndicator(
                        color: AppColor.bgcolor,
                        backgroundColor: Colors.indigo,
                      ),
                    ),
                  );
                case Status.COMPLETED:
                  var role = user.userApiResponse.data?.data?.role;
                  var thumbnailuser =user.userApiResponse.data?.data?.thumbnail?.attributes?.path;

                  return
                    role == 'Job Seeker' ? ScreenJobSeeker() :
                      role == 'Admin' ?
                      AdminScreen(uid: uid,token:tokenss,tb : thumbnailuser) :
                      ScreenHiring(msglength:          messagelength);

                case Status.ERROR:
                  return Center(
                    child: Lottie.asset('assets/images/Icon/28248-404-error.json'),
                  );
                default:
                  return Text('');
              }

            },

          );
        },

      ),

      // bottomNavigationBar: ChangeNotifierProvider<JobViewModel>(
      //   create: (context) => userviewmodel,
      //   builder: (context, child) {
      //     return  Consumer<JobViewModel>(
      //       builder: (context, result, child) {
      //         var state = result.userApiResponse.status;
      //         var roles = result.userApiResponse.data?.data?.role;
      //         print("Default Screen");
      //         print(state);
      //         print(roles);
      //         if(state  == Status.COMPLETED){
      //           WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      //           if(roles =='Job Seeker'){
      //
      //
      //           }
      //
      //
      //           });
      //
      //
      //
      //
      //
      //
      //         }
      //         else{
      //           print("True");
      //
      //
      //
      //         }
      //
      //        return  BottomNavigationBar(
      //            currentIndex:selectedindex ,
      //            elevation: 0,
      //            onTap: (value) {
      //
      //
      //              setState(() {
      //                checkauth();
      //
      //                selectedindex = value;
      //              });
      //            },
      //            backgroundColor: AppColor.thirdcolor,
      //
      //            items: List.generate(iconlabel.length,(index) {
      //              print('length: ${iconlabel.length}');
      //
      //              return  BottomNavigationBarItem(
      //                  backgroundColor: AppColor.primarycolor,
      //
      //                  label: '${navlabel[index]}',
      //                  icon: iconlabel[index]
      //              );
      //            }, )
      //
      //        );
      //       },
      //
      //     );
      //   },
      // ),
    );

  }
  void checkauth () async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    isAuth= prefs.get('tokens') ?? false; //if there is no token false
    uid = prefs.getString('userid');
    // prefs.setString('email', value)
    var tokens= prefs.getString('tokens');
    tokenss = tokens;
    print("The token in browse screen fetch ${tokens}");
    // postjobviewmodel.fetchall(tokens,null);
    await userviewmodel.fetchUserbyId( uid );
    await msgviewmodel.fetchMsg(uid);


    // print("USer info");
    //
    //   print(isAuth);
    //   print("USer info");
    //   print(  prefs.getString('userid'));

  }
}
