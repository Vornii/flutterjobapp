import 'package:flutter/material.dart';
import 'package:bulleted_list/bulleted_list.dart';
import 'package:jobapp/VIews/AuthScreen/Login.dart';
import 'package:jobapp/VIews/AuthScreen/register.dart';
import 'package:jobapp/VIews/WelcomeScreen/browsescreen.dart';
import 'package:jobapp/VIews/WelcomeScreen/categorybrowsesrc.dart';
import 'package:jobapp/VIews/Widgets/Joblistv1.dart';
import 'package:jobapp/VIews/Widgets/gridpanel.dart';
import 'package:lottie/lottie.dart';

import '../../res/Appcolor.dart';
import '../../res/Appfont.dart';
import '../Widgets/cardlistv1.dart';
import '../Widgets/categorycardlist.dart';


class DiscoverScreen extends StatefulWidget {
  var open;
 DiscoverScreen({Key? key, this.open}) : super(key: key);

  @override
  State<DiscoverScreen> createState() => _DiscoverScreenState();
}

class _DiscoverScreenState extends State<DiscoverScreen> {

  var imagelist = ['https://static.vecteezy.com/system/resources/previews/021/096/523/original/3d-icon-job-search-png.png',
  'https://cdn3d.iconscout.com/3d/premium/thumb/job-interview-4616025-3846823.png',
    'https://cdn3d.iconscout.com/3d/premium/thumb/working-employee-at-home-4637848-3864094.png'
  ];
  var cateitem =[
    "Software developer",
    "Data scientist",
    "Cyber security engineer",
    "Cloud developer",
    "DevOps engineer",
    "Digital marketing specialist",
    "Project manager",
    "Product manager",
    "UX designer",
    "UI designer",
    "Content writer",
    "SEO specialist",
    "Social media manager",
    "Customer service representative",
    "Sales representative",
    "Accountant",
    "Networing"
        "Financial analyst",
    "Human resources manager",
    "Operations manager",
    "Logistics manager",
    "Supply chain manager",
    "Healthcare professional",
    "Teacher",
    "Engineer",
    "Construction worker",
    "Truck driver",
    "Retail salesperson",

  ];
  var jobtype = ['Full Time','Part Time','Remote'];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(jobtype.length);
  }
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title:   Row(
          children: [

            Text('Discover',
              style: TextStyle(
                fontSize:AppFont.megafont,
                fontWeight: FontWeight.w500,
                color: Colors.white,

              ),
              maxLines: 6,

              overflow: TextOverflow.ellipsis,

            ),
          ],
        ),
        elevation: 0,

        backgroundColor: Colors.indigo,
      ),

      backgroundColor: AppColor.primarycolor,
      body: SafeArea(
        child: Container(

          margin: EdgeInsets.symmetric(vertical: 20,horizontal: 15),
          child: SingleChildScrollView(
            physics: ScrollPhysics(),

            child: Container(
              height:700,
              width: double.maxFinite,

              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,


                children: [
                  // Row(
                  //   children: [
                  //     Icon(Icons.travel_explore,size: 37,color: Colors.white,),
                  //     SizedBox(width: 14,),
                  //     Text('Discover',
                  //       style: TextStyle(
                  //         fontSize:AppFont.megafont,
                  //         fontWeight: FontWeight.w500,
                  //         color: Colors.white,
                  //
                  //       ),
                  //       maxLines: 6,
                  //       overflow: TextOverflow.ellipsis,
                  //
                  //     ),
                  //   ],
                  // ),


                 Divider(),

                  Text('Job types',
                    style: TextStyle(
                      fontSize:AppFont.bigfont+2,
                      fontWeight: FontWeight.w400,
                      color: Colors.white,

                    ),
                    maxLines: 6,
                    overflow: TextOverflow.ellipsis,

                  ),
                  SizedBox(height: 20,),

                  Container(
                    width: double.maxFinite,
                    height: 190,

                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30)
                    ),

                    child: ListView.builder(
                      itemCount: jobtype.length,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () {
                          print(jobtype[index]);
                          Navigator.push(context,MaterialPageRoute(builder: (context) {
                            return BrowseScreen(isRemote: true , jobtype : jobtype[index] );
                          },));

                        },
                        child: SizedBox(
                          width: 200,
                          child: Card(

                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25)
                            ),

                            color: AppColor.bgcolor,
                            child:
                            Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Image.network('${imagelist[index]}',

                                fit: BoxFit.cover,
                                  width: 100,
                                  height: 100,
                                ),
                                Text('${jobtype[index]}',
                                  style: TextStyle(
                                    fontSize:AppFont.bigfont+2,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.white,

                                  ),
                                  maxLines: 6,
                                  overflow: TextOverflow.ellipsis,

                                ),
                                // Text('5',
                                //   style: TextStyle(
                                //     fontSize:AppFont.bigfont+2,
                                //     fontWeight: FontWeight.w400,
                                //     color: Colors.white,
                                //
                                //   ),
                                //   maxLines: 6,
                                //   overflow: TextOverflow.ellipsis,
                                //
                                // ),
                              ],
                            )
                            ,),
                        ),
                      );
                    },),
                  ),
                  // CategoryCardList(),
                  SizedBox(height: 30,),
                  Text('Category',
                    style: TextStyle(
                      fontSize:AppFont.bigfont+2,
                      fontWeight: FontWeight.w400,
                      color: Colors.white,

                    ),
                    maxLines: 6,
                    overflow: TextOverflow.ellipsis,

                  ),
                  SizedBox(height: 20,),
                  Expanded(child: ListView.builder(
                    itemCount: cateitem.length,

                    itemBuilder: (context, index) {
                    return Card(
                      color: AppColor.bgcolor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)
                      ),

                      child: ListTile(
                        enabled: true,
                        contentPadding: EdgeInsets.all(10),

                        title: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              child: Text('${cateitem[index]}',style: TextStyle(
                                  color: Colors.white ,
                                  fontWeight: FontWeight.w500,
                                  fontSize: AppFont.medfont+2,
                                  overflow: TextOverflow.ellipsis
                              ),),
                            ),



                          ],
                        ),



                        // leading: Icon(
                        //
                        //   Icons.business
                        //
                        //   ,size: 40,
                        //
                        //   color: Colors.white60 ,),
                        leading: Image.network('https://cdn3d.iconscout.com/3d/premium/thumb/search-folder-6842114-5604458.png',
                        width: 56,
                          height:56,
                          fit: BoxFit.cover
                        ),
                        trailing:        GestureDetector(
                          onTap: ()=>Navigator.push(context,MaterialPageRoute(builder: (context) {
                            return CateBrowseScreen(isRemote: true,isCategory: true,cateitem:cateitem[index] ,);
                          },)),
                          child:      Text('View More',style: TextStyle(
                              color: AppColor.primarycolor  ,
                              fontSize: AppFont.smfont+1
                          ),),
                        )


                      ),
                    );
                  },))

                  // Center(
                  //   child: ElevatedButton(
                  //     style: ButtonStyle(
                  //       elevation: MaterialStatePropertyAll(0),
                  //       backgroundColor: MaterialStatePropertyAll(AppColor.bgcolor)
                  //     ),
                  //
                  //       onPressed: ()=>null,
                  //       child: Text('Views All Jobs')
                  //   ),
                  // ),

                ],
              )
            ),
          ),

        ),
      ),
    );
  }
}
