import 'package:flutter/material.dart';

import 'package:jobapp/VIews/AuthScreen/Login.dart';
import 'package:jobapp/VIews/AuthScreen/register.dart';
import 'package:jobapp/VIews/Jobs_details/filterform.dart';
import 'package:jobapp/VIews/WelcomeScreen/screendiscover.dart';
import 'package:jobapp/VIews/WelcomeScreen/screenfindjob.dart';
import 'package:jobapp/VIews/Widgets/Joblistv1.dart';
import 'package:jobapp/ViewModel/job_view_model.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../data/Response/status_response.dart';
import '../../res/Appcolor.dart';
import '../../res/Appfont.dart';
import '../Jobs_details/pfscreen.dart';
import '../Jobs_details/Msg/messages.dart';
import '../Widgets/cardlistv1.dart';
import 'browsescreen.dart';

class ScreenHiring extends StatefulWidget {
  var msglength;
  ScreenHiring ({Key? key,this.msglength}) : super(key: key);
  @override
  State<ScreenHiring> createState() => _ScreenHiringState();
}

class _ScreenHiringState extends State<ScreenHiring> {
  var navlabel =['Search','Inbox','Home','Discover','Profile'];
  var uid ;
  var appbarlabel =['Filter Jobs','Inbox','Discover','Profile'];
  var name ='';

  var isAuth;
  var messagelength = 0;
  var ScreenTab = [
    FilterScreen(open: true ),

    InboxScr(open: true),

    ScreenFindJob(),

    DiscoverScreen(open: true),

    PfScreen(open: true)

  ];



  var iconlabel =<Widget> [
    Icon(Icons.search),
    Icon(Icons.messenger_sharp),
    Icon( Icons.home_filled),
    Icon(Icons.language_sharp),
    Icon(Icons.account_circle)
  ];
  var selectedindex = 2;
  JobViewModel postjobviewmodel = JobViewModel();
  JobViewModel userviewmodel = JobViewModel();
  JobViewModel msgviewmodel = JobViewModel();
  @override

  void initState() {
    // TODO: implement initState
    checkauth();
    loadmessage();


  }
  @override
  Widget build(BuildContext context) {
    messagelength = widget.msglength ?? 0;
    return Scaffold(
      // appBar: AppBar(
      //   backgroundColor: AppColor.primarycolor,
      //   elevation: 0,
      //   iconTheme: IconThemeData(
      //       color: AppColor.primarycolor
      //   ),
      //
      // ),
      backgroundColor: Colors.indigo,

      body: ScreenTab.elementAt(selectedindex) ,

      bottomNavigationBar: ChangeNotifierProvider<JobViewModel>(

        create: (context) => userviewmodel,
        builder: (context, child) {
          return  Consumer<JobViewModel>(

            builder: (context, result, child) {
              var state = result.userApiResponse.status;
              var roles = result.userApiResponse.data?.data?.role;
              var status = msgviewmodel.messageResponse?.status;

              print("msg state : ${status}");
              print("Default Screen");
              print(state);
              print(roles);
              if(state  == Status.COMPLETED){
                WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                  if(roles =='Job Seeker'){


                  }


                });






              }
              // if(status == Status.COMPLETED){
              //   WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
              //     var total =0;
              //
              //     var index= msgviewmodel.messageResponse?.data?.data?.job?.length;
              //
              //
              //     print(total);
              //
              //     setState(() {
              //       for(int i =0;i<index!;i++){
              //         total += msgviewmodel.messageResponse!.data!.data!.job![i].messages!.length;
              //
              //       }
              //       messagelength = total;
              //
              //     });
              //
              //
              //
              //   });
              // }

              else{
                print("True");



              }

              return  BottomNavigationBar(
                  currentIndex:selectedindex ,
                  elevation: 0,
                  onTap: (value) {


                    setState(() {
                      checkauth();

                      selectedindex = value;
                    });
                  },
                  backgroundColor: Colors.indigo,

                  type: BottomNavigationBarType.shifting,
                  mouseCursor: SystemMouseCursors.grab,
                  showSelectedLabels:true,
                  showUnselectedLabels: true,

                  selectedItemColor: AppColor.whitecolor,
                  unselectedItemColor: Colors.grey,
                  // unselectedIconTheme:   IconThemeData(
                  //   size: 17
                  // ),
                  selectedIconTheme: IconThemeData(
                      size: 28
                  ),

                  selectedLabelStyle: TextStyle(
                      fontSize: AppFont.smfont-1,
                    height: 1.5
                  ),

                  selectedFontSize: AppFont.smfont+1,
                  items: List.generate(iconlabel.length,(index) {
                    print('length: ${iconlabel.length}');

                    return  BottomNavigationBarItem(
                        backgroundColor: Colors.indigo,

                        label: '${navlabel[index]}',
                        icon:

                        Stack(

                          children: [
                            iconlabel[index],
                            if(index ==1)
                              if(       messagelength>0 )
                           Positioned(
                             top: 0,
                             right: 1,
                              child: Container(
                                width: 15,
                                height: 15,
                                child: CircleAvatar(
                                  backgroundColor: AppColor.fourthcolor,
                                  radius: 50,
                                  child: Text('${messagelength ??0}',style: TextStyle(
                                    color: AppColor.thirdcolor,
                                    fontSize: AppFont.smfont+1,
                                    fontWeight: FontWeight.bold
                                  ),),
                                ),
                              ),
                            )
                          ],

                        )
                    );
                  }, )

              );
            },

          );
        },

      ),
    );

  }
  void checkauth () async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    isAuth= prefs.get('tokens') ?? false; //if there is no token false
    uid = prefs.getString('userid');
    var tokens= prefs.getString('tokens');
    print("The token in browse screen fetch ${tokens}");
    // postjobviewmodel.fetchall(tokens,null);
    print("check auth here");
    userviewmodel.fetchUserbyId( uid );
    msgviewmodel.fetchMsg(uid);

    // print("USer info");
    //
    //   print(isAuth);
    //   print("USer info");
    //   print(  prefs.getString('userid'));

  }

  void loadmessage() async{

  }
}
