import 'package:flutter/material.dart';

import 'package:jobapp/VIews/Jobs_details/filterform.dart';

import 'package:jobapp/ViewModel/job_view_model.dart';

import 'package:lottie/lottie.dart';

import 'package:provider/provider.dart';

import 'package:shared_preferences/shared_preferences.dart';

import '../../data/Response/status_response.dart';

import '../../res/Appcolor.dart';

import '../../res/Appfont.dart';

import '../../res/BaseUrl.dart';
import '../Jobs_details/job_detail.dart';

class BrowseScreen extends StatefulWidget {
var isRecommend ;
var lists;

var isRemote;
var isCategory;
var jobtype;
var cateitem;
 BrowseScreen ({Key? key,
   this.isRecommend, this.lists
   ,this.isRemote, this.isCategory, this.jobtype,this.cateitem}) : super(key: key);

  @override
  State<BrowseScreen> createState() => _BrowseScreenState();
}

class _BrowseScreenState extends State<BrowseScreen> {
  JobViewModel postjobviewmodel = JobViewModel();
  JobViewModel recommendjobviewmodel = JobViewModel();
  var token;
  var txtsearch = TextEditingController();
  var _scrollController = ScrollController();
  var        recommentlist = [];
  var Remote= false;

  var   issearch;

  @override
void initState() {
    // TODO: implement initState
    super.initState();
print(widget.cateitem);
print(widget.isCategory);
   Remote  = widget.isRemote ?? false;

    checkpreference(txtsearch.text); //TODO FETCH HERE
    if(widget.isRecommend == true){
      postjobviewmodel.FetchRecommend(widget.lists);
      return null;

    }
    // _scrollController;

      // postjobviewmodel.fetchjoball(token,1);







  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColor.primarycolor,
        elevation: 0,
        titleSpacing: 0,

        iconTheme: IconThemeData(
            color: AppColor.thirdcolor
        ),
        title:      Text('Browse',
          style: TextStyle(
            fontSize:AppFont.megafont+2,
            fontWeight: FontWeight.w500,
            color: AppColor.thirdcolor

          ),
          maxLines: 6,
          overflow: TextOverflow.ellipsis,

        ),


      ),
      backgroundColor: AppColor.primarycolor,
      
      body: SafeArea(
        child:
        widget.isRecommend == true ?
        ChangeNotifierProvider<JobViewModel>(
          create: (context) =>   postjobviewmodel ,
          builder: (context, child) {
            return Consumer<JobViewModel>(

              builder: (context, jobs, child) {

                var status =  postjobviewmodel .jobReresponse.status;


                print("job recommend  status ${status}");
                var lengths =  postjobviewmodel .jobReresponse.data?.data?.attributes?.length;


                var joblist =  postjobviewmodel .jobReresponse.data?.data?.attributes;


                print(recommentlist);
                print("Status api here  ");
                print(status);
                switch(status){
                  case Status.COMPLETED:
                    return Container(
                      height:double.maxFinite,
                      decoration: BoxDecoration(

                      ),



                      margin: EdgeInsets.symmetric(vertical: 0,horizontal: 15),
                      child:   Column(
                        children: [


                          SizedBox(height: 25,),
                          Text('${lengths == null ? '' : '${lengths} Available Jobs ' } ',
                              style: TextStyle(
                                fontSize:AppFont.bigfont+3,
                                fontWeight: FontWeight.w400,
                                color: AppColor.thirdcolor,

                              )),

                          Expanded(child:lengths == null?
                          Container(
                            margin: EdgeInsets.symmetric(vertical: 100),
                            child: Center(child: Column(
                              children: [
                                Lottie.network('https://assets5.lottiefiles.com/packages/lf20_zadfo6lc.json',


                                    fit: BoxFit.contain,
                                    width: 230,
                                    height: 230

                                ),
                                Text('Sorry, we couldn\'t find any result  ',style: TextStyle(
                                    color: AppColor.thirdcolor,
                                    fontSize: AppFont.smfont+1,
                                    fontWeight: FontWeight.w400

                                ),),
                              ],
                            ),),
                          ) :

                          ListView.builder(

                            // physics: NeverScrollableScrollPhysics(),


                            itemCount:lengths,
                            // controller:  _scrollController ,

                            itemBuilder: (context, index) {
                              print("Recommends post ");
                              print(lengths);

                              var posts = jobs.jobReresponse.data?.data?.attributes?[index];


                              // var post =  postjobviewmodel.jobApiResponse.data!.data![index].attributes;
                              // dynamic path =postjobviewmodel.jobApiResponse.data!.data!.attributes![index].user?.thumbnail?.responses?.path;
                              // print(postjobviewmodel.jobApiResponse.data!.data!.attributes![index].title);
                              print(posts);
                              return Container(
                                margin: EdgeInsets.symmetric(vertical: 20),
                                decoration: BoxDecoration(
                                    color: AppColor.thirdcolor,
                                    borderRadius: BorderRadius.circular(10)
                                ),
                                child: Padding(
                                  padding:EdgeInsets.symmetric(vertical: 15),
                                  child:
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      ListTile(

                                        style: ListTileStyle.drawer,
                                        leading: CircleAvatar(
                                          backgroundImage:posts?.user?.thumbnail?.attributes?.path == null ?
                                          NetworkImage('https://i.pinimg.com/564x/48/6c/a0/486ca00640b169300b48e9ceacd8e401.jpg') :
                                          NetworkImage('${ApiApiUrl.localurl}/${posts?.user?.thumbnail?.attributes?.path}')
                                          ,
                                        ),
                                        title:         Row(
                                          children: [
                                            Expanded(
                                              child: Text('${posts?.title.toString().substring(0)}',
                                                style: TextStyle(
                                                  fontSize:AppFont.bigfont+1,
                                                  fontWeight: FontWeight.w500,

                                                  color: Colors.black,

                                                  overflow: TextOverflow.ellipsis,


                                                ),
                                                overflow: TextOverflow.ellipsis,
                                                maxLines:1,
                                                softWrap:false,



                                              ),
                                            )

                                            ,
                                          ],
                                        ),
                                        subtitle: Text('Salary range \$ ${posts?.jobDescription?.minSalary} -'
                                            '\$ ${posts?.jobDescription?.maxSalary}'),
                                        trailing: IconButton(onPressed: ()=>Navigator.push(context,MaterialPageRoute(builder: (context) =>
                                            JobDetailSr(uid: posts?.sId) //TODO SCREEN JOB DETAIL
                                          ,)),
                                            icon: Icon(Icons.navigate_next_outlined)
                                        ),

                                        isThreeLine: true,

                                      ),

                                      Container(
                                        margin: EdgeInsets.symmetric(horizontal: 18),

                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            ElevatedButton(
                                              onPressed: ()=> null,

                                              child: Text('${posts?.jobDescription?.jobtype}',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: AppFont.smfont+1
                                                ),
                                              ),
                                              style: ButtonStyle(
                                                  backgroundColor:MaterialStatePropertyAll(AppColor.primarycolor),
                                                  elevation: MaterialStatePropertyAll(0)
                                              ),


                                            ),
                                            SizedBox(width: 25,),
                                            ElevatedButton(
                                              onPressed: ()=>null,
                                              child: Text('${posts?.jobDescription?.experience}',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: AppFont.smfont+1
                                                ),
                                              ),
                                              style: ButtonStyle(
                                                  backgroundColor:MaterialStatePropertyAll(AppColor.fourthcolor),
                                                  elevation: MaterialStatePropertyAll(0)
                                              ),


                                            ),
                                            SizedBox(width: 25,),
                                            ElevatedButton(
                                              onPressed: ()=>null,
                                              child: Text('${posts?.status == true ?  'Available' : 'Not Availble'}',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: AppFont.smfont+1
                                                ),
                                              ),
                                              style: ButtonStyle(
                                                  backgroundColor:
                                                  posts?.status == true ?

                                                  MaterialStatePropertyAll(Colors.greenAccent) :
                                                  MaterialStatePropertyAll(Colors.grey),
                                                  elevation: MaterialStatePropertyAll(0)
                                              ),


                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              );
                            },
                          )),

                          // Expanded(child: CardDisplay(title: txtsearch.text,)),
                          SizedBox(height: 25,),
                        ],
                      )


                    );

                  case Status.LOADING:
                    return
                    Center(
                      child: CircularProgressIndicator(
                        backgroundColor:Colors.indigo,
                        color: AppColor.primarycolor,



                      ),
                    );

                  case Status.ERROR:
                    return Center(
                      child: Text('Error has been occur'),
                    );
                  default:
                    return Center(
                      child: Text('Error has been occur'),
                    );
                }

              },

            );
          },

        ) :

        ChangeNotifierProvider<JobViewModel>(
          create: (context) =>  postjobviewmodel ,
      builder: (context, child) {
        return Remote == true ?
         Consumer<JobViewModel>(

          builder: (context, jobs, child) {

            var status =postjobviewmodel.jobApiResponse.status;
            var length =postjobviewmodel.jobApiResponse.data?.data?.attributes?.length;

            print("job length search ${length}");
            var lengths =jobs.jobReresponse.data?.data?.attributes?.length;


            var joblist =postjobviewmodel.jobReresponse.data?.data?.attributes;


            print(recommentlist);
            print("Status api here  ");
            print(status);
            switch(status){
              case Status.COMPLETED:
                return Container(
                  height:double.maxFinite,
                  decoration: BoxDecoration(

                  ),



                  margin: EdgeInsets.symmetric(vertical: 0,horizontal: 15),
                  child:



                  Column(

                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,


                    children: [





                      Expanded(child: NewWidget(length: length  ?? 0,

                          scrollController: _scrollController,


                          jobs: jobs,title : txtsearch.text,isRemote:Remote, type:widget?.jobtype,

                        category:widget.isCategory,item: widget.cateitem,
                      )),

                      // Expanded(child: CardDisplay(title: txtsearch.text,)),
                      SizedBox(height: 25,),
                      //display card list
                    ],
                  ),

                );

              case Status.LOADING:
                return 
                Center(
                  child: CircularProgressIndicator(
                    backgroundColor:Colors.indigo,
                    color: AppColor.primarycolor,



                  ),
                );

              case Status.ERROR:
                return Center(
                  child: Text('Error has been occur'),
                );
              default:
                return Center(
                  child: Text('Error has been occur'),
                );
            }

          },

        ) :
          Consumer<JobViewModel>(

          builder: (context, jobs, child) {

            var status =postjobviewmodel.jobApiResponse.status;
            var length =postjobviewmodel.jobApiResponse.data?.data?.attributes?.length;

            print("job length search ${length}");
            var lengths =jobs.jobReresponse.data?.data?.attributes?.length;


            var joblist =postjobviewmodel.jobReresponse.data?.data?.attributes;


            print(recommentlist);
            print("View all here ");
            print( widget.isRecommend);
            print(status);
            switch(status){
              case Status.COMPLETED:
                return Container(
                  height:double.maxFinite,
                  decoration: BoxDecoration(

                  ),



                  margin: EdgeInsets.symmetric(vertical: 0,horizontal: 15),
                  child:

                  Column(

                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,


                    children: [

                      // SizedBox(height: 20,),



                      Expanded(child: NewWidget(length: length  ?? 0,

                        scrollController: _scrollController,


                        jobs: jobs,title : txtsearch.text)),

                      // Expanded(child: CardDisplay(title: txtsearch.text,)),
                      // SizedBox(height: 5,),
                      //display card list
                    ],
                  ),

                );

              case Status.LOADING:
                return Center(
                  child: CircularProgressIndicator(
                    backgroundColor:Colors.indigo,
                    color: AppColor.primarycolor,



                  ),
                );

              // case Status.LOADING:
              //   return length == null ?
              //
              //     Padding(
              //       padding: const EdgeInsets.symmetric(vertical: 100),
              //       child: Center(child: Column(
              //         children: [
              //           Lottie.network('https://assets9.lottiefiles.com/packages/lf20_eltfOSSxwb.json',
              //
              //
              //               fit: BoxFit.cover,
              //               width: 240,
              //               height: 240
              //
              //           ),
              //           Text('Sorry, we couldnt find any related result' ,style: TextStyle(
              //               color: Colors.grey,
              //               fontSize: AppFont.smfont+1,
              //               fontWeight: FontWeight.w400
              //
              //           ),),
              //         ],
              //       ),),
              //     ):
              //     Center(
              //     child: CircularProgressIndicator(
              //       backgroundColor: AppColor.primarycolor,
              //     ),
              //   );

              case Status.ERROR:
                return Center(
                  child: Text('Error has been occur'),
                );
              default:
                return Center(
               child: Text('Error has been occur'),
                );
            }

          },

        );
      },

        ),
      ),
    );
  }


  void checkpreference(String? title)async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    var tokens= prefs.getString('tokens');
    print("The token in browse screen fetch ${tokens}");
    if(widget.isRecommend == true){
      await        recommendjobviewmodel.fetchall(tokens,title);
      return null;

    }
   await  postjobviewmodel.fetchall(tokens,title);
  }

}

class NewWidget extends StatefulWidget {
  var title;
  var isRemote;
  var type;
  var category;
  var item;
  var jobs;
 NewWidget({
    super.key,
    required this.length,

    required ScrollController scrollController,
    required this.jobs,
   this.title,
   this.isRemote,
   this.type,
   this.category,
   this.item

  }) : _scrollController = scrollController;

  final int? length;

  final ScrollController _scrollController;

  @override
  State<NewWidget> createState() => _NewWidgetState();
}

class _NewWidgetState extends State<NewWidget> {
  var isRemote;
  var _scrollControllerss = ScrollController();
  var page = 1;
var data = [];
var maxpage;
var searchtitle ;

  var isloading = false ;
  var txtsearch = TextEditingController();
  var  issearch = false ;
  JobViewModel jobviewmodel = JobViewModel();
  @override
  void initState()  {

    // TODO: implement initState
    super.initState();
    isRemote = widget.isRemote ?? false;
    print(widget.title ?? 'null title');
    print("gay 2");
    print(widget.item);
    print(widget.category);
    searchtitle = widget.title;
    print(widget.category);
    print(widget?.type);


    if(widget.category == true){
      fetchcategory();
      return null;


      print("gay");

    }
    print("Page fetching 1");


      jobviewmodel.fetchall('', '');


    maxpage = jobviewmodel.jobApiResponse?.data?.meta?.pagination?.pageSize;
    print(maxpage);
    // jobviewmodel.fet
  if(widget?.isRemote == true){
    checkpreference(txtsearch.text,page);
  }
    _scrollControllerss.addListener(getScrollBot);

  }
  Widget build(BuildContext context) {

    return ChangeNotifierProvider<JobViewModel>(

      create: (context) =>jobviewmodel ,

      builder: (context, child) {

        return widget?.isRemote != true ?Consumer<JobViewModel>(

          builder: (context, jobs, child) {
            var status = jobs.jobApiResponse.status;

            switch(status){


              case Status.COMPLETED:
                var lengths = jobs.jobApiResponse.data?.data?.attributes?.length;


              maxpage = jobs.jobApiResponse.data?.meta?.pagination?.pageSize;
                print(maxpage);

                print("Status ${lengths}");


                var dataset;
                if(txtsearch.text== ''){
                  if(lengths != 0 ){
                    data.addAll(jobviewmodel.jobApiResponse.data!.data!.attributes!);
                    dataset =data.toSet().toList();
                  }
                }

                else{
                 dataset = jobs.jobApiResponse.data?.data?.attributes;
                }

                if(isRemote == true){
                  var newdata =[];
                  newdata.addAll(jobviewmodel.jobApiResponse.data!.data!.attributes!);
                  // dataset = jobs.jobApiResponse.data?.data?.attributes?.map((e) => e.jobDescription?.jobtype ==widget.type).toList();
                  if( txtsearch.text == '' &&  lengths!=null){
                      dataset.clear();

                  }
                  dataset.clear();
                  // dataset = jobs.jobApiResponse.data?.data?.attributes;

                  newdata.toSet().toList().forEach((job) {
                 if(job.jobDescription?.jobtype ==widget.type){
                   print("true");
                   print(job.jobDescription?.jobtype);
                   dataset.add(job );
                 }


               },);

                }

                print(dataset);
                // for(int index =0 ;index<lengths!;index++ ){
                //
                //     data.
                // }

                print("data length : ${dataset?.length}");
                var lendata = dataset?.length ?? 0;
                return dataset?.length == 0 ?
                Container(
                  // margin: EdgeInsets.symmetric(vertical: 10),
                  child: Center(child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Lottie.network('https://assets5.lottiefiles.com/packages/lf20_zadfo6lc.json',


                          fit: BoxFit.contain,
                          width: 230,
                          height: 230

                      ),
                      Text('Sorry, we couldn\'t find any result  ',style: TextStyle(
                          color: AppColor.thirdcolor,
                          fontSize: AppFont.smfont+1,
                          fontWeight: FontWeight.w400

                      ),),
                    ],
                  ),),
                ) :




                Column(
                  children: [
                    //

                    Container(
                      width: double.maxFinite,

                      child: Row(
                        mainAxisAlignment:MainAxisAlignment.spaceBetween ,
                        children: [

                          Expanded(
                            flex: 15,
                            child: Container(
                              height: 60,
                              child: TextField(

                                decoration: InputDecoration(
                                  fillColor: AppColor.thirdcolor,
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(12),
                                    borderSide: BorderSide(color: AppColor.primarycolor)
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(12),
                                      borderSide: BorderSide(color: AppColor.primarycolor)
                                  ),
                                  suffixIcon:null,
                                  filled: true,
                                  enabled: true,
                                  focusColor:Colors.white ,

                                  prefixIcon: Icon(Icons.search_sharp),
                                  hintText: 'Search for jobs title',


                                ),

                                controller: txtsearch,

                                onSubmitted:(value) async {
                                  // joblist?.clear();

                                  setState(() {
                                    issearch = true;
                                    print("search title here");
                                    page = 1;
                                    maxpage = 1;
                                    txtsearch.text = value;
                                    checkpreference(txtsearch.text,page);
                                    data.clear();
                                    dataset.clear();
                                    dataset?.length = 0;
                                    lengths =0 ;
                                    lendata = 0;

                                    print(txtsearch.text);


                                  });


                                },

                                enabled: true ,



                              ),
                            ),
                          ),
                          SizedBox(width: 10,),
                          Expanded(
                              flex: 3,
                              child: Container(
                                height: 60,
                                child: ElevatedButton(
                            child: Icon(Icons.menu_open_outlined),
                            onPressed: () {
                                Navigator.push(context,MaterialPageRoute(builder: (context) {
                                  return FilterScreen();
                                },));
                            },
                                  style: ButtonStyle(
                                      backgroundColor: MaterialStatePropertyAll(Colors.indigo),
                                    shape: MaterialStatePropertyAll(RoundedRectangleBorder(

                                      borderRadius: BorderRadius.circular(14)
                                    ))
                                  ),
                          ),
                              )),


                        ],
                      ),
                    ),
                    SizedBox(height: 11,),
                    Expanded(
                      child:dataset?.length== null ?
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 30),
                        child: Center(child: Column(
                          children: [
                            Lottie.network('https://assets5.lottiefiles.com/packages/lf20_zadfo6lc.json',


                                fit: BoxFit.contain,
                                width: 230,
                                height: 230

                            ),
                            Text('Sorry, we couldn\'t find any result  ',style: TextStyle(
                                color: AppColor.thirdcolor,
                                fontSize: AppFont.smfont+1,
                                fontWeight: FontWeight.w400

                            ),),
                          ],
                        ),),
                      )
                          :
                        RefreshIndicator(
                        onRefresh: ()async{
                          // page =1 ;
                          // maxpage = 1;
                          // data.clear();
                          // dataset.clear();

                          // await jobviewmodel.fetchjoball('token', 1);
                        },
                        child: ListView.builder(


                          // physics: NeverScrollableScrollPhysics(),
                          controller:
                          issearch == true ?
                          null :


                          _scrollControllerss,

                          itemCount: isloading == true ? lendata?? 0 + 1  : lendata,




                          itemBuilder: (context, index) {
                            if(index == lendata){
                              return Center(
                                child: CircularProgressIndicator(
                                  backgroundColor: AppColor.fourthcolor,

                                ),
                              );
                            }
                            else{

                              var posts =   dataset[index];



                              // var post =  postjobviewmodel.jobApiResponse.data!.data![index].attributes;
                              // dynamic path =postjobviewmodel.jobApiResponse.data!.data!.attributes![index].user?.thumbnail?.responses?.path;
                              // print(postjobviewmodel.jobApiResponse.data!.data!.attributes![index].title);
                              print(posts);
                              return Container(
                                margin: EdgeInsets.symmetric(vertical: 12),
                                decoration: BoxDecoration(
                                  color: AppColor.thirdcolor,
                                  borderRadius: BorderRadius.circular(12)

                                ),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 15),
                                  child:
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [

                                      ListTile(

                                        style: ListTileStyle.drawer,
                                        leading: CircleAvatar(
                                          backgroundImage:posts?.user?.thumbnail?.attributes?.path == null ?
                                          NetworkImage('https://i.pinimg.com/564x/48/6c/a0/486ca00640b169300b48e9ceacd8e401.jpg') :
                                          NetworkImage('${ApiApiUrl.localurl}/${posts?.user?.thumbnail?.attributes?.path}')
                                          ,
                                        ),
                                        title:         Row(
                                          children: [
                                            Expanded(
                                              child: Text('${posts?.title.toString().substring(0)}',
                                                style: TextStyle(
                                                  fontSize:AppFont.bigfont+1,
                                                  fontWeight: FontWeight.w500,

                                                  color: Colors.black,

                                                  overflow: TextOverflow.ellipsis,


                                                ),
                                                overflow: TextOverflow.ellipsis,
                                                maxLines:1,
                                                softWrap:false,



                                              ),
                                            )

                                            ,
                                          ],
                                        ),
                                        subtitle: Text('Salary range \$ ${posts?.jobDescription?.minSalary} -'
                                            '\$ ${posts?.jobDescription?.maxSalary} '),
                                        trailing: IconButton(onPressed: ()=>Navigator.push(context,MaterialPageRoute(builder: (context) =>
                                            JobDetailSr(uid: posts?.sId) //TODO SCREEN JOB DETAIL
                                          ,)),
                                            icon: Icon(Icons.navigate_next_outlined)
                                        ),

                                        isThreeLine: true,

                                      ),

                                      Container(
                                        margin: EdgeInsets.symmetric(horizontal: 18),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            ElevatedButton(
                                              onPressed: ()=> null,

                                              child: Text('${posts?.jobDescription?.jobtype}',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: AppFont.smfont+1
                                                ),
                                              ),
                                              style: ButtonStyle(
                                                  backgroundColor:MaterialStatePropertyAll(AppColor.primarycolor),
                                                  elevation: MaterialStatePropertyAll(0)
                                              ),


                                            ),
                                            SizedBox(width: 25,),
                                            ElevatedButton(
                                              onPressed: ()=>null,
                                              child: Text('${posts?.jobDescription?.experience}',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: AppFont.smfont+1
                                                ),
                                              ),
                                              style: ButtonStyle(
                                                  backgroundColor:MaterialStatePropertyAll(AppColor.fourthcolor),
                                                  elevation: MaterialStatePropertyAll(0)
                                              ),


                                            ),
                                            SizedBox(width: 25,),
                                            ElevatedButton(
                                              onPressed: ()=>null,
                                              child: Text('${posts?.status == true ?  'Available' : 'Not Availble'}',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: AppFont.smfont+1
                                                ),
                                              ),
                                              style: ButtonStyle(
                                                  backgroundColor:
                                                  posts?.status == true ?

                                                  MaterialStatePropertyAll(Colors.greenAccent) :
                                                  MaterialStatePropertyAll(Colors.grey),
                                                  elevation: MaterialStatePropertyAll(0)
                                              ),


                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              );
                            }



                          },
                        ),
                      ),
                    ),
                  ],
                );


              case Status.LOADING:
                return Center(child: CircularProgressIndicator(),);

              default:
                return Center(child: CircularProgressIndicator(),);
            }



          },

        ) :
        Consumer<JobViewModel>(

          builder: (context, jobs, child) {
            var status = jobs.jobApiResponse.status;

            switch(status){


              case Status.COMPLETED:
                var lengths = jobs.jobApiResponse.data?.data?.attributes?.length;
                var jobitem = jobs.jobApiResponse.data?.data?.attributes;


                maxpage = jobs.jobApiResponse.data?.meta?.pagination?.pageSize;
                print(maxpage);

                print("Status ${lengths}");


                var dataset;
                // if(txtsearch.text== ''){
                //   if(lengths != 0 ){
                //     data.addAll(jobviewmodel.jobApiResponse.data!.data!.attributes!);
                //     dataset =data.toSet().toList();
                //   }
                // }
                //
                //
                // else{
                //   dataset = jobs.jobApiResponse.data?.data?.attributes;
                // }
                print("dataset");
                // print(dataset.length);
                data.addAll(jobitem! );
                print(data?.length);
                dataset =data.toSet().toList();
                if(isRemote == true){
                  var newdata =[];
                  newdata.addAll(dataset);

                  // dataset = jobs.jobApiResponse.data?.data?.attributes?.map((e) => e.jobDescription?.jobtype ==widget.type).toList();
                  if( txtsearch.text == '' &&  lengths!=null){
                    // dataset.clear();

                  }

                  // dataset = jobs.jobApiResponse.data?.data?.attributes;
                  print(widget.type);

                  newdata.toSet().toList().forEach((job) {
                    if(job.jobDescription?.jobtype ==widget.type){
                      print("true");

                      print(job.jobDescription?.jobtype);
                      dataset.add(job );
                    }
                    else{

                    }


                  },);

                }

                print(dataset.length);
                // for(int index =0 ;index<lengths!;index++ ){
                //
                //     data.
                // }

                print("data length : ${dataset?.length}");
                var lendata = dataset?.length ?? 0;
                return dataset?.length == 0 ?
                Container(
                  // margin: EdgeInsets.symmetric(vertical: 10),
                  child: Center(child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Lottie.network('https://assets5.lottiefiles.com/packages/lf20_zadfo6lc.json',


                          fit: BoxFit.contain,
                          width: 230,
                          height: 230

                      ),
                      Text('Sorry, we couldn\'t find any result  ',style: TextStyle(
                          color: AppColor.thirdcolor,
                          fontSize: AppFont.smfont+1,
                          fontWeight: FontWeight.w400

                      ),),
                    ],
                  ),),
                ) :




                Column(
                  children: [
                    //

                    Container(
                      width: double.maxFinite,

                      child: Row(
                        mainAxisAlignment:MainAxisAlignment.spaceBetween ,
                        children: [

                          Expanded(
                            flex: 15,
                            child: Container(
                              height: 60,
                              child: TextField(

                                decoration: InputDecoration(
                                  fillColor: AppColor.thirdcolor,
                                  enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(12),
                                      borderSide: BorderSide(color: AppColor.primarycolor)
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(12),
                                      borderSide: BorderSide(color: AppColor.primarycolor)
                                  ),
                                  suffixIcon:null,
                                  filled: true,
                                  enabled: true,
                                  focusColor:Colors.white ,

                                  prefixIcon: Icon(Icons.search_sharp),
                                  hintText: 'Search for jobs title',


                                ),

                                controller: txtsearch,

                                onSubmitted:(value) async {
                                  // joblist?.clear();

                                  setState(() {
                                    issearch = true;
                                    print("search title here");
                                    page = 1;
                                    maxpage = 1;
                                    txtsearch.text = value;
                                    checkpreference(txtsearch.text,page);
                                    data.clear();
                                    dataset.clear();
                                    dataset?.length = 0;
                                    lengths =0 ;
                                    lendata = 0;

                                    print(txtsearch.text);


                                  });


                                },

                                enabled: true ,



                              ),
                            ),
                          ),
                          SizedBox(width: 10,),
                          Expanded(
                              flex: 3,
                              child: Container(
                                height: 60,
                                child: ElevatedButton(
                                  child: Icon(Icons.menu_open_outlined),
                                  onPressed: () {
                                    Navigator.push(context,MaterialPageRoute(builder: (context) {
                                      return FilterScreen();
                                    },));
                                  },
                                  style: ButtonStyle(
                                      backgroundColor: MaterialStatePropertyAll(Colors.indigo),
                                      shape: MaterialStatePropertyAll(RoundedRectangleBorder(

                                          borderRadius: BorderRadius.circular(14)
                                      ))
                                  ),
                                ),
                              )),


                        ],
                      ),
                    ),
                    SizedBox(height: 11,),
                    Expanded(
                      child:dataset?.length== null ?
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 30),
                        child: Center(child: Column(
                          children: [
                            Lottie.network('https://assets5.lottiefiles.com/packages/lf20_zadfo6lc.json',


                                fit: BoxFit.contain,
                                width: 230,
                                height: 230

                            ),
                            Text('Sorry, we couldn\'t find any result  ',style: TextStyle(
                                color: AppColor.thirdcolor,
                                fontSize: AppFont.smfont+1,
                                fontWeight: FontWeight.w400

                            ),),
                          ],
                        ),),
                      )
                          :
                      RefreshIndicator(
                        onRefresh: ()async{
                          // page =1 ;
                          // maxpage = 1;
                          // data.clear();
                          // dataset.clear();

                          // await jobviewmodel.fetchjoball('token', 1);
                        },
                        child: ListView.builder(


                          // physics: NeverScrollableScrollPhysics(),



                          itemCount:     dataset?.length,




                          itemBuilder: (context, index) {
                            if(index ==  dataset.length){
                              return Center(
                                child: CircularProgressIndicator(
                                  backgroundColor: AppColor.fourthcolor,

                                ),
                              );
                            }
                            else{

                              var posts =   dataset[index];



                              // var post =  postjobviewmodel.jobApiResponse.data!.data![index].attributes;
                              // dynamic path =postjobviewmodel.jobApiResponse.data!.data!.attributes![index].user?.thumbnail?.responses?.path;
                              // print(postjobviewmodel.jobApiResponse.data!.data!.attributes![index].title);
                              print(posts);
                              return Container(
                                margin: EdgeInsets.symmetric(vertical: 12),
                                decoration: BoxDecoration(
                                    color: AppColor.thirdcolor,
                                    borderRadius: BorderRadius.circular(12)

                                ),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 15),
                                  child:
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [

                                      ListTile(

                                        style: ListTileStyle.drawer,
                                        leading: CircleAvatar(
                                          backgroundImage:posts?.user?.thumbnail?.attributes?.path == null ?
                                          NetworkImage('https://i.pinimg.com/564x/48/6c/a0/486ca00640b169300b48e9ceacd8e401.jpg') :
                                          NetworkImage('${ApiApiUrl.localurl}/${posts?.user?.thumbnail?.attributes?.path}')
                                          ,
                                        ),
                                        title:         Row(
                                          children: [
                                            Expanded(
                                              child: Text('${posts?.title.toString().substring(0)}',
                                                style: TextStyle(
                                                  fontSize:AppFont.bigfont+1,
                                                  fontWeight: FontWeight.w500,

                                                  color: Colors.black,

                                                  overflow: TextOverflow.ellipsis,


                                                ),
                                                overflow: TextOverflow.ellipsis,
                                                maxLines:1,
                                                softWrap:false,



                                              ),
                                            )

                                            ,
                                          ],
                                        ),
                                        subtitle: Text('Salary range \$ ${posts?.jobDescription?.minSalary} -'
                                            '\$ ${posts?.jobDescription?.maxSalary} '),
                                        trailing: IconButton(onPressed: ()=>Navigator.push(context,MaterialPageRoute(builder: (context) =>
                                            JobDetailSr(uid: posts?.sId) //TODO SCREEN JOB DETAIL
                                          ,)),
                                            icon: Icon(Icons.navigate_next_outlined)
                                        ),

                                        isThreeLine: true,

                                      ),

                                      Container(
                                        margin: EdgeInsets.symmetric(horizontal: 18),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            ElevatedButton(
                                              onPressed: ()=> null,

                                              child: Text('${posts?.jobDescription?.jobtype}',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: AppFont.smfont+1
                                                ),
                                              ),
                                              style: ButtonStyle(
                                                  backgroundColor:MaterialStatePropertyAll(AppColor.primarycolor),
                                                  elevation: MaterialStatePropertyAll(0)
                                              ),


                                            ),
                                            SizedBox(width: 25,),
                                            ElevatedButton(
                                              onPressed: ()=>null,
                                              child: Text('${posts?.jobDescription?.experience}',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: AppFont.smfont+1
                                                ),
                                              ),
                                              style: ButtonStyle(
                                                  backgroundColor:MaterialStatePropertyAll(AppColor.fourthcolor),
                                                  elevation: MaterialStatePropertyAll(0)
                                              ),


                                            ),
                                            SizedBox(width: 25,),
                                            ElevatedButton(
                                              onPressed: ()=>null,
                                              child: Text('${posts?.status == true ?  'Available' : 'Not Availble'}',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: AppFont.smfont+1
                                                ),
                                              ),
                                              style: ButtonStyle(
                                                  backgroundColor:
                                                  posts?.status == true ?

                                                  MaterialStatePropertyAll(Colors.greenAccent) :
                                                  MaterialStatePropertyAll(Colors.grey),
                                                  elevation: MaterialStatePropertyAll(0)
                                              ),


                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              );
                            }



                          },
                        ),
                      ),
                    ),
                  ],
                );


              case Status.LOADING:
                return Center(child: CircularProgressIndicator(),);

              default:
                return Center(child: CircularProgressIndicator(),);
            }



          },

        );
      },

    );

  }




  void checkpreference(String? title,page)async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    var tokens= prefs.getString('tokens');

    print("The token in browse screen fetch ${tokens}");
    await  jobviewmodel.fetchall(tokens,title);
  }
void getScrollBot() async {

    if(_scrollControllerss.position.pixels == _scrollControllerss.position.maxScrollExtent){
      print("reach bottom scroll maxpage : ${maxpage  }");
      print(maxpage);
      if(page != maxpage){
        setState(() {

          isloading= true;
        });
        page +=1;
        await jobviewmodel.fetchjoball('token', page);

        setState(() {
          isloading= false;
        });
      }
      // page = 1;
      // await jobviewmodel.fetchjoball('token', page);


        print(page);



    }

  }

  void fetchcategory()async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
var tokens = prefs.get('tokens');
print(tokens);
    await jobviewmodel.fetchbyCategory(tokens,1,widget.item );



  }
}


