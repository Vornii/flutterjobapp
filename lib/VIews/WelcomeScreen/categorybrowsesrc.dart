import 'package:flutter/material.dart';

import 'package:jobapp/VIews/Jobs_details/filterform.dart';

import 'package:jobapp/ViewModel/job_view_model.dart';
import 'package:jobapp/model/Categorymodel.dart';

import 'package:lottie/lottie.dart';

import 'package:provider/provider.dart';

import 'package:shared_preferences/shared_preferences.dart';

import '../../data/Response/status_response.dart';

import '../../res/Appcolor.dart';

import '../../res/Appfont.dart';

import '../../res/BaseUrl.dart';
import '../Jobs_details/job_detail.dart';

class CateBrowseScreen extends StatefulWidget {
  var isRecommend ;
  var lists;

  var isRemote;
  var isCategory;
  var jobtype;
  var cateitem;

  CateBrowseScreen ({Key? key,
    this.isRecommend, this.lists
    ,this.isRemote, this.isCategory, this.jobtype,this.cateitem}) : super(key: key);

  @override
  State<CateBrowseScreen> createState() => _CateBrowseScreenState();
}

class _CateBrowseScreenState extends State<CateBrowseScreen> {
  JobViewModel postjobviewmodel = JobViewModel();
  JobViewModel recommendjobviewmodel = JobViewModel();
  var token;
  var txtsearch = TextEditingController();
  var _scrollController = ScrollController();
  var        recommentlist = [];
  var Remote= false;
  var tokens;
  var page = 1;
  var maxpage ;
  var isloading;
  var _scrollControllers = ScrollController();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();


    print(widget.cateitem);
    print(widget.isCategory);
    Remote  = widget.isRemote ?? false;

    checkpreference(txtsearch.text); //TODO FETCH HERE







  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColor.primarycolor,
        elevation: 0,
        titleSpacing: 0,

        iconTheme: IconThemeData(
            color: AppColor.thirdcolor
        ),
        title:      Text('Browse',
          style: TextStyle(
            fontSize:AppFont.megafont+2,
            fontWeight: FontWeight.w600,
            color: AppColor.thirdcolor,

          ),
          maxLines: 6,
          overflow: TextOverflow.ellipsis,

        ),


      ),
      backgroundColor: AppColor.primarycolor,

      body: SafeArea(
        child:


        ChangeNotifierProvider<JobViewModel>(
          create: (context) =>  postjobviewmodel ,
          builder: (context, child) {
            return
            Consumer<JobViewModel>(

              builder: (context, jobs, child) {

                var status =postjobviewmodel.jobApiResponse.status;
                var length =postjobviewmodel.jobApiResponse.data?.data?.attributes?.length;

                print("job length search ${length}");
                var lengths =jobs.jobReresponse.data?.data?.attributes?.length;


                var joblist =postjobviewmodel.jobReresponse.data?.data?.attributes;


                print(recommentlist);
                print("Status api here  ");
                print(status);
                switch(status){
                  case Status.COMPLETED:
                    return Container(
                      height:double.maxFinite,
                      decoration: BoxDecoration(

                      ),



                      margin: EdgeInsets.symmetric(vertical: 0,horizontal: 15),
                      child:



                      Column(

                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,


                        children: [

                          SizedBox(height: 20,),



                          Expanded(child: NewWidget(length: length  ?? 0,

                            scrollController: _scrollController,


                            jobs: jobs,title : txtsearch.text,

                            category:widget.isCategory,item: widget.cateitem,
                          )),

                          // Expanded(child: CardDisplay(title: txtsearch.text,)),
                          SizedBox(height: 25,),
                          //display card list
                        ],
                      ),

                    );

                  case Status.LOADING:
                    return Center(
                      child: CircularProgressIndicator(
                        backgroundColor:Colors.indigo,
                        color: AppColor.primarycolor,



                      ),
                    );

                  // case Status.LOADING:
                  //   return length == null ?
                  //
                  //   Padding(
                  //     padding: const EdgeInsets.symmetric(vertical: 100),
                  //     child: Center(child: Column(
                  //       children: [
                  //         Lottie.network('https://assets9.lottiefiles.com/packages/lf20_eltfOSSxwb.json',
                  //
                  //
                  //             fit: BoxFit.cover,
                  //             width: 240,
                  //             height: 240
                  //
                  //         ),
                  //         Text('Sorry, we couldnt find any related result' ,style: TextStyle(
                  //             color: Colors.grey,
                  //             fontSize: AppFont.smfont+1,
                  //             fontWeight: FontWeight.w400
                  //
                  //         ),),
                  //       ],
                  //     ),),
                  //   ):
                  //   Center(
                  //     child: CircularProgressIndicator(
                  //       backgroundColor: AppColor.primarycolor,
                  //     ),
                  //   );

                  case Status.ERROR:
                    return Center(
                      child: Text('Error has been occur'),
                    );
                  default:
                    return Center(
                      child: Text('Error has been occur'),
                    );
                }

              },

            ) ;

          },

        ),
      ),
    );
  }


  void checkpreference(String? title)async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    var tokens= prefs.getString('tokens');
    print("The token in browse screen fetch ${tokens}");
    if(widget.isRecommend == true){
      await        recommendjobviewmodel.fetchall(tokens,title);
      return null;

    }
    await  postjobviewmodel.fetchall(tokens,title);
  }

}

class NewWidget extends StatefulWidget {
  var title;
  var isRemote;
  var type;
  var category;
  var item;
  NewWidget({
    super.key,
    required this.length,

    required ScrollController scrollController,
    required this.jobs,
    this.title,
    this.isRemote,
    this.type,
    this.category,
    this.item

  }) : _scrollController = scrollController;

  final int? length;
  var jobs;
  final ScrollController _scrollController;

  @override
  State<NewWidget> createState() => _NewWidgetState();
}

class _NewWidgetState extends State<NewWidget> {
  var isRemote;
  var _scrollControllers = ScrollController();
  var page = 1;
  List<Attributes> data = [];
  var maxpage;
  var searchtitle ;

  var isloading = false ;
  var txtsearch = TextEditingController();

  JobViewModel jobviewmodel = JobViewModel();

  @override

  void initState()  {

    // TODO: implement initState
    super.initState();
    isRemote = widget.isRemote ?? false;
    // print(widget.title ?? 'null title');
    // print("gay 2");
    print(widget.item);
    print(widget.category);
    searchtitle = widget.title;
    print(widget.category);


    fetchcategory(page  );
    _scrollControllers.addListener(getScrollBot);

    maxpage = jobviewmodel.CateResponse.data?.meta?.pagination?.pageSize;
    print(maxpage);
    // jobviewmodel.fet



  }

  Widget build(BuildContext context) {

    return ChangeNotifierProvider<JobViewModel>(

      create: (context) =>jobviewmodel ,

      builder: (context, child) {

        return Consumer<JobViewModel>(

          builder: (context, jobs, child) {
            var status = jobviewmodel.CateResponse.status;

            switch(status){


              case Status.COMPLETED:
                var lengths = jobs.CateResponse.data?.data?.attributes?.length;

                maxpage = jobs.CateResponse.data?.meta?.pagination?.pageSize;
                print(maxpage);

                print("Api lengths ${lengths}");

               List<Attributes> newlist =[];
                var dataset;
                if(txtsearch.text== ''){
                  if(lengths != null ){
                    data.addAll(jobviewmodel.CateResponse.data!.data!.attributes!);
                    newlist =data.toSet().toList();

                  }
                }

                else{
                  // List<CategoryModel> datacate = [];
                  newlist.clear();

                  dataset =  jobs.CateResponse.data?.data?.attributes;
                  // datacate.addAll(dataset);
                  newlist =       data.where((element) => element!.title!.toLowerCase().
                  startsWith(txtsearch.text.toLowerCase())).toList();
                }

                print(dataset);
                // for(int index =0 ;index<lengths!;index++ ){
                //
                //     data.
                // }

                print("data length : ${      newlist?.length}");
                // var lendata =      newlist?.length ?? 0;
                return


                  Column(
                    children: [

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            flex: 15,
                            child: TextField(
                              decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12),
                         borderSide: BorderSide(color: AppColor.primarycolor),

            ),
                     focusedBorder: OutlineInputBorder(

                  borderRadius: BorderRadius.circular(12),


            borderSide: BorderSide(color: AppColor.primarycolor)
            ),
                                fillColor: AppColor.thirdcolor,

                                filled: true,
                                enabled: true,
                                focusColor:Colors.white,


                                prefixIcon: Icon(Icons.search_sharp,color: AppColor.primarycolor,),
                                hintText: 'Search Jobs Title',



                              ),
                              controller: txtsearch,
                              onSubmitted:(value) async {
                                // joblist?.clear();

                                setState(() {

                                  // data.clear();
                                  // dataset.clear();
                                  // dataset?.length = 0;
                                  // lengths =0 ;
                                  // lendata = 0;
                                  newlist.clear();
                                  txtsearch.text = value;
                                  // checkpreference(txtsearch.text);
                                  print(txtsearch.text);


                                });


                              },

                              enabled: true ,



                            ),
                          ),
                          SizedBox(width: 10,),
                          Expanded(
                              flex: 3,
                              child: Container(
                                height: 60,
                                child: ElevatedButton(
                                  child: Icon(Icons.menu_open_outlined),
                                  onPressed: () {
                                    Navigator.push(context,MaterialPageRoute(builder: (context) {
                                      return FilterScreen();
                                    },));
                                  },
                                  style: ButtonStyle(
                                      backgroundColor: MaterialStatePropertyAll(Colors.indigo),
                                      shape: MaterialStatePropertyAll(RoundedRectangleBorder(

                                          borderRadius: BorderRadius.circular(14)
                                      ))
                                  ),
                                ),
                              )),

                        ],
                      ),

                      Expanded(
                        child:      newlist?.length ==0 ?
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 100),
                          child: Center(child: Column(
                            children: [
                              Lottie.network('https://assets5.lottiefiles.com/packages/lf20_zadfo6lc.json',


                                  fit: BoxFit.contain,
                                  width: 230,
                                  height: 230

                              ),
                              Text('Sorry, we couldn\'t find any result  ',style: TextStyle(
                                  color: AppColor.thirdcolor,
                                  fontSize: AppFont.smfont+1,
                                  fontWeight: FontWeight.w400

                              ),),
                            ],
                          ),),
                        )
                        //TODO LOADING HERE
                            :
                        RefreshIndicator(
                          onRefresh: ()async{
                            page =1 ;
                            // data.clear();
                            // dataset.clear();
                            // await jobviewmodel.fetchbyCategory(widget,1,widget.item );
                          },
                          child: ListView.builder(


                            // physics: NeverScrollableScrollPhysics(),


                            itemCount: newlist?.length,

                            // controller: _scrollControllers,


                            itemBuilder: (context, index) {
                              if(index ==newlist?.length){
                                return Center(
                                  child: CircularProgressIndicator(
                                    backgroundColor: AppColor.fourthcolor,

                                  ),
                                );
                              }
                              else{

                             var posts =     newlist![index];



                                // var post =  postjobviewmodel.jobApiResponse.data!.data![index].attributes;
                                // dynamic path =postjobviewmodel.jobApiResponse.data!.data!.attributes![index].user?.thumbnail?.responses?.path;
                                // print(postjobviewmodel.jobApiResponse.data!.data!.attributes![index].title);
                                print(posts);
                                return Container(
                                  margin: EdgeInsets.symmetric(vertical: 20),
                                  decoration: BoxDecoration(
                                    color: AppColor.thirdcolor,
                                    borderRadius: BorderRadius.circular(12)
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(vertical: 15),
                                    child:
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        ListTile(

                                          style: ListTileStyle.drawer,
                                          leading: CircleAvatar(
                                            backgroundImage:posts?.user?.thumbnail == null ?
                                            NetworkImage('https://i.pinimg.com/564x/48/6c/a0/486ca00640b169300b48e9ceacd8e401.jpg') :
                                            NetworkImage('${ApiApiUrl.localurl}/${posts?.user?.thumbnail?.attributes?.path}')
                                            ,
                                          ),
                                          title: Text('${posts?.title}'),
                                          subtitle: Text('Salary range \$ ${posts?.jobDescription?.minSalary} -'
                                              '\$ ${posts?.jobDescription?.maxSalary}'),
                                          trailing: IconButton(onPressed: ()=>Navigator.push(context,MaterialPageRoute(builder: (context) =>
                                              JobDetailSr(uid: posts?.sId) //TODO SCREEN JOB DETAIL
                                            ,)),
                                              icon: Icon(Icons.navigate_next_outlined)
                                          ),

                                          isThreeLine: true,

                                        ),

                                        Container(
                                          margin: EdgeInsets.symmetric(horizontal: 18),
                                          child: Row(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            children: [
                                              ElevatedButton(
                                                onPressed: ()=> null,

                                                child: Text('${posts?.jobDescription?.jobtype}',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: AppFont.smfont+1
                                                  ),
                                                ),
                                                style: ButtonStyle(
                                                    backgroundColor:MaterialStatePropertyAll(AppColor.primarycolor),
                                                    elevation: MaterialStatePropertyAll(0)
                                                ),


                                              ),
                                              SizedBox(width: 25,),
                                              ElevatedButton(
                                                onPressed: ()=>null,
                                                child: Text('${posts?.jobDescription?.experience}',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: AppFont.smfont+1
                                                  ),
                                                ),
                                                style: ButtonStyle(
                                                    backgroundColor:MaterialStatePropertyAll(AppColor.fourthcolor),
                                                    elevation: MaterialStatePropertyAll(0)
                                                ),


                                              ),
                                              SizedBox(width: 25,),
                                              ElevatedButton(
                                                onPressed: ()=>null,
                                                child: Text('${posts?.status == true ?  'Available' : 'Not Availble'}',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: AppFont.smfont+1
                                                  ),
                                                ),
                                                style: ButtonStyle(
                                                    backgroundColor:
                                                    posts?.status == true ?

                                                    MaterialStatePropertyAll(Colors.greenAccent) :
                                                    MaterialStatePropertyAll(Colors.grey),
                                                    elevation: MaterialStatePropertyAll(0)
                                                ),


                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                );
                              }



                            },
                          ),
                        ),
                      ),
                    ],
                  );


              case Status.LOADING:
                return Center(child: CircularProgressIndicator(),);

              default:
                return Center(child: CircularProgressIndicator(),);
            }



          },

        );
      },

    );

  }
  void checkpreference(String? title)async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    var tokens= prefs.getString('tokens');
    print("The token in browse screen fetch ${tokens}");
    await  jobviewmodel.fetchall(tokens,title);
  }
  void getScrollBot() async {

    if(_scrollControllers.position.pixels == _scrollControllers.position.maxScrollExtent){
      SharedPreferences prefs = await SharedPreferences.getInstance();
      var tokens = prefs.get('tokens');
      print("reach bottom scroll maxpage : ${maxpage  }");
      print(maxpage);
      if(page != maxpage){
        setState(() {

          isloading= true;
        });
        page +=1;
        await jobviewmodel.fetchbyCategory(tokens,page,widget.item );

        setState(() {
          isloading= false;
        });
      }
      // page = 1;
      // await jobviewmodel.fetchjoball('token', page);


      print(page);



    }

  }

  void fetchcategory(page)async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
   var tokens = prefs.get('tokens');

    print(tokens);
    await jobviewmodel.fetchbyCategory(tokens,page,widget.item );



  }
}


