 
import 'package:flutter/material.dart';

import 'package:jobapp/VIews/AuthScreen/Login.dart';
import 'package:jobapp/VIews/AuthScreen/register.dart';
import 'package:jobapp/VIews/Jobs_details/filterform.dart';
import 'package:jobapp/VIews/Widgets/Joblistv1.dart';
import 'package:jobapp/VIews/Widgets/carddisplay.dart';
import 'package:jobapp/VIews/Widgets/gridpanel.dart';
import 'package:jobapp/ViewModel/job_view_model.dart';
import 'package:jobapp/model/job_model.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../data/Response/status_response.dart';
import '../../res/Appcolor.dart';
import '../../res/Appfont.dart';
import '../Jobs_details/job_detail.dart';
import 'package:flutter/cupertino.dart';

import '../../ViewModel/job_view_model.dart';

class FilterScr extends StatefulWidget {
  var category;
  var type;
  var min;
  var max;
 FilterScr({Key? key, this.category,this.type,this.min,this.max}) : super(key: key);

  @override
  State<FilterScr> createState() => _FilterScrState();
}

class _FilterScrState extends State<FilterScr> {
  JobViewModel filterpostviewmodel = JobViewModel();
  var token;
  var txtsearch = TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(widget.category);
    print(widget.type);
    print(widget.max);
    filterpostviewmodel.Filterall(widget.category, widget.type, widget.min, widget.max);
    // checkpreference(widget.ca); //TODO FETCH HERE
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColor.primarycolor,
        elevation: 0,
        titleSpacing: 0,

        iconTheme: IconThemeData(
            color: AppColor.thirdcolor
        ),
        title:      Text('Browse',
          style: TextStyle(
            fontSize:AppFont.megafont+2,
            fontWeight: FontWeight.w500,
            color: AppColor.thirdcolor

          ),
          maxLines: 6,
          overflow: TextOverflow.ellipsis,

        ),


      ),
      backgroundColor: AppColor.primarycolor,

      body: SafeArea(
        child: ChangeNotifierProvider<JobViewModel>(
          create: (context) => filterpostviewmodel ,
          builder: (context, child) {
            return Consumer<JobViewModel>(

              builder: (context, jobs, child) {

                var status =filterpostviewmodel.FilterResponse.status;
                var length =jobs.FilterResponse.data?.data?.attributes?.length;
                print("Filter status ");
                print(status);
                switch(status){
                  case Status.COMPLETED:
                    return Container(
                      height:double.maxFinite,



                      margin: EdgeInsets.symmetric(vertical: 0,horizontal: 15),
                      child: Column(

                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,

                        children: [



                          SizedBox(height: 25,),
                          Text('${length == null ? '' : '${length} Available Jobs'} ',
                              style: TextStyle(
                                fontSize:AppFont.bigfont+1,
                                fontWeight: FontWeight.w400,
                                color: AppColor.thirdcolor,

                              )),
                          SizedBox(height: 25,),
                          Expanded(child:
                          length == null ?
                          Container(
                            margin: EdgeInsets.symmetric(vertical: 100),
                            child: Center(child: Column(
                              children: [
                                Lottie.network('https://assets5.lottiefiles.com/packages/lf20_zadfo6lc.json',


                                    fit: BoxFit.contain,
                                    width: 230,
                                    height: 230

                                ),
                                Text('Sorry, we couldn\'t find any result  ',style: TextStyle(
                                    color: AppColor.thirdcolor,
                                    fontSize: AppFont.smfont+1,
                                    fontWeight: FontWeight.w400

                                ),),
                              ],
                            ),),
                          ) :
                          ListView.builder(

                            // physics: NeverScrollableScrollPhysics(),


                            itemCount:length ?? 0,


                            itemBuilder: (context, index) {

                              var posts = jobs.FilterResponse.data?.data?.attributes?[index];
                              // var post =  postjobviewmodel.jobApiResponse.data!.data![index].attributes;
                              // dynamic path =postjobviewmodel.jobApiResponse.data!.data!.attributes![index].user?.thumbnail?.responses?.path;
                              // print(postjobviewmodel.jobApiResponse.data!.data!.attributes![index].title);
                              print(posts);
                              return Container(
                                margin: EdgeInsets.symmetric(vertical: 20),
                                decoration: BoxDecoration(
                                  color: AppColor.thirdcolor,
                                  borderRadius: BorderRadius.all(Radius.circular(14))
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 15),
                                  child:
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      ListTile(

                                        style: ListTileStyle.drawer,
                                        leading: CircleAvatar(
                                          backgroundImage:posts?.user?.thumbnail?.attributes?.path == null ?
                                          NetworkImage('https://i.pinimg.com/564x/48/6c/a0/486ca00640b169300b48e9ceacd8e401.jpg') :
                                          NetworkImage('http://10.0.2.2:3000/${posts?.user?.thumbnail?.attributes?.path}')
                                          ,
                                        ),
                                        title: Text('${posts?.title}'),
                                        subtitle: Text('Salary range \$ ${posts?.jobDescription?.minSalary} -'
                                            '\$ ${posts?.jobDescription?.maxSalary} / month'),
                                        trailing: IconButton(onPressed: ()=>Navigator.push(context,MaterialPageRoute(builder: (context) =>
                                            JobDetailSr(uid: posts?.sId) //TODO SCREEN JOB DETAIL
                                          ,)),
                                            icon: Icon(Icons.navigate_next_outlined)
                                        ),

                                        isThreeLine: true,

                                      ),

                                      Container(
                                        margin: EdgeInsets.symmetric(horizontal: 18),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            ElevatedButton(
                                              onPressed: ()=> null,

                                              child: Text('${posts?.jobDescription?.jobtype}',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: AppFont.smfont+1
                                                ),
                                              ),
                                              style: ButtonStyle(
                                                  backgroundColor:MaterialStatePropertyAll(AppColor.primarycolor),
                                                  elevation: MaterialStatePropertyAll(0)
                                              ),


                                            ),
                                            SizedBox(width: 25,),
                                            ElevatedButton(
                                              onPressed: ()=>null,
                                              child: Text('${posts?.jobDescription?.experience}',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: AppFont.smfont+1
                                                ),
                                              ),
                                              style: ButtonStyle(
                                                  backgroundColor:MaterialStatePropertyAll(AppColor.fourthcolor),
                                                  elevation: MaterialStatePropertyAll(0)
                                              ),


                                            ),
                                            SizedBox(width: 25,),
                                            ElevatedButton(
                                              onPressed: ()=>null,
                                              child: Text(''
                                                  '${
                                                  posts?.status == true ?  'Available' : 'Not Availble'}',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: AppFont.smfont+1
                                                ),
                                              ),
                                              style: ButtonStyle(
                                                  backgroundColor:
                                                  posts?.status == true ?

                                                  MaterialStatePropertyAll(Colors.greenAccent) :
                                                  MaterialStatePropertyAll(Colors.grey),
                                                  elevation: MaterialStatePropertyAll(0)
                                              ),


                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              );
                            },
                          )),

                          // Expanded(child: CardDisplay(title: txtsearch.text,)),
                          SizedBox(height: 25,),
                          //display card list
                        ],
                      ),

                    );

                  case Status.LOADING:
                    return Center(
                      child: CircularProgressIndicator(
                        backgroundColor: AppColor.primarycolor,
                      ),
                    );

                  case Status.ERROR:
                    return Center(
                      child: Text('Error has been occur'),
                    );
                  default:
                    return Center(
                      child: Text('Error has been occur'),
                    );
                }

              },

            );
          },

        ),
      ),
    );
  }
  void checkpreference(String? title)async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    var tokens= prefs.getString('tokens');
    print("The token in browse screen fetch ${tokens}");

  }

}
