import 'package:flutter/material.dart';

import 'package:jobapp/VIews/AuthScreen/Login.dart';
import 'package:jobapp/VIews/AuthScreen/register.dart';
import 'package:jobapp/VIews/Jobs_details/filterform.dart';
import 'package:jobapp/VIews/WelcomeScreen/screendiscover.dart';
import 'package:jobapp/VIews/WelcomeScreen/screenfindjob.dart';
import 'package:jobapp/VIews/Widgets/Joblistv1.dart';
import 'package:jobapp/ViewModel/job_view_model.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../data/Response/status_response.dart';
import '../../res/Appcolor.dart';
import '../../res/Appfont.dart';
import '../Jobs_details/pfscreen.dart';
import '../Jobs_details/Msg/messages.dart';
import '../Widgets/cardlistv1.dart';
import 'browsescreen.dart';

class ScreenJobSeeker extends StatefulWidget {


  @override
  State<ScreenJobSeeker> createState() => _ScreenJobSeekerState();
}

class _ScreenJobSeekerState extends State<ScreenJobSeeker> {
  var navlabel =['Search','Home','Discover','Profile'];
  var uid ;
  var appbarlabel =['Filter Jobs','Inbox','Discover','Profile'];
  var name ='';

  var isAuth;
  var ScreenTab = [
    FilterScreen(open: true ),

    ScreenFindJob(),

    DiscoverScreen(open: true),

    PfScreen(open: true)

  ];



  var iconlabel =<Widget> [
    Icon(Icons.search),

    Icon( Icons.home_filled),
    Icon(Icons.language_rounded),
    Icon(Icons.account_circle)
  ];
  var selectedindex = 1;
  JobViewModel postjobviewmodel = JobViewModel();
  JobViewModel userviewmodel = JobViewModel();
  @override

  void initState() {
    // TODO: implement initState
    checkauth();


  }
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   backgroundColor: AppColor.primarycolor,
      //   elevation: 0,
      //   iconTheme: IconThemeData(
      //       color: AppColor.primarycolor
      //   ),
      //
      // ),
      backgroundColor: Colors.indigo,

      body: ScreenTab.elementAt(selectedindex) ,

      bottomNavigationBar: ChangeNotifierProvider<JobViewModel>(
        create: (context) => userviewmodel,
        builder: (context, child) {
          return  Consumer<JobViewModel>(
            builder: (context, result, child) {
              var state = result.userApiResponse.status;
              var roles = result.userApiResponse.data?.data?.role;
              print("Default Screen");
              print(state);
              print(roles);
              if(state  == Status.COMPLETED){
                WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
                  if(roles =='Job Seeker'){


                  }


                });






              }
              else{
                print("True");



              }

              return  BottomNavigationBar(
                  currentIndex:selectedindex ,

                  elevation: 0,

                  onTap: (value) {


                    setState(() {
                      checkauth();

                      selectedindex = value;
                    });
                  },
                  backgroundColor: Colors.indigo,
                  type: BottomNavigationBarType.shifting,
                  mouseCursor: SystemMouseCursors.grab,
                  selectedFontSize: AppFont.smfont+1,
                  showSelectedLabels:true,


                  showUnselectedLabels: true,

                  selectedItemColor: AppColor.whitecolor,
                  unselectedItemColor: Colors.grey,
                  selectedIconTheme: IconThemeData(
                      size: 28
                  ),
                  // ),

                  selectedLabelStyle: TextStyle(
                      fontSize: AppFont.smfont-1,
                      height: 1.5
                  ),




                  items: List.generate(iconlabel.length,(index) {
                    print('length: ${iconlabel.length}');

                    return  BottomNavigationBarItem(
                        backgroundColor: Colors.indigo,

                        label: '${navlabel[index].toUpperCase()}',
                        icon: iconlabel[index]
                    );
                  }, )

              );
            },

          );
        },
      ),
    );

  }
  void checkauth () async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    isAuth= prefs.get('tokens') ?? false; //if there is no token false
    uid = prefs.getString('userid');
    var tokens= prefs.getString('tokens');
    print("The token in browse screen fetch ${tokens}");
    // postjobviewmodel.fetchall(tokens,null);
    userviewmodel.fetchUserbyId( uid );

    // print("USer info");
    //
    //   print(isAuth);
    //   print("USer info");
    //   print(  prefs.getString('userid'));

  }
}
