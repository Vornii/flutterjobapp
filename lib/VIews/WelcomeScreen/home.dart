import 'package:flutter/material.dart';

import 'package:jobapp/VIews/AuthScreen/Login.dart';
import 'package:jobapp/VIews/AuthScreen/register.dart';

import '../../res/Appcolor.dart';
import '../../res/Appfont.dart';

class MyHomePage extends StatelessWidget {
MyHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      backgroundColor: AppColor.bgcolor,
      body: Container(
        margin: EdgeInsets.symmetric(vertical: 0,horizontal: 0),

        child: SingleChildScrollView(


          child: Container(

            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 128,horizontal: 50),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.max,

                children: [
                    Text('Discover Your Dream Jobs ',

                    style: TextStyle(
                      fontSize: AppFont.megafont,
                      fontWeight: FontWeight.w500,
                      color: Colors.white,




                    ),
                      textAlign: TextAlign.center,
                      maxLines: 10,
                    ),
                  SizedBox(height: 20,),
                  Text('Over 1000+ new jobs offer for unemployees ',

                    style: TextStyle(
                      color: Colors.white.withOpacity(0.5),
                      fontSize: AppFont.medfont,
                      fontWeight: FontWeight.w400,



                    ),
                    maxLines: 10,
                  ),
                  SizedBox(height: 40,),
                  Image.asset('assets/images/129467-job-hunting 1.png',

                  //
                  width: 314,
                    height: 276,
                  ),
                  SizedBox(height: 40,),



                    Column(



                      children: [
                        Text('Are you new around here? let\'s get started ',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white.withOpacity(0.5),
                            fontSize: AppFont.medfont,
                            fontWeight: FontWeight.w400,
                          ),
                          maxLines: 10,
                        ),
                        SizedBox(height: 20,),
                        ElevatedButton( onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => Mylogin(),)),
                            style: ElevatedButton.styleFrom(
                              backgroundColor: AppColor.primarycolor,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(12) ,
                              )
                            ),

                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment:MainAxisAlignment.center,

                                mainAxisSize: MainAxisSize.max,
                                children: [

                                  Row(
                                    children: [
                                      Icon(Icons.login_rounded,color: Colors.white,),
                                      SizedBox(width: 10,),
                                      GestureDetector(

                                        child: Text('Sign In',
                                          textAlign: TextAlign.center,


                                          style: TextStyle(
                                            color: Colors.white,

                                            fontSize: AppFont.medfont,
                                          ),
                                        ),
                                      ),
                                    ],
                                  )

                                ],
                              ),
                            )

                        ),
                  SizedBox(height: 10,),
                        ElevatedButton(onPressed: ()=>Navigator.push(context, MaterialPageRoute(builder: (context) => Myregister(),)),
                            style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.white,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(12) ,
                                )
                            ),

                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                mainAxisAlignment:MainAxisAlignment.spaceEvenly,
                                children: [
                                  Row(
                                    children: [
                                      Icon(Icons.manage_accounts,

                                        color: Colors.black,
                                      ),
                                      SizedBox(width: 10,),
                                      Text('Sign Up',


                                        style: TextStyle(
                                          color: Colors.black,

                                          fontSize: AppFont.medfont,
                                        ),
                                      ),
                                    ],
                                  )

                                ],
                              ),
                            )

                        )
                      ],
                    )


                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
