import 'package:flutter/material.dart';

import 'package:jobapp/VIews/AuthScreen/Login.dart';
import 'package:jobapp/VIews/AuthScreen/register.dart';
import 'package:jobapp/VIews/Jobs_details/filterform.dart';
import 'package:jobapp/VIews/Jobs_details/postjob_screen.dart';
import 'package:jobapp/VIews/WelcomeScreen/screendiscover.dart';
import 'package:jobapp/VIews/Widgets/Joblistv1.dart';
import 'package:jobapp/data/Response/status_response.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../ViewModel/job_view_model.dart';
import '../../res/Appcolor.dart';
import '../../res/Appfont.dart';
import '../../res/BaseUrl.dart';
import '../Jobs_details/pfscreen.dart';
import '../Jobs_details/Msg/messages.dart';
import '../Widgets/cardlistv1.dart';
import 'browsescreen.dart';

class ScreenFindJob extends StatefulWidget {
  ScreenFindJob ({Key? key}) : super(key: key);

  @override
  State<ScreenFindJob> createState() => _ScreenFindJobState();
}

class _ScreenFindJobState extends State<ScreenFindJob> {
  var userid;
  var token;

  var selectedindex = 2;
  JobViewModel userviewmodel = JobViewModel();
  @override
  void initState() {
    // TODO: implement initState
    checkauth();


  }
  Widget build(BuildContext context) {
    return Scaffold(
   
      backgroundColor: AppColor.primarycolor,

      body: SafeArea(
        child: ChangeNotifierProvider< JobViewModel >(
          create: (context) => userviewmodel,
          builder: (context, child) {
              return Container(

                margin: EdgeInsets.symmetric(vertical: 20,horizontal: 10),

                child: Consumer<JobViewModel>(

                  builder: (context, user, child) {
                    var status = user.userApiResponse.status;
                    var profile = user.userApiResponse.data?.data?.thumbnail?.attributes?.path;
                    var interest = user.userApiResponse.data?.data?.interest;
                    var roleuser = user.userApiResponse.data?.data?.role;
                    print(interest);
                    if(roleuser =='Job Seeker'){
                      WidgetsBinding.instance.addPostFrameCallback((timeStamp) {

                        // iconlabel.removeAt(1);
                        // ScreenTab.removeAt(1);
                        // navlabel.removeAt(1);
                        // selectedindex= selectedindex-1;

                      });
                    }
                    switch(status){
                      case Status.COMPLETED:
                        return RefreshIndicator(
                          onRefresh: () async {
                            print("refreshing");
                            setState(() {
                              checkauth();
                            });
                          },
                          backgroundColor: AppColor.primarycolor,
                          child: SingleChildScrollView(

                            child: Column(

                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [

                                Row(
                                  //header row
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [

                                    Container(
                                      width: 250,
                                      child: Text('Find Your Creative Works',
                                        style: TextStyle(
                                          fontSize:AppFont.megafont+5,
                                          fontWeight: FontWeight.w600,
                                          color: AppColor.whitecolor,

                                        ),
                                        maxLines: 6,
                                        overflow: TextOverflow.ellipsis,

                                      ),
                                    ),
                                    GestureDetector(
                                      onTap: () => Navigator.push(context,MaterialPageRoute(builder: (context) => PfScreen(),)),
                                      child: CircleAvatar(
                                        maxRadius: 30,
                                        backgroundColor: Colors.white54,
                                        backgroundImage:

                                        profile == null ?
                                        NetworkImage('https://i.pinimg.com/564x/48/6c/a0/486ca00640b169300b48e9ceacd8e401.jpg')
                                            : NetworkImage('${ApiApiUrl.localurl}/${profile}')

                                      ),
                                    )
                                  ],
                                ),
                                SizedBox(height: 30,),

                                GestureDetector(


                                  child:roleuser =='Hiring Person' || roleuser =='Admin'?
                                  TextField(
                                    onTap: () =>Navigator.push(context,MaterialPageRoute(builder:

                                        (context) =>

                                        CreateScreen(id:userid,token: token,isUpdate: false,),)),
                                    readOnly: true,


                                    decoration: InputDecoration(
                                      fillColor:AppColor.thirdcolor,
                                      focusedBorder:OutlineInputBorder(

                                          borderSide: BorderSide(
                                              color: AppColor.primarycolor
                                          ),
                                          borderRadius: BorderRadius.circular(12)
                                      ),
                                      enabledBorder: OutlineInputBorder(

                                          borderSide: BorderSide(
                                              color: AppColor.primarycolor
                                          ),
                                        borderRadius: BorderRadius.circular(12)
                                      ),



                                      filled: true,
                                      enabled: true,

                                      focusColor:Colors.white ,

                                      prefixIcon: IconButton(
                                        onPressed: ()=>Navigator.push(context,
                                            MaterialPageRoute(builder: (context) => FilterScreen(),)),

                                        icon: Icon(Icons.search_sharp,color: AppColor.primarycolor),

                                      ),
                                      hintText: 'Create Your Announcement',

                                    ),

                                    enabled: true ,





                                  ) :
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,

                                    children: [
                                      Expanded(
                                        flex: 15,
                                        child: TextField(
                                          onTap: () => Navigator.push(context,MaterialPageRoute(builder: (context) =>
                                              BrowseScreen(),)),
                                          decoration: InputDecoration(
                                            fillColor: AppColor.thirdcolor,
                                            focusedBorder:OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(13),
                                                borderSide: BorderSide(
                                                    color: AppColor.primarycolor
                                                )
                                            ),
                                            enabledBorder: OutlineInputBorder(
                                              borderRadius: BorderRadius.circular(13),

                                                borderSide: BorderSide(
                                                    color: AppColor.primarycolor,

                                                )
                                            ),

                                            filled: true,
                                            enabled: true,
                                            focusColor:Colors.white ,


                                            prefixIcon: Icon(Icons.search_sharp,color: AppColor.primarycolor,),
                                            hintText: 'Search for job title',



                                          ),
                                          readOnly: true,

                                          onSubmitted:(value) {

                                            setState(() {


                                            });

                                          },

                                          enabled: true ,




                                        ),
                                      ),
                                      SizedBox(width: 10,),
                                      Expanded(
                                          flex: 3,
                                          child: Container(
                                            height: 60,
                                            child: ElevatedButton(
                                              child: Icon(Icons.menu_open_outlined),
                                              onPressed: () {
                                                Navigator.push(context,MaterialPageRoute(builder: (context) {
                                                  return FilterScreen();
                                                },));
                                              },
                                              style: ButtonStyle(
                                                  backgroundColor: MaterialStatePropertyAll(Colors.indigo),
                                                  shape: MaterialStatePropertyAll(RoundedRectangleBorder(

                                                      borderRadius: BorderRadius.circular(14)
                                                  ))
                                              ),
                                            ),
                                          )),
                                    ],
                                  ),
                                ),
                                SizedBox(height: 20,),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text('Recommended For You',
                                        style: TextStyle(
                                          fontSize:AppFont.bigfont +1,
                                          fontWeight: FontWeight.w500,
                                          color: AppColor.whitecolor,

                                        )),
                                    TextButton(

                                      onPressed: ()=>Navigator.push(context, MaterialPageRoute(builder: (context) =>
                                 BrowseScreen(
                                   isRecommend: true  ,
                                     lists:interest?.map((e) {
                                       return '"${e}"';
                                     } ).toList()
                                 ),


                                      )),
                                      child: Text('View all',
                                          style: TextStyle(
                                            fontSize:AppFont.smfont+1,
                                            fontWeight: FontWeight.w400,
                                            color: AppColor.whitecolor,

                                          )
                                      ),

                                    ),
                                  ],

                                ),
                                SizedBox(height: 15,),

                                CardListRe(click: true,lists:interest?.map((e) {
                                    return '"${e}"';
                                } ).toList()), //TODO RECOMMENDs

                                SizedBox(height: 25,),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text('Recent Jobs',
                                        style: TextStyle(
                                          fontSize:AppFont.bigfont +1,
                                          fontWeight: FontWeight.w500,
                                          color: AppColor.whitecolor,

                                        )),
                                    TextButton(

                                      onPressed: ()=>Navigator.push(context, MaterialPageRoute(builder: (context) =>
                                          BrowseScreen(),)),
                                      child: Text('View all',
                                          style: TextStyle(
                                            fontSize:AppFont.smfont+1,
                                            fontWeight: FontWeight.w400,
                                            color: AppColor.whitecolor,

                                          )
                                      ),

                                    ),
                                  ],
                                ),
                                SizedBox(height: 0),


                                Container(
                                  height: 580,

                                    //TODO JOB LIST HERE
                                    child: JoblistCard()) //TODO NEW HIRING

                              ],
                            ),
                          ),
                        );

                      case Status.LOADING:
                        return Center(
                          child:CircularProgressIndicator(
                            backgroundColor:Colors.indigo,
                            color: AppColor.primarycolor,



                          ),
                        );
                      case Status.ERROR:
                        return Center(
                          child: Text('Server is broke down, please restart your app'),
                        );
                      default:
                        return Center(
                          child: CircularProgressIndicator(
                            backgroundColor: AppColor.primarycolor,
                          ),
                        );
                    }

                  },

                ),

              );
          },

        ),
      ),

    );

  }

  void checkauth() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? id = prefs.getString('userid');
    var tokenss = prefs.get('tokens');

    setState(() {
      userid = id;
      token = tokenss;
    });
    userid = id;
    print("new user id");
    print(userid);
    userviewmodel.fetchUserbyId(userid);
    //   print( prefs.getString('userid'));
    // print(  prefs.getString('userid'));
    print(prefs.getString('tokens')); //TODO FETCH USER PROFILE
  }

}
