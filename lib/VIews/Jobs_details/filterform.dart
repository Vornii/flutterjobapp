import 'package:bulleted_list/bulleted_list.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:jobapp/VIews/WelcomeScreen/browsescreen.dart';


import '../../res/Appcolor.dart';
import '../../res/Appfont.dart';
import '../WelcomeScreen/Filterscreen.dart';

class FilterScreen extends StatefulWidget {
  var open ;
   FilterScreen({Key? key,this.open }) : super(key: key);

  @override
  State<FilterScreen> createState() => _FilterScreenState();
}

class _FilterScreenState extends State<FilterScreen> {
  var txtmin = TextEditingController();
  var txtmax = TextEditingController();
  var isSelected = false;
  var typejob;
  var iswrong = false;
  var listtype =['Part Time', 'Full Time', 'Remote'];
  int _selectedIndex = 0;
  var iscategoryselect = [false,false,false,false,false
    ,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false
    ,false,false,false,false,false,false,false,false];
  var isSelectedPt = false;
  var isSelectedFt = false;
  var isSelectedOt = false;
  var isSalary =true;
  var cateitem =[
    "Software developer",
    "Data scientist",
    "Cyber security engineer",
    "Cloud developer",
    "DevOps engineer",
    "Digital marketing specialist",
    "Project manager",
    "Product manager",
    "UX designer",
    "UI designer",
    "Content writer",
    "SEO specialist",
    "Social media manager",
    "Customer service representative",
    "Sales representative",
    "Accountant",
    "Networing"
        "Financial analyst",
    "Human resources manager",
    "Operations manager",
    "Logistics manager",
    "Supply chain manager",
    "Healthcare professional",
    "Teacher",
    "Engineer",
    "Construction worker",
    "Truck driver",
    "Retail salesperson",
  ];
  var jobreq =[1];
  Set<String> selectcategory = {};
  var allitem =[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
  var selecteditem;

  @override
  void initState() {
    // TODO: implement initState
      super.initState();
      typejob = listtype[0];
      selecteditem = cateitem[0];
  }
  Widget build(BuildContext context) {
    var open = widget.open;
    return Scaffold(
      appBar:AppBar(
        backgroundColor: Colors.indigo,
        elevation: 0,
        automaticallyImplyLeading:open == null ? true : false , //set to false = no go back bar
        iconTheme: IconThemeData(
            color: open == null ? AppColor.whitecolor:AppColor.primarycolor
        ),
        title: Text('Filter Jobs',style: TextStyle(
            color: Colors.white
        ),),
        centerTitle: true,


      ),
      backgroundColor: AppColor.thirdcolor,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.symmetric(vertical: 20),
            padding: EdgeInsets.symmetric(horizontal: 23,),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [


                Text('Category',
                  style: TextStyle(
                      fontSize: AppFont.bigfont,
                      fontWeight: FontWeight.bold,
                      color: AppColor.primarycolor

                  ),),
                SizedBox(height: 30,),
                Row(
                  children: [
                    Expanded(
                      child: Container(
                        height: 45,
                        padding: EdgeInsets.all(5),

                        decoration: BoxDecoration(
                            color: AppColor.whitecolor,
                            borderRadius: BorderRadius.circular(10)
                        ),
                        child: DropdownButton(
                            borderRadius: BorderRadius.circular(12),
                          underline: SizedBox(),
                          menuMaxHeight: 400,
                          alignment: Alignment.center,
                          iconEnabledColor: AppColor.primarycolor, //TODO ARROW ICON
                          iconDisabledColor: AppColor.bgcolor,

                          autofocus: true,
                          isExpanded: true,
                          value: selecteditem==null ? cateitem.elementAt(0) : selecteditem,
                          dropdownColor: AppColor.whitecolor,
                          focusColor: AppColor.whitecolor,



                          items:List.generate( cateitem.length , (index){
                            return DropdownMenuItem(


                              value: cateitem[index],

                              alignment: Alignment.center,

                              enabled: true,
                              child: Text('${cateitem[index]}',
                                style: TextStyle(
                                    color: AppColor.primarycolor,
                                  fontSize: AppFont.smfont+3
                                ),
                              ),
                              onTap: () {

                              },
                            );
                          }),
                          onChanged:(value) {

                            setState(() {
                              selecteditem = value;
                              print(selecteditem);
                            });
                          },
                          elevation: 0,

                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 30,),






                Text('Job Type',
                  style: TextStyle(
                      fontSize: AppFont.bigfont,
                      fontWeight: FontWeight.bold,
                      color: AppColor.primarycolor

                  ),),
                SizedBox(height: 30,),

                Container(
                  width: double.maxFinite,
                  child: Wrap(

                    alignment: WrapAlignment.spaceBetween,
                    runSpacing: 7,
                    direction: Axis.horizontal,
                    children: List.generate(listtype.length , (index) => ChoiceChip(
                      label: Text('${listtype[index]}'),
                      backgroundColor: AppColor.whitecolor,
                      selectedColor: Colors.indigo,

                      labelStyle: TextStyle(
                          color: _selectedIndex == index? Colors.white : Colors.indigo
                      ),
                      onSelected: (selected) {
                        if (selected) {
                          setState(() {
                            _selectedIndex = index;
                            typejob = listtype[index];
                            print(typejob);

                          });
                        }
                      }, selected: _selectedIndex == index,
                    )

                    ),

                  ),
                ),
                SizedBox(height: 30,),
                Row(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text('Filter Salary',
                      style: TextStyle(
                          fontSize: AppFont.bigfont,
                          fontWeight: FontWeight.bold,
                          color: AppColor.primarycolor

                      ),),
                    Switch(value: isSalary,
                        onChanged:(value) {
                          setState(() {
                            isSalary = value;
                            print(isSalary);
                            iswrong = false;
                            txtmin.clear();
                            txtmax.clear();
                          });
                        },
                      activeColor: AppColor.primarycolor,



                    )
                  ],
                ),
                SizedBox(height: 10,),

                Container(
                  child:
                      isSalary == true ?

                  Column(children: [
                    Container(
                      child:         TextField(
                        style: TextStyle(
                          fontSize: AppFont.smfont+1,
                          fontWeight: FontWeight.bold,
                          color: AppColor.primarycolor,
                          height: 2,

                        ),
                        controller: txtmin,
                        onSubmitted: (value) {
                          setState(() {
                            txtmin.text = value;
                          });
                        },
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          hintText: '100',
                          labelText: 'Mininum Salary',

                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(
                                  color: AppColor.primarycolor,
                                  width: 2
                              )
                          ),
                          helperText: iswrong == true ?

                          'Minimum salary must be smaller then Maximum' : null,
                          helperStyle: TextStyle(
                              color: AppColor.fourthcolor,
                              fontWeight: FontWeight.w500
                          ),

                          prefix: Text('\$',style: TextStyle(
                              color: AppColor.primarycolor
                          ),),


                          // helperText:'965689896' ,
                          labelStyle: TextStyle(
                              color: AppColor.primarycolor,
                              fontSize: AppFont.bigfont,
                              fontWeight: FontWeight.w400
                          ),

                          // suffix: Icon(
                          //   Icons.delete_forever,
                          //   size: 22,
                          //   color: AppColor.primarycolor,
                          // )



                        ),


                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,


                      children: [




                        Expanded(flex: 2,
                          child: Container(
                            child:         TextField(
                              style: TextStyle(
                                fontSize: AppFont.smfont+1,
                                fontWeight: FontWeight.bold,
                                color: AppColor.primarycolor,
                                height: 2,

                              ),

                              keyboardType: TextInputType.number,
                              controller: txtmax,
                              onSubmitted: (value) {
                                setState(() {
                                  txtmax.text = value;
                                  if(double.parse(txtmax.text)< double.parse(txtmin.text)){

                                  }
                                });
                              },
                              onChanged: (value) {
                                setState(() {
                                  txtmax.text = value;
                                  txtmax.selection = TextSelection.fromPosition(TextPosition(offset:      txtmax.text.length));
                                  if(double.parse(txtmax.text)< double.parse(txtmin.text)){
                                    iswrong= true;

                                  }
                                  else{
                                    iswrong = false;
                                  }

                                });
                              },
                              decoration: InputDecoration(
                                hintText:
                                '1200',

                                labelText: 'Maximum Salary',
                                focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color: AppColor.primarycolor,
                                        width: 2
                                    )
                                ),
                                helperStyle: TextStyle(
                                    color: AppColor.fourthcolor,
                                    fontWeight: FontWeight.w500
                                ),
                                prefix: Text('\$',style: TextStyle(
                                    color: AppColor.primarycolor
                                ),),

                                helperText:
                                iswrong == true ?
                                'Maximum salary must be larger then Mininum' : null,


                                // helperText:'965689896' ,
                                labelStyle: TextStyle(
                                    color: AppColor.primarycolor,
                                    fontSize: AppFont.bigfont,
                                    fontWeight: FontWeight.w400
                                ),
                                // suffix: Icon(
                                //   Icons.delete_forever,
                                //   size: 22,
                                //   color: AppColor.primarycolor,
                                // )



                              ),


                            ),
                          ),
                        ),


                      ],
                    ), //TODO Job Minuinum and maxinum

                    SizedBox(height: 30,),



                  ]) : null,

                ),
                Container(
                  child:
                  iswrong == false?

                  Row(
                    //TODO ADD AND REMOVE REQUIREMENT SECTION
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        flex: 1,
                        child: Container(
                          child:  ElevatedButton(onPressed:() {
                            if(txtmax.text == '' || txtmin.text ==''){


                            }
                            Navigator.push(context, MaterialPageRoute(builder: (context) =>
                                FilterScr(category: selecteditem,type: typejob,min: txtmin.text ,max: txtmax.text,),));
                            setState(() {

                            });
                          },

                              style: ButtonStyle(
                                  backgroundColor: MaterialStatePropertyAll(Colors.indigo),
                                  shape: MaterialStatePropertyAll(RoundedRectangleBorder(        borderRadius: BorderRadius.circular(12)))



                              ),child: Text('Apply')
                          ),
                        ),
                      ),      //TODO ADD REQUIREMENT
                      SizedBox(width: 15,),
                      //TODO REMOVE REQUIREMENT


                    ],
                  ) : null,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
