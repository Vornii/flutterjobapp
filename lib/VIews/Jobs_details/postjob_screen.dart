
import 'package:bulleted_list/bulleted_list.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/geolocation.dart';
import 'package:jobapp/VIews/WelcomeScreen/defaultscreen.dart';
import 'package:jobapp/ViewModel/job_view_model.dart';
import 'package:jobapp/model/POST/job_request_model.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:geolocator/geolocator.dart';
import '../../ViewModel/post_view_model.dart';
import '../../data/Response/status_response.dart';
import '../../res/Appcolor.dart';
import '../../res/Appfont.dart';
import '../../res/Validator/loginvalidate.dart';
import '../Admin/adminpostview.dart';
import '../CRUD/LongPosts.dart';
import '../CRUD/editpostview.dart';
import '../Googlemap/searchmark.dart';
import '../Googlemap/mappicker.dart';
class CreateScreen extends StatefulWidget {
  var id;
  var token;
  var isUpdate;

 var jobspost;
 var islong;
 var admin;
 CreateScreen({Key? key , this.id , this.token, this.isUpdate , this.jobspost, slong,
   this.admin

 }) : super(key: key);

  @override
  State<CreateScreen> createState() => _CreateScreenState();
}

class _CreateScreenState extends State<CreateScreen> with  CheckValidateLogin {
  PostViewModel uploadjobviewmodel= PostViewModel();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _telegram = GlobalKey<FormState>();
  final TextEditingController _typeAheadController = TextEditingController();
  var isSelected = false;
  var isloading = false;


  var typejob;
  var tokenss;
  var locationid;
  var    submitjob;
  var  positionlat = LatLng(0, 0);

  var updatewidget = false;
  var isClickLocation = false;

  var jobsitem;
  var lat;
  var long;

  List<String> companies  = ["Phnom Penh","Banteay Meanchey", "Battambang", "Kampong Cham", "Kampong Chhnang", "Kampong Speu", "Kampong Thom", "Kampot", "Kandal", "Kep", "Koh Kong", "Kratie", "Mondulkiri", "Oddar Meanchey", "Pailin", "Phnom Penh", "Preah Vihear", "Prey Veng", "Pursat", "Ratanakiri", "Siem Reap", "Sihanoukville", "Stung Treng", "Svay Rieng", "Takeo", "Tbong Khmum"];
  var ListOfDistrict = ['Boeung Keng Kang', 'Chamkarmon', 'Daun Penh', 'Prampir Makara', 'Toul Kork', 'Tuol Tom Poung', 'Chroy Changvar', 'Mean Chey', 'Chbar Ampov', 'Por Senchey', 'Dangkao', 'Sen Sok', 'Russei Keo'];
  // var companyLists = ["ACLEDA Bank Plc",
  //   "Cambodia Public Bank Plc",
  //   "Sathapana Bank Plc.",
  //   "Phnom Penh Commercial Bank Plc",
  //   "Canadia Bank Plc",
  //   "Cambodian Public Bank Plc",
  //   "Khmer Capital Microfinance Institution Plc",
  //   "Smart Axiata Co., Ltd",
  //   "Cambodia Brewery Limited",
  //   "Cambodia Airports",
  //   "Sabay Digital Corporation",
  //   "Koompi Computer",
  //   "Pathmazing Co., Ltd",
  //   "Cambodian Mobile Payment Solution (Cambopay)",
  //   "EZECOM",
  //   "Cambodia Post Insurance PLC",
  //   "Cambodian Health Professionals Association (CHPA)",
  //   "Cambodia Yellow Pages",
  //   "Khmerload News",
  //   "Cambodian IT Association (CITA)"
  // ];
  // List<String> companies = [
  //   'Aeon Mall Cambodia',
  //   'AirAsia Cambodia',
  //   'ANZ Royal Bank',
  //   'Beeline Cambodia',
  //   'Bitkub Capital',
  //   'Cambrew',
  //   'Canadia Bank',
  //   'Cambodia Airways',
  //   'Cambodia Deposit Insurance Corporation',
  //   'Cambodia General Insurance',
  //   'Cambodia International Bank',
  //   'Cambodia Mekong Bank',
  //   'Cambodia Post',
  //   'Cambodia Securities Exchange',
  //   'Cambodia Telecom',
  //   'Cambodia Water Supply Authority',
  //   'Chaney International',
  //   'Chip Mong Group',
  //   'Credit Suisse (Cambodia) Ltd',
  //   'Dtac Cambodia',
  //   'Fubon Bank (Cambodia) Ltd',
  //   'GMAC Cambodia',
  //   'Harvest Development',
  //   'Hun Sen Foundation',
  //   'ING (Cambodia) Ltd',
  //   'Khmer Times',
  //   'Khmer Tire Corporation',
  //   'Krung Thai Bank (Cambodia) Ltd',
  //   'LOréal Cambodia',
  //   'Mahosot Hospital',
  //   'Mekong Capital',
  //   'Mitsubishi Corporation (Cambodia) Ltd',
  //   'NagaCorp',
  //   'Num Banh Chok',
  //   'Phnom Penh Water Supply Authority',
  //   'Rising Sun Beer',
  //   'Seagate Technology (Cambodia) Ltd',
  //   'Sokha Hotels and Resorts',
  //   'Standard Chartered Bank (Cambodia) Ltd',
  //   'Sushi Tei',
  //   'Technogym Cambodia',
  //   'Union Development Group',
  //   'Wing (Cambodia) Ltd',
  //   'WorldBridge International',
  // ];

  String? _selectedCity;
  var cateitem =[
    "Software developer",
    "Data scientist",
    "Cyber security engineer",
    "Cloud developer",
    "DevOps engineer",
    "Digital marketing specialist",
    "Project manager",
    "Product manager",
    "UX designer",
    "UI designer",
    "Content writer",
    "SEO specialist",
    "Social media manager",
    "Customer service representative",
    "Sales representative",
    "Accountant",
    "Networing"
    "Financial analyst",
    "Human resources manager",
    "Operations manager",
    "Logistics manager",
    "Supply chain manager",
    "Healthcare professional",
    "Teacher",
    "Engineer",
    "Construction worker",
    "Truck driver",
    "Retail salesperson",
  ];
  var listtype =['Part Time', 'Full Time', 'Remote'];
  var listExperience = ['Senior','Junior','Intern'];
  var JobExp;
  int _selectedIndex = 0;
  int _selectedIndexExp = 0;
  var iscategoryselect = [false,false,false,false,false
  ,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false
  ,false,false,false,false,false,false,false,false

  ];
  var isSelectedPt = false;
  var isSelectedFt = false;
  var isSelectedOt = false;
  List<String> requirement = [];
  var keyposition = GlobalKey<FormState>();
 Set<String> selectcategory = {};
 var txttitle = TextEditingController();
 var jobdesc = TextEditingController();
 var txtmin = TextEditingController();
 var txtmax = TextEditingController();
 var txtcomname = TextEditingController();
 var txtmapurl  = TextEditingController();
 var txttel = TextEditingController();
 var txtemail = TextEditingController();
 var txttelegram= TextEditingController();
  var txtstreet = TextEditingController();
  var txtprovince= TextEditingController();

  List<TextEditingController> controller = [];//TODO DYNAMIC CONTROLLER TEXT


  dynamic allitem =[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
  var selecteditem;
  var postid;
var uid;

  var jobreq =[1];
  var isPick =false;
  String? _currentAddress;
  Position? _currentPosition;
  Future<bool> _handleLocationPermission() async {
    bool serviceEnabled;
    LocationPermission? permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text('Location services are disabled. Please enable the services')));
      return false;
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Location permissions are denied')));
        return false;
      }
    }
    if (permission == LocationPermission.deniedForever) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text('Location permissions are permanently denied, we cannot request permissions.')));
      return false;
    }
    return true;
  }
  Future<void> _getCurrentPosition() async {
    final hasPermission = await _handleLocationPermission();
    if (!hasPermission) return;
    await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high)
        .then((Position position) {
      setState(() => _currentPosition = position);
    }).catchError((e) {
      debugPrint(e);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    txtmapurl.text ='www.google.com';

    print(widget.id);

    updatewidget = widget.isUpdate;
    jobsitem = widget.jobspost;


    for (int i = 0; i < jobreq.length; i++) {
      controller.add(TextEditingController());

    }


    if(    updatewidget == true){
      txttitle.text = jobsitem.title ;
      jobdesc.text = jobsitem.jobDescription?.detail;
      selecteditem = jobsitem?.jobDescription?.totalPosition;
      txtmax.text = jobsitem!.jobDescription!.maxSalary.toString();
      txtmin.text = jobsitem!.jobDescription!.minSalary.toString();
      txtcomname.text = jobsitem?.jobDescription?.companyName;
      typejob = jobsitem?.jobDescription?.jobtype;
      positionlat = LatLng(jobsitem?.address?.latitude, jobsitem?.address?.longitude);

    postid = jobsitem?.sId;
      if(typejob == 'Part Time'){
        _selectedIndex = 0;
        isSelectedPt =true;
      }
      else if(typejob == 'Full Time'){
        _selectedIndex = 1;
        isSelectedFt =true;
      }
      else{
        _selectedIndex = 2;
        isSelectedOt = true;
      }


      JobExp= jobsitem?.jobDescription?.experience;
      if(JobExp == "Senior"){
        _selectedIndexExp = 0;

      }
      else if(JobExp == "Junior"){
        _selectedIndexExp = 1;

      }
      else{
        _selectedIndexExp = 2;
      }
      print(jobsitem?.requirement);
      txttelegram.text = jobsitem?.contact?.telegram;
      txtemail.text = jobsitem?.contact?.email;
      txttel.text = jobsitem?.contact?.telephone;
      txtprovince.text = jobsitem?.address?.province;
      txtstreet.text  = jobsitem?.address?.street;
      txtmapurl.text = jobsitem?.address?.urilocation;
      var length =jobsitem?.requirement?.length;
      jobreq.removeLast();
      for (int index = 0; index <length ; index++) {

        jobreq.add(index);


      }
      for (int i = 0; i < jobreq.length; i++) {
        controller.add(TextEditingController());
      }

      for (int i = 0; i <   jobreq.length; i++) {
        controller[i].text = jobsitem?.requirement?[i];
        requirement.add(controller[i].text);

      }
      print(requirement);

    var cateitems = jobsitem?.category;
      selectcategory = cateitems.toSet();
      print(cateitems);
      for(int index =0;index<cateitem.length;index++){
        if(cateitems.contains(cateitem[index])){
          iscategoryselect[index] = true;

        }

      }


    }
    else {
      print('Controller length ${controller.length}')  ;
      controller[0].text = "";
      requirement.insert(0,     controller[0].text);
      // txttitle.text = 'Job Title';
      // jobdesc.text ='lorem';
      // //
      // // txtmax.text = '10';
      // // txtmin.text ='299';
      // txtcomname.text = "Proset Solution";
      //
      // txttelegram.text = "https://tele.me.com";
      // txtemail.text = 'Panha@gmail.com';
      // txttel.text = '865689895 ';
      txtprovince.text = '';
      txtstreet.text  ='';
      // txtmapurl.text = 'wwww.google.com';
      selecteditem = 1;
      // txtmin.text = 1200.toString();
      // txtmax.text = 1400.toString();

    }
    if(widget.admin == true){
      if(updatewidget ==false) {
        print('Controller length ${controller.length}')  ;
        // controller[0].text = "";
        // requirement.insert(0,     controller[0].text);
        txttitle.text = 'Job Title';
        jobdesc.text ='lorem';
        //
        txtmax.text = 1250.toString();
        txtmin.text =2500.toString();
        txtcomname.text = "Proset Solution";

        txttelegram.text = "https://tele.me.com";
        txtemail.text = 'Panha@gmail.com';
        txttel.text = '865689895 ';
        txtprovince.text = 'Phnom Penh';
        txtstreet.text  ='468 ToulTompong 2 khan comkamun';
        txtmapurl.text = 'wwww.google.com';
        selecteditem = 1;
        txtmin.text = 1200.toString();
        txtmax.text = 1400.toString();
      }

    }

   typejob = listtype[0];
JobExp   = listExperience[0];

    gettoken();
    updatewidget = widget.isUpdate;
    print(updatewidget);



    _getCurrentPosition();
    print("Back to ${_currentPosition}");
  }

  Widget build(BuildContext context) {

    print("Jobs Requirement ${jobreq.length}");
    print("Back to ${positionlat}");
    for (int i = 0; i < jobreq.length; i++) {
      controller.add(TextEditingController());

      //
      // var removeindex = i + 1;
      // requirement.removeAt(i);
    }
    return Scaffold(
      appBar:AppBar(
        backgroundColor: Colors.indigo,
        elevation: 0,
        iconTheme: IconThemeData(
            color:Colors.white
        ),
        title:
        widget.isUpdate ?
        Text('Update Announcement',style: TextStyle(
                color:Colors.white
            ),) :
        Text('Create Announcement',style: TextStyle(
          color:Colors.white
        ),),
        centerTitle: true,


      ),
      backgroundColor: AppColor.thirdcolor,
      body: SafeArea(
        child:

        ChangeNotifierProvider<PostViewModel>(
          create:(BuildContext cx) =>  uploadjobviewmodel,
          builder: (context, child) {

            return Consumer<PostViewModel>(
              builder: (context, posts, _) {

                var status =uploadjobviewmodel.PostApiresponse.status;

                print("api status= ${status}" );

                print("update state: ${uploadjobviewmodel.PostUpdateResponse.status}");


                if(uploadjobviewmodel.PostApiresponse.status == Status.COMPLETED){

                  WidgetsBinding.instance.addPostFrameCallback((timeStamp) {

                    ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(content: Text('Your job has been published')

                      )

                    );
                    uploadjobviewmodel.PostApiresponse.status = Status.LOADING;
                    Navigator.pop(context);
                    Navigator.push(context, MaterialPageRoute(builder: (context) => ScreenDefault(),));
                  });
                }
                if(uploadjobviewmodel.PostApiresponse.status !=Status.LOADING){

                  WidgetsBinding.instance.addPostFrameCallback((timeStamp) {

                    ScaffoldMessenger.of(context).showSnackBar(

                        SnackBar(content: Text('Your job has been published')

                        )

                    );
                    uploadjobviewmodel.PostApiresponse.status = Status.LOADING;
                    Navigator.pop(context);

                    Navigator.push(context, MaterialPageRoute(builder: (context) => ScreenDefault(),));
                  });
                }
              // else if(uploadjobviewmodel.PostApiresponse.status == Status.ERROR){
              //     WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
              //
              //       ScaffoldMessenger.of(context).showSnackBar(
              //           SnackBar(content: Text('Successfully'))
              //       );
              //
              //
              //       Navigator.push(context, MaterialPageRoute(builder: (context) => ScreenDefault(),));
              //     });
              //   }

                if( uploadjobviewmodel.PostUpdateResponse.status != Status.LOADING){
                  WidgetsBinding.instance.addPostFrameCallback((timeStamp) {

                    ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(content: Text('Your Post has been saved'))
                    );
                    uploadjobviewmodel.PostUpdateResponse.status = Status.LOADING;

                    if(widget.islong == true){
                      uploadjobviewmodel.PostUpdateResponse.status = Status.LOADING;
                      Navigator.pop(context);
                      Navigator.push

                        (context,MaterialPageRoute(builder:(context) => ScreenDefault(),) );

                      return null;


                    }
                  if(widget.admin == true){
                      uploadjobviewmodel.PostApiresponse.status = Status.LOADING;
                      Navigator.pop(context);
                      Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(
                          builder: (context) => postadminview(isadmin: true,)), (Route route)

                      => route.isFirst);
                      return null;
                    }
                    uploadjobviewmodel.PostUpdateResponse.status = Status.LOADING;
                    Navigator.pop(context);
                    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(
                        builder: (context) => UserPostScreen()), (Route route)

                    => route.isFirst);




                  });
                }

                if(_currentPosition?.latitude!=0){
                  WidgetsBinding.instance.addPostFrameCallback((timeStamp) {

                    setState(() {

                    });








                  });
                }

               // else if(   isPick == true){
               //    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
               //
               //      ScaffoldMessenger.of(context).showSnackBar(
               //          SnackBar(content: Text('Location has been picked'))
               //      );
               //
               //
               //
               //
               //
               //
               //
               //
               //    });
               //  }
                return SingleChildScrollView(
                  child: Container(
                    margin: EdgeInsets.symmetric(vertical: 20),
                    padding: EdgeInsets.symmetric(horizontal: 23,),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [


                        TextField(
                          style: TextStyle(
                            fontSize: AppFont.smfont+1,


                            color: AppColor.primarycolor,
                            height: 2,

                          ),
                          
                          decoration: InputDecoration(
                              hintText: 'Describe your job title',
                              labelText: 'Job Title',
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: AppColor.primarycolor,
                                      width: 2
                                  )
                              ),

                              // helperText:'965689896' ,
                              labelStyle: TextStyle(
                                  color: AppColor.primarycolor,
                                  fontWeight: FontWeight.bold,
                                  fontSize: AppFont.bigfont
                              ),
                              suffix: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    txttitle.clear();
                                  });
                                },
                                child: Icon(
                                  Icons.highlight_remove,
                                  size: 22,
                                  color: AppColor.primarycolor,
                                ),
                              )



                          ),
                          controller: txttitle,
                          onSubmitted: (value) {
                            setState(() {
                              txttitle.text = value;
                            });
                          },
                          onChanged: (value) {
                            setState(() {
                              txttitle.text = value;
                              txttitle.selection = TextSelection.fromPosition(TextPosition(
                                  offset: txttitle.text.length));

                            });

                          },

                        ),

                        TextField(
                          style: TextStyle(
                            fontSize: AppFont.smfont+1,

                            color: AppColor.primarycolor,
                            height: 2,

                          ),
                          controller:jobdesc,
                          onChanged: (value) {
                            setState(() {
                              jobdesc.text = value;
                              jobdesc.selection = TextSelection.fromPosition(TextPosition(
                                  offset:   jobdesc.text.length));

                            });

                          },
                          onSubmitted: (value) {
                            setState(() {
                              jobdesc.text = value;
                            });

                          },
                          maxLines: 3,
                          decoration: InputDecoration(
                              hintText: ' Describe your company history, experience , worksplace or condition...',
                              labelText: 'Describe Job',
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: AppColor.primarycolor,
                                      width: 2
                                  )
                              ),





                              // helperText:'965689896' ,
                              labelStyle: TextStyle(
                                  color: AppColor.primarycolor,
                                  fontWeight: FontWeight.bold,
                                  fontSize: AppFont.bigfont
                              ),
                              suffix: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    jobdesc.clear();
                                  });
                                },
                                child: Icon(
                                  Icons.highlight_remove,
                                  size: 22,
                                  color: AppColor.primarycolor,
                                ),
                              )



                          ),


                        ),
                        
                        SizedBox(height: 30,),

                        Container(
                          width: double.maxFinite,
                          child: Text('Total Positions',
                            style: TextStyle(
                                fontSize: AppFont.bigfont,
                                fontWeight: FontWeight.bold,
                                color: AppColor.primarycolor

                            ),),
                        ),

                        SizedBox(height: 30,),

                        Container(

                          height: 45,
                          width: double.maxFinite,


                          padding: EdgeInsets.all(5),

                          decoration: BoxDecoration(
                              color: AppColor.whitecolor,
                              borderRadius: BorderRadius.circular(10)
                          ),
                          child: DropdownButton(

                            value: selecteditem==null ? allitem.elementAt(0) : selecteditem,

                            dropdownColor: AppColor.whitecolor,

                            focusColor: AppColor.whitecolor,

                            menuMaxHeight: 270,
                            borderRadius: BorderRadius.circular(12),
                            iconEnabledColor: AppColor.primarycolor, //TODO ARROW ICON
                            iconDisabledColor: AppColor.bgcolor,
                            style: TextStyle(

                            ),

                            alignment: Alignment.center,
                            isExpanded: true,
                            underline: SizedBox(),

                            items:List.generate( allitem?.length  ?? 0, (index){

                              return DropdownMenuItem(

                                enabled: true,
                                value: allitem[index],
                                alignment: Alignment.center,

                                child: Text('${allitem[index]}',
                                  style: TextStyle(
                                      color: Colors.black
                                  ),
                                ),
                                onTap: () {

                                },
                              );
                            }),
                            onChanged:(value) {

                              setState(() {
                                selecteditem = value;
                                print(selecteditem);
                              });
                            },
                            elevation: 0,

                          ),
                        ),








                        SizedBox(height: 30,),
                        
                        Container(
                          width: double.maxFinite,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,




                            children: [

                              Expanded(

                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text('Minimum Salary',
                                      style: TextStyle(
                                          fontSize: AppFont.bigfont,
                                          fontWeight: FontWeight.bold,
                                          color: AppColor.primarycolor

                                      ),),




                                    Container(







                                      child:     TextField(
                                        style: TextStyle(
                                            fontSize: AppFont.bigfont,
                                            fontWeight: FontWeight.w400,
                                            color: AppColor.primarycolor,

                                            height: 2

                                        ),
                                        keyboardType: TextInputType.number,
                                        controller: txtmin,
                                        onSubmitted: (value) {
                                          setState(() {
                                            txtmin.text = value;
                                          });
                                        },
                                        onChanged: (value) {
                                          setState(() {
                                            txtmin.text  = value;
                                            txtmin.selection = TextSelection.fromPosition(TextPosition(
                                                offset:       txtmin.text.length));

                                          });

                                        },
                                        decoration: InputDecoration(
                                            hintText: '300',
                                          focusedBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: AppColor.primarycolor,
                                                  width: 2
                                              )
                                          ),
                                            hintStyle: TextStyle(

                                              color: AppColor.primarycolor
                                            ),




                                            prefix: Icon(Icons.attach_money,size: 16,),
                                          // enabledBorder: OutlineInputBorder(
                                          //
                                          //   borderSide: BorderSide(
                                          //
                                          //     color: AppColor.primarycolor
                                          //   )
                                          // ),
                                          //  focusedBorder: OutlineInputBorder(
                                          //       borderSide: BorderSide(
                                          //           color: AppColor.primarycolor,
                                          //
                                          //
                                          //       )
                                          //   )
                                          // suffix: IconButton(
                                          //   onPressed: (){
                                          //
                                          //   },
                                          //   icon: Icon(Icons.delete_forever_rounded),
                                          // ),

                                        ),


                                      ),
                                    ),


                                  ],
                                ),
                              ),
                              SizedBox(width: 20,),


                              Expanded(

                                child: Column(

                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text('Maximum Salary',
                                      style: TextStyle(
                                          fontSize: AppFont.bigfont,
                                          fontWeight: FontWeight.bold,
                                          color: AppColor.primarycolor

                                      ),),



                                    Container(







                                      child:     TextField(
                                        style: TextStyle(
                                            fontSize: AppFont.bigfont,
                                            fontWeight: FontWeight.w400,
                                            color: AppColor.primarycolor,

                                            height: 2

                                        ),
                                        keyboardType: TextInputType.number,
                                        controller: txtmax,
                                        onChanged: (value) {
                                          setState(() {
                                            txtmax.text = value;
                                            txtmax.selection = TextSelection.fromPosition(TextPosition(
                                                offset:    txtmax.text.length));

                                          });

                                        },

                                        onSubmitted: (value) {
                                          setState(() {
                                            txtmax.text = value;
                                                if(double.parse(txtmax.text) < double.parse(txtmin.text)){
                                                    print("True");
                                              showDialog(context: context, builder: (context) {
                                                      return AlertDialog(
                                                        shape: RoundedRectangleBorder(
                                                            borderRadius: BorderRadius.circular(14)
                                                        ),
                                                        backgroundColor:AppColor.primarycolor ,

                                                        elevation: 0,

                                                        title: Column(
                                                          children: [
                                                            Icon(Icons.warning,size: 60,color: AppColor.fourthcolor,),
                                                            SizedBox(height: 10,),
                                                            Text('Wrong Format',style:
                                                            TextStyle(
                                                                color:AppColor.fourthcolor,
                                                                fontSize: 18,
                                                                fontWeight: FontWeight.w500
                                                            ),),
                                                            SizedBox(height: 10,),
                                                            Text('Your maximum salary \$ must be larger then the minimum salary',style:
                                                            TextStyle(
                                                              color:AppColor.fourthcolor,
                                                              fontSize: 12,

                                                              fontWeight: FontWeight.w400,

                                                            ),
                                                              textAlign: TextAlign.center,

                                                            ),

                                                            SizedBox(height: 10,),

                                                            Row(
                                                              children: [
                                                                Expanded(
                                                                  child: ElevatedButton(onPressed: () => Navigator.pop(context),
                                                                    child: Text('Comfirm'),
                                                                    style: ButtonStyle(
                                                                        backgroundColor: MaterialStatePropertyAll(AppColor.fourthcolor  )
                                                                    ),
                                                                  ),
                                                                ),
                                                              ],
                                                            )
                                                          ],
                                                        ),

                                                      );
                                                    });
                                                }
                                          });
                                        },
                                        decoration: InputDecoration(
                                            hintText: '\700',

                                            focusedBorder: UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: AppColor.primarycolor,
                                                    width: 2
                                                )
                                            ),


                                            prefix: Icon(Icons.attach_money,size: 16,)
                                          // suffix: IconButton(
                                          //   onPressed: (){
                                          //
                                          //   },
                                          //   icon: Icon(Icons.delete_forever_rounded),
                                          // ),

                                        ),


                                      ),
                                    ),


                                  ],
                                ),
                              ),

                            ],
                          ),
                        ),
                        SizedBox(height: 20,),



                        TextField(

                          style: TextStyle(

                            fontSize: AppFont.smfont+1,

                            fontWeight: FontWeight.w400,
                            color: AppColor.primarycolor,
                            height: 2,

                          ),
                          controller: txtcomname,
                          onSubmitted: (value) {
                            setState(() {
                              txtcomname.text = value;
                            });
                          },
                          onChanged: (value) {
                            setState(() {
                              txtcomname.text  = value;
                              txtcomname.selection = TextSelection.fromPosition(TextPosition(
                                  offset: txtcomname.text.length));

                            });

                          },
                          decoration: InputDecoration(
                              hintText: 'Company alias name',
                              labelText: 'What is your company work name?',
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: AppColor.primarycolor,
                                      width: 2
                                  )
                              ),




                              // helperText:'965689896' ,
                              labelStyle: TextStyle(
                                  color: AppColor.primarycolor,
                                  fontSize: AppFont.bigfont,
                                fontWeight: FontWeight.bold
                              ),
                              suffix: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    txtcomname.clear();
                                  });
                                },
                                child: Icon(
                                  Icons.highlight_remove,
                                  size: 22,
                                  color: AppColor.primarycolor,
                                ),
                              )



                          ),


                        ),


                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [

                                  TextField(
                                    style: TextStyle(
                                      fontSize: AppFont.smfont+1,
                                      fontWeight: FontWeight.w400,
                                      color: AppColor.primarycolor,
                                      height: 2,

                                    ),
                                    controller: txtstreet,
                                    onSubmitted: (value) {
                                      txtstreet.text = value;
                                    },
                                    onChanged: (value) {
                                      setState(() {
                                        txtstreet.text  = value;
                                        txtstreet.selection = TextSelection.fromPosition(TextPosition(
                                            offset: txtstreet.text.length));

                                      });

                                    },
                                    decoration: InputDecoration(
                                        hintText: 'Street 458, Toul Tompong 2 ,Khan Chomkamon',
                                        labelText: 'Company Home Address',
                                        focusedBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: AppColor.primarycolor,
                                                width: 2
                                            ),

                                        ),


                                        suffix: GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              txtstreet.clear();
                                            });
                                          },
                                          child: Icon(
                                            Icons.highlight_remove,
                                            size: 22,
                                            color: AppColor.primarycolor,
                                          ),
                                        ),

                                        // helperText:'965689896' ,
                                        labelStyle: TextStyle(
                                            color: AppColor.primarycolor,
                                            fontSize: AppFont.bigfont,
                                          fontWeight: FontWeight.bold
                                        ),

                                        // prefix:Text('+855')


                                    ),


                                  ),
                                ],
                              ),
                            ),

                            // SizedBox(width: 25,),

                          ],
                        ), //TODO ADDRESS

                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                                height: 68,
                                child:

                                Form(
                                  key: keyposition,

                                  child: TypeAheadFormField(

                                    textFieldConfiguration: TextFieldConfiguration(
                                        controller:    txtprovince,
                                        onSubmitted: (value) {
                                          setState(() {
                                            txtprovince.text = value;
                                          });
                                        },

                                        decoration: InputDecoration(
                                          hintText: 'Province / City',
                                          suffix: GestureDetector(
                                              onTap: () {
                                                setState(() {
                                                  txtprovince.clear();
                                                });
                                              },

                                              child: Icon(Icons.highlight_remove_rounded,size: 15)),
                                          labelStyle: TextStyle(
                                            color: AppColor.primarycolor,
                                            fontWeight: FontWeight.bold
                                          ),

                                          filled:false,
                                          label:  Text('Province / City'),
                                          enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: AppColor.primarycolor,
                                                  width: 2
                                              )
                                          ),

                                          focusedBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: AppColor.primarycolor,
                                                  width: 2
                                              )
                                          ),
                                        ),



                                        style: TextStyle(
                                            color: AppColor.primarycolor,
                                            fontSize: AppFont.smfont+2
                                        )
                                    ),
                                    suggestionsCallback: (pattern) {

                                      return companies.where((element) => element.toLowerCase().
                                      startsWith(     txtprovince.text.toLowerCase())

                                      );


                                    },
                                    itemBuilder: (context, suggestion) {
                                      return Container(
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(10)
                                        ),
                                        child: ListTile(
                                          title: Text(suggestion.toString(),style: TextStyle(

                                              color: AppColor.primarycolor,
                                              fontSize: AppFont.smfont+2
                                          ),),
                                        ),
                                      );
                                    },



                                    transitionBuilder: (context, suggestionsBox, controller) {
                                      return suggestionsBox;

                                    },

                                    onSuggestionSelected: (suggestion) {
                                      setState(() {
                                        txtprovince.text = suggestion.toString();
                                        print(      txtprovince.text );

                                      });

                                    },
                                    validator: (value) {
                                      if (value!.isEmpty) {
                                        return 'Please Select Province or City';
                                      }
                                    },
                                    onSaved: (value) {

                                      txtprovince.text  = value.toString();
                                      print(      txtprovince.text );

                                    },

                                  ),
                                )

                            ),
                            // TextField(
                            //   style: TextStyle(
                            //     fontSize: AppFont.smfont+1,
                            //     fontWeight: FontWeight.bold,
                            //     color: AppColor.primarycolor,
                            //     height: 2,
                            //
                            //   ),
                            //   controller:  txtprovince,
                            //
                            //   onSubmitted: (value) {
                            //     txtprovince.text = value;
                            //   },
                            //   onChanged: (value) {
                            //     setState(() {
                            //       txtprovince.text  = value;
                            //       txtprovince.selection = TextSelection.fromPosition(TextPosition(
                            //           offset:txtprovince.text.length));
                            //
                            //     });
                            //
                            //   },
                            //
                            //   decoration: InputDecoration(
                            //     hintText: 'Phnom Penh',
                            //     labelText: 'City / Province',
                            //     focusedBorder: UnderlineInputBorder(
                            //         borderSide: BorderSide(
                            //             color: AppColor.primarycolor,
                            //             width: 2
                            //         )
                            //     ),
                            //
                            //     suffix: GestureDetector(
                            //       onTap: () {
                            //         setState(() {
                            //           txtprovince.clear();
                            //         });
                            //       },
                            //       child: Icon(
                            //         Icons.highlight_remove,
                            //         size: 22,
                            //         color: AppColor.primarycolor,
                            //       ),
                            //     ),
                            //
                            //
                            //     // helperText:'965689896' ,
                            //     labelStyle: TextStyle(
                            //         color: AppColor.primarycolor,
                            //         fontSize: AppFont.bigfont
                            //     ),
                            //
                            //
                            //
                            //
                            //   ),
                            //
                            //
                            // ),
                          ],
                        ),

                        //
                        SizedBox(height: 30,),
                        Text('Work place location',
                          style: TextStyle(
                              fontSize: AppFont.bigfont,
                              fontWeight: FontWeight.bold,
                              color: AppColor.primarycolor

                          ),),
                        SizedBox(height: 30,),

                        Container(
                          width: double.maxFinite,
                          child:  ElevatedButton(onPressed:()  async {
                            _getCurrentPosition();

                            print("location");
                            // print(_currentAddress);
                            print(_currentPosition);




                            positionlat=  await Navigator.push(context,MaterialPageRoute(builder: (context) {
                              return


                                MapPicker(latitude: _currentPosition?.latitude,
                                  longitude: _currentPosition?.longitude,


                                );
                            },));
                            setState(() async  {

                              print("Back to ${positionlat}");

                              print(    positionlat);



                            });






                          },
                              style: ButtonStyle(
                                  backgroundColor: MaterialStatePropertyAll(AppColor.bgcolor),
                                  shape: MaterialStatePropertyAll(RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(12)
                                  ))
                              ),child:

                              positionlat.latitude ==0 ?
                              Text('Choose Location',style: TextStyle(color: Colors.white),) :
                              Text('Location has been selected',style: TextStyle(color: Colors.white),)
                          ),
                        ),
                        SizedBox(height: 30,),
                        // GestureDetector(
                        //   onTap: () {
                        //     print("Google values");
                        //     print(  positionlat.longitude);
                        //     print(positionlat.latitude);
                        //   },
                        //   child: Text('show location'),
                        // ),


                        Text('Job Type',
                          style: TextStyle(
                              fontSize: AppFont.bigfont,
                              fontWeight: FontWeight.bold,
                              color: AppColor.primarycolor

                          ),),
                        SizedBox(height: 30,),
                        
                        Container(
                          width: double.maxFinite,
                          child: Wrap(

                            alignment: WrapAlignment.spaceBetween,
                            runSpacing: 7,
                            direction: Axis.horizontal,
                            children: List.generate(listtype.length , (index) => ChoiceChip(
                              label: Text('${listtype[index]}'),
                              backgroundColor: AppColor.whitecolor,
                              selectedColor: AppColor.primarycolor,

                              labelStyle: TextStyle(
                                  color: _selectedIndex == index? Colors.white : AppColor.primarycolor
                              ),
                              onSelected: (selected) {
                                if (selected) {
                                  setState(() {
                                    _selectedIndex = index;
                                    typejob = listtype[index];
                                    print(typejob);

                                  });
                                }
                              }, 
                              selected: _selectedIndex == index,
                            )

                            ),

                          ),
                        ),
                        
                        SizedBox(height: 30,),
                        
                        Text('Experience Level',
                          style: TextStyle(
                              fontSize: AppFont.bigfont,
                              fontWeight: FontWeight.bold,
                              color: AppColor.primarycolor

                          ),),
                        SizedBox(height: 30,),
                        Container(
                          width: double.maxFinite,
                          child: Wrap(

                            alignment: WrapAlignment.spaceBetween,
                            runSpacing: 7,
                            direction: Axis.horizontal,
                            children: List.generate(listExperience.length , (index) => ChoiceChip(
                              label: Text('${listExperience[index]}'),
                              backgroundColor: AppColor.whitecolor,
                              selectedColor: AppColor.primarycolor,

                              labelStyle: TextStyle(
                                  color:   _selectedIndexExp  == index? Colors.white : AppColor.primarycolor
                              ),
                              onSelected: (selected) {
                                if (selected) {
                                  setState(() {
                                    _selectedIndexExp = index;
                                   JobExp = listExperience[index];
                                    print(     JobExp );

                                  });
                                }
                              }, selected: _selectedIndexExp == index,
                            )

                            ),

                          ),
                        ),
                        
                        SizedBox(height: 30,),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [

                            TextField(
                              style: TextStyle(
                                  fontSize: AppFont.smfont+1,
                                  fontWeight: FontWeight.bold,
                                  color: AppColor.primarycolor,
                                  height: 2

                              ),
                              controller: txtemail,
                              onSubmitted: (value) {
                                txtemail.text = value;
                              },
                              onChanged: (value) {
                                setState(() {
                                  txtemail.text  = value;
                                  txtemail.selection = TextSelection.fromPosition(TextPosition(
                                      offset:       txtemail.text.length));

                                });

                              },
                              decoration: InputDecoration(
                                hintText: 'youremail@gmail.com',
                                hintStyle: TextStyle(
                                  // wordSpacing: 0.6,
                                  // letterSpacing: 1,



                                ),
                                labelText: 'Email',
                                suffix: GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      txtemail.clear();
                                    });
                                  },
                                  child: Icon(
                                    Icons.highlight_remove,
                                    size: 22,
                                    color: AppColor.primarycolor,
                                  ),
                                ),
                                // helperText:'965689896' ,
                                labelStyle: TextStyle(
                                    color: AppColor.primarycolor,
                                    fontSize: AppFont.bigfont
                                ),

                                focusColor: AppColor.primarycolor,
                                focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color: AppColor.primarycolor,
                                        width: 2
                                    )
                                ),
                                // prefix: Icon(Icons.email,size: 17,)


                              ),


                            ),
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [

                            TextField(
                              style: TextStyle(
                                fontSize: AppFont.smfont+1,
                                fontWeight: FontWeight.bold,
                                color: AppColor.primarycolor,
                                height: 2,

                              ),
                              controller: txttel,
                              onSubmitted: (value) {




                                setState(() {

                                  txttel.text = value;
                                  if(txttel.text.contains('0',0)){
                                    print("true contain 0");
                                    txttel.text = value.substring(1);;

                                    print(  txttel.text) ;





                                  }
                                });


                              },
                              onChanged: (value) {
                                setState(() {
                                  txttel.text  = value;
                                  txttel.selection = TextSelection.fromPosition(TextPosition(
                                      offset:       txttel.text.length));

                                });

                              },
                              decoration: InputDecoration(
                                  hintText: '96-56-89-896',
                                  labelText: 'Telephone',
                                  focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          color: AppColor.primarycolor,
                                          width: 2
                                      )
                                  ),

                                  suffix: GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        txttel.clear();
                                      });
                                    },
                                    child: Icon(
                                      Icons.highlight_remove,
                                      size: 22,
                                      color: AppColor.primarycolor,
                                    ),
                                  ),


                                  // helperText:'965689896' ,
                                  labelStyle: TextStyle(
                                      color: AppColor.primarycolor,
                                      fontSize: AppFont.bigfont
                                  ),

                                  prefix:Text('+855')


                              ),


                            ),

                          ],

                        ),

                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [

                            Form(
                              key: _telegram,
                              child: TextFormField(
                                style: TextStyle(
                                  fontSize: AppFont.smfont+1,
                                  fontWeight: FontWeight.bold,
                                  color: AppColor.primarycolor,
                                  height: 2,

                                ),
                                controller:  txttelegram,

                               onFieldSubmitted: (value) {



                                  setState(() {
                                    txttelegram.text = value;
                                    if(_telegram.currentState!.validate()){
                                      _telegram.currentState!.save();
                                    }
                                  });

                               },
                                validator: (value) {
                                  if(isTelegramvalid(value.toString())){
                                    return null;
                                  }
                                  else{
                                    return 'Wrong telegram link format';
                                  }

                                },
                                onChanged: (value) {
                                  setState(() {
                                    txttelegram.text  = value;
                                    txttelegram.selection = TextSelection.fromPosition(TextPosition(
                                        offset:       txttelegram.text.length));
                                    if(_telegram.currentState!.validate()){
                                      _telegram.currentState!.save();
                                    }

                                  });

                                },
                                decoration: InputDecoration(
                                    hintText: 'https://t.me/vornii',
                                    labelText: 'Telegram Links',
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: AppColor.primarycolor,
                                            width: 2
                                        )
                                    ),

                                  suffix: GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        txttelegram.clear();
                                      });
                                    },
                                    child: Icon(
                                      Icons.highlight_remove,
                                      size: 22,
                                      color: AppColor.primarycolor,
                                    ),
                                  ),


                                    // helperText:'965689896' ,
                                    labelStyle: TextStyle(
                                        color: AppColor.primarycolor,
                                        fontSize: AppFont.bigfont
                                    ),




                                ),


                              ),
                            ),
                          ],
                        ),
                        
                        SizedBox(height: 10,),

                        
                        Column(
                          children: List.generate( jobreq.length ?? 0, (index) =>

                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  TextField(
                                    style: TextStyle(
                                      fontSize: AppFont.smfont+1,

                                      color: AppColor.primarycolor,
                                      height: 2,

                                    ),

                                    controller: controller[index], //TODO DYNAMIC TEXT CONTROLLER
                                    onTap: () {
                                      setState(() {
                                        submitjob = true;
                                        controller[index].text =='';
                                        requirement.insert(index,controller[index].text);
                                        var removeindex = index + 1;
                                        requirement.removeAt(removeindex );
                                      });
                                    },
                                    onSubmitted: (value) {

                                      setState(() {
                                        submitjob = true;
                                        controller[index].text == value.toString();
                                        requirement.insert(index,controller[index].text);
                                        var removeindex = index + 1;
                                        requirement.removeAt(removeindex );
                                        print(requirement);
                                      });


                                    },
                                    onChanged: (value) {
                                      setState(() {
                                        submitjob = true;
                                        controller[index].text == value.toString();
                                        requirement.insert(index,controller[index].text);
                                        var removeindex = index + 1;
                                        requirement.removeAt(removeindex );
                                        print(requirement);
                                      });


                                    },

                                    decoration: InputDecoration(
                                      suffix:                    Container(
                                        //TODO DELETE SKILLS
                                        child:jobreq.length <=1 ?Text('')   :CircleAvatar(
                                          radius: 20,
                                          backgroundColor: AppColor.thirdcolor   ,

                                          child: IconButton(onPressed:() {


                                            setState(() {
                                              //
                                              // controller.removeLast();
                                              //
                                              // skills.removeLast() ;
                                              print("press delete on ${index}");
                                              print("press ${jobreq.length}");
                                              jobreq.removeAt(index);

                                              // jobskills.map((e) => jobskills.add(e));

                                              controller.removeAt(index);
                                            requirement.removeAt(index);

                                              // skills.removeAt(index);
                                              // jobskills.removeLast();
                                              // skills.removeLast();
                                              // controller.removeAt(jobskills.length-1);


                                            });
                                          },
                                            style: ButtonStyle(


                                            ),

                                            icon:Icon(Icons.remove_circle_sharp,color:AppColor.bgcolor,size: 28,),
                                          ),
                                        ),
                                      ),


                                        hintText: 'Write your job requirement',
                                        labelText: 'Job Requirement ${index+1}',
                                        focusedBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: AppColor.primarycolor,
                                                width: 2
                                            )
                                        ),





                                        // helperText:'965689896' ,
                                        labelStyle: TextStyle(
                                            color: AppColor.primarycolor,
                                            fontWeight: FontWeight.bold,
                                            fontSize: AppFont.bigfont
                                        ),
                                        // suffix:   Container(
                                        //   child:jobreq.length ==1 ?Text('')   :CircleAvatar(
                                        //     radius: 15,
                                        //     backgroundColor: AppColor.fourthcolor,
                                        //     child: IconButton(onPressed:() {
                                        //
                                        //       setState(() {
                                        //         jobreq.removeLast();
                                        //
                                        //       });
                                        //     },
                                        //       style: ButtonStyle(
                                        //           backgroundColor: MaterialStatePropertyAll(AppColor.fourthcolor)
                                        //       ),
                                        //
                                        //       icon:Icon(Icons.remove,color: Colors.white,size: 14,),
                                        //     ),
                                        //   ),
                                        // ),




                                    ),



                                  ),
                                  SizedBox(height: 20,),


                                ],
                              ),

                          ),
                        ),
                        // GestureDetector(
                        //   onTap: () {
                        //     print("final job requirement ${requirement}");
                        //   },
                        //   child: Text('Check requirement'),
                        // ),
                        SizedBox(height: 20,),
                        Row(
                          children: [
                            Container(
                              child:    jobreq.length ==12 ?null   :

                              CircleAvatar(
                                radius: 15,
                                backgroundColor: AppColor.primarycolor,
                                child: IconButton(

                                  onPressed:() {
                                    setState(() {

                                      jobreq.add(jobreq.length +1);
                                      var index = jobreq.length-1;
                                      // controller[index].text = "";
                                      print( "Job Length: ${jobreq.length}" );
                                      print(jobreq);
                                      requirement.insert(index, controller[index].text );
                                      print("final job requirement ${requirement}");


                                    });
                                  },
                                  style: ButtonStyle(
                                      backgroundColor: MaterialStatePropertyAll(AppColor.fourthcolor)
                                  ),
                                  icon:Icon(Icons.add,color: Colors.white,size: 14,),
                                ),
                              )  ,
                            ),
                            SizedBox(width: 10,),




                          ],
                        ),



                        SizedBox(height: 30,),


                        SizedBox(height: 30,),
                        Text('Choose your job category',
                          style: TextStyle(
                              fontSize: AppFont.bigfont,
                              fontWeight: FontWeight.bold,
                              color: AppColor.primarycolor

                          ),),
                        SizedBox(height: 30,),
                        Container(
                          width: double.maxFinite,
                          child: Wrap(
                            //TODO Categories chip

                            alignment: WrapAlignment.spaceBetween,
                            spacing: 2,

                            runSpacing: 3,
                            direction: Axis.horizontal,
                            children:
                            List.generate(cateitem.length ?? 0,

                                  (index) =>

                                GestureDetector(

                                  onTap: () {

                                    setState(() {

                                      selectcategory.add(cateitem[index]);

                                      print(cateitem[index]);

                                    });

                                  },

                                  child: InputChip(

                                    label: Text('${cateitem[index]}',style:

                                    TextStyle(

                                      color: iscategoryselect[index] ==true ? Colors.white : Colors.black

                                      ,   fontSize: AppFont.smfont+1,

                                    ),),


                                    elevation: 0,

                                    backgroundColor:Colors.white ,

                                    selected:iscategoryselect[index] == true ? true : false ,

                                    onDeleted: () {

                                      setState(() {

                                        iscategoryselect[index] = false;

                                        selectcategory.remove(cateitem[index]);

                                      });

                                    },

                                    onSelected: (value) {

                                      setState(() {

                                        iscategoryselect[index] =true;

                                        print(cateitem[index]);

                                        selectcategory.add(cateitem[index]);
                                        // selectcategory.add(value.toString());
                                        print(   selectcategory);

                                      });
                                      // print(selectcategory);
                                    },

                                    selectedColor: AppColor.primarycolor,
                                   deleteIconColor: AppColor.whitecolor,


                                    isEnabled: true,

                                    iconTheme: IconThemeData(

                                        color: AppColor.secondcolor
                                    ),

                                    checkmarkColor: AppColor.primarycolor,



                                  ),

                                ),),
                          ),
                        ), //TODO CATEGORY JOBS HERE
                        
                        SizedBox(height: 30,),
                        
                        Row(
                          //TODO ADD AND REMOVE REQUIREMENT SECTION
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [

                            Expanded(
                              flex: 1,
                              child:
                              Container(
                                child:  ElevatedButton(onPressed:()  async {
                                  var category = selectcategory.toList();


                                  // print(jobreq);
                                  print(txttitle.text);

                                  print("lan ${positionlat?.longitude}");
                                  print("lon ${positionlat?.latitude}");
                                  print(jobdesc.text);
                                  print(selecteditem); //TODO TOTAL
                                  print(txtmin.text);
                                  print(txtmax.text);
                                  print(txtcomname.text);
                                  print(typejob);
                                  print(txttel.text);
                                  print(txtemail.text);

                                  print(requirement);
                                  print(txtstreet.text);
                                  print(txtprovince.text);

                                  print( category );
                                  print(tokenss);
                                  print(uid);


                                  print(positionlat);
                                  //TODO POST OBJECT SEND TO VIEW MODEL

                                  if(
                                  txttitle.text =='' ||
                                   txtemail.text =='' ||
                                   txttelegram.text=='' ||
                                  txtcomname.text=='' ||
                                  jobdesc.text=='' ||
                                  JobExp == null ||
                                  selecteditem == null   ||
                                  typejob == null  ||
                                  txtmax.text=='' ||
                                  txtmin.text=='' ||
                                  category.length == 0 ||
                                  txtprovince.text =='' ||
                                  txtprovince.text =='' ||
                                  txtstreet.text =='' ||
                                  txtmapurl.text =='' ||
                                  requirement.length == 0 ||
                                  tokenss ==''||
                                  uid ==  ''


                                  ){
                                    print("one or two missing info");

                                    return        showDialog(context: context, builder: (context) {
                                      return AlertDialog(
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(14)
                                        ),
                                        backgroundColor:AppColor.primarycolor ,

                                        elevation: 0,

                                        title: Column(
                                          children: [
                                            Icon(Icons.warning,size: 60,color: AppColor.fourthcolor,),
                                            SizedBox(height: 10,),
                                            Text('Missing',style:
                                            TextStyle(
                                                color:AppColor.fourthcolor,
                                                fontSize: 18,
                                                fontWeight: FontWeight.w500
                                            ),),
                                            SizedBox(height: 10,),
                                            Text('One or two more missing informations in your dialog , please fill everything',style:
                                            TextStyle(
                                              color:AppColor.fourthcolor,
                                              fontSize: 12,

                                              fontWeight: FontWeight.w400,

                                            ),
                                              textAlign: TextAlign.center,

                                            ),

                                            SizedBox(height: 10,),

                                            Row(
                                              children: [
                                                Expanded(
                                                  child: ElevatedButton(onPressed: () => Navigator.pop(context),
                                                    child: Text('Try again'),
                                                    style: ButtonStyle(
                                                        backgroundColor: MaterialStatePropertyAll(AppColor.fourthcolor  )
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            )
                                          ],
                                        ),

                                      );
                                    },);
                                  }
                                  if(widget.admin == true){

                                  }


                                  var object = JobRequest(
                                      title: txttitle.text,
                                      status: true,

                                      // address: Address(
                                      //   city:
                                      // ),
                                      contact: Contact(
                                        email: txtemail.text,
                                        telegram:  txttelegram.text,
                                        telephone: txttel.text
                                      ),
                                      jobDescription: JobDescription(
                                          companyName: txtcomname.text,
                                          detail: jobdesc.text,
                                          experience: JobExp,
                                          totalPosition:selecteditem ,
                                          jobtype: typejob,
                                          maxSalary: double.parse(txtmax.text),
                                          minSalary:double.parse(txtmin.text)
                                      ),

                                      category: category,
                                      address: Address(
                                        city: txtprovince.text,
                                        province: txtprovince.text,
                                        street: txtstreet.text,
                                        latitude: positionlat!.latitude!,
                                        longitude: positionlat!.longitude!,
                                        urilocation: txtmapurl.text,
                                      ),

                                      requirement:requirement,

                                      user:uid



                                  );
                                  setState(() {
                                    isloading = true;
                                  });

                                  //
                                  if(updatewidget == true) {
                                    print("Updating");
                                    print(tokenss);

                                    await uploadjobviewmodel.updateJobAPI(object,tokenss,postid);
                                  }
                                  else{
                                  await uploadjobviewmodel.postJobAPI(object, tokenss);
                                  }

                                  //





                                  // postapi(object);


                                  //TODO POST HERE


                                  // print(controller.toList());
                                },
                                    style: ButtonStyle(
                                        backgroundColor: MaterialStatePropertyAll(AppColor.bgcolor),
                                      shape: MaterialStatePropertyAll(RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(12)
                                      ))
                                    ),child:updatewidget == true?
                                        isloading == true ?
                                        Center(

                                            child: SizedBox(
                                              height: 20,
                                              width: 20,
                                              child: CircularProgressIndicator(
                                                backgroundColor:Colors.indigo,
                                                color:AppColor.primarycolor,
                                              ),
                                            ))  :
                                    Text('Update',style: TextStyle(color: Colors.white),)
                                        :
                                    isloading == true ?
                                    Center(

                                        child: SizedBox(
                                          height: 20,
                                          width: 20,
                                          child: CircularProgressIndicator(
                                            backgroundColor:Colors.indigo,
                                            color:AppColor.primarycolor,
                                          ),
                                        )) :
                                    Text('Publish',style: TextStyle(color: Colors.white),)


                                ),
                              )


                            ),
                            //TODO ADD REQUIREMENT
                            SizedBox(width: 15,),
                            //TODO REMOVE REQUIREMENT

                            // Expanded(
                            //   flex: 1,
                            //   child: Container(
                            //     child: ElevatedButton(onPressed:() {
                            //      Navigator.pop(context);
                            //     },
                            //       style: ButtonStyle(
                            //           backgroundColor: MaterialStatePropertyAll(AppColor.fourthcolor)
                            //       ),
                            //
                            //       child: Text('Cancel'),
                            //     ),
                            //   ),
                            // ),

                          ],
                        ),
                      ],
                    ),
                  ),
                );
              },

            );
          },

        ),
      ),
    );
  }

  void postapi(object)  async {
 //
    SharedPreferences prefs = await SharedPreferences.getInstance();
 var token = prefs.get('tokens') ?? '';

    uploadjobviewmodel.postJobAPI(object, token);
  }

  void gettoken() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      tokenss = prefs.get('tokens');
      uid = prefs.getString('userid');
      print("admin token ${tokenss}");
      print("userid ${uid }");
    });
  }
}



