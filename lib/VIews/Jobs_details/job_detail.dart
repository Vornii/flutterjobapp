

import 'dart:async';

import 'package:bulleted_list/bulleted_list.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:jobapp/VIews/Jobs_details/pfscreen.dart';
import 'package:jobapp/VIews/WelcomeScreen/screenfindjob.dart';
import 'package:jobapp/ViewModel/job_view_model.dart';
import 'package:jobapp/ViewModel/post_view_model.dart';
import 'package:jobapp/data/Response/status_response.dart';
import 'package:jobapp/model/POST/message_request_model.dart';
import 'package:jobapp/res/BaseUrl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:url_launcher/url_launcher_string.dart';
import 'package:geolocator/geolocator.dart';

import '../../res/Appcolor.dart';
import '../../res/Appfont.dart';
import '../WelcomeScreen/defaultscreen.dart';



class JobDetailSr extends StatefulWidget {
  var uid;
  LatLng? value;
  JobDetailSr  ({Key? key, this.uid}) : super(key: key);
  @override
  State<JobDetailSr> createState() => _JobDetailSrState();
}

class _JobDetailSrState extends State<JobDetailSr> {
  GoogleMapController? mapController;
  var txtsmgController = TextEditingController();
  String? addressurl;
  var newlocation;
  var filepath;
  JobViewModel postidviewmodel = JobViewModel();
  PostViewModel msgviewmodel = PostViewModel();
  var message = TextEditingController();
  var uid;
  var lat = 104.91378989070654;
  var lon = 11.539558607920105;
  var first = true;
  var isloading = false;

  var dropcvname = "Drop Your Files CV here";
  Position? _currentPosition;
  Future<bool> _handleLocationPermission() async {
    bool serviceEnabled;
    LocationPermission? permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text('Location services are disabled. Please enable the services')));
      return false;
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Location permissions are denied')));
        return false;
      }
    }
    if (permission == LocationPermission.deniedForever) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text('Location permissions are permanently denied, we cannot request permissions.')));
      return false;
    }
    return true;
  }
  Future<void> _getCurrentPosition() async {
    final hasPermission = await _handleLocationPermission();
    if (!hasPermission) return;
    await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high)
        .then((Position position) {
      setState(() => _currentPosition = position);
    }).catchError((e) {
      debugPrint(e);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getCurrentPosition();
    postidviewmodel.FetchPostByID(widget.uid);

    getuserfetch();

  }
  Widget build(BuildContext context) {
    print("User id details ${widget.uid}");
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.indigo,
        elevation: 0,

        iconTheme: IconThemeData(
            color:Colors.white,

        ),
        title: Text('Job Detail',style: TextStyle(
          color:Colors.white
        )),
        centerTitle: true ,


      ),
      backgroundColor: AppColor.primarycolor,
      body: ChangeNotifierProvider<JobViewModel>(
        create: (context) => postidviewmodel,
        builder: (context, child) {
          return  Consumer<JobViewModel>(
            builder:(context, jobs, child) {
              var  status = jobs.jobApiResponseID.status;
              var job = postidviewmodel.jobApiResponseID.data?.data?.attributes;

              print("PostID ${status}");

              switch(status){

                case Status.COMPLETED:
                  print("lang is ${job?.address?.latitude}");
                  print("long is ${job?.address?.longitude}");


                  return  ChangeNotifierProvider<PostViewModel>(
                    create: (context) => msgviewmodel,
                    builder: (context, child) {
                      return Consumer<PostViewModel>(
                        builder: (context, msgres, child) {
                          print("submission response");
                          print(msgviewmodel.PostMsgResponse.status );
                          if( msgres.PostMsgResponse.status == Status.COMPLETED){
                            WidgetsBinding.instance.addPostFrameCallback((timeStamp) {

                              ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(content: Text('Your CV has been submitted, Thanks you'))
                              );
                              Navigator.push(context, MaterialPageRoute(builder: (context) => ScreenDefault(),));
                            });
                          }
                          if(msgres.PostMsgResponse.status != Status.LOADING){

                              WidgetsBinding.instance.addPostFrameCallback((timeStamp) {

                                ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(content: Text('Your CV has been submitted, Thanks you'))
                                );
                                Navigator.push(context, MaterialPageRoute(builder: (context) => ScreenDefault(),));
                              });

                          }
                          if(jobs.jobApiResponseID.status == Status.COMPLETED){
                            WidgetsBinding.instance.addPostFrameCallback((timeStamp) {

                                if(first == true){
                                  setState(() {
                                    first = false;
                                    //
                                    // lon = job!.address!.longitude! ;
                                    // lat = job!.address!.latitude! ;
                                  });
                                }



                            });
                          }
                          return Container(
                            margin: EdgeInsets.symmetric(vertical: 30,horizontal: 20),
                            child: SingleChildScrollView(
                              physics: ScrollPhysics(),

                              child: Container(

                                  width: double.maxFinite,



                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,

                                    children: [

                                      Container(


                                        child: Stack(


                                          children: [
                                            Container(
                                              margin: EdgeInsets.symmetric(vertical: 50),
                                              width:370,

                                              height:309,
                                              decoration: BoxDecoration(
                                                  color:AppColor.thirdcolor,
                                                  borderRadius:  BorderRadiusDirectional.circular(15)
                                              ),
                                              child: Column(
                                                children: [
                                                  SizedBox(height: 50,),
                                                  Padding(
                                                    padding: const EdgeInsets.all(8.0),
                                                    child: Text('${job?.title }',

                                                        style: TextStyle(

                                                          fontSize:AppFont.bigfont +5,
                                                          fontWeight: FontWeight.w500,
                                                          color: AppColor.primarycolor,


                                                        ),
                                                       maxLines: 1,
                                                      overflow: TextOverflow.ellipsis

                                                    ),
                                                  ),
                                                  SizedBox(height: 10,),
                                                  Row(
                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                    children: [
                                                      Text('Company: ',
                                                          style: TextStyle(
                                                            fontSize:AppFont.bigfont,
                                                            color: AppColor.primarycolor,
                                                            fontWeight: FontWeight.w400,

                                                          )),
                                                      // Icon(Icons.account_balance_sharp,color: Colors.white,),
                                                      Text('${job?.jobDescription?.companyName}',
                                                          style: TextStyle(
                                                            fontSize:AppFont.bigfont,
                                                            color: AppColor.primarycolor,
                                                            fontWeight: FontWeight.w400,

                                                          )),
                                                      SizedBox(height: 20,),
                                                    ],
                                                  ),
                                                  Padding(
                                                    padding: const EdgeInsets.symmetric(horizontal: 42,vertical: 10),
                                                    child: Divider(
                                                      color: AppColor.primarycolor.withOpacity(0.64),


                                                    ),
                                                  ),
                                                  SizedBox(height: 10,),
                                                  Row(
                                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,

                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                    children: [

                                                     Expanded(
                                                       child: Column(
                                                         crossAxisAlignment: CrossAxisAlignment.center,

                                                          children: [
                                                            Icon(Icons.location_on,color: AppColor.fourthcolor,),
                                                            SizedBox(height: 10,),
                                                            Text('${job?.address?.city}',
                                                                style: TextStyle(
                                                                  fontSize:AppFont.bigfont-1,
                                                                  fontWeight: FontWeight.w500,
                                                                  color: AppColor.primarycolor,

                                                                )),
                                                          ],
                                                        ),
                                                     ),
                                                      


                                                      Expanded(
                                                        child: GestureDetector(
                                                          onTap: () async {
                                                            final call = Uri.parse('tel:+855${job?.contact?.telephone}');
                                                            if (await canLaunchUrl(call)) {
                                                              launchUrl(call);
                                                            } else {
                                                              throw 'Could not launch $call';
                                                            }


                                                          },
                                                          child: Column(
                                                            crossAxisAlignment: CrossAxisAlignment.center,
                                                            children: [
                                                              Icon(Icons.phone,color: AppColor.primarycolor,),
                                                              SizedBox(height: 10,),
                                                              Text('+855 ${job?.contact?.telephone}',
                                                                  style: TextStyle(
                                                                    fontSize:AppFont.bigfont-1,
                                                                    fontWeight: FontWeight.w500,
                                                                    color: AppColor.primarycolor,

                                                                  )),
                                                            ],
                                                          ),
                                                        ),
                                                      ),
                                                    
                                              
                                                    Expanded(
                                                      child: Column(
                                                        crossAxisAlignment: CrossAxisAlignment.center,
                                                          children: [
                                                            Icon(Icons.people_alt,color: AppColor.primarycolor,),
                                                            SizedBox(height: 10,),
                                                            Text(' ${job?.jobDescription?.totalPosition}',
                                                                style: TextStyle(
                                                                  fontSize:AppFont.bigfont-1,
                                                                  fontWeight: FontWeight.w500,
                                                                  color: AppColor.primarycolor,

                                                                )),
                                                          ],
                                                        ),
                                                    ),



                                                    ],
                                                  ),
                                                  SizedBox(height: 20,),
                                                  Row( //TODO EDIT PROFILE POST
                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                    children: [
                                                      Expanded(
                                                        child: Padding(
                                                          padding: const EdgeInsets.symmetric(horizontal: 25),
                                                          child: TextButton(

                                                            style: ButtonStyle(
                                                                backgroundColor:job?.status == true ?
                                                                MaterialStatePropertyAll(Colors.indigo) :
                                                                MaterialStatePropertyAll(AppColor.fourthcolor),
                                                              shape: MaterialStatePropertyAll(RoundedRectangleBorder(
                                                                borderRadius: BorderRadius.circular(10)
                                                              ))
                                                            ),
                                                            onPressed: ()=>print('button'),
                                                            child: Text(    '${job?.status == true ? 'Available' : 'Not Available'}',
                                                                style: TextStyle(
                                                                  fontSize:AppFont.bigfont,
                                                                  fontWeight: FontWeight.w500,
                                                                  color: Colors.white,

                                                                )),),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                            GestureDetector(
                                              onTap: () {
                                                Navigator.push(context, MaterialPageRoute(builder: (context) =>

                                                    PfScreen(isViewPf: true,Pfid:job?.user?.sId),));
                                              },
                                              child: Center(

                                                child: CircleAvatar(
                                                  radius: 50,
                                                  backgroundImage:job?.user?.thumbnail?.attributes?.path == null ?
                                                  NetworkImage('https://i.pinimg.com/564x/48/6c/a0/486ca00640b169300b48e9ceacd8e401.jpg'):
                                                  NetworkImage('${ApiApiUrl.localurl}/${job?.user?.thumbnail?.attributes?.path}'),


                                                ),
                                              ),
                                            ),


                                          ],
                                        ),
                                      ), //TODO Detail Jobs
                                      Text('Job Description',
                                        style: TextStyle(
                                          fontSize:AppFont.bigfont+4,
                                          fontWeight: FontWeight.w400,
                                          color: Colors.white,

                                        ),
                                        maxLines: 6,
                                        overflow: TextOverflow.ellipsis,

                                      ),
                                      SizedBox(height: 20,),
                                      Text('${job?.jobDescription?.detail}',
                                        style: TextStyle(
                                          fontSize:AppFont.bigfont,
                                          height: 2,
                                          fontWeight: FontWeight.w400,
                                          color: Colors.grey,

                                        ),
                                        maxLines: 6,
                                        overflow: TextOverflow.ellipsis,

                                      ), //TODO jobs desc
                                      SizedBox(height: 30,),

                                      Container(
                                        padding: EdgeInsets.all(18),
                                        decoration: BoxDecoration(
                                            color: AppColor.bgcolor,
                                            borderRadius: BorderRadius.circular(20)
                                        ),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [

                                            Container(

                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: [
                                                  Text('Experience',
                                                    style: TextStyle(
                                                      color: Colors.white,
                                                      fontWeight: FontWeight.w400,

                                                    ),),
                                                  SizedBox(height: 8,),
                                                  Text('${job?.jobDescription?.experience} Level',
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: AppFont.bigfont+1,
                                                      fontWeight: FontWeight.w500
                                                    ),)
                                                ],
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.all(15),
                                              decoration: BoxDecoration(

                                                  border: Border(

                                                    right: BorderSide(
                                                      color: Colors.grey,


                                                    ),
                                                    left:  BorderSide(
                                                      color: Colors.grey,


                                                    ),
                                                  )
                                              ),
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: [
                                                  Text('Job Type',
                                                    style: TextStyle(
                                                      color: Colors.white,
                                                      fontWeight: FontWeight.w400,
                                                        fontSize: AppFont.medfont,

                                                    ),),
                                                  SizedBox(height: 8,),
                                                  Text('${job?.jobDescription?.jobtype}',
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: AppFont.bigfont+4,
                                                        fontWeight: FontWeight.w500
                                                    ),)
                                                ],
                                              ),
                                            ),
                                            Container(
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: [
                                                  Text('Salary Range',
                                                    style: TextStyle(
                                                      color: Colors.white,
                                                      fontWeight: FontWeight.w400,

                                                        fontSize: AppFont.medfont,

                                                    ),),
                                                  SizedBox(height: 8,),
                                                  Text('${job?.jobDescription?.minSalary}\$ - ${job?.jobDescription?.maxSalary}\$',
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: AppFont.bigfont+1,
                                                        fontWeight: FontWeight.bold
                                                    ),)
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ), //TODO Show Experience,Jobtype, Salary
                                      SizedBox(height: 54,),
                                      Text('Requirement',
                                        style: TextStyle(
                                          fontSize:AppFont.bigfont+4,
                                          fontWeight: FontWeight.w400,
                                          color: Colors.white,

                                        ),
                                        maxLines: 6,
                                        overflow: TextOverflow.ellipsis,

                                      ),// TODO jobs Requirements
                                      SizedBox(height: 20,),
                                      BulletedList(
                                          style: TextStyle(
                                            fontSize:AppFont.bigfont,
                                            height: 2,


                                            color: Colors.grey,
                                            fontWeight: FontWeight.w400,

                                          ),
                                          bullet: Icon(
                                            Icons.check,
                                            size: 20,
                                            color: Colors.green,
                                          ),

                                          listItems:List.generate(job!.requirement!.length,(index) {
                                            return Text('${job.requirement?[index]}',style: TextStyle(
                                              color: Colors.grey,
                                              fontWeight: FontWeight.w400,
                                            ),);
                                          },)
                                      ),
                                      SizedBox(height: 35,),
                                      Text('Map Locations',
                                        style: TextStyle(
                                          fontSize:AppFont.bigfont+4,
                                          fontWeight: FontWeight.w400,
                                          color: Colors.white,

                                        ),
                                        maxLines: 6,
                                        overflow: TextOverflow.ellipsis,

                                      ),
                                      // Text('${job?.address?.longitude}',
                                      //   style: TextStyle(
                                      //     color: Colors.grey,
                                      //     fontWeight: FontWeight.w400,
                                      //     fontSize:AppFont.bigfont,
                                      //     height: 2,
                                      //
                                      //
                                      //
                                      //   ),
                                      //   maxLines: 6,
                                      //   overflow: TextOverflow.ellipsis,
                                      //
                                      // ), //
                                      // Text('${job?.address?.latitude}',
                                      //   style: TextStyle(
                                      //     color: Colors.grey,
                                      //     fontWeight: FontWeight.w400,
                                      //     fontSize:AppFont.bigfont,
                                      //     height: 2,
                                      //
                                      //
                                      //
                                      //   ),
                                      //   maxLines: 6,
                                      //   overflow: TextOverflow.ellipsis,
                                      //
                                      // ), //
                                      SizedBox(height: 25,),
                                   Container(

                                     width: double.maxFinite,
                                     height: 450,
                                     child:
                                     job?.address?.latitude!= null ?
                                     Stack(
                                       children: [
                                         Positioned(
                                           child: GoogleMap(

                                             onMapCreated: OnMapCreate,


                                             initialCameraPosition: CameraPosition(
                                               target:LatLng(job!.address!.latitude!, job!.address!.longitude!),
                                               // target:  LatLng(lat,lon),

                                               zoom: 14.0,



                                             ),

                                             onCameraMove: (CameraPosition newPosition) {
                                               // print(newPosition.target.toJson());
                                               //TODO Return new location value
                                               widget.value = newPosition.target;

                                               setState(() {
                                                 newlocation = widget.value;
                                               });
                                               print(newlocation);
                                             },
                                             mapType: MapType.normal,
                                             myLocationButtonEnabled: true,
                                             myLocationEnabled: true,
                                             zoomGesturesEnabled: true,
                                             padding: const EdgeInsets.all(0),
                                             buildingsEnabled: true,
                                             cameraTargetBounds: CameraTargetBounds.unbounded,
                                             compassEnabled: true,
                                             indoorViewEnabled: false,
                                             mapToolbarEnabled: true,
                                             minMaxZoomPreference: MinMaxZoomPreference.unbounded,
                                             rotateGesturesEnabled: true,
                                             scrollGesturesEnabled: true,
                                             tiltGesturesEnabled: true,
                                             trafficEnabled: false,

                                             markers: {
                                               Marker(
                                                 markerId: const MarkerId("marker1"),
                                                 position: LatLng(job!.address!.latitude!, job!.address!.longitude!),
                                                 draggable: true,
                                                 onDragEnd: (value) {
                                                   // value is the new position
                                                 },
                                                 // To do: custom marker icon
                                               ),
                                               // Marker(
                                               //   markerId: const MarkerId("user location"),
                                               //   position: LatLng(_currentPosition!.latitude!,_currentPosition!.latitude!),
                                               //   draggable: true,
                                               //
                                               //   onDragEnd: (value) {
                                               //     // value is the new position
                                               //   },
                                               //   // To do: custom marker icon
                                               // ),




                                             },

                                           ),
                                         ),
                                         Positioned(

                                           child: Container(

                                             padding: EdgeInsets.all(10),

                                             width: double.maxFinite,
                                             child: Padding(
                                               padding: const EdgeInsets.symmetric(horizontal: 22,vertical: 0),
                                               child: ElevatedButton(onPressed:()  async{
                                                 var latitute = job.address?.latitude;
                                                 var longtitute = job.address?.longitude;

                                                 // print("true ${ latitute}");

                                                 // final url = Uri.parse("https://www.google.com/maps/search/?api=1&query=$latitute,$longtitute");
                                                 String? url ='https://www.google.com/maps/dir/?api=1&destination=$latitute,$longtitute';;
                                                 if (await canLaunchUrl(Uri.parse(url))) {
                                                   await launchUrl(Uri.parse(url));
                                                 } else {
                                                   throw 'Could not launch $url';
                                                 }
                                                 // if (await canLaunchUrl(url)) {
                                                 //   await launchUrl(url,
                                                 //       mode: LaunchMode.externalApplication,
                                                 //       webViewConfiguration: WebViewConfiguration(
                                                 //         enableJavaScript: true,
                                                 //
                                                 //       )
                                                 //
                                                 //   );
                                                 // }

                                               },
                                                   style: ButtonStyle(

                                                       backgroundColor: MaterialStatePropertyAll(AppColor.primarycolor),
                                                       shape: MaterialStatePropertyAll(RoundedRectangleBorder(
                                                           borderRadius: BorderRadius.circular(6)
                                                       ))
                                                   ),child:Text('Open in google map',style: TextStyle(
                                                     color: Colors.white
                                                   ),)
                                               ),
                                             ),
                                           ),
                                         ),
                                       ],

                                     )
                                 :
                                     CircularProgressIndicator(),
                                   ), //TODO GOOGLE MAP HERE

                                      SizedBox(height: 35,),

                                      Row(
                                  children: [
                                    Expanded(
                                      child:
                                      ElevatedButton(
                                        onPressed: () {
                                          // setState(() {
                                          //
                                          // });
                                          showModalBottomSheet(


                                            elevation: 0,
                                            enableDrag:true ,



                                            isScrollControlled: true  ,

                                            context: context,
                                            shape: RoundedRectangleBorder(

                                                borderRadius:BorderRadius.only(
                                                    bottomLeft: Radius.circular(0),
                                                    topLeft: Radius.circular(36),
                                                    topRight: Radius.circular(36)
                                                )

                                            ),

                                            backgroundColor: AppColor.primarycolor,

                                            builder:(BuildContext context) {




                                              return
                                                Container(
                                                height: 630,
                                                child:Padding(
                                                  padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 10),
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: [
                                                      SizedBox(height: 10,),

                                                      Center(
                                                        child: Text('Apply',

                                                          style: TextStyle(
                                                              fontSize: AppFont.bigfont+4,
                                                              color: Colors.white
                                                          ),
                                                        ),
                                                      ),
                                                      Center(
                                                        child: Container(
                                                          margin: EdgeInsets.symmetric(vertical: 15),
                                                          width: 100,
                                                          height: 1,
                                                          decoration: BoxDecoration(
                                                              color: Colors.white
                                                          ),
                                                        ),
                                                      ),
                                                      SizedBox(height: 30,),
                                                      // SizedBox(height: 50,),
                                                      Text('Additional Message',

                                                        style: TextStyle(
                                                            fontSize: AppFont.bigfont,
                                                            color: Colors.grey
                                                        ),
                                                      ),

                                                      SizedBox(height: 20,),
                                                      TextField(
                                                        controller:txtsmgController,
                                                        onSubmitted: (value) {


                                                          setState(() {
                                                            txtsmgController.text = value;
                                                          });

                                                        },

                                                        style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: AppFont.smfont+3,
                                                        ),
                                                        textAlign: TextAlign.center,

                                                        decoration: InputDecoration(
                                                            hintText: 'Send Us a Message',
                                                            contentPadding: EdgeInsets.all(1),
                                                            enabledBorder: OutlineInputBorder(
                                                                borderRadius: BorderRadius.circular(10),
                                                                borderSide:  BorderSide(color: AppColor.primarycolor)
                                                            ),
                                                            focusedBorder: OutlineInputBorder(
                                                                borderRadius: BorderRadius.circular(10),
                                                                borderSide:  BorderSide(color: Colors.white)
                                                            ),

                                                            fillColor: AppColor.bgcolor,
                                                            filled: true ,

                                                            hintStyle: TextStyle(
                                                                color: Colors.white,
                                                                fontSize: AppFont.bigfont-3

                                                            )
                                                        ),
                                                      ),
                                                      SizedBox(height: 30,),
                                                      Text('Contact Info',

                                                        style: TextStyle(
                                                            fontSize: AppFont.bigfont,
                                                            color: Colors.grey
                                                        ),
                                                      ),
                                                      SizedBox(height: 30,),

                                                      Row(
                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                        children: [
                                                          Expanded(

                                                            child: GestureDetector(

                                                              child: Column(
                                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                                children: [
                                                                  Icon(Icons.phone,color: AppColor.bgcolor),
                                                                  SizedBox(height: 10,),
                                                                  Text('+855${job?.contact?.telephone} ',

                                                                    style: TextStyle(
                                                                        fontSize: AppFont.bigfont-3,
                                                                        color: Colors.grey
                                                                    ),
                                                                    maxLines: 1,
                                                                    overflow: TextOverflow.ellipsis,
                                                                  ),
                                                                ],
                                                              ),
                                                              onTap: () async {
                                                                final call = Uri.parse('tel:+855${job?.contact?.telephone}');
                                                                if (await canLaunchUrl(call)) {
                                                                  launchUrl(call);
                                                                } else {
                                                                  throw 'Could not launch $call';
                                                                }


                                                              },
                                                            ),
                                                            flex: 1,
                                                          ),

                                                          Expanded(
                                                            child: GestureDetector(

                                                              onTap: () async {
                                                                final String? email = job?.contact?.email;
                                                                final String? subject = "${job?.title}";

                                                                final url ="mailto:${email}?subject=${subject}&body=";

                                                                if (!await launchUrlString(url)) {
                                                                  throw Exception('Could not launch $url');
                                                                }
                                                                else{
                                                                  await launchUrlString(url);
                                                                }
                                                              },

                                                              child: Column(
                                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                                children: [
                                                                  GestureDetector(
                                                                      onTap: () async {
                                                                        final String? email = job?.contact?.email;
                                                                        final String? subject = "${job?.title}";

                                                                        final url ="mailto:${email}?subject=${subject}&body=";

                                                                        if (!await launchUrlString(url)) {
                                                                          throw Exception('Could not launch $url');
                                                                        }
                                                                        else{
                                                                          await launchUrlString(url);
                                                                        }
                                                                      },
                                                                      child: Icon(Icons.email_sharp,color: AppColor.fourthcolor)),
                                                                  SizedBox(height: 10,),
                                                                  GestureDetector(
                                                                    onTap: () async {
                                                                      final String? email = job?.contact?.email;
                                                                      final String? subject = "${job?.title}";

                                                                      final url ="mailto:${email}?subject=${subject}&body=";

                                                                      if (!await launchUrlString(url)) {
                                                                      throw Exception('Could not launch $url');
                                                                      }
                                                                      else{
                                                                      await launchUrlString(url);
                                                                      }
                                                                    },
                                                                    child: Text('${job?.contact?.email}',

                                                                      style: TextStyle(
                                                                          fontSize: AppFont.bigfont-3,
                                                                          color: Colors.grey
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                            flex: 2,
                                                          ),

                                                          Expanded(
                                                            child: GestureDetector(
                                                              onTap: () async {
                                                                final web = Uri.parse(
                                                                  '${job.contact?.telegram}',
                                                                );
                                                                if (await canLaunchUrl( web)) {
                                                                  await launchUrl(web,
                                                                      mode: LaunchMode.externalApplication,
                                                                      webViewConfiguration: WebViewConfiguration(
                                                                        enableJavaScript: true,

                                                                      )

                                                                  );
                                                                }
                                                              else {
                                                                  throw 'Could not launch $web';
                                                                }


                                                              },
                                                              child: Column(
                                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                                children: [
                                                                  Icon(Icons.telegram,color: AppColor.bgcolor,size: 30,),
                                                                  SizedBox(height: 10,),
                                                                  Text('${job?.contact?.telegram}',

                                                                    style: TextStyle(
                                                                        fontSize:AppFont.bigfont-3,
                                                                        color: Colors.grey
                                                                    ),
                                                                    maxLines: 1,
                                                                    overflow: TextOverflow.ellipsis,
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                            flex: 1,
                                                          )
                                                        ],
                                                      ),

                                                      SizedBox(height: 30,),
                                                      Text('Upload your CV or Document in pdf / doc',

                                                        style: TextStyle(
                                                            fontSize: AppFont.bigfont,
                                                            color: Colors.grey
                                                        ),
                                                      ),

                                                      SizedBox(height: 30,),


                                                      Container(


                                                        width: double.maxFinite,
                                                        decoration: BoxDecoration(
                                                          borderRadius: BorderRadius.circular(25)
                                                        ),
                                                        child: Row(
                                                          children: [
                                                            Expanded(
                                                              child: ElevatedButton(onPressed:() async {


                                                                print("working");

                                                                FilePickerResult? result = await FilePicker.platform.pickFiles();

                                                                if (result == null) { //if they dontsubmitfile

                                                                  return null;
                                                                }


                                                                else {
                                                                  final file = result.files.first;


                                                                  print(file.path);
                                                                  print(file.extension);

                                                                  print(file.name);
                                                                  if(file.extension.toString() == 'pdf' || file.extension.toString()== 'docx'){
                                                                    setState(() {

                                                                      filepath = file.path;
                                                                      print(filepath);

                                                                      var filesdrop = file.path.toString().split('/file_picker');
                                                                      dropcvname = filesdrop![1].substring(1);

                                                                      print(filepath);
                                                                      Navigator.pop(context);
                                                                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Your Files has uploaded')));

                                                                      // Navigator.restorablePush(context, (context, arguments) => )

                                                                    });

                                                                  }
                                                                  else{
                                                                    return   showDialog(context: context, builder: (context) {
                                                                      return AlertDialog(

                                                                        elevation: 0,

                                                                        title: Column(
                                                                          children: [
                                                                            Icon(Icons.warning,size: 60,color: AppColor.primarycolor,),
                                                                            SizedBox(height: 10,),
                                                                            Text('Invalid Files Type',style:
                                                                            TextStyle(
                                                                                color:AppColor.primarycolor,
                                                                                fontSize: 18
                                                                            ),),
                                                                            SizedBox(height: 10,),
                                                                            Text('Please submit your form in pdf or docx format',style:
                                                                            TextStyle(
                                                                                color: AppColor.primarycolor,
                                                                                fontSize: 14,
                                                                                fontWeight: FontWeight.w400
                                                                            ),),

                                                                            SizedBox(height: 10,),

                                                                            Row(
                                                                              children: [
                                                                                Expanded(
                                                                                  child: ElevatedButton(onPressed: () => Navigator.pop(context),
                                                                                    child: Text('Try again'),
                                                                                    style: ButtonStyle(
                                                                                        backgroundColor: MaterialStatePropertyAll(AppColor.primarycolor)
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ],
                                                                            )
                                                                          ],
                                                                        ),

                                                                      );
                                                                    },);
                                                                  }


                                                                }


                                                                // ScaffoldMessenger.of(context).showSnackBar(
                                                                //     SnackBar(content:   Text('Your CV has been submitted'))
                                                                // );






                                                              },
                                                                child: Text('${dropcvname}',
                                                                  style: TextStyle(color: Colors.white,
                                                                      fontWeight: FontWeight.w400,
                                                                      fontSize: AppFont.bigfont-3),




                                                                ),

                                                                style: ButtonStyle(
                                                                    backgroundColor: MaterialStatePropertyAll(AppColor.bgcolor),
                                                       shape: MaterialStatePropertyAll(RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)))
                                                                ),

                                                              ),
                                                            ),

                                                          ],
                                                        ),
                                                      ),
                                                      SizedBox(height: 30,),
                                                      //TODO APPLYY JOB HERE
                                                      Container(
                                                        child: job?.status == true ?


                                                        Row(
                                                          children: [
                                                            Expanded(
                                                              child: ElevatedButton(onPressed: () async {
                                                                print("btn press!!!");
                                                                print(filepath);
                                                                print(txtsmgController.text);
                                                                print(job?.user?.sId);
                                                                print( job?.sId);

                                                                //TODO POST MSG HERE COME
                                                                print(uid);
                                                                if(filepath == null || txtsmgController.text == ''){
                                                                  print("No info input");
                                                                  return  showDialog(context: context, builder: (context) {
                                                                    return AlertDialog(
                                                                      shape: RoundedRectangleBorder(
                                                                          borderRadius: BorderRadius.circular(11)
                                                                      ),
                                                                      backgroundColor:AppColor.primarycolor ,

                                                                      elevation: 0,

                                                                      title: Column(
                                                                        children: [
                                                                          Icon(Icons.warning,size: 60,color: AppColor.fourthcolor,),
                                                                          SizedBox(height: 10,),
                                                                          Text('Missing',style:
                                                                          TextStyle(
                                                                              color:AppColor.fourthcolor,
                                                                              fontSize: 18,
                                                                              fontWeight: FontWeight.w500
                                                                          ),),
                                                                          SizedBox(height: 10,),
                                                                          Text('One or two more missing informations in your dialog , please fill everything',style:
                                                                          TextStyle(
                                                                            color:AppColor.fourthcolor,
                                                                            fontSize: 12,

                                                                            fontWeight: FontWeight.w400,

                                                                          ),
                                                                            textAlign: TextAlign.center,

                                                                          ),

                                                                          SizedBox(height: 10,),

                                                                          Row(
                                                                            children: [
                                                                              Expanded(
                                                                                child: ElevatedButton(onPressed: () => Navigator.pop(context),
                                                                                  child: Text('Try again'),
                                                                                  style: ButtonStyle(
                                                                                      backgroundColor: MaterialStatePropertyAll(AppColor.fourthcolor  )
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                            ],
                                                                          )
                                                                        ],
                                                                      ),

                                                                    );
                                                                  },);

                                                                }
                                                                setState(() {
                                                                  isloading == true;
                                                                });

                                                                await msgviewmodel.postMessageAPI(txtsmgController.text,

                                                                   uid  ,

                                                                    job?.sId, filepath, 'none');
                                                                Navigator.push(context, MaterialPageRoute(builder: (context) => ScreenDefault()   ,));



                                                              },
                                                                child:
                                                                    isloading !=true?


                                                                Text('Apply Now',style: TextStyle(color:Colors.white,fontWeight: FontWeight.w400,
                                                                fontSize: AppFont.smfont+1

                                                                ),) :
                                                                    Center(child: Container(
                                                                      height: 30,
                                                                      width: 30,
                                                                      child: CircularProgressIndicator(

                                                                        color: AppColor.primarycolor,
                                                                        backgroundColor: Colors.indigo,
                                                                      ),
                                                                    ),)
                                                     ,
                                                                style: ButtonStyle(
                                                                    backgroundColor: MaterialStatePropertyAll( AppColor.fourthcolor),
                                                                  shape: MaterialStatePropertyAll(RoundedRectangleBorder(borderRadius: BorderRadius.circular(14)))
                                                                ),

                                                              ),
                                                            ),
                                                            // SizedBox(width: 25,),
                                                            // ElevatedButton(onPressed: ()=>Navigator.pop(context),
                                                            //   child: Text('Cancel',style: TextStyle(color: Colors.white),),
                                                            //   style: ButtonStyle(
                                                            //       backgroundColor: MaterialStatePropertyAll(AppColor.fourthcolor)
                                                            //   ),
                                                            //
                                                            // ),
                                                          ],
                                                        ) :
                                                        null,
                                                      )


                                                    ],
                                                  ),
                                                ),
                                              );
                                            },



                                          );
                                        },



                                        child: Text('More Detail',
                                          style: TextStyle(
                                              color: Colors.white
                                          ),
                                        ),
                                        style: ButtonStyle(
                                          backgroundColor:MaterialStatePropertyAll(AppColor.bgcolor),
                                          elevation: MaterialStatePropertyAll(0),
                                          shape: MaterialStatePropertyAll(RoundedRectangleBorder(borderRadius:BorderRadius.circular(14) ))

                                        ),


                                      ),
                                    ),
                                  ],
                                )
                                    ],
                                  )
                              ),
                            ),

                          );
                        },

                      );
                    },

                  );
                case Status.LOADING:
                  return Center(
                    child: CircularProgressIndicator(
                      backgroundColor:Colors.indigo,
                      color: AppColor.primarycolor,



                    ),
                  );

                case Status.ERROR:
                  return Center(
                    child: CircularProgressIndicator(
                      backgroundColor: AppColor.primarycolor,
                    ),
                  );

                default:
                  return Center(
                    child: CircularProgressIndicator(
                      backgroundColor: AppColor.primarycolor,
                    ),
                  );
              }

            },

          );
        },

      ),
    );
  }

  void OnMapCreate(GoogleMapController controller) {
    setState(() {
        mapController = controller;
    });
  }

  void getuserfetch() async {

    SharedPreferences prefs = await     SharedPreferences.getInstance();
    String? id = prefs.getString('userid');
    uid = id;

  }
}

class SomeWidget extends StatelessWidget {
  const SomeWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Placeholder();
  }
}


