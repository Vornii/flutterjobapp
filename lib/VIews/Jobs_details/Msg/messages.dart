import 'package:bulleted_list/bulleted_list.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:jobapp/ViewModel/job_view_model.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:timeago/timeago.dart' as timeago;


import '../../../../res/Appcolor.dart';
import '../../../../res/Appfont.dart';
import '../../../data/Response/status_response.dart';
import '../../../res/BaseUrl.dart';
import 'msg_detail.dart';

class  InboxScr extends StatefulWidget {
  var open;
  InboxScr({Key? key, this.open}) : super(key: key);

  @override
  State<InboxScr> createState() => _InboxScrState();
}

class _InboxScrState extends State<InboxScr> {
  JobViewModel msgviewmodel = JobViewModel();
  var totallen = 0;
  var customicon  =Icon(Icons.search);
  var iconbar =false;
  var joblen =0;
var stop = false;
  var messagelen = 0;
  var NoNewMsg = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setupfetch();
  }
  Widget build(BuildContext context) {
    var open = widget.open;
    return Scaffold(

      backgroundColor: AppColor.thirdcolor,
      body: ChangeNotifierProvider<JobViewModel>(
        create:(context) =>  msgviewmodel,
        builder: (context, child) {
          return  Consumer<JobViewModel>(
            builder: (context, msg, child) {
              var status = msgviewmodel.messageResponse.status;
              print('${msg.messageResponse?.data?.data?.job?.length}' );
              var fulllength = msg.messageResponse?.data?.data?.job?.length;


              print("Status API MEssage ${status}");
              print(fulllength);

              switch(status){


                case Status.COMPLETED:
                  if(status == Status.COMPLETED){


                      WidgetsBinding.instance.addPostFrameCallback((timeStamp) {



                          // setState(() {
                          //   joblen = fulllength ?? 0;
                          // });
                        if(stop == false){
                          setState(() {
                            joblen = fulllength ?? 0;
                          });
                        }
                        else{

                        }










                        joblen = fulllength ?? 0;

                      });



                  }


                  return


                    CustomScrollView(
                    slivers: [

                       SliverAppBar(

                              expandedHeight: 180,
                              elevation: 0,


                              automaticallyImplyLeading:false,

                              pinned: true,
                              titleSpacing: 0,








                              flexibleSpace:  FlexibleSpaceBar(
                                collapseMode: CollapseMode.parallax,
                            titlePadding: EdgeInsets.symmetric(vertical: 15,horizontal: 40),


                                title: Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [

                                    Row(
                                      children: [
                                        Image.network(
                                            'https://static.vecteezy.com/system/resources/previews/008/509/301/original/3d-mail-email-message-icon-illustration-png.png',
                                        fit: BoxFit.contain,
                                          height: 30,
                                          width: 30,
                                        ),
                                        Expanded(
                                          child: Text('Inbox',style: TextStyle(
                                              fontSize: AppFont.megafont-4,

                                              fontWeight: FontWeight.w500
                                          ),),
                                        ),

                                      ],
                                    ),
                                    SizedBox(height: 2,),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,

                                      children: [
                                        Expanded(
                                          child: Text('${totallen} new messages',style: TextStyle(
                                            fontSize: AppFont.smfont-2,
                                            fontWeight: FontWeight.w400
                                          ),),
                                        ),

                                        Expanded(
                                          child: Text('${joblen} announcement',style: TextStyle(
                                              fontSize: AppFont.smfont-2,
                                              fontWeight: FontWeight.w400
                                          ),),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),






                              ),
                              // title: Text('Inbox'),

                              backgroundColor: AppColor.primarycolor,
                              
                            ),

                      SliverList(

                      delegate: SliverChildBuilderDelegate(


                     childCount: msg.messageResponse?.data?.data?.job?.length ,

                            (context, index) {
                              var message = msg.messageResponse?.data?.data?.job?[index];
                              var msglength = message?.messages?.length;
                              var msgs = message?.messages;
                              totallen = msglength!;
                              // if( totallen > msglength ){
                              //
                              //  stop = true;
                              //  totallen = totallen;
                              //  print("check true ${stop}");
                              // }
                              if(totallen > 0){
                                NoNewMsg = true;
                              }
                              print('total msg length ${msglength}');
                              print('total len :${totallen}');


                        return

                        Container(

                          padding: EdgeInsets.symmetric(vertical: 0),


                          child:fulllength == 0 ? Text('no message') :
                          Column(
                            children: [


                              GestureDetector(

                                child:


                                Card(



                                  margin: EdgeInsets.symmetric(vertical: 0,horizontal: 0),
                                  color: AppColor.thirdcolor,
                                  elevation: 0,




                                  // elevation: 2,
                                  child:

                                  Column(
                                    children: List.generate( msglength??0 , (index) {

                                      return msglength == 0 ?
                                          Text('np meessage') :


                                      GestureDetector(
                                        onTap: () {
                                          Navigator.push(context, MaterialPageRoute(builder:
                                              (context) => MsgDetailScr(
                                            mid: msgs?[index].sId,
                                          )



                                          ));
                                          setState(() {
                                            msgviewmodel.dispose();
                                            stop = true;
                                          });

                                        },
                                        child: Column(

                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [

                                            ListTile(

                                              tileColor: AppColor.thirdcolor,


                                              leading: CircleAvatar(
                                                radius: 25,
                                                backgroundImage: msgs?[index].user?.thumbnail?.attributes?.path ==null  ?
                                                NetworkImage(

                                                    'https://i.pinimg.com/564x/48/6c/a0/486ca00640b169300b48e9ceacd8e401.jpg') :
                                                NetworkImage(

                                                    '${ApiApiUrl.localurl}/${msgs?[index].user?.thumbnail?.attributes?.path }'),
                                              ),
                                              contentPadding: EdgeInsets.all(5),


                                              title: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [

                                                  Text('${msgs?[index].user?.name}',style: TextStyle(
                                                    fontWeight: FontWeight.w500,
                                                    color: AppColor.primarycolor,
                                                  ),),

                                                  Text('${timeago.format(DateTime.parse('${msgs?[index].updatedAt}'))}',style: TextStyle(
                                                      fontWeight: FontWeight.w400,
                                                      color: AppColor.primarycolor,
                                                      fontSize: AppFont.medfont-2
                                                  ),),

                                                ],
                                              ),
                                              subtitle: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  SizedBox(height: 5,),

                                                  Text('${msgs?[index].description}',style: TextStyle(
                                                      fontWeight: FontWeight.w300,
                                                      color: AppColor.primarycolor
                                                  ),),

                                                ],
                                              ),



                                            ),
                                            Divider(),

                                            SizedBox(height: 20,),
                                          ],
                                        ),
                                      );
                                    }),
                                  ),
                                ),
                              ),

                            ],
                          ),
                        ) ;






                      },



                      )


                  ),

                      // SliverToBoxAdapter(
                      //   child:
                      //
                      //   NoNewMsg  == false ?
                      //   Container(
                      //     margin: EdgeInsets.symmetric(vertical: 100),
                      //     child: Center(child: Column(
                      //       children: [
                      //
                      //        Image.network('https://cdn3d.iconscout.com/3d/premium/thumb/mail-download-6985959-5691446.png',
                      //
                      //
                      //             fit: BoxFit.cover,
                      //             width: 200,
                      //             height: 200
                      //
                      //         ),
                      //         Text('No new messages ' ,style: TextStyle(
                      //             color: AppColor.primarycolor,
                      //             fontSize: AppFont.bigfont,
                      //             fontWeight: FontWeight.w600
                      //
                      //         ),),
                      //         SizedBox(height: 5,),
                      //
                      //         Text('You have no mail in your job announcement' ,style: TextStyle(
                      //             color: AppColor.primarycolor,
                      //             fontSize: AppFont.smfont+1,
                      //             fontWeight: FontWeight.w400
                      //
                      //         ),),
                      //       ],
                      //     ),),
                      //   )
                      //        :
                      //
                      //
                      //   null,
                      // )



                    ],

                  );

                case Status.LOADING:
                  return Center(
                    child: CircularProgressIndicator(
                      backgroundColor:Colors.indigo,
                      color: AppColor.primarycolor,



                    ),
                  );
                default:
                  return Center(
                    child: CircularProgressIndicator(

                    ),
                  );

              }

            },

          );
        },

      ),
    );
  }

  void setupfetch() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String? id = prefs.getString('userid');
    print("user id ${id}");
    msgviewmodel.fetchMsg(id);

  }
}
