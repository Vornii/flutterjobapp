

import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:bulleted_list/bulleted_list.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:jobapp/ViewModel/post_view_model.dart';
import 'package:provider/provider.dart';
import 'package:dio/dio.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:url_launcher/url_launcher.dart';
import 'package:url_launcher/url_launcher_string.dart';
import '../../../../res/Appcolor.dart';
import '../../../../res/Appfont.dart';
import 'package:open_file/open_file.dart';
//OpenFile.open("/sdcard/example.txt", type: "text/plain", uti: "public.plain-text");
import '../../../data/Response/status_response.dart';
import '../../../res/BaseUrl.dart';
import '../job_detail.dart';
import '../pfscreen.dart';
class MsgDetailScr extends StatefulWidget {
  var mid;
 MsgDetailScr({Key? key , this.mid}) : super(key: key);

  @override
  State<MsgDetailScr> createState() => _MsgDetailScrState();
}

class _MsgDetailScrState extends State<MsgDetailScr> {
  PostViewModel msgidviewmodel = PostViewModel();
  var         isDownloading = false;
  var isopenfile = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("Get msg id: ");
    print(widget.mid);
    msgidviewmodel.fetchmsgbyid(widget.mid);
  }
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor:  AppColor.bgcolor,
        elevation: 0,
        iconTheme: IconThemeData(
            color: Colors.white,
        ),
        title: Text('Message',style: TextStyle(color: Colors.white),),



        centerTitle: true,

      ),
      backgroundColor:  AppColor.primarycolor,


      body: SafeArea(
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 0,horizontal: 0),
          child: ChangeNotifierProvider<PostViewModel>(
            create: (context) => msgidviewmodel,
            builder: (context, child) {
              return   Consumer<PostViewModel>(
                builder: (context, msg, child) {
                  var status = msg.GetIdMsgResponse.status;
                  var message = msg.GetIdMsgResponse.data?.data;
                  var pdflink =  message?.pdfLink?.split('\document');
                  print(message?.user?.sId);
                  print(pdflink?[1]);
                  print(status);
                  switch(status){
                    case Status.COMPLETED:
                      return Container(

                          width: double.maxFinite,
                          height: double.maxFinite,
                          padding: EdgeInsets.all(13),

                          child: SingleChildScrollView(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [


                                ListTile(
                                  leading: GestureDetector(
                                    onTap: () {
                                      Navigator.push(context, MaterialPageRoute(builder: (context) =>

                                          PfScreen(isViewPf: true,Pfid:message?.user?.sId),));

                                    },
                                    child: CircleAvatar(
                                      radius: 25,
                                      backgroundImage: message?.user?.thumbnail?.attributes?.path == null ?

                                      NetworkImage(

                                          'https://i.pinimg.com/564x/48/6c/a0/486ca00640b169300b48e9ceacd8e401.jpg') :
                                      NetworkImage(

                                          '${ApiApiUrl.localurl}/${message?.user?.thumbnail?.attributes?.path}'),
                                    ),
                                  ),
                                  contentPadding: EdgeInsets.all(5),

                                  title: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text('${message?.user?.name}',style: TextStyle(
                                          fontWeight: FontWeight.w400,
                                        color: Colors.white,
                                      ),),
                                      Text('${timeago.format(DateTime.parse('${message?.publishDate}'))}',style: TextStyle(
                                          fontWeight: FontWeight.w400,
                                          color: Colors.white,
                                          fontSize: AppFont.medfont-2
                                      ),),
                                      GestureDetector(
                                          onTap: () async {
                                            final call = Uri.parse('tel:+855${message?.user?.profile?.telephone}');
                                            if (await canLaunchUrl(call)) {
                                              launchUrl(call);
                                            } else {
                                              throw 'Could not launch $call';
                                            }
                                          },
                                          child: Icon(Icons.phone,color: Colors.green,))
                                    ],
                                  ),
                                  subtitle: Text('to me',style: TextStyle(
                                    color: Colors.white.withOpacity(0.6),
                                  ),),
                                ),

                                Divider(
                                  color: Colors.white.withOpacity(0.15),

                                ),
                                SizedBox(height: 10,),
                                Text('${message?.job?.title}',
                                  textAlign: TextAlign.justify,
                                  style: TextStyle(
                                      fontSize: AppFont.megafont,
                                      color: Colors.white,

                                      fontWeight: FontWeight.w500
                                  ),
                                ),

                                SizedBox(height: 10,),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,

                                  children: [
                                    Text('Reply to your post',

                                      style: TextStyle(
                                          fontSize: AppFont.bigfont,
                                          color: Colors.white,
                                          fontWeight: FontWeight.w400,
                                          height: 2
                                      ),
                                    ),
                                    SizedBox(height: 10,),
                                    IconButton(onPressed: () {
                                      Navigator.push(context, MaterialPageRoute(builder: (context) {
                                        return JobDetailSr(uid: message?.job?.sId);
                                      },));

                                    }, icon: Icon(Icons.arrow_forward,size: 20,

                                      color: AppColor.bgcolor,
                                    ))
                                  ],
                                ),      SizedBox(height: 10,),
                                Text('${message?.description}',

                                  style: TextStyle(
                                      fontSize: AppFont.bigfont,
                                     color: Colors.white.withOpacity(0.54),
                                      fontWeight: FontWeight.w400,
                                      height: 2
                                  ),
                                ),
                                SizedBox(height: 20,),

                                SizedBox(height: 10,),

                                Text('Upload File',

                                  style: TextStyle(
                                      fontSize: AppFont.medfont+1,
                                      color: Colors.white.withOpacity(0.8),
                                      fontWeight: FontWeight.w500
                                  ),
                                ),

                                SizedBox(height: 20,),
                                Divider(
                                  color: Colors.white.withOpacity(0.25),

                                ),
                                SizedBox(height: 20,),


                                Card(
                                  color:Colors.white,
                                  elevation: 2,
                                  child: ListTile(
                                  leading: Icon(Icons.file_copy,color: AppColor.bgcolor,),
                                   title: Row(
                                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text("${pdflink![1].substring(1)}",
                                          style: TextStyle(
                                            fontSize: AppFont.medfont,
                                            fontWeight: FontWeight.bold,

                                          ),

                                        ),


                                        // Text("${pdflink![1].substring(1).split('.')?[1]}",
                                        //   style: TextStyle(
                                        //       fontSize: AppFont.medfont,
                                        //
                                        //   ),
                                        //
                                        // ),
                                      ],
                                    ),
                                    trailing:       isDownloading == false ?
                                    IconButton(
                                      onPressed: () async{
                                        setState(() {
                                          isDownloading = true;

                                        });

                                        await openfile(
                                            url: '${ApiApiUrl.localurl}/${message?.pdfLink}',
                                            fileName:'${message?.pdfLink}'
                                        );
                                        setState(() {
                                          isDownloading = false;
                                          isopenfile = true;
                                        });
                                      },
                                      tooltip: 'click here to download',
                                      icon: isopenfile ==true ? Icon(Icons.file_download_done_sharp,color: AppColor.primarycolor,) :
                                      Icon(Icons.download,color: AppColor.primarycolor,)
                                    )
                                    :CircularProgressIndicator(
                                      backgroundColor: AppColor.bgcolor,
                                  color:  AppColor.primarycolor,

                                    ),
                                    
                                  ),
                                )
                              ],
                            ),
                          )
                      );

                    case Status.LOADING:
                      return Center(
                        child: CircularProgressIndicator(

                        ),
                      );
                    default:
                      return Center(
                        child: CircularProgressIndicator(

                        ),
                      );
                  }

                },

              );
            },

          ),

        ),
      ),
    );
  }

  // Future openfile({required String url, required String fileName}) async {
  //
  //    final file = await downloadFile(url,fileName); //TODO FUNCTION TO DOWNLOAD FILES
  //    print('file download ${url}');
  //    print(file);
  //
  //    if(file==null){
  //      //TODOO if null we end
  //       return null;
  //       print("null");
  //    }
  //    else{
  //      print("open file noww ${url}");
  //      await requestManageExternalStoragePermission();
  //       OpenFile.open(url).then((value) => print(value.message)); //TODO aFTER DOWWNLOAD WE OPEN THE FILES
  //
  //      // final result =await FilePicker.platform.pickFiles();
  //
  //    }
  //
  //
  // }

  Future<void> openfile({required String url, required String fileName}) async {
    try {
      final file = await downloadFile(url, fileName);
      if (file == null) {
        print("File download failed.");
        return;
      }

      print("File downloaded to: ${file.path}");

      // Check if the file exists before attempting to open it
      if (await file.exists()) {
        print("File exists, opening...");
        await requestManageExternalStoragePermission();

        OpenFile.open(file.path).then((result) {
          if (result.type == ResultType.done) {
            print("File opened successfully: ${result.message}");
          } else {
            print("Failed to open file: ${result.message}");
          }
        }).catchError((error) {
          print("Error opening file: $error");
        });
      } else {
        print("File does not exist at the specified path: ${file.path}");
      }
    } catch (e) {
      print("Error downloading or opening file: $e");
      // Handle the error appropriately
    }
  }


  Future<void> requestManageExternalStoragePermission() async {
    // Check if permission is already granted
    var status = await Permission.manageExternalStorage.status;
    print('app permission ${status}');
    if (status.isGranted) {
      // Permission is already granted, proceed with file operations
      return;
    }
    if(status == 'PermissionStatus.denied') {
      print('request');
      status = await Permission.manageExternalStorage.request();
      return;
    }
    // If permission is denied, request it
    if (status.isDenied || status.isPermanentlyDenied  ) {
      status = await Permission.manageExternalStorage.request();
      if (status.isGranted) {
        // Permission granted, proceed with file operations
        return;
      }
    }

    // Permission is permanently denied, open app settings so the user can grant the permission manually
    if (status.isPermanentlyDenied) {
      openAppSettings();
    }
  }


}
Future<File> downloadFile(String url, String fileName) async {
  try {
    final appStorage = await getApplicationDocumentsDirectory();
    final file = File('${appStorage.path}/$fileName');

    // Ensure the directory exists
    await file.parent.create(recursive: true);

    final response = await Dio().get(url,
        options: Options(
          responseType: ResponseType.bytes,
          followRedirects: false,
          sendTimeout: null,
        ));

    await file.writeAsBytes(response.data);
    return file;
  } catch (e) {
    print(e);
    throw e; // Rethrow the exception to be handled elsewhere if needed
  }
}
