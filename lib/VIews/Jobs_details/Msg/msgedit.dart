import 'package:bulleted_list/bulleted_list.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';


import '../../../../res/Appcolor.dart';
import '../../../../res/Appfont.dart';




class Msgmodify extends StatelessWidget {
  const Msgmodify({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColor.primarycolor,
        elevation: 0,
        iconTheme: IconThemeData(
            color: Colors.white
        ),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.max,
          children: [
            Text('Message'),
            Text('4 messages',style: TextStyle(
                fontSize: AppFont.smfont+1
            ),),
          ],
        ),
        centerTitle: true,

      ),
      backgroundColor: AppColor.thirdcolor,
      body: Container(
        margin: EdgeInsets.symmetric(vertical: 5,horizontal: 0),
        child: Container(

            width: double.maxFinite,
            height: double.maxFinite,

            child: Column(
              children: [

                Expanded(
                  child: ListView.builder(
                    physics: ScrollPhysics(),
                    itemCount: 5,
                    itemBuilder: (context, index) {
                      return Card(
                        child: Container(
                          padding: EdgeInsets.all(5),

                          child: ListTile(
                            leading: Icon(Icons.telegram,size: 45,color: AppColor.primarycolor,),
                            contentPadding: EdgeInsets.all(5),

                            title: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text('I would Like to apply this CV....',style: TextStyle(
                                    fontWeight: FontWeight.w500
                                ),),
                                Text('3d ago',style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    fontSize: AppFont.medfont
                                ),),
                              ],
                            ),
                            subtitle: Text('coverletter.pdf'),
                            trailing: IconButton(
                              icon: Icon(Icons.edit),
                              onPressed: () => showDialog(context: context, builder: (context) {
                                return AlertDialog(
                                  title: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text('Message'),
                                      IconButton(onPressed: ()=>Navigator.pop(context), icon: Icon(Icons.close))

                                    ],
                                  ),


                                  elevation: 0,
                                  scrollable: false,
                                  content: Container(
                                    height:65,
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text('Edit'),
                                        SizedBox(height: 20,),
                                        Text('Remove'),
                                      ],
                                    ),
                                  ),

                                  actions: [

                                  ],
                                );
                              },),
                            )
                          ),
                        ),
                      );
                    },

                  ),
                ),
                SizedBox(height: 10,),
                ElevatedButton(
                    style: ButtonStyle(
                        elevation: MaterialStatePropertyAll(0),
                        backgroundColor: MaterialStatePropertyAll(AppColor.primarycolor)
                    ),

                    onPressed: ()=>null,
                    child: Text('Load More')
                ),
              ],
            )
        ),

      ),
    );
  }
}
