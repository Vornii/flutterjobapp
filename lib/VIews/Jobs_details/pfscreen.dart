import 'package:bulleted_list/bulleted_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:jobapp/VIews/Jobs_details/Editprofile.dart';
import 'package:jobapp/ViewModel/job_view_model.dart';
import 'package:jobapp/model/user_model.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:url_launcher/url_launcher_string.dart';


import '../../data/Response/status_response.dart';
import '../../res/Appcolor.dart';
import '../../res/Appfont.dart';
import '../../res/BaseUrl.dart';
import '../Admin/adminview.dart';
import '../AuthScreen/Resetpassword.dart';
import '../CRUD/editpostview.dart';
import '../Choose/Load1.dart';
import '../WelcomeScreen/home.dart';

class PfScreen extends StatefulWidget {
  var open;
  var userid ;
  var isViewPf;
  var Pfid;
  var isupdate;
 PfScreen({Key? key, this.open
   ,this.userid ,
   this.isViewPf,this.Pfid,

   this.isupdate}) : super(key: key);

  @override

  State<PfScreen> createState() => _PfScreenState();
}

class _PfScreenState extends State<PfScreen>  {
  var languages = ['English','Khmer','Vietnam'];
 var userid;
 var isloading = false;

 JobViewModel userviewmodel = JobViewModel();

  var sf = ['Web Developer','Network Engineer','Database Admin'];


  @override
  void initState() {
    // TODO: implement initState

    print("init");
    print("userid: ${widget.userid}");

    if( widget.isViewPf == true ){

        viewpfauth(widget.Pfid);


    }
    else{

        checkauth();

    }
    print("Sending");
print(userid);





  }


  Widget build(BuildContext context) {
    var open  = widget.open;


    double screenWidth = MediaQuery.of(context).size.width;
    print(screenWidth);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.indigo,
        elevation: 0,
        iconTheme: IconThemeData(
            color: Colors.white
        ),
        title: Text('Profile',style: TextStyle(
            color: Colors.white,
          fontWeight: FontWeight.w400
        )),
        actions: [
          Container(
            child:
      widget.isViewPf == true ?
          null :
            IconButton(
              icon: const Icon(Icons.more_vert_outlined,   color: Colors.white),
              tooltip: 'Logout',
            onPressed: () => showDialog(

            context: context,
            builder:(context) => AlertDialog(
              backgroundColor: Colors.indigo  ,
              elevation: 0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(14)
              ),

              title: Row(
                children: [
                  Icon(Icons.settings,color:     AppColor.thirdcolor,),
                  SizedBox(width: 10,),
                  Text('Setting',style: TextStyle(
                     color: AppColor.thirdcolor
                  ),),
                ],
              ),
              content: Container(
                height:       userviewmodel.userApiResponse?.data?.data?.role == 'Admin' ? 260
                  :200,

                child:
                userviewmodel.userApiResponse?.data?.data?.role == 'Admin' ?


                Column(
                  children: [

                    Divider(color:Colors.white,),

                    ListTile(
                      leading: Icon(Icons.key,     color:AppColor.thirdcolor,),

                      title: GestureDetector(

                          onTap: () {
                            logoutcheck();
                            Navigator.push(context, MaterialPageRoute(builder: (context) => MyResetScreen() ,));
                          },
                          child
                              : Text('Reset Your Password',style: TextStyle(
                              color:AppColor.thirdcolor,
                              fontWeight: FontWeight.w400
                          ),)),
                    ),
                    ListTile(
                      leading: Icon(Icons.supervisor_account_outlined,     color:AppColor.thirdcolor,),
                      title: GestureDetector(
                          onTap: () async {
                            print("Press Telephone");
                            final String telephone  = 'tel:+855 96 56 898 95';
                            if(await canLaunchUrl(Uri.parse(telephone!))){
                              launchUrl(Uri.parse(telephone!));
                            }
                            else{
                              throw Exception('Cannot be launch');
                            }

                          },
                          child
                              : Text('Contact Admin Support',style: TextStyle(
                              color:AppColor.thirdcolor,
                              fontWeight: FontWeight.w400
                          ),)),
                    ),
                    ListTile(
                      leading: Icon(Icons.admin_panel_settings_rounded,         color:AppColor.thirdcolor,),
                      title: GestureDetector(
                          onTap: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context) =>   AdminScreen(uid: userviewmodel.userApiResponse?.data?.id,tb : userviewmodel.userApiResponse?.data?.data?.thumbnail?.attributes?.path),));

                          },
                          child
                              : Text('Switch To Admin',style: TextStyle(
                              color:AppColor.thirdcolor,
                              fontWeight: FontWeight.w400
                          ),)),
                    ),
                    ListTile(
                      leading: Icon(Icons.logout_outlined,     color:Colors.redAccent,),
                      title: GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                            showDialog(context: context, builder: (context) {
                              return AlertDialog(
                                elevation: 0,





                                backgroundColor: Colors.indigo,

                                // scrollable: true,




                                title: Text('Log out ',style: TextStyle(
                                  fontSize: AppFont.bigfont+5,
                                    color: AppColor.fourthcolor,
                                  fontWeight: FontWeight.w500
                                ),),
                               content: Text('Are you sure you want to sign out?',style: TextStyle(
                                   fontSize: AppFont.bigfont,
                                   color: AppColor.thirdcolor
                               ),),
                               actions: [
                                 Padding(
                                   padding: const EdgeInsets.all(8.0),
                                   child: GestureDetector(


                                     child: Text('CANCEL',style: TextStyle(
                                         fontSize: AppFont.bigfont,
                                         color: AppColor.thirdcolor,

                                     ),



                                     ),
                                     onTap: () {
                                       Navigator.pop(context);
                                     },
                                   ),
                                 ),
                                 Padding(
                                   padding: const EdgeInsets.all(8.0),
                                   child: GestureDetector(

                                     child: Text('LOG OUT',style: TextStyle(
                                         fontSize: AppFont.bigfont,
                                         color: AppColor.fourthcolor
                                     ),



                                     ),
                                     onTap: () {
                                       logoutcheck();
                                       Navigator.push(context, MaterialPageRoute(builder: (context) => MyHomePage() ,));
                                     },
                                   ),
                                 )
                               ],


                              );
                            },
                            );

                          },
                          child
                              : Text('Log out of this account',style: TextStyle(
                              color:Colors.redAccent,
                              fontWeight: FontWeight.w500
                          ),)),
                    ),


                  ],
                ) :
                Column(
                  children: [

                    Divider(color:Colors.white,),

                    ListTile(
                      leading: Icon(Icons.key,     color:AppColor.thirdcolor,),

                      title: GestureDetector(

                          onTap: () {
                            logoutcheck();
                            Navigator.push(context, MaterialPageRoute(builder: (context) => MyResetScreen() ,));
                          },
                          child
                              : Text('Reset Your Password',style: TextStyle(
                              color:AppColor.thirdcolor,
                              fontWeight: FontWeight.w400
                          ),)),
                    ),
                    GestureDetector(
                      onTap: () async {
                        print("Press Telephone");
                        final String telephone  = 'tel:+855 96 56 898 95';
                        if(await canLaunchUrl(Uri.parse(telephone!))){
                          launchUrl(Uri.parse(telephone!));
                        }
                        else{
                          throw Exception('Cannot be launch');
                        }

                      },
                      child: ListTile(
                        leading: Icon(Icons.supervisor_account_outlined,     color:AppColor.thirdcolor,),
                        title: Text('Contact Admin Support',style: TextStyle(
                        color:AppColor.thirdcolor,
                        fontWeight: FontWeight.w400
                            ),),
                      ),
                    ),

                    ListTile(
                      leading: Icon(Icons.logout_outlined,     color:Colors.redAccent,),
                      title: GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                            showDialog(context: context, builder: (context) {
                              return AlertDialog(
                                elevation: 0,





                                backgroundColor: Colors.indigo,

                                // scrollable: true,




                                title: Text('Log out ',style: TextStyle(
                                    fontSize: AppFont.bigfont+5,
                                    color: AppColor.fourthcolor,
                                    fontWeight: FontWeight.w500
                                ),),
                                content: Text('Are you sure you want to sign out?',style: TextStyle(
                                    fontSize: AppFont.bigfont,
                                    color: AppColor.thirdcolor
                                ),),
                                actions: [
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: GestureDetector(


                                      child: Text('CANCEL',style: TextStyle(
                                        fontSize: AppFont.bigfont,
                                        color: AppColor.thirdcolor,

                                      ),



                                      ),
                                      onTap: () {
                                        Navigator.pop(context);
                                      },
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: GestureDetector(

                                      child: Text('LOG OUT',style: TextStyle(
                                          fontSize: AppFont.bigfont,
                                          color: AppColor.fourthcolor
                                      ),



                                      ),
                                      onTap: () {
                                        logoutcheck();
                                        Navigator.push(context, MaterialPageRoute(builder: (context) => MyHomePage() ,));
                                      },
                                    ),
                                  )
                                ],


                              );
                            },
                            );

                          },
                          child
                              : Text('Log out of this account',style: TextStyle(
                              color:Colors.redAccent,
                              fontWeight: FontWeight.w500
                          ),)),
                    ),


                  ],
                )
              ),

              // actions: [
              //   TextButton(
              //       onPressed:() {
              //            logoutcheck();
              //       Navigator.push(context, MaterialPageRoute(builder: (context) => MyHomePage() ,));
              //
              //
              //       },
              //       child: Text('Logout',style: TextStyle(
              //         color: AppColor.fourthcolor
              //       ),))
              // ],
            ),

      ),),
          ),

        ],
        centerTitle: true ,
        automaticallyImplyLeading:open == null ? true :
        widget.isupdate == true ?
            true:
        false , //set to false = no go back bar

      ),
      backgroundColor: AppColor.thirdcolor,
      body:



      ChangeNotifierProvider<JobViewModel>(
        create: (context) => userviewmodel,
        builder: (context, child) {
          return Consumer<JobViewModel>(


            builder: (context, users, child) {
              print("User Profile");

              var status = userviewmodel.userApiResponse.status;
            var user =  users.userApiResponse.data?.data;
            print("user skills ${user?.profile?.skill?.length}");
              // var userss =  users.userApiResponse.data?.data? ;
              print(user);
              print(user?.name);
              print(status);
              switch (status){
                case Status.LOADING:
                  return Center(
                    child: CircularProgressIndicator(
                      backgroundColor:Colors.indigo,
                      color: AppColor.primarycolor,



                    ),
                  );
                case Status.ERROR:
                  return Center(
                    child: CircularProgressIndicator(
                      backgroundColor: AppColor.primarycolor,

                    ),
                  );
                case Status.COMPLETED:
                  return Container(
                    height: double.maxFinite,
                    margin: EdgeInsets.symmetric(vertical: 30,horizontal: 20),
                    child: SingleChildScrollView(
                      physics: ScrollPhysics(),

                      child: Container(

                          width: double.maxFinite,



                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,

                            children: [

                              Container(
                                child: Stack(


                                  children: [

                                    Container(


                                      margin: EdgeInsets.symmetric(vertical: 50),
                                      width:370,

                                      height:widget.isViewPf == true ?
                                          290 :

                                      340,
                                      decoration: BoxDecoration(
                                          color:AppColor.primarycolor,
                                          borderRadius:  BorderRadiusDirectional.circular(15)
                                      ),
                                      padding: EdgeInsets.all(4),

                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.center,
                                        children: [
                                          SizedBox(height: 50,),
                                          Text('${user?.name}',
                                              style: TextStyle(
                                                fontSize:AppFont.bigfont +5,
                                                fontWeight: FontWeight.w400,
                                                color: Colors.white,

                                              )),
                                          SizedBox(height: 10,),

                                          Text(user?.position ?? 'No Position',
                                              style: TextStyle(
                                                fontSize:AppFont.bigfont+3,
                                                fontWeight: FontWeight.w400,
                                                color: Colors.grey,

                                              )),
                                          SizedBox(height: 10,),
                                          Text('Company: ${user?.profile?.currentCompany ?? 'No Detail'}',
                                              style: TextStyle(
                                                fontSize:AppFont.bigfont-2,
                                                fontWeight: FontWeight.w400,
                                                color: Colors.grey,


                                              )),
                                          SizedBox(height: 10,),
                                          Padding(
                                            padding: const EdgeInsets.symmetric(horizontal: 22),
                                            child: Divider(color: Colors.white.withOpacity(0.55),),
                                          ),

                                          SizedBox(height: 10,),

                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,



                                            children: [
                                              Expanded(

                                                child: GestureDetector(
                                                  onTap: () async {
                                                    final call = Uri.parse('tel:+855${user?.profile?.telephone}');
                                                    if (await canLaunchUrl(call)) {
                                                      if(user?.profile?.telephone =='' || user?.profile?.telephone ==null){
                                                        return null;
                                                      }
                                                      launchUrl(call);
                                                    } else {
                                                      throw 'Could not launch $call';
                                                    }


                                                  },
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                    children: [
                                                      Icon(Icons.phone,color: AppColor.bgcolor),
                                                      SizedBox(height: 10,),
                                                      Text('${user?.profile?.telephone  == null || user?.profile?.telephone == '' ? 'No Number' :'+855 ${user?.profile?.telephone}'}',

                                                        style: TextStyle(
                                                            fontSize: AppFont.bigfont-3,
                                                            color: Colors.grey
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                flex: 2,

                                                child: GestureDetector(
                                                  onTap: () async {
                                                    final String? email = user?.email;
                                                    // final String? subject = "${ }";

                                                    final url ="mailto:${email}?subject=<subject>&body=";

                                                    if (!await launchUrlString(url)) {
                                                      throw Exception('Could not launch $url');
                                                    }
                                                    else{

                                                      await launchUrlString(url);
                                                    }
                                                  },
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                    children: [
                                                      Icon(Icons.email_sharp,color: AppColor.fourthcolor),
                                                      SizedBox(height: 10,),
                                                      Text('${user?.email}',

                                                        style: TextStyle(
                                                            fontSize: AppFont.smfont+1,
                                                            color: Colors.grey
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                child: GestureDetector(
                                                  onTap: () async {

                                                    final web = Uri.parse(
                                                      '${user?.profile?.telegram  == '' || user?.profile?.telegram == null

                                                      ?'No info' : user?.profile?.telegram}',
                                                    );
                                                    final url = Uri.parse('${web}');
                                                    if (await canLaunchUrl(url)) {
                                                      if(user?.profile?.telegram  =='' ||user?.profile?.telegram ==null){
                                                        return null;
                                                      }
                                                      await launchUrl(url,
                                                      mode: LaunchMode.externalApplication,
                                                        webViewConfiguration: WebViewConfiguration(
                                                          enableJavaScript: true,

                                                        )

                                                      );
                                                    }


                                                  },
                                                  child: Column(
                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                    children: [
                                                      Icon(Icons.telegram,color: AppColor.bgcolor,size: 30,),
                                                      SizedBox(height: 10,),
                                                      Text('${user?.profile?.telegram == null ||  user?.profile?.telegram =='' ? 'No detail' :  user?.profile?.telegram}'   ,

                                                        style: TextStyle(
                                                            fontSize:AppFont.bigfont-3,
                                                            color: Colors.grey,
                                                          overflow: TextOverflow.ellipsis
                                                        ),

                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),






                                            ],
                                          ),
                                          SizedBox(height: 20,),

                                          Container(
                                            child: widget.isViewPf == true ?
                                                null :
                                            Row( //TODO EDIT PROFILE POST
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: [
                                                ElevatedButton(onPressed:() {
                                                  Navigator.push(context,MaterialPageRoute(builder: (context) {
                                                    return EditPfScreen(users:user , userid: userid ,);
                                                  },));


                                                },
                                                    style: ButtonStyle(
                                                        backgroundColor: MaterialStatePropertyAll(AppColor.bgcolor)
                                                    ),
                                                    child: Text('Edit Profile',style: TextStyle(
                                                      color: Colors.white
                                                    ),)
                                                ),

                                                SizedBox(width: 14,),

                                                Expanded(
                                                  child: GestureDetector(
                                                  
                                                    child:user?.role != 'Job Seeker' ?
                                                  
                                                    Row(
                                                      children: [
                                                        ElevatedButton(onPressed:() {
                                                  
                                                          Navigator.push(context,MaterialPageRoute(builder: (context) {
                                                  
                                                            return     UserPostScreen(message: false,);
                                                          },));
                                                        },
                                                            style: ButtonStyle(
                                                                backgroundColor: MaterialStatePropertyAll(AppColor.bgcolor)
                                                            ),
                                                            child: Text('Edit Post',style: TextStyle(
                                                              color: Colors.white
                                                            ),)
                                                        ),
                                                        SizedBox(width:10 ,),
                                                        Expanded(
                                                          child: ElevatedButton(onPressed:() {
                                                                                                            
                                                            Navigator.push(context,MaterialPageRoute(builder: (context) {
                                                              return
                                                                UserPostScreen(message:true );
                                                                                                            
                                                            },));
                                                                                                            
                                                                                                            
                                                                                                            
                                                          },
                                                              style: ButtonStyle(
                                                                  backgroundColor: MaterialStatePropertyAll(AppColor.bgcolor)
                                                              ),
                                                              child: Text('Message',style: TextStyle(
                                                                color: Colors.white
                                                              ),)
                                                          ),
                                                        ),
                                                      ],
                                                    ) :
                                                  
                                                    ElevatedButton(onPressed:() {
                                                  
                                                      Navigator.push(context,MaterialPageRoute(builder: (context) {
                                                        return
                                                          UserPostScreen(message:true );
                                                  
                                                      },));
                                                  
                                                  
                                                  
                                                                                  },
                                                                                              style: ButtonStyle(
                                                                                           backgroundColor: MaterialStatePropertyAll(AppColor.bgcolor)
                                                                                           ),
                                                                                        child: Text('Edit Message',style: TextStyle(
                                                                                          color: Colors.white
                                                                                        ),)
                                                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),


                                        ],
                                      ),
                                    ),

                                    Center(
                                      child: Positioned(
                                        top: 0,
                                        child: CircleAvatar(
                                          radius: 50,
                                          backgroundColor: Colors.grey.withOpacity(0.5),

                                          backgroundImage:user?.thumbnail?.attributes == null  ?

                                          NetworkImage('https://i.pinimg.com/564x/48/6c/a0/486ca00640b169300b48e9ceacd8e401.jpg'):

                                          NetworkImage('${ApiApiUrl.localurl}/${user?.thumbnail?.attributes?.path}'),
                                        ),
                                      ),
                                    ),


                                  ],
                                ),
                              ),
                              //TODO Detail Jobs

                              SizedBox(height: 20,),

                              Text('Biography',
                                style: TextStyle(
                                  fontSize:AppFont.bigfont+4,
                                  fontWeight: FontWeight.w500,
                                  color: AppColor.primarycolor,

                                ),
                                maxLines: 6,
                                overflow: TextOverflow.ellipsis,

                              ),

                              SizedBox(height: 20,),

                              Text(user?.profile?.bio == '' || user?.profile?.bio == null ? 'This users has no bio yet'
                                : '${user?.profile?.bio}'
                                ,
                                // Text('This user has no bio yet',
                                style: TextStyle(
                                  fontSize:AppFont.bigfont,
                                  height: 2,
                                  fontWeight: FontWeight.w400,
                                  color: AppColor.primarycolor.withOpacity(0.57),

                                ),
                                maxLines: 6,
                                overflow: TextOverflow.ellipsis,

                              ),

                              //TODO jobs desc

                              SizedBox(height: 30,),

                              Text('Skills and References',
                                style: TextStyle(
                                  fontSize:AppFont.bigfont+4,
                                  fontWeight: FontWeight.w500,
                                  color: AppColor.primarycolor,

                                ),
                                maxLines: 6,
                                overflow: TextOverflow.ellipsis,

                              ),// TODO Skill  and References

                              SizedBox(height: 30,),

                              Container(
                                height:33,
                                width: double.maxFinite,

                                child: user?.profile?.skill ==null || user?.profile?.skill?.length == 0  ?
                                Text('This user has no skill yet',style: TextStyle(
                                  color: AppColor.primarycolor.withOpacity(0.57),
                                ),) : ListView.builder(

                                  scrollDirection: Axis.horizontal,


                                  itemCount:user?.profile?.skill?.length ?? 0,
                                  itemBuilder: (context, index) {
                                    var skills = user?.profile?.skill;
                                    print(user?.profile?.skill?.length );
                                    return   Padding(
                                      padding: const EdgeInsets.symmetric(vertical: 0,horizontal: 7),
                                      child: TextButton(
                                          style: ButtonStyle(
                                              backgroundColor: MaterialStatePropertyAll(Colors.white),
                                              shape: MaterialStatePropertyAll(RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)))
                                          ),

                                          onPressed: ()=>null,
                                          child: Text( '${skills?[index] }',style: TextStyle(
                                              color: Colors.black
                                          ),)
                                      ),
                                    );
                                  },),
                              ),

                              SizedBox(height: 35,),

                              Text('Languages',
                                style: TextStyle(
                                  fontSize:AppFont.bigfont+4,
                                  fontWeight: FontWeight.w500,
                                  color: AppColor.primarycolor,

                                ),
                                maxLines: 6,
                                overflow: TextOverflow.ellipsis,

                              ),// TODO Languages

                              SizedBox(height: 35,),

                              Container(
                                height:33,
                                width: double.maxFinite,
                                child:user?.profile?.languages?.length ==0? Text('This user has no info about them yet',style: TextStyle(
                                  color: AppColor.primarycolor.withOpacity(0.57),
                                ),) : ListView.builder(

                                  scrollDirection: Axis.horizontal,

                                  itemCount:  user?.profile?.languages?.length,

                                  itemBuilder: (context, index) {
                                    var languages = user?.profile?.languages;
                                    return   Padding(
                                      padding: const EdgeInsets.symmetric(vertical: 0,horizontal: 7),
                                      child: TextButton(
                                          style: ButtonStyle(
                                              backgroundColor: MaterialStatePropertyAll(Colors.white),
                                            shape: MaterialStatePropertyAll(RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)))
                                          ),

                                          onPressed: ()=>null,
                                          child: Text(languages![index],style: TextStyle(
                                              color: Colors.black,

                                          ),)
                                      ),
                                    );
                                  },),
                              ),

                              SizedBox(height: 35,),

                              Text('Experiences',
                                style: TextStyle(
                                  fontSize:AppFont.bigfont+4,
                                  fontWeight: FontWeight.w500,
                                  color: AppColor.primarycolor,

                                ),
                                maxLines: 6,
                                overflow: TextOverflow.ellipsis,

                              ),// TODO jobs Requirements

                              SizedBox(height: 15,),

                              Container(


                                width: double.maxFinite,
                                height: 400,

                                child: user?.experience?.length == 0 ? Text('This user has no info about them yet',style: TextStyle(
                                  color: AppColor.primarycolor.withOpacity(0.57),
                                ),
                                ) : ListView.builder(



                                  itemCount:user?.experience?.length ?? 0,
                                  itemBuilder: (context, index) {
                                    return  Card(
                                      margin: EdgeInsets.symmetric(vertical: 6),
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(14)
                                      ),
                                      color: Colors.white,
                                      child: ListTile(

                                        title: Text('${user?.experience![index].position}', style: TextStyle(
                                            color:Colors.black

                                        ),),
                                        subtitle: Text('Company: ${user?.experience![index].companyname }',
                                          style: TextStyle(
                                            color: Colors.grey,

                                              fontWeight: FontWeight.w500,
                                            fontSize: AppFont.medfont


                                          ),),
                                        trailing: Text('${user?.experience![index].yearstart}-${user?.experience![index].yearend}',
                                          style: TextStyle(
                                              color:Colors.black.withOpacity(0.75),
                                              fontWeight: FontWeight.w400,
                                              fontSize: AppFont.medfont

                                          ),),
                                      ),
                                    );
                                  },),
                              ),

                            ],
                          )
                      ),
                    ),

                  );
                default:
                  return Text('');
              }

            },

          );
        },

      ),
    );
  }
 void checkauth () async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

String? id = prefs.getString('userid');
// print(id);


userid = id;
   print("new user id");
print(userid);
   userviewmodel.fetchUserbyId(userid);
    //   print( prefs.getString('userid'));
    // print(  prefs.getString('userid'));

  }

  void viewpfauth (userid) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String? id = prefs.getString('userid');
// print(id);



    print("new user id");
    print(userid);
    userviewmodel.fetchUserbyId(userid);
    //   print( prefs.getString('userid'));
    // print(  prefs.getString('userid'));

  }
  logoutcheck() async {
     SharedPreferences prefs = await SharedPreferences.getInstance();
     print("remove");
     prefs.remove('tokens');
     prefs.remove('userid');

  }
}
