import 'package:flutter/material.dart';
import 'package:jobapp/VIews/Choose/Load1.dart';
import 'package:jobapp/VIews/Choose/Load2.dart';
import 'package:jobapp/VIews/Jobs_details/job_detail.dart';
import 'package:jobapp/VIews/Jobs_details/pfscreen.dart';
import 'package:jobapp/VIews/WelcomeScreen/browsescreen.dart';
import 'package:flutter_config/flutter_config.dart';
import 'package:jobapp/VIews/subject.dart';

import 'WelcomeScreen/home.dart';
import 'WelcomeScreen/screendiscover.dart';
import 'WelcomeScreen/screenfindjob.dart';
import 'list.dart';
class Tester extends StatefulWidget {
  const Tester({Key? key}) : super(key: key);

  @override
  State<Tester> createState() => _TesterState();
}

class _TesterState extends State<Tester> {
  var txtsubject = TextEditingController();
  var txtrequire = TextEditingController();
  var lists = [

  ];
  List<ListCart> lreq = [ListCart(1,'txt')];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [

               Container(
                 padding: EdgeInsets.all(20),
                   child: TextField(
                     controller: txtsubject,
                     // onChanged: (value) {
                     //   txtsubject.text = value;
                     //   print(txtsubject.text);
                     // },
                     onTap: () => print(txtsubject),
                     onSubmitted:(value) {
                       txtsubject.text = value;
                       setState(() {

                         lists.add(Subject(sub: txtsubject.text));
                         txtsubject.clear();
                       });
                       print(txtsubject.text);
                       
                     },

                     decoration: InputDecoration(
                       hintText: 'Enter Subjects',
                      suffixIcon: IconButton(
                         onPressed: (){
                           setState(() {
                             txtsubject.clear();
                           });
                         },
                         icon: Icon(Icons.delete),
                       )
                     ),
                   )),
                Container(
                  width: double.maxFinite,
                    padding: EdgeInsets.all(20),

                    child: lists.isEmpty ? Center(child: Text('Your list is empty')) :
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: lists.map((items) =>Container(

                          width: double.maxFinite,
                          child: Container(



                              child: Card(


                                  child: Padding(

                                padding: const EdgeInsets.all(18.0),


                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text('${items.sub}'),
                                    IconButton(onPressed: () {
                                              setState(() {
                                                lists.remove(items);
                                              });
                                    }, icon: Icon(Icons.delete_forever_sharp))
                                  ],
                                ),
                              )))),).toList(),
                    )
                ),

                Container(
                    width: double.maxFinite,
                    padding: EdgeInsets.all(20),

                    child:
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: List.generate(lreq.length,(index){
                        return Container(

                            width: double.maxFinite,
                            child:     Container(
                                padding: EdgeInsets.all(20),
                                child: TextField(
                                  controller: txtrequire,
                                  // onChanged: (value) {
                                  //   txtsubject.text = value;
                                  //   print(txtsubject.text);
                                  // },
                                  onTap: () => print(txtrequire),
                                  onSubmitted:(value) {
                                    txtrequire.text = value;
                                    setState(() {

                                      lists.add(Subject(sub: txtsubject.text));
                                      txtrequire.clear();
                                    });
                                    print(txtrequire.text);

                                  },

                                  decoration: InputDecoration(
                                      hintText: 'Requirement one',
                                      suffixIcon: IconButton(
                                        onPressed: (){
                                          setState(() {
                                            txtsubject.clear();
                                          });
                                        },
                                        icon: Icon(Icons.delete),
                                      )
                                  ),
                                )));
                      }),
                    )
                ),
                Container(
                    padding: EdgeInsets.all(20),
                    child: ElevatedButton(
                      onPressed: () {
                        setState(() {
                          lreq.add( ListCart(1, 'listitem') );
                        });

                      },
                      child: Text('Add More'),
                    )
                ),

              ],
            )
          ),
        ),
      ),

    );
  }
}
