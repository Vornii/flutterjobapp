import 'package:jobapp/data/Response/status_response.dart';

class ApiResponse<T>{
  Status? status;
  T? data; // If the model User inherit Generatic type Data
  String? messaege;

  ApiResponse(this.status, this.data, this.messaege);

  ApiResponse.loading():  status = Status.LOADING;
  ApiResponse.Completed(this.data):status = Status.COMPLETED;
  ApiResponse.error(this.messaege): status =Status.ERROR;

  @override
  String toString() {
    return 'ApiResponse{status: $status, data: $data, messaege: $messaege}';
  }
}