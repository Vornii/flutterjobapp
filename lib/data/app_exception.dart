class AppException implements Exception {

var _message;
var _prefix;
AppException(this._message, this._prefix);

@override
  String toString() {
    return 'AppException{_message: $_message, _prefix: $_prefix}';
  }
}
class FetchDataException extends AppException{
  FetchDataException(message): super(message,'Error Data during Communication');
}

class ServerRequestException extends AppException{
  ServerRequestException(message):super(message,'Bad Request From Server');


}
