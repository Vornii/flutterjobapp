import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:jobapp/model/authresponse_model.dart';
import 'package:jobapp/model/job_model.dart';

import '../../model/POST/message_request_model.dart';
import '../../model/authorize/AuthModel.dart';
import '../app_exception.dart';
class LocalApiservice{

  Future<dynamic> getApiResponse(String url,token) async{
    // print(url);
    // print(token);

    // var responsejson;
    // try{
    //   var headers = {
    //     'Authorization':
    //     'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0NzFhZDc5ZTQ4ZDI2NGFiOTlhZTE2NyIsInJlc3BvbnNlIjp7ImVtYWlsIjoiVm9ybmtoMjg3QGdtYWlsLmNvbSIsInBhc3N3b3JkIjoiMjIyMiJ9LCJpYXQiOjE2ODUzNDU0NDQsImV4cCI6MTY4NTM0OTA0NH0.Lcim_Zv3yOj2b4lXE_fvLyrkHXCVBxADwVWciSWR2_M',
    //     'Content-Type': 'application/json'
    //   };
    //   // http.get(Uri.https('api.punkapi.com', '/v2/beers'));
    //   var response = await http.get(Uri.parse('http://10.0.2.2:3000/api/jobs'));
    //
    //   response.headers.addAll(headers);
    //
    //   http.StreamedResponse responses = await  response.send();
    //
    //
    //   responsejson=  returnresponse(response); //check statusCode after wew fetch
    //   print(response );
    // }on SocketException{
    //   throw FetchDataException('Wrong Url Link please check again');
    // }
    // return responsejson   ;
    //
    var headers = {
      'Authorization': 'Bearer ${token}',
      'Content-Type': 'application/json'
    };
    var request = http.Request('GET', Uri.parse(url));
    request.bodyFields = {};
    request.headers.addAll(headers);

    var response = await request.send();

    if (response.statusCode == 200) {
      // print(await response.stream.bytesToString());
      var res = await response.stream.bytesToString();
      return json.decode(res);
    }
    else {
      print(response.reasonPhrase);
    }



  }
  Future<dynamic> getReResponse(String url) async{
    print(url);


    // var responsejson;
    // try{
    //   var headers = {
    //     'Authorization':
    //     'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0NzFhZDc5ZTQ4ZDI2NGFiOTlhZTE2NyIsInJlc3BvbnNlIjp7ImVtYWlsIjoiVm9ybmtoMjg3QGdtYWlsLmNvbSIsInBhc3N3b3JkIjoiMjIyMiJ9LCJpYXQiOjE2ODUzNDU0NDQsImV4cCI6MTY4NTM0OTA0NH0.Lcim_Zv3yOj2b4lXE_fvLyrkHXCVBxADwVWciSWR2_M',
    //     'Content-Type': 'application/json'
    //   };
    //   // http.get(Uri.https('api.punkapi.com', '/v2/beers'));
    //   var response = await http.get(Uri.parse('http://10.0.2.2:3000/api/jobs'));
    //
    //   response.headers.addAll(headers);
    //
    //   http.StreamedResponse responses = await  response.send();
    //
    //
    //   responsejson=  returnresponse(response); //check statusCode after wew fetch
    //   print(response );
    // }on SocketException{
    //   throw FetchDataException('Wrong Url Link please check again');
    // }
    // return responsejson   ;
    //
    var headers = {
      'Content-Type': 'application/json'
    };
    var request = http.Request('GET', Uri.parse(url));
    request.bodyFields = {};
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      // print(await response.stream.bytesToString());
      var res = await response.stream.bytesToString();
      return jsonDecode(res);
    }
    else {
      print(response.reasonPhrase);
    }



  }
  Future<dynamic> getApiCateResponse  (String url , token) async{
    print(url);

    var headers = {
      'Authorization':'Bearer ${token}'
    };
    var request = http.Request('GET', Uri.parse('${url}'));

    request.headers.addAll(headers);

    var response = await request.send();

    if (response.statusCode == 200) {
      // print(await response.stream.bytesToString());
      var res = await response.stream.bytesToString();
      return json.decode(res);
    }
    else {
      print(response.reasonPhrase);
    }




  }
  Future<dynamic> getMessageUserResponse(String url) async{
    print(url);


    // var responsejson;
    // try{
    //   var headers = {
    //     'Authorization':
    //     'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0NzFhZDc5ZTQ4ZDI2NGFiOTlhZTE2NyIsInJlc3BvbnNlIjp7ImVtYWlsIjoiVm9ybmtoMjg3QGdtYWlsLmNvbSIsInBhc3N3b3JkIjoiMjIyMiJ9LCJpYXQiOjE2ODUzNDU0NDQsImV4cCI6MTY4NTM0OTA0NH0.Lcim_Zv3yOj2b4lXE_fvLyrkHXCVBxADwVWciSWR2_M',
    //     'Content-Type': 'application/json'
    //   };
    //   // http.get(Uri.https('api.punkapi.com', '/v2/beers'));
    //   var response = await http.get(Uri.parse('http://10.0.2.2:3000/api/jobs'));
    //
    //   response.headers.addAll(headers);
    //
    //   http.StreamedResponse responses = await  response.send();
    //
    //
    //   responsejson=  returnresponse(response); //check statusCode after wew fetch
    //   print(response );
    // }on SocketException{
    //   throw FetchDataException('Wrong Url Link please check again');
    // }
    // return responsejson   ;
    //
    var headers = {
      'Content-Type': 'application/json'
    };
    var request = http.Request('GET', Uri.parse(url));
    request.bodyFields = {};
    request.headers.addAll(headers);

    var response = await request.send();

    if (response.statusCode == 200) {
      // print(await response.stream.bytesToString());
      var res = await response.stream.bytesToString();
      print(res);
      return jsonDecode(res);
    }
    else {
      print(response.reasonPhrase);
    }



  }
  Future<dynamic> getFilterRes(String url) async{
    print(url);
    var responsejson;
    try{
      var headers = {
        'Content-Type': 'application/json'
      };
      // http.get(Uri.https('api.punkapi.com', '/v2/beers'));
      var response = await http.get(Uri.parse(url));

      response.headers.addAll(headers);


      responsejson=  returnresponse(response); //check statusCode after wew fetch
      print(response);
    }on SocketException{
      throw FetchDataException('Wrong Url Link please check again');
    }
    return responsejson   ;





  }
  Future<dynamic> getUserResponse(String? url, uid) async{
    var responsejson;
    try{
      // http.get(Uri.https('api.punkapi.com', '/v2/beers'));
      var response = await http.get(Uri.parse('${url}${uid}'));

      responsejson=  returnresponse(response); //check statusCode after wew fetch
      print(response );
    }on SocketException{
      throw FetchDataException('Wrong Url Link please check again');
    }
    return responsejson   ;




  }

  Future<dynamic> getJobidResponse(String? url, uid) async{
    var responsejson;
    try{
      // http.get(Uri.https('api.punkapi.com', '/v2/beers'));
      var response = await http.get(Uri.parse('${url}${uid}'));

      responsejson=  returnresponse(response); //check statusCode after wew fetch
      print(response );
    }on SocketException{
      throw FetchDataException('Wrong Url Link please check again');
    }
    return responsejson   ;




  }



  Future<dynamic> getMsgByidResponse(String? url,mid) async{
    var responsejson;
    try{
      // http.get(Uri.https('api.punkapi.com', '/v2/beers'));
      var response = await http.get(Uri.parse(url!));

      responsejson=  returnresponse(response); //check statusCode after wew fetch
      print(response );
    }on SocketException{
      throw FetchDataException('Wrong Url Link please check again');
    }
    return responsejson   ;




  }
 Future<dynamic> signupRequest(String url, object) async{
   var headers = {
     'Content-Type': 'application/json'
   };
   var request = http.Request('POST', Uri.parse(url));
   request.body = json.encode(object);
   request.headers.addAll(headers);

   http.StreamedResponse response = await request.send();
   print(response);

   if (response.statusCode == 200) {
     // print(await response.stream.bytesToString());
     var res = await response.stream.bytesToString();
     return json.decode(res);
   }
   else {
     print(response.reasonPhrase);
   }

 }
  Future<dynamic> checkemailRequest(String url, object) async{
    var headers = {
      'Content-Type': 'application/json'
    };
    var request = http.Request('POST', Uri.parse('${url}'));
    request.body = json.encode({
      "email": object
    });
    request.headers.addAll(headers);

var response = await request.send();

    if (response.statusCode == 200) {
      // print(await response.stream.bytesToString());
    return jsonDecode(await response.stream.bytesToString());
    }
    else {
      print(response.reasonPhrase);
    }


  }
Future<dynamic> messageApiResponse(String url) async{
  print(url);
  var responsejson;
  try{
    var headers = {
      'Content-Type': 'application/json'
    };
    // http.get(Uri.https('api.punkapi.com', '/v2/beers'));
    var response = await http.get(Uri.parse(url));

    response.headers.addAll(headers);


    responsejson=  returnresponse(response); //check statusCode after wew fetch
    print(response);
  }on SocketException{
    throw FetchDataException('Wrong Url Link please check again');
  }
  return responsejson   ;

}
  Future<dynamic> postImageThumbnail(String? url,imagepath) async{
    var request = http.MultipartRequest('POST', Uri.parse(url!));
    request.files.add(await http.MultipartFile.fromPath('upload', imagepath.toString()));
    print('image url${imagepath}');

    var response = await request.send();
    print('response ${response }');
    if (response.statusCode == 200) {
      var res  = await response.stream.bytesToString(); //TODO Response
      return json.decode(res);

    }
    else {
      print("Sth error ${response.statusCode} ${response.reasonPhrase}");
      print(response.reasonPhrase);
    }

  }

  Future<dynamic> loginApiRequest(String url,object) async{
    var headers = {
      'Content-Type': 'application/json'
    };
    var request = http.Request('POST', Uri.parse(url));
    request.body = json.encode(object);
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      //TODO api response back from Server here
      // print(await response.stream.bytesToString());
      var res  = await response.stream.bytesToString(); //TODO Response
      return json.decode(res);
    }
    else if(response.statusCode == 401){
      var res  = await response.stream.bytesToString(); //TODO Response
      print(res);
      var responses = json.decode(res);
      return UserAuthResponse.fromJson(responses);
      // return json.decode(res);
    }
    else {
      print("error is");
      print(response.reasonPhrase);
      var res = response.reasonPhrase;
      return json.decode(res!);
    }


  }
  Future<dynamic> Resetpassword(String url,object) async{
    var headers = {
      'Content-Type': 'application/json'
    };
    var request = http.Request('POST', Uri.parse('${url}'));
    print("reset password");
    print(object);
    request.body = json.encode({
      "email": object
    });
    request.headers.addAll(headers);

     var response = await request.send();

    if (response.statusCode == 200) {
      print(await response.stream.bytesToString());
      var res = await response.stream.bytesToString();
      return json.decode(res);
    }
    else {
      print(response.reasonPhrase);
    }



  }
  Future<dynamic> deleteMessageResponse(String? url) async{
    var responsejson;

    var request = http.Request('DELETE', Uri.parse('${url}'));


      var response = await request.send();

    if (response.statusCode == 200) {
      print(await response.stream.bytesToString());
      var resp = await response.stream.bytesToString();
      return json.decode(resp);
    }
    else {
      print(response.reasonPhrase);
    }



  }

  returnresponse(http.Response response) {
    switch(response.statusCode){

      case 200:
        print("Status Code repsonse${response.statusCode}");
        var responsejson = jsonDecode(response.body);
        return responsejson ;

      case 400:
        print(response.statusCode);
        throw ServerRequestException('Server is broken');
      case 404:
        print(response.statusCode);
        throw FetchDataException('No Internet');

    }
  }


  Future<dynamic> postApiRequest(String url,token , obj ) async{

    var headers = {
      'Authorization':'Bearer ${token}',
      'Content-Type': 'application/json'
    };
    var request = http.Request('POST', Uri.parse(url));
    request.body = json.encode(obj);
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();


    if (response.statusCode == 200) {

      var res = await response.stream.bytesToString();
      print(await response.stream.bytesToString());
      return json.decode(res);
    }
    else {
      print(response.reasonPhrase);
    }


  }

  Future<dynamic> UpdateApiRequest(String url,token , obj ) async{

    var headers = {
      'Authorization':'Bearer ${token}',
      'Content-Type': 'application/json'
    };
    var request = http.Request('PUT', Uri.parse(url));
    request.body = json.encode(obj);
    request.headers.addAll(headers);

    var response = await request.send();


    if (response.statusCode == 200) {

      var res = await response.stream.bytesToString();
      print(await response.stream.bytesToString());
      return json.decode(res);
    }
    else {
      print(response.reasonPhrase);
    }


  }


  Future<dynamic> postMessage (String? url, desc,userid,jobsid,filepath) async{
    var request = http.MultipartRequest('POST', Uri.parse(url!));
    request.fields.addAll({
      'description': desc,
      'user':userid,
      'job': jobsid
    });
        request.files.add(await http.MultipartFile.fromPath('files',
        '${filepath}'));

    http.StreamedResponse response = await request.send();


    if (response.statusCode == 200) {
      //TODO api response back from Server here
      print(await response.stream.bytesToString());
      var res  = await response.stream.bytesToString(); //TODO Response
      return json.decode(res);
    }
    else {
      print(response.reasonPhrase);
    }


  }
  Future<dynamic> putMessage(String? url,desc,userid,jobsid,filepath) async{
    var request = http.MultipartRequest('PUT', Uri.parse(url!));
    request.fields.addAll({
      'description': desc,
      'user':userid,
      'job': jobsid
    });
    request.files.add(await http.MultipartFile.fromPath('files', '${filepath}'));

    var response = await request.send();

    if (response.statusCode == 200) {
      print(await response.stream.bytesToString());
      var resp = await response.stream.bytesToString();
      return json.decode(resp);
    }
    else {
      print(response.reasonPhrase);
    }


  }
  Future<dynamic> putNoPdfMessage(String? url,desc,userid,jobsid) async{
    var request = http.MultipartRequest('PUT', Uri.parse('${url}'));
    request.fields.addAll({
      'description': '${desc}',
      'user': '${userid}',
      'job': '${jobsid}'
    });


  var  response = await request.send();

    if (response.statusCode == 200) {
      print(await response.stream.bytesToString());
      var resp = await response.stream.bytesToString();
      return json.decode(resp);
    }
    else {
      print(response.reasonPhrase);
    }



  }
  Future<dynamic> putUser(String? url , token , object) async{
    var headers = {
      'Authorization': 'Bearer ${token}',
      'Content-Type': 'application/json'
    };
    var request = http.Request('PUT', Uri.parse(url!));
    request.body = json.encode(object);
    request.headers.addAll(headers);

    var response = await request.send();

    if (response.statusCode == 200) {
      // print(await response.stream.bytesToString());
      var res = await response.stream.bytesToString();
      return json.decode(res);
    }
    else {
    print(response.reasonPhrase);
    }

  }

  Future<dynamic> patchJobs(String? url , token , object) async{
    var headers = {
      'Authorization': 'Bearer ${token}',
      'Content-Type': 'application/json'
    };
    var request = http.Request('PUT', Uri.parse(url!));
    request.body = json.encode(object);
    request.headers.addAll(headers);

    var response = await request.send();

    if (response.statusCode == 200) {
      // print(await response.stream.bytesToString());
      var res = await response.stream.bytesToString();
      return json.decode(res);
    }
    else {
      print(response.reasonPhrase);
    }

  }

  Future<dynamic> deletePosts (String? url, token ) async{
    var headers = {
      'Authorization': 'Bearer ${token}'
    };
    var request = http.Request('DELETE', Uri.parse('${url}'));

    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();
    print(response.statusCode);
    if (response.statusCode == 200) {

      var res = await response.stream.bytesToString();
      print(res);
      return res;
    }
    else {
      print(response.reasonPhrase);
    }


  }

  Future<dynamic> getPostByUserId(String url) async{
      print(url);


    // var responsejson;
    // try{
    //   var headers = {
    //     'Authorization':
    //     'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0NzFhZDc5ZTQ4ZDI2NGFiOTlhZTE2NyIsInJlc3BvbnNlIjp7ImVtYWlsIjoiVm9ybmtoMjg3QGdtYWlsLmNvbSIsInBhc3N3b3JkIjoiMjIyMiJ9LCJpYXQiOjE2ODUzNDU0NDQsImV4cCI6MTY4NTM0OTA0NH0.Lcim_Zv3yOj2b4lXE_fvLyrkHXCVBxADwVWciSWR2_M',
    //     'Content-Type': 'application/json'
    //   };
    //   // http.get(Uri.https('api.punkapi.com', '/v2/beers'));
    //   var response = await http.get(Uri.parse('http://10.0.2.2:3000/api/jobs'));
    //
    //   response.headers.addAll(headers);
    //
    //   http.StreamedResponse responses = await  response.send();
    //
    //
    //   responsejson=  returnresponse(response); //check statusCode after wew fetch
    //   print(response );
    // }on SocketException{
    //   throw FetchDataException('Wrong Url Link please check again');
    // }
    // return responsejson   ;
    //
    var headers = {
      'Content-Type': 'application/json'
    };
    var request = http.Request('GET', Uri.parse(url));
    request.bodyFields = {};
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      // print(await response.stream.bytesToString());
      var res = await response.stream.bytesToString();
      return jsonDecode(res);
    }
    else {
      print(response.reasonPhrase);
    }



  }
  Future<dynamic> getAll(String url) async{
    print(url);


    // var responsejson;
    // try{
    //   var headers = {
    //     'Authorization':
    //     'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0NzFhZDc5ZTQ4ZDI2NGFiOTlhZTE2NyIsInJlc3BvbnNlIjp7ImVtYWlsIjoiVm9ybmtoMjg3QGdtYWlsLmNvbSIsInBhc3N3b3JkIjoiMjIyMiJ9LCJpYXQiOjE2ODUzNDU0NDQsImV4cCI6MTY4NTM0OTA0NH0.Lcim_Zv3yOj2b4lXE_fvLyrkHXCVBxADwVWciSWR2_M',
    //     'Content-Type': 'application/json'
    //   };
    //   // http.get(Uri.https('api.punkapi.com', '/v2/beers'));
    //   var response = await http.get(Uri.parse('http://10.0.2.2:3000/api/jobs'));
    //
    //   response.headers.addAll(headers);
    //
    //   http.StreamedResponse responses = await  response.send();
    //
    //
    //   responsejson=  returnresponse(response); //check statusCode after wew fetch
    //   print(response );
    // }on SocketException{
    //   throw FetchDataException('Wrong Url Link please check again');
    // }
    // return responsejson   ;
    //
    var headers = {
      'Content-Type': 'application/json'
    };
    var request = http.Request('GET', Uri.parse(url));
    request.bodyFields = {};
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      // print(await response.stream.bytesToString());
      var res = await response.stream.bytesToString();
      return jsonDecode(res);
    }
    else {
      print(response.reasonPhrase);
    }



  }
  Future<dynamic> getAllUser(String url) async{
    print(url);


    // var responsejson;
    // try{
    //   var headers = {
    //     'Authorization':
    //     'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0NzFhZDc5ZTQ4ZDI2NGFiOTlhZTE2NyIsInJlc3BvbnNlIjp7ImVtYWlsIjoiVm9ybmtoMjg3QGdtYWlsLmNvbSIsInBhc3N3b3JkIjoiMjIyMiJ9LCJpYXQiOjE2ODUzNDU0NDQsImV4cCI6MTY4NTM0OTA0NH0.Lcim_Zv3yOj2b4lXE_fvLyrkHXCVBxADwVWciSWR2_M',
    //     'Content-Type': 'application/json'
    //   };
    //   // http.get(Uri.https('api.punkapi.com', '/v2/beers'));
    //   var response = await http.get(Uri.parse('http://10.0.2.2:3000/api/jobs'));
    //
    //   response.headers.addAll(headers);
    //
    //   http.StreamedResponse responses = await  response.send();
    //
    //
    //   responsejson=  returnresponse(response); //check statusCode after wew fetch
    //   print(response );
    // }on SocketException{
    //   throw FetchDataException('Wrong Url Link please check again');
    // }
    // return responsejson   ;
    //
    var headers = {
      'Content-Type': 'application/json'
    };
    var request = http.Request('GET', Uri.parse(url));
    request.bodyFields = {};
    request.headers.addAll(headers);

    http.StreamedResponse response = await request.send();

    if (response.statusCode == 200) {
      // print(await response.stream.bytesToString());
      var res = await response.stream.bytesToString();
      return jsonDecode(res);
    }
    else {
      print(response.reasonPhrase);
    }



  }

  Future<dynamic> deleteoneUser(String url) async{
    var request = http.Request('DELETE', Uri.parse('${url}'));

    var response = await request.send();

    if (response.statusCode == 200) {
      print(await response.stream.bytesToString());
      var res = await response.stream.bytesToString();
      return json.decode(res);
    }
    else {
      print(response.reasonPhrase);
    }

  }
}

