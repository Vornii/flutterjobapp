import 'package:flutter/cupertino.dart';

import '../data/Response/api_response_service.dart';
import '../model/POST/message_request_model.dart';
import '../model/image_response_model.dart';
import '../repository/post_repository.dart';

class ImageViewModel  extends ChangeNotifier{
  final _imageviewmodel= PostRepository();

  ApiResponse<ImageResponseModel> GetImageRes = ApiResponse.loading();
  Future<dynamic> postImage(filepath) async{

    await _imageviewmodel.ImageResponse(filepath).then((res) =>{
      setImageResponse(ApiResponse.Completed(res))

    }).onError((error, stackTrace) => {
      setImageResponse(ApiResponse.error(error.toString()))
    });
  }

  setImageResponse(ApiResponse<ImageResponseModel> apiResponse) {
    GetImageRes= apiResponse ;
    notifyListeners();
  }
}