// import 'dart:js_util';

import 'package:flutter/material.dart';
import 'package:jobapp/data/Response/api_response_service.dart';
import 'package:jobapp/model/Admin/UserDeleteModel.dart';
import 'package:jobapp/model/POST/job_reponse_model.dart';
import 'package:jobapp/model/image_response_model.dart';
import 'package:jobapp/model/messageid_model.dart';
import 'package:jobapp/repository/login_repository.dart';
import 'package:jobapp/repository/post_repository.dart';

import '../model/CRUD/UpdatePostResponse_model.dart';
import '../model/CRUD/messageupdate_response.dart';
import '../model/POST/message_request_model.dart';
import '../model/Put/user_update_response_model.dart';
import '../model/ResetResponse.dart';
import '../model/message_model.dart';

class PostViewModel extends ChangeNotifier{
  final _postviewmodel = PostRepository();

  final _authviewmodel = LoginRepository();
  ApiResponse<JobResponse> PostApiresponse = ApiResponse.loading();
  ApiResponse<MessageResponse> PostMsgResponse = ApiResponse.loading();
  ApiResponse<MessageIDResponse> GetIdMsgResponse = ApiResponse.loading();


  ApiResponse< ResponseUpdateUserModel> UserUpdateResponse = ApiResponse.loading();
  ApiResponse<PasswordResetModel> ResetPassResponse = ApiResponse.loading();
  ApiResponse<UpdatePostResponseModel> PostUpdateResponse = ApiResponse.loading();
  ApiResponse<MessageUpdateModel > UpdateMessageResponse = ApiResponse.loading();


  setUpdatePost(ApiResponse<UpdatePostResponseModel> apiResponse) {
    PostUpdateResponse = apiResponse;



    notifyListeners();



  }
  Future<dynamic>  postJobAPI(object,token) async{

    await _postviewmodel.postJob(object,token).then((res) =>{
      setPostJobRes(ApiResponse.Completed(res))

    }).onError((error, stackTrace) => {
      setPostJobRes(ApiResponse.error(error.toString()))
    });
  }

  Future<dynamic>  postResetPassword(uid,email) async{

    await _authviewmodel.ResetPwApi(uid ,email).then((res) =>{
      setResetEmailRes(ApiResponse.Completed(res))

    }).onError((error, stackTrace) => {
      setResetEmailRes(ApiResponse.error(error.toString()))
    });
  }
  Future<dynamic>  updateJobAPI(object,token,id) async{

    await _postviewmodel.updateApiJobs(object,token,id).then((res) =>{
      setUpdatePost(ApiResponse.Completed(res))

    }).onError((error, stackTrace) => {
      setUpdatePost(ApiResponse.error(error.toString()))
    });
  }
  Future<dynamic> updateUserAPI(uid,object,token) async{

    await _postviewmodel.updateUser(uid,object,token).then((res) =>{
      setUpdateuserres(ApiResponse.Completed(res))

    }).onError((error, stackTrace) => {
      setUpdateuserres(ApiResponse.error(error.toString()))
    });
  }
  Future<dynamic>  fetchmsgbyid(mid) async{

    await _postviewmodel.getMessageid(mid).then((res) =>{
      setMsgResponseId(ApiResponse.Completed(res))

    }).onError((error, stackTrace) => {
      setMsgResponseId(ApiResponse.error(error.toString()))
    });
  }

  Future<dynamic>  updateMessageAPI(mid,desc,userid,jobid,filepath) async{

    await _postviewmodel.putMsgJob(mid,desc,userid,jobid,filepath).then((res) =>{
      setUpdateMsg(ApiResponse.Completed(res))

    }).onError((error, stackTrace) => {
      setUpdateMsg(ApiResponse.error(error.toString()))
    });
  }

  Future<dynamic>  updateMessageNopdf(mid,desc,userid,jobid) async{

    await _postviewmodel. putMsgJobNoFiles(mid,desc,userid,jobid).then((res) =>{
      setUpdateMsg(ApiResponse.Completed(res))

    }).onError((error, stackTrace) => {
      setUpdateMsg(ApiResponse.error(error.toString()))
    });
  }


  Future<dynamic>  postMessageAPI(desc,userid,jobid,filepath,token) async{

    await _postviewmodel.postMsgJob(desc,userid,jobid,filepath,token).then((res) =>{
      setMsgJobRes(ApiResponse.Completed(res))

    }).onError((error, stackTrace) => {
      setMsgJobRes(ApiResponse.error(error.toString()))
    });
  }



  setPostJobRes(ApiResponse<JobResponse> apiResponse) {
    PostApiresponse = apiResponse  ;
    notifyListeners();

  }
  setMsgJobRes(ApiResponse<MessageResponse> apiResponse) {
   PostMsgResponse= apiResponse  ;
    notifyListeners();

  }

  setMsgResponseId(ApiResponse<MessageIDResponse> apiResponse) {
      GetIdMsgResponse = apiResponse  ;
      notifyListeners();
  }

  setUpdateuserres(ApiResponse<ResponseUpdateUserModel> apiResponse) {
    UserUpdateResponse = apiResponse;
    notifyListeners();
  }

  setUpdateMsg(ApiResponse<   MessageUpdateModel> apiResponse) {
  UpdateMessageResponse= apiResponse;
    notifyListeners();
  }

  setResetEmailRes(ApiResponse<PasswordResetModel> apiResponse) async {
  ResetPassResponse= apiResponse;
    notifyListeners();
  }



}

