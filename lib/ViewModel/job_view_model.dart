import 'package:flutter/cupertino.dart';
import 'package:http/http.dart';
import 'package:jobapp/data/Response/api_response_service.dart';
import 'package:jobapp/model/Admin/AdminJobPostModel.dart';
import 'package:jobapp/model/Admin/AdminUserModel.dart';
import 'package:jobapp/model/CRUD/Deletemessage_model.dart';
import 'package:jobapp/model/POST/job_reponse_model.dart';
import 'package:jobapp/model/POST/job_request_model.dart';
import 'package:jobapp/model/POST/message_request_model.dart';
import 'package:jobapp/model/job_byid_model.dart';
import 'package:jobapp/model/job_model.dart';
import 'package:jobapp/model/loginerrormodel.dart';
import 'package:jobapp/repository/job_repository.dart';
import 'package:jobapp/repository/login_repository.dart';


import '../model/Admin/UserDeleteModel.dart';
import '../model/CRUD/PostDeleteResponse_model.dart';
import '../model/CRUD/messageuser_model.dart';
import '../model/Categorymodel.dart';
import '../model/POST/UserbyJobModel.dart';
import '../model/POST/message_request_model.dart';
import '../model/POST/message_request_model.dart';

import '../model/Put/user_update_response_model.dart';
import '../model/Recommend_model.dart';
import '../model/authorize/CheckEmailResponse.dart';
import '../model/authresponse_model.dart';
import '../model/image_response_model.dart';
import '../model/message_model.dart';
import '../model/messageid_model.dart';
import '../model/signupresponse.dart';
import '../model/user_model.dart';
import '../repository/post_repository.dart';
import '../repository/signup_repository.dart';

class JobViewModel extends ChangeNotifier{

  final _jobviewmodel = JobRepository();
  final _loginviewmodel = LoginRepository(  );
  final _Signupviewmodel =  SignupRepository();

  final _postviewmodel = PostRepository();


  final _imageviewmodel= PostRepository();
  ApiResponse<LoginResponse> loginApiResponse = ApiResponse.loading();
  ApiResponse<LoginErrorModel> loginApiResponseError = ApiResponse.loading();

  ApiResponse<DeleteUserResponse> DeleteuserRes = ApiResponse.loading();
  ApiResponse< RegisterResponse > signupApiResponse = ApiResponse.loading();
 ApiResponse<UserResponse> userApiResponse =  ApiResponse.loading();
  ApiResponse<UserAdminModel> userApiResponseAll =  ApiResponse.loading();
ApiResponse<JobModel> FilterResponse =  ApiResponse.loading();
ApiResponse<JobModel>  jobApiResponse = ApiResponse.loading();
  ApiResponse<JobAdminModel>  jobApiResponseAll = ApiResponse.loading();
  ApiResponse<PostID>  jobApiResponseID = ApiResponse.loading();
ApiResponse<RecommendResponse> jobReresponse =ApiResponse.loading();
  ApiResponse<JobResponse>  postResponse =ApiResponse.loading();
  ApiResponse<MessageInboxRes >  messageResponse =ApiResponse.loading();
  ApiResponse< ResponseUpdateUserModel> UserUpdateResponse = ApiResponse.loading();
  ApiResponse<JobByUserID> PostByUIdResponse = ApiResponse.loading();
  ApiResponse<PostDeleteModel> PostDeleteResponse = ApiResponse.loading();
  ApiResponse<ImageResponseModel> GetImageRes = ApiResponse.loading();
  ApiResponse<CategoryModel> CateResponse = ApiResponse.loading();
  ApiResponse<CheckEmailResponse>     checkemailResponse= ApiResponse.loading();
  ApiResponse<MessageModel> MessageResponse =  ApiResponse.loading();
  ApiResponse<MessageDeleteModel> DeleteResponseMsg =  ApiResponse.loading();
  setImageResponse(ApiResponse<ImageResponseModel> apiResponse) {
    GetImageRes= apiResponse ;
    print(apiResponse);
    notifyListeners();
  }
  setMsgResponseId(ApiResponse<ResponseUpdateUserModel> apiResponse) {
    UserUpdateResponse = apiResponse  ;
    notifyListeners();
  }
  setUpdateuserres(ApiResponse<ResponseUpdateUserModel> apiResponse) {
    UserUpdateResponse = apiResponse;
    notifyListeners();
  }
  Future<dynamic>  deleteUserApi(userid) async{

    await _loginviewmodel.DelUser(userid).then((res) =>{
      setDeleteUser(ApiResponse.Completed(res))

    }).onError((error, stackTrace) => {
      setDeleteUser(ApiResponse.error(error.toString()))
    });
  }
  setDeleteUser(ApiResponse<DeleteUserResponse> apiResponse) {
    DeleteuserRes = apiResponse ;
    notifyListeners();

  }
  Future<dynamic>   getJobPostsByuid(uid) async{
    await _jobviewmodel.getJobPostsByuid(uid).then((value) =>{
      setJobUserID(ApiResponse.Completed(value))

    }).onError((error, stackTrace) => {
      setJobUserID(ApiResponse.error(error.toString()))
    });
  }
  Future<dynamic>   getJobPostsByAll(uid) async{
    await _jobviewmodel.getJobPostsAll(uid).then((value) =>{
      setJobAlle(ApiResponse.Completed(value))

    }).onError((error, stackTrace) => {
      setJobAlle(ApiResponse.error(error.toString()))
    });
  }
  Future<dynamic>   getusersByAll() async{
    await _jobviewmodel.getUserAll().then((value) =>{
      setUserStateAll(ApiResponse.Completed(value))

    }).onError((error, stackTrace) => {
      setUserStateAll(ApiResponse.error(error.toString()))
    });
  }



  Future<dynamic> updateUserClient(uid,postid,status,token) async{

    await _postviewmodel.updateByClient(uid,postid,status,token).then((res) =>{
      setUpdateuserres(ApiResponse.Completed(res))

    }).onError((error, stackTrace) => {
      setUpdateuserres(ApiResponse.error(error.toString()))
    });
  }
  Future<dynamic> updateUserAPI(uid,object,token) async{

    await _postviewmodel.updateUser(uid,object,token).then((res) =>{
      setUpdateuserres(ApiResponse.Completed(res))

    }).onError((error, stackTrace) => {
      setUpdateuserres(ApiResponse.error(error.toString()))
    });
  }

  Future<dynamic> postUserAPI(uid,object,token) async{

    await _postviewmodel.updateUser(uid,object,token).then((res) =>{
      setUpdateuserres(ApiResponse.Completed(res))

    }).onError((error, stackTrace) => {
      setUpdateuserres(ApiResponse.error(error.toString()))
    });
  }


  Future<dynamic>   deleteByJobId(pid,token) async{
    await _jobviewmodel.deleteJobs(pid,token).then((value) =>{
      setJobDeleteState(ApiResponse.Completed(value))

    }).onError((error, stackTrace) => {
      setJobDeleteState(ApiResponse.error(error.toString()))
    });
  }

  Future<dynamic>   deletemsgbyid(mid) async{
    await _jobviewmodel.deleteMessage(mid).then((value) =>{
      setMsgDeleteState(ApiResponse.Completed(value))

    }).onError((error, stackTrace) => {
      setMsgDeleteState(ApiResponse.error(error.toString()))
    });
  }
  Future<dynamic> postImage(filepath) async{

    await _imageviewmodel.ImageResponse(filepath).then((res) =>{
      setImageResponse(ApiResponse.Completed(res))

    }).onError((error, stackTrace) => {

      setImageResponse(ApiResponse.error(error.toString()))
    });
  }

  Future<dynamic>   getMsgById(uid) async{
    await _jobviewmodel.getmessageByID(uid).then((value) =>{
      setMessageState(ApiResponse.Completed(value))

    }).onError((error, stackTrace) => {
      setMessageState(ApiResponse.error(error.toString()))
    });
  }
  setJobstate(ApiResponse<JobModel> apiResponse) {
    jobApiResponse = apiResponse;
    notifyListeners();
  }
  setJobAlle(ApiResponse<JobAdminModel> apiResponse) {
    jobApiResponseAll = apiResponse;
    notifyListeners();
  }
  setPostJobRes(ApiResponse<JobResponse> apiResponse) {

    postResponse = apiResponse;
    notifyListeners();
  }
  setJobFilter(ApiResponse<JobModel> apiResponse) {
    FilterResponse = apiResponse;
    notifyListeners();
  }
  setUserState(ApiResponse<UserResponse> apiResponse) {
    userApiResponse = apiResponse;
    notifyListeners();
  }
  setUserStateAll(ApiResponse<UserAdminModel> apiResponse) {
    userApiResponseAll = apiResponse;
    notifyListeners();
  }
  setLoginState(ApiResponse<LoginResponse> apiResponse){
    loginApiResponse = apiResponse;
    notifyListeners();
  }
  setSignupState(ApiResponse< RegisterResponse > apiResponse){
    signupApiResponse = apiResponse;
    notifyListeners();
  }
  setCheckEmailState(ApiResponse< CheckEmailResponse  > apiResponse){
    checkemailResponse = apiResponse;
    notifyListeners();
  }
  setJobByIDState(ApiResponse<PostID> apiResponse) {
    jobApiResponseID = apiResponse;
    notifyListeners();
  }
  Future postSignup(object) async{
    // print(object);
    await _Signupviewmodel.SignUp(object).then((res) {
          setSignupState(ApiResponse.Completed(res));
    }, ).onError((error, stackTrace) {

      setSignupState(ApiResponse.error(error.toString()));

    },);
  }
  Future checkEmail(object) async{
    // print(object);
    await _Signupviewmodel.CheckEmailSignup(object).then((res) {
     setCheckEmailState(ApiResponse.Completed(res));
    }, ).onError((error, stackTrace) {

      setCheckEmailState(ApiResponse.error(error.toString()));

    },);
  }
  Future<dynamic> fetchall(token,String? title) async{
    await _jobviewmodel.getJobsPost(token,title).then((value) =>{
      setJobstate(ApiResponse.Completed(value))

    }).onError((error, stackTrace) => {
      setJobstate(ApiResponse.error(error.toString()))
    });
  }
  Future<dynamic> fetchjoball(token,page) async{
    await _jobviewmodel.getJobsall(token,page).then((value) =>{
      setJobstate(ApiResponse.Completed(value))

    }).onError((error, stackTrace) => {
      setJobstate(ApiResponse.error(error.toString()))
    });
  }
  Future<dynamic> fetchMsg(uid) async{
    await _jobviewmodel.getMessagePost(uid).then((value) =>{
   setMsgState(ApiResponse.Completed(value))
    }).onError((error, stackTrace) => {

      setMsgState(ApiResponse.error(error.toString()))
    });
  }
  Future<dynamic> fetchbyCategory(token,page,cate) async{
    await _jobviewmodel.getJobsByCategory(token,page,cate).then((value) =>{
      setCateState(ApiResponse.Completed(value))
    }).onError((error, stackTrace) => {

      setCateState(ApiResponse.error(error.toString()))
    });
  }
  Future<dynamic> Filterall(category,type,min,max) async{
    await _jobviewmodel.getFilterPost(category,type,min,max).then((value) =>{
      setJobFilter(ApiResponse.Completed(value))

    }).onError((error, stackTrace) => {
      setJobFilter(ApiResponse.error(error.toString()))
    });
  }

  Future<dynamic> FetchRecommend(lists) async{

   await _jobviewmodel.getJobsRecommend(lists).then((value) {
     setJobReRespose(ApiResponse.Completed(value));

   },).onError((error, stackTrace) {
     setJobReRespose(ApiResponse.error(error.toString()));
   },);
  }

  Future<dynamic> fetchUserbyId(String? uid) async{
    await _jobviewmodel.getUserid(uid).then((value) =>{
      setUserState(ApiResponse.Completed(value))

    }).onError((error, stackTrace) => {

    });
  }
  Future<dynamic> FetchPostByID(String? uid) async{
    await _jobviewmodel.getJobid(uid).then((value) =>{
      setJobByIDState(ApiResponse.Completed(value))

    }).onError((error, stackTrace) => {
      setJobByIDState(ApiResponse.error(error.toString()))

    });
  }
  Future postLogin(email,pass) async{

    await _loginviewmodel.loginApi(email,pass).then((res) {
      setLoginState(ApiResponse.Completed(res));
    },).onError((error, stackTrace) {
      print("sth error in view wmodel");
      setLoginState(ApiResponse.error(error.toString()));

    },);
  }


  setJobReRespose(ApiResponse<RecommendResponse> apiResponse) {
    jobReresponse = apiResponse;
    notifyListeners();

  }

  setMsgState(ApiResponse<MessageInboxRes> apiResponse) {
    messageResponse = apiResponse;
    notifyListeners();
  }

  setJobUserID(ApiResponse<JobByUserID> apiResponse) {
    PostByUIdResponse = apiResponse;
    notifyListeners();
  }


  setJobDeleteState(ApiResponse<PostDeleteModel> apiResponse) {
    PostDeleteResponse = apiResponse;
    notifyListeners();

  }

  setMessageState(ApiResponse< MessageModel> apiResponse) {
    MessageResponse = apiResponse;
    notifyListeners();
  }

  setMsgDeleteState(ApiResponse<MessageDeleteModel> apiResponse) {
    DeleteResponseMsg = apiResponse;
    notifyListeners();

  }

  setCateState(ApiResponse<CategoryModel> apiResponse) {
   CateResponse = apiResponse;
    notifyListeners();
  }





}

