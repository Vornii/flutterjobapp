

mixin Signupvalidator{
  bool isNameEmpty(name){
    return name.length >0;
  }
  bool isEmailEmpty(email){
    return email.length >0;
  }
  bool NameValid(name){
    return RegExp('[a-zA-z]{4,10}').hasMatch(name);
    //name must be letter only no num

  }
  bool isValidEmail(email) {
    return RegExp(
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(email);
  }
  bool isPasswordEmpty (pass){
    return pass.length >0;
  }
  bool isPasswordValidate (pass){
    return pass.length >=5;
  }

  // bool isValidname(name){
  //
  // }
}