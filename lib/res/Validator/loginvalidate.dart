mixin CheckValidateLogin{
  //create function
  bool isPasswordLength(password){
    return password.length >=3;
  }
  bool isEmailLength(email){
    return email.length >0;
  }
  bool isValidEmail(email) {
    return RegExp(
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(email);
  }

  bool isTelegramvalid(telegram) {
    return RegExp(
        r'(https?:\/\/)?(www[.])?(telegram|t)\.me\/([a-zA-Z0-9_-]*)\/?$')
        .hasMatch(telegram);
  }
 }