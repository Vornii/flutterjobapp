import 'package:flutter/material.dart';

mixin AppFont{

  static var  megafont = 22.00;
  static   var bigfont = 14.00;
  static   var medfont = 12.80;
  static   var smfont = 11;
}
