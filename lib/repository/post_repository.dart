import 'package:jobapp/model/Put/update_user_model.dart';
import 'package:jobapp/data/Network/local_api_service.dart';
import 'package:jobapp/model/POST/job_reponse_model.dart';

import 'package:jobapp/model/Recommend_model.dart';
import 'package:jobapp/model/image_response_model.dart';
import 'package:jobapp/model/job_byid_model.dart';
import 'package:jobapp/model/message_model.dart';
import 'package:jobapp/model/messageid_model.dart';
import '../model/CRUD/UpdateAvRequest.dart';
import '../model/CRUD/UpdatePostResponse_model.dart';
import '../model/CRUD/messageupdate_response.dart';
import '../model/POST/job_request_model.dart';
import '../model/job_model.dart';
import '../model/user_model.dart';
import '../res/BaseUrl.dart';
import '../res/api_url.dart';

class PostRepository{
  var _postjobservice =LocalApiservice(); //OBKJECT


  Future<dynamic > postJob(object , token ) async{
    try{

      var posts= JobRequest(
          title: object.title,
          status: object.status,
          contact: object.contact,
          address: object.address,
          jobDescription: object.jobDescription,
          requirement:object.requirement,
          category: object.category,
          user: object.user

      );
      print("Repository");
      print(posts);
      print(token);
      // print(loginauth);//TODO MODEL REQUEST

    var jobs =  await _postjobservice.postApiRequest('${ApiApiUrl.localurl}/api/jobs',token,posts.toJson(), );
      print(jobs );
      return jobs  =  JobResponse.fromJson(jobs); //reeturn response

    }catch(error){
      print(error);
      rethrow;
    }

  }

  Future<dynamic > updateApiJobs(object , token, id ) async{
    try{



      var posts= JobRequest(
          title: object.title,
          status: object.status,
          contact: object.contact,
          address: object.address,
          jobDescription: object.jobDescription,
          requirement:object.requirement,
          category: object.category,
          user: object.user

      );
      print("Repository Update");
      print(posts);
      print(token);
      // print(loginauth);//TODO MODEL REQUEST

      var jobs =  await _postjobservice.UpdateApiRequest('${ApiApiUrl.localurl}/api/jobs/edit/${id}',
        token,posts.toJson(), );
      print(jobs );
      return jobs  =   UpdatePostResponseModel.fromJson(jobs); //reeturn response

    }catch(error){
      print(error);
      rethrow;
    }

  }
  Future<dynamic > updateUser(uid,object,token  ) async{
    try{

      var posts= UpdateuserModel(
          interest: object.interest,
          experience:object.experience,
          name:object.name,
          position: object.position,
          role: object.role,

          profile: object.profile,
          thumbnail:object.thumbnail


      );
      print("Repository User Profile");
      print(posts);
      print(token);
      // print(loginauth);//TODO MODEL REQUEST

      var jobs =  await _postjobservice.putUser('${ApiApiUrl.localurl}/api/auth/${uid}/edit',token,posts.toJson(), );
      print(jobs );
      return jobs  =  JobResponse.fromJson(jobs); //reeturn response

    }catch(error){
      print(error);
      rethrow;
    }

  }
  Future<dynamic > updateByClient(uid,postid,status,token  ) async{
    try{

      var posts= UserPostRequest(
        status: status,
        user: uid
      );
      print("Repository");
      print(posts);
      print(token);
      // print(loginauth);//TODO MODEL REQUEST

      var jobs =  await _postjobservice.patchJobs('${ApiApiUrl.localurl}/api/jobs/post/${postid}',token,posts.toJson(), );
      print(jobs );
      return jobs  =  JobResponse.fromJson(jobs); //reeturn response

    }catch(error){
      print(error);
      rethrow;
    }

  }
  Future<dynamic > postMsgJob(desc,userid,jobid,filepath,token ) async{
    try{


      print("Repository MEssage");


      print(token);
      print(jobid);
      print(filepath);
      // print(loginauth);//TODO MODEL REQUEST

      var  msg  =  await _postjobservice.postMessage('${ApiApiUrl.localurl}/api/message', desc,userid,jobid,filepath);
      print(msg);
      return msg = MessageInboxRes.fromJson(msg); //reeturn response

    }catch(error){
      print(error);
      rethrow;
    }

  }
  Future<dynamic > putMsgJob(mid,desc,userid,jobid,filepath ) async{
    try{


      print("Repository MEssage");



      print(jobid);
      print(filepath);
      // print(loginauth);//TODO MODEL REQUEST

      var  msg  =  await _postjobservice.putMessage('${ApiApiUrl.localurl}/api/message/edit/${mid}', desc,userid,jobid,filepath);
      print(msg);
      return msg = MessageUpdateModel.fromJson(msg); //reeturn response

    }catch(error){
      print(error);
      rethrow;
    }

  }
  Future<dynamic > putMsgJobNoFiles(mid,desc,userid,jobid ) async{
    try{


      print("Repository MEssage");



      print(jobid);

      // print(loginauth);//TODO MODEL REQUEST

      var  msg  =  await _postjobservice.putNoPdfMessage('${ApiApiUrl.localurl}/api/message/edit/${mid}', desc,userid,jobid);
      print(msg);
      return msg = MessageUpdateModel.fromJson(msg); //reeturn response

    }catch(error){
      print(error);
      rethrow;
    }

  }
  Future<dynamic > getMessageid(mid) async{
    try{


      print("Message");

      // print(loginauth);//TODO MODEL REQUEST

      var message =  await _postjobservice.getMsgByidResponse('${ApiApiUrl.localurl}/api/message/${mid}',mid);
      print( message );
      return  message =  MessageIDResponse.fromJson( message); //reeturn response

    }catch(error){
      print(error);
      rethrow;
    }

  }

  Future<dynamic > ImageResponse(filepath) async{
    try{


      print("Message Images Res");

      // print(loginauth);//TODO MODEL REQUEST

      var thumbnail=  await _postjobservice.postImageThumbnail('${ApiApiUrl.localurl}/upload/thumbnail','${filepath.toString()}');

      return thumbnail =  ImageResponseModel.fromJson(thumbnail) ;//reeturn response

    }catch(error){
      print("error occur");
      print(error);
      rethrow;
    }

  }



}