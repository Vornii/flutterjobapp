import 'package:jobapp/data/Network/local_api_service.dart';
import 'package:jobapp/model/ResetResponse.dart';
import 'package:jobapp/model/authorize/AuthModel.dart';
import 'package:jobapp/model/loginerrormodel.dart';
import '../model/Admin/UserDeleteModel.dart';
import '../model/EmailModel.dart';
import '../model/authrequest_model.dart';
import '../model/authresponse_model.dart';
import '../model/job_model.dart';
import '../res/BaseUrl.dart';
import '../res/api_url.dart';


class LoginRepository{
  var _loginservice =LocalApiservice();

  Future<dynamic > loginApi(email,password ) async{
    dynamic response;
    try{
      var loginauth =  LoginRequest(email: email,password: password);
      // print(loginauth);//TODO MODEL REQUEST

    response =  await _loginservice.loginApiRequest('${ApiApiUrl.localurl}/api/auth/login',loginauth.toJson());
    // UserAuthResponse res = response;
    //  print("response is : ${response .message}");
    //  // if(response.message)
    //   if(   response .message =='User is not sign up yet'){
    //     print("true");
    //     return response = UserAuthResponse.fromJson(response );
    //   }

        return response = LoginResponse.fromJson(response);





      //reeturn response




      return response;
    }catch(error){
      print("error is ${error}");
      return response ;


      rethrow;
    }

  }
  Future<dynamic > ResetPwApi(uid,email ) async{
    try{
      var emailauth=  EmailRequestModel(email: email);
      // print(loginauth);//TODO MODEL REQUEST

      dynamic response =  await _loginservice.Resetpassword('${ApiApiUrl.localurl}/api/auth/email',email);
      // print(response);
      return response = PasswordResetModel.fromJson(response); //reeturn response

      // print(response);


      return response;
    }catch(error){
      // print(error);
      rethrow;
    }

  }
  Future<dynamic> DelUser(uid) async{
    try{
      var response = await _loginservice.deleteoneUser('${ApiApiUrl.mainurl}/api/auth/delete/${uid}');
      print(response);
      return response = DeleteUserResponse.fromJson(response);

    }catch(err){
      print(err);
      rethrow ;
    }

  }
}