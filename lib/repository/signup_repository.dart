import 'package:jobapp/data/Network/local_api_service.dart';
import 'package:jobapp/model/authorize/CheckEmailResponse.dart';
import '../model/authrequest_model.dart';
import '../model/authresponse_model.dart';
import '../model/job_model.dart';
import '../model/signuprequest_model.dart';
import '../model/signupresponse.dart';
import '../res/BaseUrl.dart';
import '../res/api_url.dart';

class SignupRepository{
  var _signupservice = LocalApiservice();
  var _checkemail = LocalApiservice();
  Future<dynamic > SignUp(object) async{
    try{
      print("User object");
      // print(object.name);
      // print(object.email);
      // print(object.password);
      // print(object.role);
      // print(object.interest);
      var register=  RegisterRequest(
        name: object.name,
        email: object.email,
        password: object.password,
        role: object.role,
        profile: object.profile,
        position: object.position,
        interest: object.interest
      );
      // print(register);//TODO MODEL REQUEST

      dynamic response =  await _signupservice.signupRequest('${ApiApiUrl.localurl}/api/auth/signup',register.toJson());

      
      print(response);
      return response = RegisterResponse.fromJson(response); //reeturn response

      return response;
    }catch(error){
      print(error);
      rethrow;
    }

  }


  Future<dynamic > CheckEmailSignup(object) async{
    try{
      print("User object");
      // print(object.name);
      // print(object.email);
      // print(object.password);
      // print(object.role);
      // print(object.interest);

      // print(register);//TODO MODEL REQUEST

      dynamic response =  await _checkemail.checkemailRequest('${ApiApiUrl.localurl}/api/auth/checkemail',object);
      // print(response);
      return response = CheckEmailResponse.fromJson(response); //reeturn response

      // print(response);


      return response;
    }catch(error){
      print(error);
      rethrow;
    }

  }
}