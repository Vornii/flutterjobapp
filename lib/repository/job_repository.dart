import 'package:jobapp/data/Network/local_api_service.dart';
import 'package:jobapp/model/Admin/AdminUserModel.dart';
import 'package:jobapp/model/CRUD/Deletemessage_model.dart';
import 'package:jobapp/model/CRUD/messageuser_model.dart';
import 'package:jobapp/model/POST/job_reponse_model.dart';
import 'package:jobapp/model/Recommend_model.dart';
import 'package:jobapp/model/job_byid_model.dart';
import '../model/Admin/AdminJobPostModel.dart';
import '../model/CRUD/PostDeleteResponse_model.dart';
import '../model/Categorymodel.dart';
import '../model/POST/UserbyJobModel.dart';
import '../model/POST/job_request_model.dart';
import '../model/POST/message_request_model.dart';
import '../model/job_model.dart';
import '../model/message_model.dart';
import '../model/user_model.dart';
import '../res/BaseUrl.dart';
import '../res/api_url.dart';

class JobRepository{
  var _jobservice =LocalApiservice();
  var _userservice = LocalApiservice();
  var _msgservice = LocalApiservice();

  Future<dynamic> getJobsPost(token,String? title) async{
    try{
      var response =  await _jobservice.getApiResponse('${ApiApiUrl.localurl}/api/jobs/title?title=${title}',token);
      print("token ${token}");
      print(response);

      //fetch api from server
     response =  JobModel.fromJson(response);



     print(response);
     return response;
    }catch(error){
      print(error);
      rethrow;
    }

  }

  Future<dynamic> getJobsByCategory(token,page,category) async{
    try{
      var response =  await _jobservice.getApiCateResponse(
          '${ApiApiUrl.localurl}/api/jobs/category?page=${page}&category=${category}',token);
      print("token ${token}");
      print(response);

      //fetch api from server
      response =  CategoryModel.fromJson(response);



      print(response);
      return response;
    }catch(error){
      print(error);
      rethrow;
    }

  }

  Future<dynamic> getmessageByID(userid) async{
    try{
      var response =  await _jobservice.getMessageUserResponse('${ApiApiUrl.localurl}/api/message/user/${userid}');

      print(response);

      //fetch api from server
      response =  MessageModel.fromJson(response);



      print(response);
      return response;
    }catch(error){
      print(error);
      rethrow;
    }

  }
  Future<dynamic> getJobsall(token, page) async{
    try{
      var response =  await _jobservice.getApiResponse('${ApiApiUrl.localurl}/api/jobs?page=${page}',token);
      print("token ${token}");
      print(response);

      //fetch api from server
      response =  JobModel.fromJson(response);



      print(response);
      return response;
    }catch(error){
      print(error);
      rethrow;
    }

  }


  Future<dynamic> getJobsRecommend(lists) async{
    try{
      var response =  await _jobservice.getReResponse('${ApiApiUrl.localurl}/api/jobs/interest/?skill=${lists}');

      print(response);

      //fetch api from server
      response =  RecommendResponse.fromJson(response);



      print(response);
      return response;
    }catch(error){
      print(error);
      rethrow;
    }

  }

  Future<dynamic> getFilterPost(category,type,min,max) async{
    try{
      var response =  await _jobservice.getFilterRes('${ApiApiUrl.localurl}/api/jobs/filter?page=1&category=${category}&type=${type}&min=${min}&max=${max}');

      print(response);

      //fetch api from server
      response =  JobModel.fromJson(response);



      print(response);
      return response;
    }catch(error){
      print(error);
      rethrow;
    }

  }

  Future<MessageInboxRes> getMessagePost(uid)async{
    try{
      var response = await _msgservice.messageApiResponse('${ApiApiUrl.localurl}/api/message/filter/${uid}');

        print(uid);
         response =  MessageInboxRes.fromJson(response);
      print(    response );
        return  response ;
    }catch(error){
      print(error);
      print("error occur");
      rethrow;
    }
  }

  Future<PostID> getJobid(uid) async{
    try{
      var response =  await _jobservice.getJobidResponse('${ApiApiUrl.localurl}/api/jobs/',uid);
      print("Jobs");
      print(response);

      //fetch api from server
      response =  PostID.fromJson(response);

      print(response);
      return response;
    }catch(error){
      print(error);
      rethrow;
    }

  }

  Future<dynamic> getJobPostsByuid(uid) async{
    try{
      var response =  await _jobservice.getPostByUserId('${ApiApiUrl.localurl}/api/auth/users/${uid}');

      print(response);

      //fetch api from server
      response = JobByUserID.fromJson(response);



      print(response);
      return response;
    }catch(error){
      print(error);
      rethrow;
    }

  }

  Future<dynamic> getJobPostsAll(uid) async{
    try{
      var response =  await _jobservice.getAll('${ApiApiUrl.localurl}/api/jobs/admin');

      print(response);

      //fetch api from server
      response = JobAdminModel.fromJson(response);



      print(response);
      return response;
    }catch(error){
      print(error);
      rethrow;
    }

  }

  Future<dynamic> getUserAll() async{
    try{
      var response =  await _jobservice.getAllUser('${ApiApiUrl.localurl}/api/auth/users');

      print(response);

      //fetch api from server
      response = UserAdminModel.fromJson(response);



      print(response);
      return response;
    }catch(error){
      print(error);
      rethrow;
    }

  }

  Future<UserResponse> getUserid(uid) async{
    try{
      var response =  await _jobservice.getUserResponse('${ApiApiUrl.localurl}/api/auth/users/',uid);
      print("user Profile");
      print(response);

      //fetch api from server
      response =  UserResponse.fromJson(response);

      print(response);
      return response;
    }catch(error){
      print(error);
      rethrow;
    }

  }
  Future<PostDeleteModel> deleteJobs(pid,token) async{
    try{
      print(pid);
      var response =  await _jobservice.deletePosts('${ApiApiUrl.localurl}/api/jobs/${pid}',token );

      print('Delete ${response}');

      //fetch api from server
      response =  PostDeleteModel.fromJson(response);



      print(response);
      return response;
    }catch(error){
      print(error);
      rethrow;
    }

  }
  Future<MessageDeleteModel> deleteMessage(mid) async{
    try{
      print(mid);
      var response =  await _jobservice.deleteMessageResponse('${ApiApiUrl.localurl}/api/message/${mid}');

      print(response);

      //fetch api from server
      response =  MessageDeleteModel.fromJson(response);



      print(response);
      return response;
    }catch(error){
      print(error);
      rethrow;
    }

  }



}